#ifndef GLOBAL_H
#define GLOBAL_H

#include <cstring>
#include <singleton.h>
#include <common.h>
#include "audio_g711.h"
#include "librtsps_i.h"
#include "sample_comm.h"
#include "hi_comm_ive.h"
#define VI_PORT_NUMBER 4
#define THREAD_CANCEL "cancel"

#define INFOHEADER 4

static bool g_isupdata = false;
#define HI31D_4HDMI     "SNJ2301fd_100_V2"
#define HI31D_4MUX      "SNJ"
#define VD_CHN_MAX      8
#define CMD_SN_NUM      16
#define Data_BUF_DEEP   16
#define RECV_BUF_SIZE   512
#define PACK_LEN        1300

//#define PACK_LEN        4000



#define INFO_HEADER     16
#define SZ_IPADDR       (24)

#define FLG_MULTI       0x80000000
#define FLG_IDR         0x40000000
#define FLG_265         0x20000000

//#define INFO_HEADER_H0  0   //固定‘S’
//#define INFO_HEADER_H1  1   //固定‘L’
//#define INFO_HEADER_SN  2   //包序号：0～255；区分不同的包
//#define INFO_HEADER_TP  3   //总包数
//#define INFO_HEADER_CP  5   //当前包号
//#define INFO_HEADER_FD  7   //帧号：0～59
//#define INFO_HEADER_FG  8   //标志：b0:是否是I帧，b1:是否是265
//#define INFO_HEADER_FQ  9   //帧率
//#define INFO_HEADER_VA  10  //标志：'V'：视频，‘A’:音频

//#define INFO_HEADER_WW  10  //宽度（2字节）
//#define INFO_HEADER_HH  12  //高度（2字节）



#define INFO_HEADER_H0  0   //固定‘S’
#define INFO_HEADER_H1  1   //固定‘L’
#define INFO_HEADER_SN  2   //包序号：0～255；区分不同的包
#define INFO_HEADER_TP  3   //总包数
#define INFO_HEADER_CP  5   //当前包号
#define INFO_HEADER_FD  7   //帧号：0～59
#define INFO_HEADER_FG  8   //标志：b0:是否是I帧，b1:是否是265; //音频编码格式:00：pcm,01:g711a,10:aac
#define INFO_HEADER_FQ  9   //帧率
#define INFO_HEADER_WW  10  //宽度（2字节）
#define INFO_HEADER_HH  12  //高度（2字节）
#define INFO_HEADER_VE  14  //编码通道号
#define INFO_HEADER_VA  15  //标志：'V'：视频，‘A’:音频




#define IPSIZE 16




#define AMCAST_LEN 2048

#define SWCOLORFILE     "./color.bin"
#define HDPATH      "/version/hardware_info"

#define F_HDMIVERSION    "SHNJ2301fd_300_V1"
#define F_MIXVERSION     "SHNJ2301fd_400_V1"

#define SDISOCKTPORT    22467
#define HDMI_4  0
#define MIX_4   1
#define IPC_KEY     0x123
#define SHARE_MAX   32
#include <hi_common.h>

typedef struct viParameter_
{
    SAMPLE_VI_MODE_E enViMode;
    HI_U32 u32ViWidth;
    HI_U32 u32ViHeigth;
    HI_U32 u32SrcFrmRate;
    SIZE_S stVencSize;
    SIZE_S stMinorSize;

    SIZE_S stSnapSize;
    HI_U32 u32Gop[2];           //0--Master   1--Slave
    HI_U32 u32Profile[2];       //0--Master   1--Slave
    HI_U32 u32BitRate[2];       //0--Master   1--Slave
    HI_U32 u32DstFrameRate[2];  //0--Master   1--Slave
    PAYLOAD_TYPE_E enType[2];   //0--Master   1--Slave
    SAMPLE_RC_E enRcMode[2];    //0--Master   1--Slave
    HI_U32 u32MinQp[2];         //0--Master   1--Slave
    HI_U32 u32MaxQp[2];
    HI_U32 u32MinIProp[2];
    HI_U32 u32MaxIProp[2];
    HI_U32 u32MinIQp[2];
    HI_U32 u32IQp[2];
    HI_U32 u32PQp[2];
    HI_U32 u32BQp[2];
    HI_BOOL mcast_enable[2];
    HI_CHAR mcast_ip[2][IPSIZE];
    HI_S32 mcast_port[2];
    HI_S32 SnapQuality;
    HI_S32 snapPort;
    HI_S32 input_type;    //0--VI隔行图像  1--VI逐行图像
    HI_BOOL osd_enable;
    HI_BOOL vi_crop_enable;
    HI_U32 u32X;
    HI_U32 u32Y;
    HI_U32 u32W;
    HI_U32 u32H;
    HI_BOOL venc_SAME_INPUT[2];
    HI_BOOL sw_color_expand;
 }PARAMETER;


typedef struct {
    SIZE_S snapSize = {320,180};
    HI_U32 snapPort = 54321;
    HI_U32 snapLeng = 1200;
    HI_U32 snapDelay = 1000;
    HI_U32 snapQuality = 50;
    HI_BOOL Snapmcast_enable = HI_FALSE;
    HI_CHAR Snapmcast_ip[IPSIZE]=  "0.0.0.0";
    HI_S32  Snapmcast_port = 0;
    HI_S32  Snapmcast_freq = 20;
}SNAPPARAM;

typedef struct TransMit{
    int clock_swing_sum;
    int tick_delta;
    uint m1;
    uint m2;
}TransMit;



extern bool gbTimeTick;
extern int  glFrameID;
extern int  glLocalTickDelta;
extern int  LocalTickDelta;
extern bool sync_ok;
extern int  sync_offset;

extern int ttl ;
extern int  glDataBuf[60][256*1024];
extern int  glDBWptr;
extern int  glDBRptr;


extern int mcast_enable[8];
extern int mcast_len[8];
extern int mcast_delay[8];
extern char mcast_ip[8][IPSIZE];
extern int mcast_port[8];
extern int repeat_time[8];
extern HI_BOOL aumcast_enable[3];
extern char aumcast_ip[3][IPSIZE];
extern int aumcast_port[4];



extern int SnapDelay[4];
extern int VencStreamCnt;
extern int Venc1StreamCnt;
extern int Venc2StreamCnt;
extern int Venc3StreamCnt;
extern video_type v_type[8];
extern unsigned int u32DstFrameRate[8];

extern char gClientIP[];
extern int  gClientPort;

extern int diff_index;
extern IVE_MEM_INFO_S  pstMap;
extern SNAPPARAM veSnap[4];

extern HI_BOOL g_vi0_hdmi;
extern HI_BOOL g_vi1_hdmi;
extern HI_BOOL g_vi2_hdmi;
extern HI_BOOL g_vi3_hdmi;
extern int versionFlag;

void * multiSnapProc(void * arg);
void * multiSnapProc1(void * arg);
 void * multiSnapProc2(void * arg);
void * multiSnapProc3(void * arg);
void *snap0ServerProc(void *arg);
void *snap1ServerProc(void *arg);
void *snap2ServerProc(void *arg);
void *snap3ServerProc(void *arg);

bool reSetSnap(int num,VENC_CHN snapChn,SIZE_S snapSize);



#endif // GLOBAL_H
