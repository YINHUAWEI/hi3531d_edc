#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include "himpp_master.h"
#include "mytimer.h"
#include "udpsocket/udpsocket.h"
#include "cnFreeType.h"











#ifdef __TINY_RTSP_SERVER__
tiny_rtsp_server_i* g_prtspserver   = 0;
#else
aacenc *g_audio_aac[4] = {NULL,NULL,NULL,NULL};
#endif

int g_stream[8];
HI_S32 mcast_enable[8] ={HI_FALSE,HI_FALSE,HI_FALSE,HI_FALSE,HI_FALSE,HI_FALSE,HI_FALSE,HI_FALSE};
HI_S32 mcast_len[8];
HI_S32 mcast_delay[8];
HI_BOOL aumcast_enable[3];  //3路音频输入
HI_S32 SnapDelay[4];
VB_BLK VbBlk;
int osd_enable[VI_PORT_NUMBER];
unsigned short testData[VI_PORT_NUMBER][30];
slangBmp *chnBmp[VI_PORT_NUMBER];
IMAGEDATA charColor[VI_PORT_NUMBER];
POINT_S startPoint[VI_PORT_NUMBER];
//bool l_startloop[4] = { ,true,true,true};
SAMPLE_VI_MODE_E gEnViMode1;
SAMPLE_VI_MODE_E gEnViMode2;
bool l_startloop[4] = {false,false,false,false};
bool vpssbing_4k[8]= {false,false,false,false,false,false,false,false};
bool snapmcast_loop[4] = {true,true,true,true};
bool mult_snap[4] = {true,true,true,true};
SIZE_S stMaxSize_ = {3840,2160};
HI_U32  edid_4k = 0;
int vi_flag[VI_PORT_NUMBER]={0,0,0,0};
bool ai_mm35 = true;
bool VO_open_fd = true;
bool gbTimeTick = true;
int  glFrameID = -1;
int  glLocalTickDelta = 0;
int  LocalTickDelta = 0;
bool sync_ok = false;
int  sync_offset = 0;

  int  glDataBuf[60][256*1024];
  int  glDBWptr = 0;
  int  glDBRptr = 59;

int ttl = 64;
IVE_MEM_INFO_S  pstMap;

struct timeval    tv;
struct timezone tz;

struct tm         *p;
/*void sysLocalTime()
{
    struct timeval    tv;
    struct timezone tz;

    struct tm         *p;

    gettimeofday(&tv, &tz);
    p = localtime(&tv.tv_sec);
    printf("time_now:%d-%d-%d-%d-%d-%d.%ld\n", 1900+p->tm_year, 1+p->tm_mon, p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec, tv.tv_usec);

}*/

HimppMaster::HimppMaster()
{

}

HimppMaster::~HimppMaster()
{
    HI_U32 i = 0;
    HI_S32 s32Ret,Ch_n = 0;
    printf("i am is himppmaster ctrl + c \n");

    for(i = 0 ; i <  3  ; i++)
    {
        if((edid_4k != 1) && (i != 2) )
        {

        VencModuleDestroy_1(Ch_n, Ch_n + 2 ,i);
        Ch_n = ( Ch_n + 2);
        printf("Ch_n =  ==%d\n",Ch_n);
        }
     }


   // SAMPLE_COMM_VPSS_Stop(9 , 10, 1);
    printf("venc stop rcv\n");
    // VpssModuleDestroy();

    VI_CHN unchn = 4;
    VIDEO_FRAME_INFO_S stFrame;
    AI_CHN_PARAM_S stAiParam;
    stAiParam.u32UsrFrmDepth = 0;
    for(int i=0;i<2;i++){

        s32Ret = HI_MPI_AI_SetChnParam( i, 0, &stAiParam);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("set max depth err:0x%x\n", s32Ret);
        }
    }
    AudioModuleDestroy();
    VoModuleDestroy(0);
    VoModuleDestroy(1);
    s32Ret == SAMPLE_COMM_VO_HdmiStop();

    StopViModule(1,4);
    StopViModule(3,12);
    StopViModule(5,20);
    StopViModule(7,28);

    VpssModuleDestroy();
    SAMPLE_COMM_VPSS_Stop(8,10,1);
    sleep(1);


    s32Ret = HI_MPI_VB_ReleaseBlock(VbBlk);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VB_ReleaseBlock failed, err:0x%x!\n", s32Ret);
    }

    SAMPLE_COMM_SYS_Exit();
}

bool HimppMaster::InitHimpp1(SIZE_S stVdSize, SIZE_S *stVoSize, HI_U32 *u32DispFrmRt, \
                            VO_INTF_SYNC_E enIntfSync, PAYLOAD_TYPE_E enType, HI_S32 s32VdChnCnt, \
                            HI_S32 s32VpGrpCnt, HI_S32 s32VencChnCnt)
{
    VB_CONF_S stVbConf, stModVbConf;
    HI_S32 s32Ret = HI_SUCCESS;

    /************************************************
    step1:  init SYS and common VB
    *************************************************/
    SAMPLE_COMM_VDEC_Sysconf(&stVbConf, &stVdSize);
    s32Ret = SAMPLE_COMM_SYS_Init(&stVbConf);
    if(s32Ret != HI_SUCCESS)
    {

        system("reboot\n"); //保证程序不会进入无限循环的重启
    }

    /************************************************
    step2:  init mod common VB
    *************************************************/
    SAMPLE_COMM_VDEC_ModCommPoolConf(&stModVbConf, enType, &stVdSize, s32VdChnCnt, HI_FALSE);
    s32Ret = SAMPLE_COMM_VDEC_InitModCommVb(&stModVbConf);
    if(s32Ret != HI_SUCCESS)
    {

        system("reboot\n"); //保证程序不会进入无限循环的重启
    }

    s32Ret = HI35xx_COMM_SYS_MemConfig(s32VdChnCnt, s32VpGrpCnt, s32VencChnCnt,0);
    if(s32Ret != HI_SUCCESS)
    {

        system("reboot\n"); //保证程序不会进入无限循环的重启
    }

    s32Ret = SAMPLE_COMM_VO_GetWH(enIntfSync, &stVoSize->u32Width, &stVoSize->u32Height, u32DispFrmRt);
    if (s32Ret != HI_SUCCESS){
  return false;
    }

    //NOTE: create VB pool
//    s32Ret = HI35xx_VB_PoolInit(SIZE_S{stVoSize->u32Width, stVoSize->u32Height});
//    if (s32Ret != HI_SUCCESS){
//  return false;
//    }

    /* 只要有一个进程进行初始化即可，不需要所有的进程都做系统初始化的操作 */


}
#if 1
bool HimppMaster::InitHimpp(HI_S32 vdecMaxCnt,HI_S32 vpssMaxCnt, HI_S32 vencMaxCnt)
{

    HI_S32 s32Ret;
    HI_U32 u32BlkSize;
    VB_CONF_S stVbConf;
    VB_CONF_S    stModVbConf;
    HI_U32 Maxcnt = 68;
    memset(&stVbConf,0,sizeof(VB_CONF_S));
   // stVbConf.u32MaxPoolCnt = 64; //整个系统缓存池个数
   stVbConf.u32MaxPoolCnt = 64;
    /*if (HI_SUCCESS != SAMPLE_COMM_SYS_GetPicSize(enNorm, enPicSize, &stMaxSize_)){
        SAMPLE_PRT("SAMPLE_COMM_SYS_GetPicSize failed!\n");
        return false;
    }*/

//    if(edid_4k ==0 )
//    {
//        stMaxSize_ = {1920,1080};
//    }


    SAMPLE_PRT("chris width %d,height %d\n",stMaxSize_.u32Width,stMaxSize_.u32Height);

    //step  1: init variable
    u32BlkSize = HI35xx_COMM_SYS_CalcPicVbBlkSize(&stMaxSize_, SAMPLE_PIXEL_FORMAT, SAMPLE_SYS_ALIGN_WIDTH,COMPRESS_MODE_SEG);
    SAMPLE_PRT("chris blksize %d\n",u32BlkSize);
 //pstVbConf->astCommPool[0].u32BlkSize = (pstSize->u32Width * pstSize->u32Height * 4) >> 1;

 //  stVbConf.astCommPool[0].u32BlkSize = (stMaxSize_.u32Width * stMaxSize_.u32Height * 4) >> 1;
//    stVbConf.astCommPool[0].u32BlkCnt = 3;
     stVbConf.astCommPool[0].u32BlkSize = u32BlkSize;
     stVbConf.astCommPool[0].u32BlkCnt = Maxcnt;


     MPP_SYS_CONF_S stSysConf = {0};

     stSysConf.u32AlignWidth = SAMPLE_SYS_ALIGN_WIDTH;
     s32Ret = HI_MPI_SYS_SetConf(&stSysConf);
     if (HI_SUCCESS != s32Ret)
     {
         SAMPLE_PRT("HI_MPI_SYS_SetConf failed\n");
         return HI_FAILURE;
     }

     s32Ret = HI_MPI_SYS_Init();
     if (HI_SUCCESS != s32Ret)
     {
         SAMPLE_PRT("HI_MPI_SYS_Init failed!\n");
         return HI_FAILURE;
     }


    //step 2: mpp system init.
    s32Ret = SAMPLE_COMM_SYS_Init(&stVbConf);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("system init failed     1with %d!\n", s32Ret);
        system("reboot\n"); //保证程序不会进入无限循环的重启
    }



    if(INITIVE){
        s32Ret = SAMPLE_COMM_IVE_CreateMemInfo(&pstMap,sizeof(IVE_MAP_LUT_MEM_S));
        if (HI_SUCCESS != s32Ret)
        {
            printf("SAMPLE_COMM_IVE_CreateMemInfo err:0x%x\n", s32Ret);
        }
        int len = 0;
        getFileAll((char*)pstMap.pu8VirAddr, SWCOLORFILE, &len);
        if( len == 0 )
        {
            printf("open file error!");
            return false;
        }
    }





    //NOTE: create VB pool






    return true;
}
#endif

bool HimppMaster::VpssModuleInitvo( HI_S32 vpstart,HI_S32 vpssGrp, HI_S32 vpssChn,SIZE_S vpchnsize)
{
    HI_S32 s32Ret;
    VPSS_GRP_ATTR_S stGrpAttr;
    VpssGrpCnt_ = vpssGrp;
    VpssChnCnt_ = vpssChn;
    memset(&stGrpAttr,0,sizeof(VPSS_GRP_ATTR_S));
    stGrpAttr.u32MaxW   = stMaxSize_.u32Width;
    stGrpAttr.u32MaxH   = stMaxSize_.u32Height;
    stGrpAttr.enPixFmt  = SAMPLE_PIXEL_FORMAT;
    stGrpAttr.bIeEn     = HI_FALSE;
    stGrpAttr.bNrEn     = HI_FALSE;
    stGrpAttr.bDciEn    = HI_FALSE;
    stGrpAttr.bHistEn   = HI_FALSE;
    stGrpAttr.bEsEn     = HI_FALSE;
    stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
   SIZE_S chn_size =  vpchnsize;

   HI_U32    edid4k = edid_4k ;

//    edid4k = HI_FALSE;
//    chn_size = {1920,1080};

    s32Ret = HI35xx_COMM_VPSS_Startvo(vpstart,VpssGrpCnt_, &chn_size, VpssChnCnt_, NULL,edid4k);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("Start Vpss failed %d\n",s32Ret);
        return false;
    }
    return true;
}



bool HimppMaster::VpssModuleInit( HI_S32 vpstart,HI_S32 vpssGrp, HI_S32 vpssChn,SIZE_S vpchnsize)
{
    HI_S32 s32Ret;
    VPSS_GRP_ATTR_S stGrpAttr;
    VpssGrpCnt_ = vpssGrp;
    VpssChnCnt_ = vpssChn;
    memset(&stGrpAttr,0,sizeof(VPSS_GRP_ATTR_S));
    stGrpAttr.u32MaxW   = stMaxSize_.u32Width;
    stGrpAttr.u32MaxH   = stMaxSize_.u32Height;
    stGrpAttr.enPixFmt  = SAMPLE_PIXEL_FORMAT;
    stGrpAttr.bIeEn     = HI_FALSE;
    stGrpAttr.bNrEn     = HI_FALSE;
    stGrpAttr.bDciEn    = HI_FALSE;
    stGrpAttr.bHistEn   = HI_FALSE;
    stGrpAttr.bEsEn     = HI_FALSE;
    stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
   SIZE_S chn_size =  vpchnsize;

   HI_U32    edid4k = edid_4k ;

//    edid4k = HI_FALSE;
//    chn_size = {1920,1080};

    s32Ret = HI35xx_COMM_VPSS_Start(vpstart,VpssGrpCnt_, &chn_size, VpssChnCnt_, NULL,edid4k);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("Start Vpss failed %d\n",s32Ret);
        return false;
    }
    return true;
}



bool HimppMaster::ViModuleInit_1( VI_DEV ViDev, VI_CHN ViChn,VIDEO_NORM_E enNorm,SAMPLE_VI_MODE_E enViMode,SIZE_S stTargetSize ,RECT_S stCapRect,HI_U32 SrcFrmRate ,HI_U32 DrcFrmRate,HI_U32 DrcFrmRate_S1)
{
    HI_S32 s32Ret;
//    stVoSize_[0].u32Width  = stCapRect.u32Width;
//    stVoSize_[0].u32Height = stCapRect.u32Height;
//    ViDev_[num] = ViDev;
//    ViChn_[num] = ViChn;
   // HI_S32 HI35xx_COMM_VI_Start(VI_DEV ViDev, VI_CHN ViChn,VIDEO_NORM_E enNorm, SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, SIZE_S pstTarSize,SAMPLE_VI_CHN_SET_E enViChnSet);


   // s32Ret= HI35xx_COMM_VI_Start_1(edid_4k,ViDev, ViChn, enNorm ,enViMode,cap_recv,&starge_size, SrcFrmRate);

   s32Ret= HI35xx_COMM_VI_Start_1(vo_size_vi,edid_4k,ViDev, ViChn, enNorm ,enViMode,stCapRect,&stTargetSize, SrcFrmRate,DrcFrmRate);
   if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("start vi failed\n");
        return false;
    }
    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn, 1);
    if (HI_SUCCESS != s32Ret)
    {
        printf("set max depth err:0x%x\n", s32Ret);
        return s32Ret;
    }

//    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn +1, 1);
//    if (HI_SUCCESS != s32Ret)
//    {
//        printf("set max depth err:0x%x\n", s32Ret);
//        return s32Ret;
//    }


    SAMPLE_PRT("chris videv%d vichn%d\n",ViDev,ViChn);
    return true;
}


bool HimppMaster::VencModuleInit_1(HI_S32 veNum,HI_U32 u32VeChnCnt, SIZE_S stSize,HI_S32 snapFlag)
{
     HI_S32 s32Ret;
    HI_U32 i = 0;
    VENC_CHN venc_chn[u32VeChnCnt];
SIZE_S snap_MAX = {1920,1080};
   printf("venc-width==%d   venc--hight==%d\n",stSize.u32Width,stSize.u32Height);
 if(snapFlag != -1 ) {

      s32Ret = HI35xx_COMM_VENC_SnapStart_1(VencSnapChn_[snapFlag], stSize ,snap_MAX);
      if (HI_SUCCESS != s32Ret){
          SAMPLE_PRT("HI35xx_COMM_VENC_SnapStart  %d failed with: 0x%x\n",VencSnapChn_[snapFlag], s32Ret);
          return false;
        }
}


 if(u32VeChnCnt != 0){
    for(i = 0; i < u32VeChnCnt; i++) {
       venc_chn[i] = (veNum * 2 + i );

        printf("VencModuleInit_1 ---venum==%d--type==%d--viParam--venc-h=%d--w=%d \n",veNum,viParam[veNum].enType[i],viParam[veNum].stVencSize.u32Height,viParam[veNum].stVencSize.u32Width);
         s32Ret = hi35_SAMPLE_COMM_VENC_Start_1(venc_chn[i],viParam[veNum],i,viParam[veNum].stVencSize);


        }

}

   // log_i("himpp venc module init succeed!");
//    u32VencChnCnt_ = u32VeChnCnt;
//    bVencModuleInit_ = HI_TRUE;
    return true;
}



bool HimppMaster::ViModuleInit(VI_DEV ViDev, VI_CHN ViChn,SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type,HI_S32 num)
{
    HI_S32 s32Ret;
    stVoSize_[0].u32Width  = stCapRect.u32Width;
    stVoSize_[0].u32Height = stCapRect.u32Height;
    ViDev_[num] = ViDev;
    ViChn_[num] = ViChn;

    s32Ret= HI35xx_COMM_VI_Start(ViDev, ViChn, enViMode, stCapRect, input_type);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("start vi failed\n");
        return false;
    }
    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn, 1);
    if (HI_SUCCESS != s32Ret)
    {
        printf("set max depth err:0x%x\n", s32Ret);
        return s32Ret;
    }

    SAMPLE_PRT("chris videv%d vichn%d\n",ViDev,ViChn);
    return true;
}

bool HimppMaster::Vi1ModuleInit(SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type)
{
    HI_S32 s32Ret;
    stVoSize_[1].u32Width  = stCapRect.u32Width;
    stVoSize_[1].u32Height = stCapRect.u32Height;

    s32Ret= HI35xx_COMM_VI_Start(ViDev_[1], ViChn_[1], enViMode, stCapRect, input_type);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("start vi failed\n");
        return false;
    }
    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn_[1], 1);
    if (HI_SUCCESS != s32Ret)
    {
        printf("set max depth err:0x%x\n", s32Ret);
        return s32Ret;
    }

    SAMPLE_PRT("chris videv%d vichn%d\n",ViDev_[1],ViChn_[1]);
    return true;
}

bool HimppMaster::Vi2ModuleInit(SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type)
{
    HI_S32 s32Ret;
    stVoSize_[2].u32Width  = stCapRect.u32Width;
    stVoSize_[2].u32Height = stCapRect.u32Height;

    s32Ret= HI35xx_COMM_VI_Start(ViDev_[2], ViChn_[2], enViMode, stCapRect, input_type);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("start vi failed\n");
        return false;
    }
    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn_[2], 1);
    if (HI_SUCCESS != s32Ret)
    {
        printf("set max depth err:0x%x\n", s32Ret);
        return s32Ret;
    }

    SAMPLE_PRT("chris videv%d vichn%d\n",ViDev_[2],ViChn_[2]);
    return true;
}

bool HimppMaster::Vi3ModuleInit(SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type)
{
    HI_S32 s32Ret;
    stVoSize_[3].u32Width  = stCapRect.u32Width;
    stVoSize_[3].u32Height = stCapRect.u32Height;

    s32Ret= HI35xx_COMM_VI_Start(ViDev_[3], ViChn_[3], enViMode, stCapRect, input_type);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("start vi failed\n");
        return false;
    }
    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn_[3], 1);
    if (HI_SUCCESS != s32Ret)
    {
        printf("set max depth err:0x%x\n", s32Ret);
        return s32Ret;
    }

    SAMPLE_PRT("chris videv%d vichn%d\n",ViDev_[3],ViChn_[3]);
    return true;
}




bool HimppMaster::VencModuleInit(SIZE_S stVencSize, SIZE_S stMinor1Size,SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp,HI_S32 viPort,HI_BOOL setFlag)
{
    if((stVencSize.u32Height == 0) || (stVencSize.u32Width == 0)) {
        printf("vi nosignal\n");
        stVencSize.u32Height = 1080;
        stVencSize.u32Width = 1920;
        stMinor1Size.u32Width = 1280;
        stMinor1Size.u32Height = 720;
        stMinor2Size.u32Width = 480;
        stMinor2Size.u32Height = 270;
        u32SrcFrmRate = 60;
        u32DstFrameRate[0] = 60;
        u32DstFrameRate[1] = 30;

    }
    HI_S32 s32Ret,i;
    switch (viPort) {
        case 0:
            stVencSize_[0] = stVencSize;
            stVencSize_[1] = stMinor1Size;
            stVencSize_[2] = stMinor2Size;
            for(i = 0;i<VencStreamCnt;i++){
                for(uint j = 0; j < 3; j++) {
                    printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVencSize_[i].u32Width,stVencSize_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                    s32Ret = HI35xx_COMM_VENC_Start(VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVencSize_[i], enRcMode[i], u32Profile[i],
                                                    u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                                                    u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                                                    u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                    if (HI_SUCCESS != s32Ret){
                        SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                        usleep(20000);
                    }
                    else {
                        SAMPLE_PRT("Start VencChn[%d] succed\n",VencChn_[viPort*VencStreamCnt+i]);
                        break;
                    }
                }

            }
            break;
        case 1:
            stVenc1Size_[0] = stVencSize;
            stVenc1Size_[1] = stMinor1Size;
            stVenc1Size_[2] = stMinor2Size;
            for(i = 0;i<VencStreamCnt;i++){
                printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVenc1Size_[i].u32Width,stVenc1Size_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                s32Ret = HI35xx_COMM_VENC_Start(VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVenc1Size_[i], enRcMode[i], u32Profile[i],
                                                u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                                                u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                                                u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                if (HI_SUCCESS != s32Ret){
                    SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                    return false;
                }
            }
            break;
        case 2:
            stVenc2Size_[0] = stVencSize;
            stVenc2Size_[1] = stMinor1Size;
            stVenc2Size_[2] = stMinor2Size;
            for(i = 0;i<VencStreamCnt;i++){
                printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVenc2Size_[i].u32Width,stVenc2Size_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                s32Ret = HI35xx_COMM_VENC_Start(VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVenc2Size_[i], enRcMode[i], u32Profile[i],
                                                u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                                                u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                                                u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                if (HI_SUCCESS != s32Ret){
                    SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                    return false;
                }
            }
            break;
        case 3:
            stVenc3Size_[0] = stVencSize;
            stVenc3Size_[1] = stMinor1Size;
            stVenc3Size_[2] = stMinor2Size;
            for(i = 0;i<VencStreamCnt;i++){
                printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVenc3Size_[i].u32Width,stVenc3Size_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                s32Ret = HI35xx_COMM_VENC_Start(VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVenc3Size_[i], enRcMode[i], u32Profile[i],
                                                u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                                                u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                                                u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                if (HI_SUCCESS != s32Ret){
                    SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                    return false;
                }
            }
            break;
        default:
            break;
    }

    return true;
}

/*
 * VPSS 0 - VENC 8
 * VPSS 1 - VENC 9
 * VPSS 2 - VENC 10
 * VPSS 3 - VENC 11
 * */
bool HimppMaster::VencSnapInit_1(HI_S32 useChn, SNAPPARAM * SNAP)
{
     HI_S32 s32Ret;
    HI_U32 i = 0;
      VENC_CHN snapchn = VencMax ;
    for(i = 0; i < SNAPMAX; i++){

        VencSnapChn_[i] = snapchn++;


    }


    return true;
}

HI_S32 HimppMaster::VencSnapInit(HI_S32 veChn,SIZE_S stSize,HI_BOOL bindFlag)
{
    HI_S32 s32Ret;
    VPSS_GRP VpssGrp = veChn-8;
    s32Ret = HI35xx_COMM_VENC_SnapStart(veChn, &stSize, bindFlag,VpssGrp);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI35xx_COMM_VENC_SnapStart  %d failed with: 0x%x",veChn, s32Ret);
    }

}

bool HimppMaster::VencSnapDestroy(VENC_CHN veChn)
{
    HI_S32 s32Ret;
    s32Ret = SAMPLE_COMM_VENC_Stop(veChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("SAMPLE_COMM_VENC_Stop failed, err:0x%x!\n", s32Ret);
    }

    s32Ret = HI_MPI_VENC_DestroyChn(veChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", \
                   veChn, s32Ret);
    }
    SAMPLE_PRT("himpp venc snap module destroy succeed!\n");

    return true;
}

#if 1

#endif
#if 0
bool HimppMaster::VoModeuleInit()
{  VO_CSC_S pstVideoCSC;
    HI_S32 s32Ret;
    VO_PUB_ATTR_S stVoPubAttr;
    VO_VIDEO_LAYER_ATTR_S stLayerAttr;
    VO_HDMI_PARAM_S pstHdmiParam;
    stVoPubAttr.enIntfSync = VO_OUTPUT_1080P60;
    stVoPubAttr.enIntfType = VO_INTF_HDMI;
    stVoPubAttr.u32BgColor = 0x009632;
    HI_U32   u32DispFrmRt = 60;

    SIZE_S stVoSiz_get;
//    s32Ret = HI35xx_COMM_VO_GetWH(&stVoPubAttr.enIntfSync, stVoSiz_get, u32DispFrmRt);
//    if (HI_SUCCESS != s32Ret)
//    {
//        SAMPLE_PRT("Start HI35xx_COMM_VO_GetWH failed!\n");
//        return false;
//    }
//    printf("***ddddddddddddddddddd* stVoPubAttr.enIntfSync %d ****\n",stVoPubAttr.enIntfSync);



    s32Ret = SAMPLE_COMM_VO_StartDev(VoDev_, &stVoPubAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
        return false;
    }
    s32Ret = SAMPLE_COMM_VO_StartDev(VoDev_+1, &stVoPubAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
        return false;
    }







    memset(&(stLayerAttr), 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
    s32Ret = SAMPLE_COMM_VO_GetWH(stVoPubAttr.enIntfSync, \
                                  &stLayerAttr.stImageSize.u32Width, \
                                  &stLayerAttr.stImageSize.u32Height, \
                                  &stLayerAttr.u32DispFrmRt);



    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_GetWH failed!\n");
        return false;
    }
    stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    stLayerAttr.stDispRect.s32X       = 0;
    stLayerAttr.stDispRect.s32Y       = 0;
    stLayerAttr.stDispRect.u32Width   = stLayerAttr.stImageSize.u32Width;
    stLayerAttr.stDispRect.u32Height  = stLayerAttr.stImageSize.u32Height;

    s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer_, &stLayerAttr);

    s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer_ +1, &stLayerAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartLayer failed!\n");
        return false;
    }

    if (HI_SUCCESS != HI_MPI_VO_GetVideoLayerCSC(SAMPLE_VO_LAYER_VHD0, &pstVideoCSC)){
        COMMON_PRT("HI_MPI_VO_GetVideoLayerCSC failed!\n");
        return false;
    }

    if (HI_SUCCESS != HI_MPI_VO_GetVideoLayerCSC(SAMPLE_VO_LAYER_VHD1, &pstVideoCSC)){
        COMMON_PRT("HI_MPI_VO_GetVideoLayerCSC failed!\n");
        return false;
    }



    pstVideoCSC.u32Luma = 50;
    pstVideoCSC.u32Contrast = 50;
    pstVideoCSC.u32Hue = 50;
    pstVideoCSC.u32Saturation = 50;





   if (HI_SUCCESS != HI_MPI_VO_SetVideoLayerCSC(SAMPLE_VO_LAYER_VHD0, &pstVideoCSC)){
       COMMON_PRT("HI_MPI_VO_SetVideoLayerCSC failed!\n");
       return false;
   }

   if (HI_SUCCESS != HI_MPI_VO_SetVideoLayerCSC(SAMPLE_VO_LAYER_VHD1, &pstVideoCSC)){
       COMMON_PRT("HI_MPI_VO_SetVideoLayerCSC failed!\n");
       return false;
   }









    if (stVoPubAttr.enIntfType & VO_INTF_HDMI){

        if (HI_SUCCESS != HI_MPI_VO_GetHdmiParam(VoDev_, &pstHdmiParam)){
            COMMON_PRT("HI_MPI_VO_GetHdmiParam failed!\n");
            return false;
        }
        pstHdmiParam.stCSC.u32Luma = 49;

        if (HI_SUCCESS != HI_MPI_VO_SetHdmiParam(VoDev_, &pstHdmiParam)){
            COMMON_PRT("HI_MPI_VO_SetHdmiParam failed!\n");
            return false;
        }
//        Hi_SetReg(0x1302D02C, 0x00000410);
//        Hi_SetReg(0x1302D034, 0x00000428);
//        Hi_SetReg(0x1302D03C, 0x00000428);



        if (HI_SUCCESS != SAMPLE_COMM_VO_HdmiStart(stVoPubAttr.enIntfSync)){
            COMMON_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
            return false;
        }
    }

    s32Ret = HI35xx_COMM_VO_StartChn(VoLayer_, VoChn_);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartChn failed!\n");
        return false;
    }

    s32Ret = HI35xx_COMM_VO_StartChn(VoLayer_ +1 , VoChn_);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartChn failed!\n");
        return false;
    }
    return true;
}
#endif

bool HimppMaster::VoModeuleInit(SIZE_S Vi_size_vo,HI_U32 vo_dev)
{
    VO_PUB_ATTR_S stVoPubAttr;
    SIZE_S stVoChnSize = {1920,1080};
    HI_U32   u32DispFrmRt = 60;
      HI_U32 u32BgColor = 0x003355;
 auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
   // HI_U32 u32BgColor =    0x009632;
    HI_S32 s32Ret ;
    VO_HDMI_PARAM_S pstHdmiParam;
    SAMPLE_VO_MODE_E enMode;
    enMode = VO_MODE_1MUX;
    VO_INTF_SYNC_E enIntfSync = VO_OUTPUT_1080P60;

     //auto thisini = Singleton<SoftwareConfig>::getInstance();

     vo_size_vi = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kOut_SameIn));
     if(vo_dev == 0)
    {
        if(edid_4k == 1)
        {
            stVoChnSize = {3840,2160};
            enIntfSync =  VO_OUTPUT_3840x2160_30;
            u32DispFrmRt = 30;
        }
        else
        {
            stVoChnSize = {1920,1080};
            enIntfSync = VO_OUTPUT_1080P60;
            u32DispFrmRt = 60;
        }
        //  auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
#if 0
        if((vo_size_vi != HI_TRUE  )  && ((Vi_size_vo.u32Height != 0) && (Vi_size_vo.u32Width != 0)))
        {
            printf("i am is VoModeuleInit ++++++++++++++++++++++++====vo\n");
            s32Ret = HI35xx_COMM_VO_GetWH(&enIntfSync, Vi_size_vo, u32DispFrmRt);
            if (HI_SUCCESS != s32Ret)
            {
                SAMPLE_PRT("Start HI35xx_COMM_VO_GetWH failed!\n");
                return false;
            }

            s32Ret = SAMPLE_COMM_VO_GetWH(enIntfSync, \
                                          &stVoChnSize.u32Width, \
                                          &stVoChnSize.u32Height, \
                                          &u32DispFrmRt);


        }
#endif


        s32Ret = HI35xx_COMM_VO_StartDevLayer(edid_4k ,SAMPLE_VO_DEV_DHD0, enIntfSync, stVoChnSize.u32Width, stVoChnSize.u32Height, u32DispFrmRt, u32BgColor );
        if(s32Ret != HI_SUCCESS){

            return HI_FAILURE;
        }
    }
    else if(vo_dev == 1)
    {
        if(edid_4k == 0)
        {

            SIZE_S    stVoChnSize1 = {1920,1080};
            enIntfSync = VO_OUTPUT_1080P60;

            s32Ret = HI35xx_COMM_VO_StartDevLayer( edid_4k,SAMPLE_VO_DEV_DHD1, enIntfSync, stVoChnSize1.u32Width, stVoChnSize1.u32Height, u32DispFrmRt, u32BgColor );
            if(s32Ret != HI_SUCCESS){

                return HI_FAILURE;
            }
        }
    }

    //enIntfSync = VO_OUTPUT_1080P60;

    if(VO_open_fd == true)
    {
         VO_open_fd = false;

    if(enIntfSync == VO_OUTPUT_1080P60)
        HI35xx_COMM_VO_StartGraphicLayer(POINT_S{(HI_S32)stVoChnSize.u32Width, (HI_S32)stVoChnSize.u32Height},
                                         POINT_S{(HI_S32)stVoChnSize.u32Width, (HI_S32)stVoChnSize.u32Height},
                                         POINT_S{0, 0});


    else if(enIntfSync == VO_OUTPUT_3840x2160_30)
        HI35xx_COMM_VO_StartGraphicLayer(POINT_S{1920, 1080},
                                         POINT_S{(HI_S32)stVoChnSize.u32Width, (HI_S32)stVoChnSize.u32Height},
                                         POINT_S{960, 540});


    }

    if(edid_4k == 0)
    {
        if(vo_dev == 0)
        {
            s32Ret = HI35xx_COMM_VO_PipStart( SAMPLE_VO_DEV_DHD0, stVoChnSize.u32Width, stVoChnSize.u32Height, u32DispFrmRt );
            if(s32Ret != HI_SUCCESS){
                //log_e("HI35xx_COMM_VO_PipStart failed!");
                return HI_FAILURE;
            }
        }
//        else if(vo_dev == 1)
//        {
//            s32Ret = HI35xx_COMM_VO_PipStart( SAMPLE_VO_DEV_DHD1, stVoChnSize.u32Width, stVoChnSize.u32Height, u32DispFrmRt );
//            if(s32Ret != HI_SUCCESS){
//                printf("HI35xx_COMM_VO_PipStart failed!");
//                //  return HI_FAILURE;
//            }
//        }
    }


    VO_CSC_S pstVideoCSC;
    if (HI_SUCCESS != HI_MPI_VO_GetVideoLayerCSC(SAMPLE_VO_LAYER_VHD0, &pstVideoCSC)){
        COMMON_PRT("HI_MPI_VO_GetVideoLayerCSC failed!\n");
        //return false;
    }

    if (HI_SUCCESS != HI_MPI_VO_GetVideoLayerCSC(SAMPLE_VO_LAYER_VHD1, &pstVideoCSC)){
        COMMON_PRT("HI_MPI_VO_GetVideoLayerCSC failed!\n");
        //   return false;
    }


    //   pstVideoCSC.enCscMatrix = VO_CSC_MATRIX_IDENTITY;
    pstVideoCSC.u32Luma = str2int(softwareconfig->GetConfig(SoftwareConfig::kLuma));
    pstVideoCSC.u32Contrast = str2int(softwareconfig->GetConfig(SoftwareConfig::kContrast));
    pstVideoCSC.u32Hue = str2int(softwareconfig->GetConfig(SoftwareConfig::kHue));
    pstVideoCSC.u32Saturation = str2int(softwareconfig->GetConfig(SoftwareConfig::kSaturation));


//    pstVideoCSC.u32Luma = 50;
//    pstVideoCSC.u32Contrast = 50;
//    pstVideoCSC.u32Hue = 50;
//    pstVideoCSC.u32Saturation = 50;

    if(vo_dev == 0)
    {
        if (HI_SUCCESS != HI_MPI_VO_SetVideoLayerCSC(SAMPLE_VO_LAYER_VHD0, &pstVideoCSC)){
            COMMON_PRT("HI_MPI_VO_SetVideoLayerCSC failed!\n");
            // return false;
        }
    }
    else if(vo_dev == 1)
    {

        if (HI_SUCCESS != HI_MPI_VO_SetVideoLayerCSC(SAMPLE_VO_LAYER_VHD1, &pstVideoCSC)){
            COMMON_PRT("HI_MPI_VO_SetVideoLayerCSC failed!\n");
            // return false;
        }
    }




#if 0
    //  if (stVoPubAttr.enIntfType & VO_INTF_HDMI){
    if (HI_SUCCESS != HI_MPI_VO_GetHdmiParam(SAMPLE_VO_DEV_DHD0, &pstHdmiParam)){
        COMMON_PRT("HI_MPI_VO_GetHdmiParam failed!\n");
        // return false;
    }
    //      pstHdmiParam.stCSC.u32Luma = 49;


    printf(">>>>>>>>>>>>>>>>>>>>>>>HI_MPI_VO_GetHdmiParam--%d\n",pstHdmiParam.stCSC.enCscMatrix);
    //  pstHdmiParam.stCSC.enCscMatrix = VO_CSC_MATRIX_RGB_TO_BT601_PC;
    pstHdmiParam.stCSC.u32Luma = 50;
    pstHdmiParam.stCSC.u32Contrast = 50;
    pstHdmiParam.stCSC.u32Hue = 50;
    pstHdmiParam.stCSC.u32Saturation = 50;

    if (HI_SUCCESS != HI_MPI_VO_SetHdmiParam(SAMPLE_VO_DEV_DHD0, &pstHdmiParam)){
        COMMON_PRT("HI_MPI_VO_SetHdmiParam failed!\n");
        return false;
    }
    //  }
#endif




if(vo_dev == 0)
 {

    s32Ret = RSAMPLE_COMM_VO_StartChn(SAMPLE_VO_LAYER_VHD0,enMode);
    if(s32Ret != HI_SUCCESS){
        printf("HI35xx_COMM_VO_Startchn failed  SAMPLE_VO_LAYER_VHD0\n ");
        return HI_FAILURE;
    }
   }
else if(vo_dev == 1)
 {
    if(edid_4k == 0)
    {
        s32Ret = RSAMPLE_COMM_VO_StartChn(SAMPLE_VO_LAYER_VHD1,enMode);
        if(s32Ret != HI_SUCCESS){
            printf("HI35xx_COMM_VO_Startchn failed");
            return HI_FAILURE;
        }
    }

}
//    s32Ret = RSAMPLE_COMM_VO_StartChn(SAMPLE_VO_LAYER_VPIP,enMode);
//    if(s32Ret != HI_SUCCESS){
//        printf("HI35xx_COMM_VO_Startchn failed  SAMPLE_VO_LAYER_VPIP\n");
//        return HI_FAILURE;
//    }

//        s32Ret = RSAMPLE_COMM_VO_StartChn(SAMPLE_VO_LAYER_VSD0,enMode);
//        if(s32Ret != HI_SUCCESS){
//            printf("HI35xx_COMM_VO_Startchn failed SAMPLE_VO_LAYER_VSD0\n");
//            return HI_FAILURE;
//        }

}



bool HimppMaster::BindPreVoewSys(VO_LAYER vodev,VO_CHN voChN,VENC_CHN vestart,VENC_CHN veend)
{

   HI_S32 sRet;
    VI_DEV  videv = 1,videv1 = 3;
    VI_CHN   vichn = 5, vichn1 = 13;
    VPSS_GRP vpsgrp = 4,vpsgro_vi = 8;
    VPSS_CHN vpschn = 2, vpschn_vi = 0;
    VO_LAYER vodev_ = 0,vodev1 = 1;
    VO_CHN vochn = 0 ,vochn1 = 0;



  if(edid_4k == 1)
  {
      SAMPLE_COMM_Vi_vpss_bind(vpsgro_vi,vpschn_vi,vichn,videv);
      SAMPLE_COMM_VO_BindVpss(vodev_ ,vochn ,vpsgro_vi,vpschn_vi);

  }
  else if(edid_4k == 0)
  {
    SAMPLE_COMM_Vi_vpss_bind(vpsgro_vi,vpschn_vi,vichn1,videv1);
    SAMPLE_COMM_VO_BindVpss(vodev_ ,vochn ,vpsgro_vi,vpschn_vi);

    SAMPLE_COMM_Vi_vpss_bind(vpsgro_vi + 1,vpschn_vi,vichn  ,videv);
   SAMPLE_COMM_VO_BindVpss(vodev1 ,vochn1,vpsgro_vi +1 ,vpschn_vi );
  }


      //SAMPLE_COMM_VO_BindVpss(vodev_,vochn,vpsgrp,vpschn);
   // SAMPLE_COMM_VO_BindVi(vodev1+1,vochn1,vichn1,videv1);
    //SAMPLE_COMM_VO_BindVpss(vodev1,vochn1,vpsgrp + 1,vpschn);
  //  SAMPLE_COMM_VO_BindVi(vodev1,vochn1,vichn,videv);

    return true;


}






bool HimppMaster::BindPreViewSys(HI_S32 vpss_bin )
{


     VPSS_GRP VpssGrp = vpss_bin;

#if 1
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencSnapChn_[vpss_bin], VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }


          VpssGrp = (vpss_bin + 4);
        HI_U32 chnbin = (vpss_bin * 2 + 0 );
        if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss( VencChn_[chnbin], VpssGrp, VPSS_CHN0)){
            SAMPLE_PRT("Venc bind Vpss failed!\n");
            return false;
        }

         chnbin = (vpss_bin * 2 + 1 );
        if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss( VencChn_[chnbin], VpssGrp, VPSS_CHN1)){
            SAMPLE_PRT("Venc bind Vpss failed!\n");
            return false;
        }




#endif







    return true;
}

/*
 * VI0 -- VPSS 0 -- VENC 8
 * VI1 -- VPSS 1 -- VENC 9
 * VI2 -- VPSS 2 -- VENC 10
 * VI3 -- VPSS 3 -- VENC 11
 * */

bool HimppMaster::SnapBindPreViewSys(VI_DEV viDev,VI_CHN viChn)
{
    VPSS_GRP  VpssGrp = viDev;
    if (HI_SUCCESS != HI35xx_COMM_VI_BindVpss(viDev, VpssGrp, viChn))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(8+viDev, VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    return true;
}

bool HimppMaster::SnapUNBindPreViewSys(VI_DEV viDev,VI_CHN viChn)
{
    VPSS_GRP  VpssGrp = viDev;
    if (HI_SUCCESS != HI35xx_COMM_VI_UnBindVpss(viDev, VpssGrp, viChn))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_UnBindVpss(8+viDev, VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    return true;
}

bool HimppMaster::SnapUNBindVpss(VENC_CHN snapVenc)
{
    VPSS_GRP vpssGrp = snapVenc - 8;
    if (HI_SUCCESS != SAMPLE_COMM_VENC_UnBindVpss(snapVenc, vpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    return true;

}

int FpsPoolCnt = 60;
bool bFpsPool[60];
int last_gframeID = 0;
int freq;
int local_tick = 0, err_count = 0;

HI_U32 get_pts(){

    freq = viParam[0].u32SrcFrmRate;
    for(int i = 0; i < FpsPoolCnt; i++)
        bFpsPool[i] = false;

    double frame_step = (FpsPoolCnt * 1.0) / freq;
    double drop_tick = 0.0;
    int fps_id = 0;
    for(int i = 0; i < freq; i++){
        bFpsPool[fps_id] = true;
        drop_tick += frame_step;
        fps_id = min(59, (int)(drop_tick + 0.5));
    }

    int closest = 0;
    for(int i = 0; i < FpsPoolCnt / 2; i++){
        //NOTE: 往后查找 i 位
        if(bFpsPool[(glFrameID + i) % FpsPoolCnt]){

            closest = (glFrameID + i) % FpsPoolCnt;
            break;
        }

        //NOTE: 往前查找 i 位
        if(bFpsPool[(glFrameID + FpsPoolCnt - i) % FpsPoolCnt]){

            closest = (glFrameID + FpsPoolCnt - i) % FpsPoolCnt;
            break;
        }
    }

    // NOTE: 在池子中查找 上次使用的id 的下一个可用id
    int lastnext = 0;
    for(int i = 1; i < FpsPoolCnt; i++){
        if(bFpsPool[(i + local_tick) % FpsPoolCnt]){
            lastnext = (i + local_tick) % FpsPoolCnt;
            break;
        }
    }

    if(last_gframeID!= glFrameID){
        int date1 = abs(lastnext - closest);
        int date2 = 60 - date1;
        if(date1 > (2 * 60 / freq) &&
           date2 > (2 * 60 / freq)){
            err_count ++;
            if(err_count == 10){
                //printf("---------------------------c: %d, l: %d", closest, lastnext);
                lastnext = closest;
                err_count = 0;
            }
        }
        else{
            err_count = 0;
        }
    }
    //printf("---------------------------c: %d, l: %d\n", closest, lastnext);
    last_gframeID = glFrameID;

    local_tick = lastnext;
    return local_tick;

}

char* last_index = (char*)glDataBuf;
unsigned int last_pts = 0;
bool bNewFrame = false;


#if 1
bool HimppMaster::StartLoop(HI_U32 streamstart, HI_U32 streamend, HI_U32 vi_port)
{
   // printf("venc start  %d,end  %d\n",streamstart,streamend);
    int MAX_LEN = 0;
    HI_S32 s32Ret,packlen;
    HI_S32 maxfd = 0;
    HI_U32 j,m,i  = 0;
    HI_U8 *pu8Addr = NULL;
    HI_U32 pts;
    VIDEO_FRAME_INFO_S stFrame;
    HI_S32 s32MilliSec = -1;
    POINT_S chrPoint;
    HI_U32 charOffset = 6;
    HI_U32 standY = 0;
    HI_U32 bmp_max_height = 0;
    int osdname_len = 0;
    HI_BOOL getFlag = HI_FALSE;
    HI_BOOL uv_IVE = HI_TRUE;
    POINT_S dstSize_ = {0,0};

    VI_CHN vichn = 4+vi_port*8;
   VPSS_GRP  vpGRP;
    //theloop++;



 //   vi_flag[vi_port] = 0;
    if((osd_enable[vi_port] == 1) && (vi_flag[vi_port] == 1) ) {

        osdname_len = set_font(vi_port, &bmp_max_height);

    }


    HI_S32 VencFd[16];
    for(i = streamstart;i<streamend;i++){

        VencFd[i] = HI_MPI_VENC_GetFd(VencChn_[i]);
        if (VencFd[i] < 0){
            SAMPLE_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd[i]);
            return false;
        }
        if (maxfd <= VencFd[i])
        {
            maxfd = VencFd[i];
        }


    }

    fd_set read_fds;
    VENC_CHN_STAT_S stStat,stStat1;
    VENC_STREAM_S stVencStream ,stVencStream1;
    struct timeval TimeoutVal;
    struct timeval tStart,tStop,diff;
       int time_1 = 0;
//    pu8Addr = (HI_U8*)malloc(1500*1000);
      pu8Addr = (HI_U8*)malloc(stMaxSize_.u32Width * stMaxSize_.u32Height*3/2);
    HI_U16 frame_number[VD_CHN_MAX] = {0};
    char frame_data[INFO_HEADER + PACK_LEN];


#if 1

    UDPSocket *MultiSendSocket = new UDPSocket();


    memset(frame_data,0, (INFO_HEADER + PACK_LEN));
    frame_data[INFO_HEADER_H0] = 'S';
    frame_data[INFO_HEADER_H1] = 'L';
    frame_data[INFO_HEADER_VA] = 'V';

    int socket1 = MultiSendSocket->CreateUDPClient(nullptr,41235, false); //设为阻塞
    MultiSendSocket->BindLocalAddr(ETH0);


      setsockopt(socket1, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl));
    setsockopt(socket1, IPPROTO_IP, IP_TTL, (char *)&ttl, sizeof(ttl));
    int value = (PACK_LEN + INFO_HEADER)*16;
    setsockopt(socket1, SOL_SOCKET, SO_SNDBUF,(char *)&value, sizeof(value));

 //   setsockopt(socket, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl));
#endif
   s32MilliSec = 100;
    bool getVi = false;
     HI_U32 regValue,svValue = 0x0;

  HI_U32   mac_send ;
//printf("----1----------------StartLoop----------start=%d---streamend=%d--vichn=%d\n",streamstart,streamend,vichn);
int find_venc = 0;
    while( l_startloop[vi_port] ){

//        if((viParam[vi_port].u32ViHeigth <= 0) || (viParam[vi_port].u32ViWidth <= 0))
//            s32MilliSec = 100;



        s32Ret = HI_MPI_VI_GetFrame(vichn,&stFrame, s32MilliSec);
        if (HI_SUCCESS != s32Ret)
        {
            printf("%d get vi %d frame err:0x%x\n",theloop,vichn, s32Ret);

         //   getFlag = HI_FALSE;
            continue;
        }
        else
          {
            //printf("ok os vi =%d\n",vichn);
            getVi  = true;
          }


        stFrame.stVFrame.u64pts &= 0xFFFFFFFFFFFFFF;
        stFrame.stVFrame.u64pts |= (HI_U64)glFrameID << 56;


#if 1

         switch (vi_port) {
             case 0:
                 Hi_GetReg(VI_CH0_REG,&regValue);
                 if((regValue != 0x00) && (svValue == 0x0)) {
                     errFrame[vi_port] ++;
                     COMMON_PRT("--------get vi chn 0 reg-->0x%x--------\n",regValue);
                 }
                 else {
                     errFrame[vi_port] = 0;
                 }
                 svValue = regValue;
                 break;
             case 1:
                 Hi_GetReg(VI_CH8_REG,&regValue);
                 if((regValue != 0x00) && (svValue == 0x0)) {
                     COMMON_PRT("--------get vi chn 8 reg-->0x%x--------\n",regValue);
                     errFrame[vi_port] ++;
                 }
                 else {
                     errFrame[vi_port] = 0;
                 }
                 svValue = regValue;
                 break;
             case 2:
                 Hi_GetReg(VI_CH16_REG,&regValue);
                 if((regValue != 0x00) && (svValue == 0x0)) {
                     COMMON_PRT("--------get vi chn 16 reg-->0x%x--------\n",regValue);
                     errFrame[vi_port] ++;
                 }
                 else {
                     errFrame[vi_port] =0;
                 }
                 svValue = regValue;
                 break;
             case 3:
                 Hi_GetReg(VI_CH24_REG,&regValue);
                 if((regValue != 0x00) && (svValue == 0x0)) {
                     COMMON_PRT("--------get vi chn 24 reg-->0x%x--------\n",regValue);
                     errFrame[vi_port] ++;
                 }
                 else {
                     errFrame[vi_port] = 0;
                 }
                 svValue = regValue;
                 break;
             default:
                 break;
         }


         if(errFrame[vi_port] == kerrFrameNum ) {
             COMMON_PRT("error frame+++++++++++++++++++++%d\n",vi_port);
             errFrame[vi_port] = 0;
             EnableUserPic(vi_port);
             s32Ret = HI_MPI_VI_ReleaseFrame(ViChn_[vi_port], &stFrame);
             if (HI_SUCCESS != s32Ret)
             {
                 printf("release vi frame err:0x%x\n", s32Ret);
             }
             continue;
         }







#endif




            if(viParam[vi_port].sw_color_expand == HI_TRUE){
           //   if(viParam[vi_port].vi_crop_enable == HI_TRUE){
            printf("HI35xx_IVE_MAP--\n");
                if( !HI35xx_IVE_MAP(&stFrame, &stFrame, dstSize_, RECT_S{0,0,stFrame.stVFrame.u32Width,stFrame.stVFrame.u32Height}, HI_TRUE, uv_IVE)) {
                    printf("<------ive_map failed------>\n");
                }
            }




#if 1
       // if((osd_enable[vi_port] == 1)&&(vi_flag[vi_port] == 1)&&getFlag){
          if((osd_enable[vi_port] == 1) && (vi_flag[vi_port] ==1))
        {




              if(osd_reset[vi_port] == 1)
                osdname_len = set_font(vi_port, &bmp_max_height);


              osd_reset[vi_port] = 0;

              //printf("osd_neabele-----len=%d---------------loop\n",osdname_len);
              stFrame.stVFrame.pVirAddr[0] = HI_MPI_SYS_Mmap(stFrame.stVFrame.u32PhyAddr[0], stFrame.stVFrame.u32Width * stFrame.stVFrame.u32Height);
            if(NULL == stFrame.stVFrame.pVirAddr[0])
                printf("mmap %d error\n",vi_port);
            stFrame.stVFrame.pVirAddr[1] = HI_MPI_SYS_Mmap(stFrame.stVFrame.u32PhyAddr[1], stFrame.stVFrame.u32Width * stFrame.stVFrame.u32Height/2);
            if(NULL == stFrame.stVFrame.pVirAddr[1])
                printf("mmap %d error\n",vi_port);


            for(int n = 0; n < osdname_len; n++) {
                if(0 != n) {
                    if((testData[vi_port][n]&0xffff) == 0x0020)
                        chrPoint.s32X += chnBmp[vi_port][n-1].width + 30;
                    else
                        chrPoint.s32X +=  chnBmp[vi_port][n-1].width + charOffset;

                    chrPoint.s32Y = standY + (chnBmp[vi_port][0].rows - chnBmp[vi_port][n].rows);
                }
                else {
                    chrPoint.s32X = startPoint[vi_port].s32X;
                    standY = startPoint[vi_port].s32Y + bmp_max_height-chnBmp[vi_port][0].rows;
                    chrPoint.s32Y = standY;
                }
                if((testData[vi_port][n]&0xffff) == 0x4e00) //"一"居中
                    chrPoint.s32Y = standY + (chnBmp[vi_port][0].rows - chnBmp[vi_port][n].rows) - bmp_max_height/2;
                if(checkLetter(testData[vi_port][n]))//gjpqy下沉
                    chrPoint.s32Y = standY + (chnBmp[vi_port][0].rows - chnBmp[vi_port][n].rows) + chnBmp[vi_port][n].rows/3;

                if(((chrPoint.s32X + chnBmp[vi_port][n].width) <= stVoSize_[vi_port].u32Width)&&((chrPoint.s32Y + bmp_max_height) <= stVoSize_[vi_port].u32Height))
                    putTextFrame(chnBmp[vi_port][n], chrPoint, &stFrame, charColor[vi_port], false, bmp_max_height);
            }

            HI_MPI_SYS_Munmap(stFrame.stVFrame.pVirAddr[0], stFrame.stVFrame.u32Width * stFrame.stVFrame.u32Height);
            HI_MPI_SYS_Munmap(stFrame.stVFrame.pVirAddr[1], stFrame.stVFrame.u32Width * stFrame.stVFrame.u32Height/2);
        }




 #endif



//          if(streamstart == 0)
//          {
              vpGRP = (vi_port + 4);
              s32Ret = HI_MPI_VPSS_SendFrame( vpGRP, &stFrame, -1);
              if (HI_SUCCESS != s32Ret)
              {
                  printf("VPSS Send Frame err:0x%x\n",s32Ret);
                  sleep(1);
                  continue;
              }

       //   }
//          else{
//              for (i = streamstart; i < streamend; i++){





        if(snap_loop[vi_port] == HI_TRUE)
        {
         s32Ret = HI_MPI_VPSS_SendFrame(vi_port, &stFrame, -1);
         if (HI_SUCCESS != s32Ret)
         {
             printf("VPSS Send Frame err:0x%x\n",s32Ret);
             sleep(1);
             continue;
         }


        }



//



           /***************************************************
               step 2.4 : call mpi to get one-frame stream
               ***************************************************/





       // printf("----------------------------------StartLoop=%d-------------------\n",theloop);
        s32Ret = HI_MPI_VI_ReleaseFrame(vichn, &stFrame);
        if (HI_SUCCESS != s32Ret)
        {
            printf("release vi frame err:0x%x\n", s32Ret);
        }

    if(find_venc == 1)
        return false;


        FD_ZERO(&read_fds);
        for (i = streamstart; i < streamend; i++){
            FD_SET(VencFd[i], &read_fds);
        }
        TimeoutVal.tv_sec  = 1;
       // TimeoutVal.tv_usec = 10000/viParam[vi_port].u32DstFrameRate[0];
         TimeoutVal.tv_usec  =160;



        s32Ret = select(maxfd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            SAMPLE_PRT("select failed!\n");
            break;
        }
        else if (s32Ret == 0){
            SAMPLE_PRT("get venc%d stream time out, exit thread\n",maxfd);
            continue;
        }
        else{

//            if(vi_port == 0)
//                SAMPLE_PRT("get venc %d stream\n");
            for(i = streamstart;i<streamend;i++){
                if (FD_ISSET(VencFd[i], &read_fds)){
                    /***************************************************
                        step 2.1 : query how many packs in one-frame stream.
                        ***************************************************/
                    s32Ret = HI_MPI_VENC_Query(VencChn_[i], &stStat);
                    if (HI_SUCCESS != s32Ret){
                        //SAMPLE_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n", VencChn_[i], s32Ret);
                        break;
                    }
                    if(0 == stStat.u32CurPacks){
                        SAMPLE_PRT("NOTE: Current frame is NULL!\n");
                        continue;
                    }
                    /***************************************************
                        step 2.3 : malloc corresponding number of pack nodes.
                        ***************************************************/


                    memset(&stVencStream, 0, sizeof(stVencStream));
                    stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                    if (NULL == stVencStream.pstPack){
                        SAMPLE_PRT("malloc stream pack failed!\n");
                        break;
                    }
                    /***************************************************
                        step 2.4 : call mpi to get one-frame stream
                        ***************************************************/
                    stVencStream.u32PackCount = stStat.u32CurPacks;
                    s32Ret = HI_MPI_VENC_GetStream(VencChn_[i], &stVencStream, -1);
                    if (HI_SUCCESS != s32Ret){
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                        SAMPLE_PRT("HI_MPI_VENC_GetStream%d    failed with %#x!\n", VencChn_[i], s32Ret);
                        break;
                    }
//                    printf("get venc:%d frame\n",VencChn_[i]);
//                    HI_U32 u32Len = 0;
//                    for (m = 0; m < stVencStream.u32PackCount; m++)
//                        u32Len += (stVencStream.pstPack[m].u32Len-stVencStream.pstPack[m].u32Offset);



                    HI_U32 u32Len = 0;





                    for (j = 0; j < stVencStream.u32PackCount; j++){
                        memcpy(pu8Addr + u32Len,
                               stVencStream.pstPack[j].pu8Addr + stVencStream.pstPack[j].u32Offset,
                               stVencStream.pstPack[j].u32Len - stVencStream.pstPack[j].u32Offset);
                        u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);
                    }








                    if(frame_number[i] > 255)
                        frame_number[i] = 0;
                    frame_number[i]++;
                    packlen = u32Len;
                    //if(i == 0)
                    //printf("data %02x,%02x--------%d,\n", *((char*)pu8Addr+3),*((char*)pu8Addr+4), u32Len);
                    bool i_frame = false;
                    if((*((char*)pu8Addr+4) == 0x67)||(*((char*)pu8Addr+4) == 0x40)){
                        i_frame = true;
                      // printf("len %d,################## I 帧\n",u32Len);
                    }
#ifdef __TINY_RTSP_SERVER__
                    g_prtspserver->push_live_video_data( g_stream[i], (void*)pu8Addr, u32Len, stVencStream.pstPack[0].u64PTS/1000 );
#else

                   // printf("g_stream[%d]==%d                     rtsp\n",i,g_stream[i]);
                    crtsps_pushvideo( g_stream[i], (void*)pu8Addr, u32Len, true, (stVencStream.pstPack[0].u64PTS & 0xFFFFFFFFFFFFFF)/1000 );

#endif
                 //   printf("stvenc = %lld  stvi =%lld \n",stVencStream.pstPack[0].u64PTS,stFrame.stVFrame.u64pts);
                    int vi_type = 0;
                 if(i == (streamend -1))
                   vi_type  = 1;
                 else
                   vi_type = 0;

#if 1

                    if(mcast_enable[i] == HI_TRUE){
          // printf(" %d   i_frame =%d.PACK_LEN = %d  --%d len ip%s --%d \n", frame_number[i],i_frame ,PACK_LEN ,packlen ,mcast_ip[i],mcast_port[i]);

                        frame_data[INFO_HEADER_H0] = 'S';
                        frame_data[INFO_HEADER_H1] = 'L';
                        frame_data[INFO_HEADER_VA] = 'V';
                        frame_data[INFO_HEADER_VE] = i&0xFF;


                        frame_data[INFO_HEADER_SN] = frame_number[i];
                        if(u32Len%PACK_LEN > 0)
                            *(HI_U16*)&frame_data[INFO_HEADER_TP] = u32Len/PACK_LEN + 1;
                        else
                            *(HI_U16*)&frame_data[INFO_HEADER_TP] = u32Len/PACK_LEN;

                        //写入265标识
                        int video_typ = 0;
                        if(viParam[vi_port].enType[vi_type] == VIDEO_H265)
                            video_typ = 2;


                        if(1 == vi_type )
                        {
                            frame_data[INFO_HEADER_WW] =  viParam[vi_port].stMinorSize.u32Width;
                            frame_data[INFO_HEADER_HH] = viParam[vi_port].stMinorSize.u32Height;
                        }
                        else
                        {
                             frame_data[INFO_HEADER_WW] =  viParam[vi_port].stVencSize.u32Width;
                             frame_data[INFO_HEADER_HH] = viParam[vi_port].stVencSize.u32Height;
                        }


                         frame_data[INFO_HEADER_FG] = i_frame | video_typ;
                         frame_data[INFO_HEADER_FQ] = viParam[vi_port].u32DstFrameRate[i];
                     //   frame_data[INFO_HEADER_FD] = stFrame.stVFrame.u64pts >> 56;

                         frame_data[INFO_HEADER_FD] = stVencStream.pstPack[0].u64PTS >> 56;

                        *(HI_U16*)&frame_data[INFO_HEADER_CP] = 0;
                        char* bkup_data = (char*)pu8Addr;
                        int while_len = 0;
                        while ((packlen > 0 )&& (mcast_enable[i] == HI_TRUE)){

                            memcpy(&frame_data[INFO_HEADER], bkup_data, min(PACK_LEN,packlen));
                         //   printf("len=%d    packlen=%d  while_len=%d\n",min(PACK_LEN,packlen),packlen,while_len);
                            while_len++;
                            bkup_data += min(PACK_LEN,packlen);
                       mac_send =    MultiSendSocket->SendTo(
                                        mcast_ip[i],
                                        mcast_port[i],
                                        (char*)frame_data, min(PACK_LEN,packlen) + INFO_HEADER);
                         //   printf("amc_send %d\n",mac_send);

                            packlen -= min(PACK_LEN,packlen);
                            (*(HI_U16*)&frame_data[INFO_HEADER_CP]) ++;                              //指向下一包
                        }

                    }


#endif



                    /***************************************************
                            step 2.7 : free pack nodes
                            ***************************************************/

                    s32Ret = HI_MPI_VENC_ReleaseStream(VencChn_[i], &stVencStream);
                    if (HI_SUCCESS != s32Ret){
                        printf("free venc%d failed %#x\n",VencChn_[i],s32Ret);
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                    }
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                }
//                gettimeofday(&tStop, 0);
     //            tim_subtract(&diff, &tStart, &tStop);
//                 if(diff.tv_usec > 50000)
//                 {
//                     time_1 ++ ;
//                 }
//                 else
//                     time_1 = 0;
//                printf(" time_1 =%d venc %d,diff.tv_sec: %d, diff.tv_usec: %d\n", time_1,VencChn_[i],diff.tv_sec, diff.tv_usec);
            }
        }



    }

    if((osd_enable[vi_port] == 1)&&(vi_flag[vi_port] == 1)){
        for(int n = 0; n < osdname_len; n++){
            free(chnBmp[vi_port][n].buffer);
            chnBmp[vi_port][n].buffer = NULL;
        }
        free(chnBmp[vi_port]);
        chnBmp[vi_port] = NULL;
    }
#if 0
//    if(getVi) {
       s32Ret = HI_MPI_VI_ReleaseFrame(vichn, &stFrame);
        if (HI_SUCCESS != s32Ret){
            printf("free gervi%d failed %#x\n",vi_port,s32Ret);
            free(stVencStream.pstPack);
            stVencStream.pstPack = NULL;
        }
     IVE_MMZ_FREE(pstMap.u32PhyAddr,pstMap.pu8VirAddr);
 //   }
#endif
    free(pu8Addr);
    pu8Addr = NULL;
     delete MultiSendSocket;
    printf("thread vi:%d  end\n",vi_port);
   //close(socket1);
    return true;
}
#endif
#if 1

#endif




#if 0
bool HimppMaster::GetVencRateSnap(HI_U32 SnapChn, UDPSocket *socket, const char *clientip, const HI_U32 clientport, HI_U32 snaplen, HI_U32 sockfd, HI_U32 tcpflag)
{
    #if 1
    /* Set Venc Fd. */
    HI_S32 s32Ret;
    //HI_U8 *pu8Data = NULL;
    HI_S32 VencFd = HI_MPI_VENC_GetFd(  SnapChn );
    if (VencFd < 0){
        COMMON_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd);
        return false;
    }
    //COMMON_PRT("Snap VencFd: %d\n", VencFd);
    int mac_chn  = (SnapChn - 8);
    //pu8Data = (HI_U8*)malloc(1200*3000);
    fd_set read_fds;
    VENC_CHN_STAT_S stStat;
    VENC_STREAM_S stVencStream;
    struct timeval TimeoutVal;
    HI_U8 *pu8Data = (HI_U8*)malloc(1920*1080*3);
     //   HI_U8 * pu8Data = NULL;
  //  printf("++++++++++while++++snaplen+++++++%d+++++++++%d\n", SnapChn,mult_snap_runlag[SnapChn -8]    );

    while((veSnap[mac_chn].Snapmcast_enable ) && (mult_snap_runlag[SnapChn -8] == true) ){

        FD_ZERO(&read_fds);
        FD_SET(VencFd, &read_fds);
        TimeoutVal.tv_sec  = 1;
        TimeoutVal.tv_usec = 0;
        s32Ret = select(VencFd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            COMMON_PRT("select failed!\n");
            continue;
        }
        else if (s32Ret == 0){
            COMMON_PRT("get venc stream time out, exit thread\n");
            continue;
        }
        else{
            if ((FD_ISSET(VencFd, &read_fds))  && (mult_snap_runlag[SnapChn -8] == true)){




                s32Ret = HI_MPI_VENC_Query( SnapChn,  &stStat);
                if (HI_SUCCESS != s32Ret){
                    COMMON_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n",  SnapChn, s32Ret);
                    break;
                }
                if(0 == stStat.u32CurPacks){
                    COMMON_PRT("NOTE: Current frame chn=%d is NULL!\n",SnapChn);
                    continue;
                }

                memset(&stVencStream, 0, sizeof(stVencStream));
                stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                if (NULL == stVencStream.pstPack){
                    COMMON_PRT("malloc stream pack failed!\n");
                    break;
                }

                stVencStream.u32PackCount = stStat.u32CurPacks;

                s32Ret = HI_MPI_VENC_GetStream(SnapChn, &stVencStream, -1);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                }

                HI_U32 u32Len = 0;
                for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++)
                    u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);

                //pu8Data = (HI_U8*)malloc(u32Len+4);

                u32Len = 0;
                for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++){
                    memcpy(pu8Data + u32Len,
                           stVencStream.pstPack[j].pu8Addr + stVencStream.pstPack[j].u32Offset,
                           stVencStream.pstPack[j].u32Len - stVencStream.pstPack[j].u32Offset);
                    u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);
                }


                if(tcpflag == 0) {
                     // printf("+2++++++++sendend++++snaplen+++++++%d\n", SnapChn);
                    this->SendSnap(pu8Data, u32Len, snaplen, socket, veSnap[SnapChn - 8].Snapmcast_ip,veSnap[SnapChn -8].Snapmcast_port,veSnap[SnapChn-8].snapDelay,HI_TRUE);
//
   // S ackLen, UDPSocket *socket, const char* clientip, const uint clientport, HI_S32 SnapDelay)

                    printf("multicast send ok snapchn=%d\n",SnapChn -8);
                }
                else if(tcpflag == 1){
                    s32Ret = send(sockfd,pu8Data, u32Len,0);
                    /*if(u32Len > max_len)
                    max_len = u32Len;*/
                    //printf("success len %d,send len %d\n",u32Len,s32Ret);
                }



                s32Ret = HI_MPI_VENC_ReleaseStream(SnapChn, &stVencStream);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                    printf("venc release failed--->0x%x\n",s32Ret);
                    break;
                }


                free(stVencStream.pstPack);
               // snap_running = 2;
            }
        }
    }
    free(pu8Data);

    pu8Data = NULL;
   // snap_running = 2;

     #endif
 // int ret_teue;
    return true;
}

#endif








#if 1
bool HimppMaster::GetVencRateSnap(HI_U32 SnapChn, UDPSocket *socket, const char *clientip, const HI_U32 clientport, HI_U32 snaplen, HI_U32 sockfd, HI_U32 tcpflag)
{
    #if 1
     FILE* pFile[64];
    /* Set Venc Fd. */
    HI_S32 s32Ret;
    //HI_U8 *pu8Data = NULL;
    HI_S32 VencFd = HI_MPI_VENC_GetFd(  SnapChn );
    if (VencFd < 0){
        COMMON_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd);
        return false;
    }
    //COMMON_PRT("Snap VencFd: %d\n", VencFd);
    int mac_chn  = (SnapChn - 8);
    //pu8Data = (HI_U8*)malloc(1200*3000);
    fd_set read_fds;
    VENC_CHN_STAT_S stStat;
    VENC_STREAM_S stVencStream;
    struct timeval TimeoutVal;
    HI_U8 *pu8Data = (HI_U8*)malloc(1920*1080*3);

   // printf("++++++++++while++++snaplen+++++++%d\n", SnapChn);

    while((veSnap[mac_chn].Snapmcast_enable ) && (mult_snap_runlag[SnapChn - 8] == true) ){

        FD_ZERO(&read_fds);
        FD_SET(VencFd, &read_fds);
        TimeoutVal.tv_sec  = 1;
        TimeoutVal.tv_usec = 0;
        s32Ret = select(VencFd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            COMMON_PRT("select failed!\n");
            continue;
        }
        else if (s32Ret == 0){
            COMMON_PRT("get venc stream time out, exit thread\n");
            continue;
        }
        else{
            if (((FD_ISSET(VencFd, &read_fds))&& (snap_running[SnapChn -8] == HI_TRUE)) && (mult_snap_runlag[SnapChn -8] == true)){
              //  snap_running = HI_TRUE;
                s32Ret = HI_MPI_VENC_Query( SnapChn,  &stStat);
                if (HI_SUCCESS != s32Ret){
                    COMMON_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n",  SnapChn, s32Ret);
                    break;
                }
                if(0 == stStat.u32CurPacks){
                    COMMON_PRT("NOTE: Current frame chn=%d is NULL!\n",SnapChn);
                    continue;
                }

                memset(&stVencStream, 0, sizeof(stVencStream));
                stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                if (NULL == stVencStream.pstPack){
                    COMMON_PRT("malloc stream pack failed!\n");
                    break;
                }

                stVencStream.u32PackCount = stStat.u32CurPacks;

                s32Ret = HI_MPI_VENC_GetStream(SnapChn, &stVencStream, -1);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                }

                HI_U32 u32Len = 0;
                for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++)
                    u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);


                u32Len = 0;
                for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++){
                    memcpy(pu8Data + u32Len,
                           stVencStream.pstPack[j].pu8Addr + stVencStream.pstPack[j].u32Offset,
                           stVencStream.pstPack[j].u32Len - stVencStream.pstPack[j].u32Offset);
                    u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);
                }








                //                pFile[1] = fopen("/home/education/stream_yin1.jpg", "wb");
                //                                           if (!pFile[1])
                //                                           {
                //                                               SAMPLE_PRT("open file err!\n");
                //                                               return NULL;
                //                                           }

                //                        SAMPLE_COMM_VENC_SaveStream(PT_MJPEG,pFile[1],&stVencStream);




 //veSnap[vi].Snapmcast_ip, veSnap[vi].Snapmcast_port
                if(tcpflag == 0) {
                    this->SendSnap(pu8Data, u32Len, snaplen, socket, veSnap[SnapChn -8].Snapmcast_ip,veSnap[SnapChn -8].Snapmcast_port,veSnap[SnapChn-8].snapDelay,HI_TRUE,SnapChn -8);

                }
                else if(tcpflag == 1){
                    s32Ret = send(sockfd,pu8Data, u32Len,0);
                    /*if(u32Len > max_len)
                    max_len = u32Len;*/
                    //printf("success len %d,send len %d\n",u32Len,s32Ret);
                }



                s32Ret = HI_MPI_VENC_ReleaseStream(SnapChn, &stVencStream);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                    printf("venc release failed--->0x%x\n",s32Ret);
                    break;
                }


                free(stVencStream.pstPack);
               // snap_running = 2;
            }
        }
    }
    free(pu8Data);

    pu8Data = NULL;
   // snap_running = 2;

     #endif
 // int ret_teue;
    return true;
}


#endif







bool HimppMaster::GetVencSnap(HI_U32 SnapChn, UDPSocket *socket, const char *clientip, const HI_U32 clientport, HI_U32 snaplen)
{
    /* Set Venc Fd. */
    HI_S32 s32Ret;
   // HI_U8 *pu8Data = NULL;
    HI_S32 VencFd = HI_MPI_VENC_GetFd( VencSnapChn_[SnapChn] );
    if (VencFd < 0){
        COMMON_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd);
        return false;
    }
    //COMMON_PRT("Snap VencFd: %d\n", VencFd);
    FILE* pFile[64];
  //  HI_U8 * pu8Data = NULL;
   // HI_U8 * pu8Data = (HI_U8*)malloc(1200*3000);
   //  HI_U8 * pu8Data = (HI_U8*)malloc(1200*3000);
     // HI_U8 *  pu8Data = (HI_U8*)malloc(veSnap[SnapChn].snapSize.u32Width * veSnap[SnapChn].snapSize.u32Height*3/2);
    fd_set read_fds;
    VENC_CHN_STAT_S stStat;
    VENC_STREAM_S stVencStream;
    struct timeval TimeoutVal;
    HI_U32 counter = 0;
    HI_U32 pic_counter = 0;


      SNAP_DATA snapdata;
      HI_S32 sendlen  = 0;
     // struct timeval start,stop,diff;




        HI_S32 s32MilliSec = -1;





    while( (counter < 5 )&&(snap_runlag[SnapChn] == true)  ) {

        FD_ZERO(&read_fds);
        FD_SET(VencFd, &read_fds);
        TimeoutVal.tv_sec  = 1;
        TimeoutVal.tv_usec = 0;
        s32Ret = select(VencFd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            COMMON_PRT("select failed!\n");
            counter++;
            break;
        }
        else if (s32Ret == 0){
            COMMON_PRT("get venc stream time out, exit thread\n");
            counter++;
            continue;

        }
        else{
            if ((FD_ISSET(VencFd, &read_fds)) &&(snap_runlag[SnapChn] == true ) ){

                s32Ret = HI_MPI_VENC_Query(VencSnapChn_[SnapChn], &stStat);
                if (HI_SUCCESS != s32Ret){
                    COMMON_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n", VencSnapChn_[SnapChn], s32Ret);
                    break;
                }
                if(0 == stStat.u32CurPacks){
                    COMMON_PRT("NOTE: Current frame is NULL!\n");
                    continue;
                }

                memset(&stVencStream, 0, sizeof(stVencStream));
                stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                if (NULL == stVencStream.pstPack){
                    COMMON_PRT("malloc stream pack failed!\n");
                    break;
                }

                //                COMMON_PRT("u32LeftPics: %d, u32LeftEncPics: %d, u32LeftRecvPics: %d\n",
                //                           stStat.u32LeftPics, stStat.u32LeftEncPics, stStat.u32LeftRecvPics);
                pic_counter = 0; //记录每次while循环中读取的图片张数
                stVencStream.u32PackCount = stStat.u32CurPacks;
                while(1){ //加上循环，为了读取缓存中全部张数，防止出现缓存过多的情况
                    s32Ret = HI_MPI_VENC_GetStream(VencSnapChn_[SnapChn], &stVencStream, 1);
                    if (HI_SUCCESS != s32Ret){
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                       // COMMON_PRT("HI_MPI_VENC_GetStream failed with %#x!\n", s32Ret);
                        break;
                    }

                   // COMMON_PRT("pic_counter: %d\n", pic_counter);
                    if(pic_counter == 0){ //只对第一张图进行发送。
                        //TO DO:Send it
                                //snap_loop[SnapChn] =HI_FALSE;
//                            if(SnapChn == 0)
//                            {
//                                pFile[SnapChn] = fopen("/home/education/stream_yin.jpg", "wb");
//                                                  if (!pFile[0])
//                                                  {
//                                                      SAMPLE_PRT("open file err!\n");
//                                                      return NULL;
//                                                  }
//                             }
//                           else  if(SnapChn == 1)
//                            {
//                                 pFile[SnapChn] = fopen("/home/education/stream_yin1.jpg", "wb");
//                                                            if (!pFile[1])
//                                                            {
//                                                                SAMPLE_PRT("open file err!\n");
//                                                                return NULL;
//                                                            }
//                             }
                                                            //    SAMPLE_COMM_VENC_SaveStream(PT_MJPEG,pFile[1],&stVencStream);


                        HI_U32 u32Len = 0;
                        for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++)
                            u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);

                       HI_U8 *   pu8Data = (HI_U8*)malloc(u32Len + 4);
                    //       HI_U8 *pu8Data = (HI_U8*)malloc(u32Len);

                         u32Len = 0;
                        for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++){
                       //     printf("pu8Data=%x\n",pu8Data + u32Len);
                            memcpy(pu8Data + u32Len,
                                   stVencStream.pstPack[j].pu8Addr + stVencStream.pstPack[j].u32Offset,
                                   stVencStream.pstPack[j].u32Len - stVencStream.pstPack[j].u32Offset);
                            u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);
                           //   printf("pu8Data=%x\n",pu8Data + u32Len);
                        }



                     //   printf("___________________________%d\n",veSnap[1].snapDelay);
             this->SendSnap(pu8Data, u32Len, snaplen, socket, clientip, clientport, veSnap[SnapChn].snapDelay,HI_FALSE,SnapChn);




                        free(pu8Data);
                        pu8Data = NULL;
                       // fclose(pFile[0]);
                       // fclose(pFile[1]);
                        //COMMON_PRT("send pic ok!\n");
                    }
                    pic_counter++;
//                    free(pu8Data);
//                    pu8Data = NULL;

                    s32Ret = HI_MPI_VENC_ReleaseStream(VencSnapChn_[SnapChn], &stVencStream);
                    if (HI_SUCCESS != s32Ret){
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                        break;
                    }
                }

                free(stVencStream.pstPack);
                stVencStream.pstPack = NULL;
                break;
            }
        }
    }
//    free(pu8Data);
//    pu8Data = NULL;

//    free(stVencStream.pstPack);
//    stVencStream.pstPack = NULL;

    return true;
}



#if 0
bool HimppMaster::SendSnap(HI_U8 *pu8Data, HI_U32 u32TotalLen, HI_U32 u32PackLen, UDPSocket *socket, const char* clientip, const uint clientport, HI_U32 SnapDelay,HI_BOOL snap_muil)
{
    SNAP_DATA snapdata;
    HI_S32 sendlen  = 0;
    struct timeval start,stop,diff;
 FILE* pFile[64];
    //    snapdata.pData = new HI_U8[u32PackLen];
    snapdata.u32TotalPack = (u32TotalLen + u32PackLen - 1) / u32PackLen; //总包数
    short audio_left  = 0;
    short  audio_right = 0;

    *(short*)&pu8Data[u32TotalLen] = audio_left;
    *(short*)&pu8Data[u32TotalLen+2] = audio_right;

    if(snap_muil == HI_TRUE)
        u32TotalLen = u32TotalLen + 4;


    for(HI_U32 i = 0; i < snapdata.u32TotalPack; i++){
        //        COMMON_PRT("u32TotalLen: %d, now: %d, totalPackNum: %d\n", u32TotalLen, i, snapdata.u32TotalPack);
        snapdata.u32CurrPack = i; //当前包序号
        memcpy(snapdata.pData, pu8Data, MIN2(u32PackLen, u32TotalLen));




        sendlen = socket->SendTo(clientip, clientport, (char*)&snapdata, MIN2((1200+2 * sizeof(HI_U32)), (u32TotalLen+2 * sizeof(HI_U32))));
        //        COMMON_PRT("snap sendto len: %d\n", sendlen);

        u32TotalLen -= u32PackLen;
        pu8Data     += u32PackLen;
        gettimeofday(&start, 0);
        do{
            gettimeofday(&stop, 0);

            tim_subtract(&diff, &start, &stop);

            //printf("diff.tv_sec: %d, diff.tv_usec: %d,mcast_delay %d\n", diff.tv_sec, diff.tv_usec, mcast_delay[i]);
        }while(diff.tv_usec < SnapDelay);
        //usleep(800);
    }

    //    delete[] snapdata.pData;

    return true;
}
#endif
HI_S16 audio_left[4] ={0,0,0,0}, audio_right[4] ={0,0,0,0};


bool HimppMaster::SendSnap(HI_U8 *pu8Data, HI_U32 u32TotalLen, HI_U32 u32PackLen, UDPSocket *socket, const char* clientip, const uint clientport, HI_U32 SnapDelay,HI_BOOL snap_muil,HI_U32 Snap_chN)
{
    SNAP_DATA snapdata;
    HI_S32 sendlen  = 0;


    struct timeval start,stop,diff;




    *(short*)&pu8Data[u32TotalLen] = audio_left[Snap_chN];
    *(short*)&pu8Data[u32TotalLen+2] = audio_right[Snap_chN];


    if(snap_muil == HI_TRUE)
    {
        u32TotalLen = u32TotalLen + 4;
    }

//printf("u32totlen++++++++%d\n",u32TotalLen);

    //    snapdata.pData = new HI_U8[u32PackLen];
    snapdata.u32TotalPack = (u32TotalLen + u32PackLen - 1) / u32PackLen; //总包数

    for(HI_U32 i = 0; i < snapdata.u32TotalPack; i++){

        snapdata.u32CurrPack = i; //当前包序号
        memcpy(snapdata.pData, pu8Data, MIN2(u32PackLen, u32TotalLen));


        sendlen = socket->SendTo(clientip, clientport, (char*)&snapdata, MIN2((1200+2 * sizeof(HI_U32)), (u32TotalLen+2 * sizeof(HI_U32))));
      //  COMMON_PRT("u32TotalLen: %d, now: %d, totalPackNum: %d     %d\n", u32TotalLen, i, snapdata.u32TotalPack,sendlen);
        u32TotalLen -= u32PackLen;
        pu8Data     += u32PackLen;

        gettimeofday(&start, 0);
        do{
            gettimeofday(&stop, 0);
            tim_subtract(&diff, &start, &stop);

        }while(diff.tv_usec < SnapDelay);
        //usleep(800);
    }

    //    delete[] snapdata.pData;

    return true;
}


bool HimppMaster::SetUserPic(){

    printf(" ia mSetUserPic           111\n");
    if(SetNosignal)
        return false;
    SetNosignal =  true;
    HI_S32 s32Ret,a = 0;
    VI_USERPIC_ATTR_S stUsrPic;
    stUsrPic.bPub = HI_TRUE;
    VI_CHN  Userchn = 4;
    FILE* fBGFile = NULL;
   // fBGFile  = fopen("/home/4in_input/picture.yuv","rb");
   fBGFile = fopen("./picture.yuv","rb");
    if(fBGFile == NULL){
        printf("fopen filedSetUserPic1111111111111111111111\n");
        stUsrPic.enUsrPicMode = VI_USERPIC_MODE_BGC;
        stUsrPic.unUsrPic.stUsrPicBg.u32BgColor = 0x009632;//0xC8C8C8;
    }else{
        stUsrPic.enUsrPicMode = VI_USERPIC_MODE_PIC;
        HI_U32 u32Width = 1920;
        HI_U32 u32Height = 1080;
        HI_U32 u32PicLStride;
        HI_U32 u32PicCStride;
        HI_U32 u32LumaSize;
        HI_U32 u32ChrmSize;
        HI_U32 u32Size;
        //VB_BLK VbBlk;
        HI_U32 u32PhyAddr;
        HI_U8 *pVirAddr;

        u32PicLStride = CEILING_2_POWER(u32Width, SAMPLE_SYS_ALIGN_WIDTH);
        u32PicCStride = CEILING_2_POWER(u32Width, SAMPLE_SYS_ALIGN_WIDTH);
        u32LumaSize = (u32PicLStride * u32Height);
        u32ChrmSize = (u32PicCStride * u32Height) >> 2;
        u32Size = u32LumaSize + (u32ChrmSize << 1);

        VbBlk = HI_MPI_VB_GetBlock(VB_INVALID_POOLID, u32Size, NULL);
        if (VB_INVALID_HANDLE == VbBlk)
        {
            printf("HI_MPI_VB_GetBlock error VB_INVALID_HANDLE\n");
            return -1;
        }
        /* get physical address*/
        u32PhyAddr = HI_MPI_VB_Handle2PhysAddr(VbBlk);
        if (0 == u32PhyAddr)
        {
            printf("HI_MPI_VB_Handle2PhysAddr error\n");
            return -1;
        }

        pVirAddr = (HI_U8 *) HI_MPI_SYS_Mmap( u32PhyAddr, u32Size );
        if(pVirAddr == NULL)
        {
            printf("Mem dev may not open\n");
            HI_MPI_VB_ReleaseBlock(VbBlk);
            sleep(6);
        }

        /* get pool id */
        stUsrPic.unUsrPic.stUsrPicFrm.u32PoolId = HI_MPI_VB_Handle2PoolId(VbBlk);
        if (VB_INVALID_POOLID == stUsrPic.unUsrPic.stUsrPicFrm.u32PoolId)
        {
            printf("HI_MPI_VB_Handle2PoolId error\n");
            return -1;
        }

        memset(&stUsrPic.unUsrPic.stUsrPicFrm.stVFrame, 0, sizeof(VIDEO_FRAME_S));
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[0] = u32PhyAddr;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[1] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[0] + u32LumaSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[2] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[1] + u32ChrmSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[0] = pVirAddr;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[1] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[0] + u32LumaSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[2] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[1] + u32ChrmSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Width = u32Width;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Height = u32Height;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Stride[0] = u32PicLStride;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Stride[1] = u32PicCStride;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Stride[2] = u32PicCStride;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.enPixelFormat = SAMPLE_PIXEL_FORMAT;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.enVideoFormat  = VIDEO_FORMAT_LINEAR;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.enCompressMode = COMPRESS_MODE_NONE;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Field       = VIDEO_FIELD_FRAME;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u64pts     = (a * 40);
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32TimeRef = (a * 2);

//        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.s16OffsetTop = 0 ;
//         stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.s16OffsetBottom = 540;
//          stUsrPic.unUsrPic.stUsrPicFrm.stVFrame .s16OffsetLeft = 0;
//           stUsrPic.unUsrPic.stUsrPicFrm.stVFrame .s16OffsetRight = 960;
//           stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Width = 960;
//           stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Height = 540;

        a++;
        if(a >= 100)
            a=0;

        for(int i = 0; i < u32Height; i ++)
            fread((char*)(stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[0]) + stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Width * i, 1, 1920,fBGFile);

        for(int i = 0; i < u32Height/2; i ++)
            fread((char*)(stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[1]) + stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Width * i, 1, 1920,fBGFile);

        fclose(fBGFile);
    }

    s32Ret = HI_MPI_VI_SetUserPic(Userchn, &stUsrPic); //目前此接口中的参数 VI 通道号未被使用，即设置用户图像针对的是所有 VI 通道，而不是具体某个 VI 通道
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VI_SetUserPic failed, err:0x%x!\n", s32Ret);
    }



    return true;
}

bool HimppMaster::EnableUserPic(VI_CHN ViChn)
{
    HI_S32 s32Ret;
    s32Ret = HI_MPI_VI_EnableUserPic(ViChn); //启用插入的用户图片
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VI_EnableUserPic[%d] failed, err:0x%x!\n", ViChn, s32Ret);
    }

    return true;
}

bool HimppMaster::DisableUserPic(VI_CHN ViChn)
{
   // HI_MPI_VI_DisableUserPic(ViChn_[ViChn]); //禁用插入的用户图片
   HI_MPI_VI_DisableUserPic(ViChn);

    return true;
}

bool HimppMaster::StopViModule(VI_DEV ViDev, VI_CHN ViChn)
{
    HI_S32 s32Ret,s32RetG;
    HI_BOOL Fram_vi = HI_TRUE;
     VIDEO_FRAME_INFO_S stFrame;
    while( HI_MPI_VI_GetFrame(ViChn, &stFrame, 0) == HI_SUCCESS)
    {
        printf("fram==%d ---vichn=%d\n",Fram_vi,ViChn);

           s32Ret = HI_MPI_VI_ReleaseFrame(ViChn, &stFrame);
            if (HI_SUCCESS != s32Ret){
                SAMPLE_PRT("HI_MPI_VI_ReleaseFrame failed, err:0x%x!\n", s32Ret);
                Fram_vi = HI_FALSE;
                break;
            }

    }

    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn, 0);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VI_SetFrameDepth failed, err:0x%x!\n", s32Ret);
    }


   // s32Ret = HI_MPI_VI_DisableUserPic(ViChn_[ViChn]);
    s32Ret = HI_MPI_VI_DisableUserPic(ViChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VI_DisableUserPic failed with %#x\n",s32Ret);
        return HI_FAILURE;
    }
   s32Ret = HI35xx_COMM_VI_Stop(ViDev,ViChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VI_DisableUserPic failed with %#x\n",s32Ret);
        return HI_FAILURE;
    }

    return true;
}

bool HimppMaster::VdecModuleDestroy(HI_U32 u32VdCnt)
{
    HI_S32 s32Ret;
    for (HI_U32 i = 0; i < u32VdCnt; i++){
        s32Ret = SAMPLE_COMM_VDEC_Stop(VdChn_[i]);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("HI3536_COMM_VENC_SnapStop failed, err:0x%x!\n", s32Ret);
        }
    }

    SAMPLE_PRT("himpp vdec module destroy succeed!");
    return HI_SUCCESS;
}


bool HimppMaster::VencModuleDestroy_1(HI_U32 u32VeChnStart, HI_U32 u32VeChnCnt,HI_S32 Snap)
{
    HI_S32 s32Ret;


  if((u32VeChnCnt > 0) && (u32VeChnCnt <= VencMax) ){
    for (HI_U32 i = u32VeChnStart; i < u32VeChnCnt; i++){
        s32Ret = SAMPLE_COMM_VENC_Stop(VencChn_[i]);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("SAMPLE_COMM_VENC_Stop failed, err:0x%x!\n", s32Ret);
        }

        s32Ret = HI_MPI_VENC_DestroyChn(VencChn_[i]);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", \
                       VencChn_[i], s32Ret);
            sleep(1);
             return HI_FAILURE;
        }
    }
  }
    if((Snap >= 0 )  &&( Snap <= SNAPMAX))
    {
           printf("VencModuleDestroy  vecnstart==%d----end=%d\n",u32VeChnStart,u32VeChnCnt);
        s32Ret = SAMPLE_COMM_VENC_Stop(VencSnapChn_[Snap]);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("SAMPLE_COMM_VENC_Stop failed, err:0x%x!\n", s32Ret);
        }

        s32Ret = HI_MPI_VENC_DestroyChn(VencSnapChn_[Snap]);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", \
                       VencSnapChn_[Snap], s32Ret);
            sleep(1);
                return HI_FAILURE;
        }

    }else{
        printf("havent snap\n");
    }


    SAMPLE_PRT("himpp venc module destroy succeed!\n");
    return HI_SUCCESS;
}


bool HimppMaster::SnapModuleDestroy(HI_U32 VencSnapChn)
{
    HI_S32 s32Ret;

    s32Ret = SAMPLE_COMM_VENC_Stop(VencSnapChn_[VencSnapChn]);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("SAMPLE_COMM_VENC_Stop failed, err:0x%x!\n", s32Ret);
    }
    s32Ret = HI_MPI_VENC_DestroyChn(VencSnapChn_[VencSnapChn]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", \
                   VencSnapChn_[VencSnapChn], s32Ret);
        sleep(1);
//            return HI_FAILURE;
    }
}


bool HimppMaster::VpssModuleDestroy()
{


    VpssGrpCnt_ = 8;
    HI_S32 Grp_start = 0;
    if(HI_SUCCESS != SAMPLE_COMM_VPSS_Stop(Grp_start , VpssGrpCnt_, VpssChnCnt_)){
        SAMPLE_PRT("SAMPLE_COMM_VPSS_Stop failed!\n");
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

bool HimppMaster::VoModuleDestroy(HI_U32 Vo_dev)
{

    if(edid_4k == 0)
    {
        if(HI_SUCCESS != HI35xx_COMM_VO_StopChn(VoLayer_ +2 , VoChn_))
            return HI_FAILURE;

        if(HI_SUCCESS != SAMPLE_COMM_VO_StopLayer(VoLayer_ +2))
            return HI_FAILURE;
    }
    if(Vo_dev == 0)
    {
        if(HI_SUCCESS != HI35xx_COMM_VO_StopChn(VoLayer_, VoChn_))
            return HI_FAILURE;


        if(HI_SUCCESS != SAMPLE_COMM_VO_StopLayer(VoLayer_))
            return HI_FAILURE;


        if(HI_SUCCESS != SAMPLE_COMM_VO_StopDev(VoDev_))
            return HI_FAILURE;
    }
    else if(Vo_dev == 1)
    {



        if(HI_SUCCESS != HI35xx_COMM_VO_StopChn(VoLayer_ +1 , VoChn_))
            return HI_FAILURE;

        if(HI_SUCCESS != SAMPLE_COMM_VO_StopLayer(VoLayer_ +1))
            return HI_FAILURE;




        if(HI_SUCCESS != SAMPLE_COMM_VO_StopDev(VoDev_ +1))
            return HI_FAILURE;
    }

//    if(HI_SUCCESS != SAMPLE_COMM_VO_HdmiStop())
//        return HI_FAILURE;

    return HI_SUCCESS;
}


bool HimppMaster::AiModeuleInit_1(AUDIO_DEV aiDev,AI_CHN aiChnCnt,HI_U32 numPerFrm,AUDIO_SAMPLE_RATE_E enAiSampleRate)
{
     AIO_ATTR_S stAioAttr;
     HI_S32  s32Ret;

     stAioAttr.enSamplerate   = enAiSampleRate;
     stAioAttr.enBitwidth     = AUDIO_BIT_WIDTH_16;
     stAioAttr.enWorkmode     = AIO_MODE_I2S_SLAVE;
     stAioAttr.enSoundmode    = AUDIO_SOUND_MODE_MONO;
     stAioAttr.u32EXFlag      = 1;
     stAioAttr.u32FrmNum      = 30;
     stAioAttr.u32PtNumPerFrm = numPerFrm;
     stAioAttr.u32ChnCnt      = 2;
     stAioAttr.u32ClkChnCnt   = 2;
     stAioAttr.u32ClkSel      = 0;


    if(aiDev == 2)
    {
        //stAioAttr.enSamplerate =
     stAioAttr.enWorkmode =AIO_MODE_I2S_MASTER;
    stAioAttr.enSoundmode = AUDIO_SOUND_MODE_STEREO;
    stAioAttr.u32ClkSel = 1;

    }




     AI_VQE_CONFIG_S pstAiVqeAttr;
     pstAiVqeAttr.bHpfOpen = HI_FALSE;
     pstAiVqeAttr.bRnrOpen = HI_TRUE;
     pstAiVqeAttr.bAecOpen = HI_FALSE;
     pstAiVqeAttr.bAnrOpen = HI_FALSE;
     pstAiVqeAttr.bAgcOpen = HI_FALSE;
     pstAiVqeAttr.bEqOpen = HI_FALSE;
     pstAiVqeAttr.bHdrOpen = HI_FALSE;
     pstAiVqeAttr.enWorkstate = VQE_WORKSTATE_MUSIC; //VQE_WORKSTATE_COMMON,VQE_WORKSTATE_MUSIC,VQE_WORKSTATE_NOISY
     pstAiVqeAttr.s32WorkSampleRate = AUDIO_SAMPLE_RATE_48000;
     pstAiVqeAttr.s32FrameSample = numPerFrm;
     pstAiVqeAttr.stRnrCfg.bUsrMode = HI_TRUE;//HI_FALSE;
     pstAiVqeAttr.stRnrCfg.s32NrMode = 0;
     pstAiVqeAttr.stRnrCfg.s32MaxNrLevel = 20;
     pstAiVqeAttr.stRnrCfg.s32NoiseThresh = -50;
     //pstAiVqeAttr->stAgcCfg.bUsrMode = HI_FALSE;

     s32Ret = SAMPLE_COMM_AUDIO_StartAi(aiDev, aiChnCnt, &stAioAttr, AUDIO_SAMPLE_RATE_BUTT, HI_FALSE, &pstAiVqeAttr, 0);
     if (s32Ret != HI_SUCCESS)
     {
         printf("sample_comm_audio_startAi failed--->%x\n",s32Ret);
         return false;
     }






     return true;
}





bool HimppMaster::StartAudioMcast(AUDIO_DEV AiDev,AI_CHN AiChn)
{
    HI_S32 s32Ret,packlen;
    AUDIO_FRAME_S pstFrm;
    AEC_FRAME_S pstAecFrm;
    AI_CHN_PARAM_S pstChnParam;
    HI_S32 s32MilliSec = -1;

    int aframe_number = 0;
    char aframe_data[INFO_HEADER + PACK_LEN];
    memset(aframe_data,0, (INFO_HEADER + PACK_LEN));
    aframe_data[INFO_HEADER_H0] = 'S';
    aframe_data[INFO_HEADER_H1] = 'L';
    //aframe_data[INFO_HEADER_VA] = 'A';
    UDPSocket *MultiSendSocket = new UDPSocket();
    int socket = MultiSendSocket->CreateUDPClient(nullptr,41235, false);
    MultiSendSocket->BindLocalAddr(ETH0);

    int value = (PACK_LEN + INFO_HEADER)*16;
    setsockopt(socket, SOL_SOCKET, SO_SNDBUF,(char *)&value, sizeof(value));

    s32Ret = HI_MPI_AI_GetChnParam(AiDev, AiChn, &pstChnParam);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%s: Get ai chn param failed:0x%x\n", __FUNCTION__,s32Ret);
        return NULL;
    }

    pstChnParam.u32UsrFrmDepth = 2;

    s32Ret = HI_MPI_AI_SetChnParam(AiDev, AiChn, &pstChnParam);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%s: set ai chn param failed\n", __FUNCTION__);
        return NULL;
    }


    auto thisini = Singleton<SoftwareConfig>::getInstance();

int i = 0;








    //while( aumcast_enable[AiDev] ){
    while(1){


        if( AiDev == 0) {
            i = 0;
            aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
            memset(aumcast_ip[i],0,IPSIZE);
            //                 sprintf(aumcast_ip[i],"%s",thisini->GetConfig(Config::kHdmi0_Au_McastIp).c_str());
            //                aumcast_port[i] = str2int(thisini->GetConfig(Config::kHdmi0_Au_McastPort));
            sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kChn0_M_McastIp).c_str());
            aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_McastPort));

            printf("----------aumcast_ip =%s   aumcast_port =%d\n",aumcast_ip[i],aumcast_port[i]);
        }
        else if(AiDev == 1)
        {
            i = 1;
            aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
            memset(aumcast_ip[i],0,IPSIZE);
            //                sprintf(aumcast_ip[i],"%s",thisini->GetConfig(Config::kHdmi1_Au_McastIp).c_str());
            //                aumcast_port[i] = str2int(thisini->GetConfig(Config::kHdmi1_Au_McastPort));
            sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kChn1_M_McastIp).c_str());
            aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_McastPort));
            thisini->SetConfig(SoftwareConfig::kChn1_M_McastIp,aumcast_ip[i]) ;
            thisini->SetConfig(SoftwareConfig::kChn1_M_McastPort,int2str(aumcast_port[i]));
            printf("----------aumcast_ip =%s   aumcast_port =%d\n",aumcast_ip[i],aumcast_port[i]);
        }






        s32Ret = HI_MPI_AI_GetFrame(AiDev, AiChn, &pstFrm, &pstAecFrm, s32MilliSec);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_AI_GetFrame aiDev:%d -- aiChn:%d err:0x%x\n",AiDev,AiChn, s32Ret);
            sleep(1);
            continue;
            //return s32Ret;
        }

        else {
            printf("gat ai pcm\n");
        }
//        pstFrm.u64TimeStamp &= 0xFFFFFFFFFFFFFF;
//        pstFrm.u64TimeStamp |= (HI_U64)glFrameID << 56;
        //printf("#### len %d, frame %d\n",pstFrm.u32Len,pstFrm.u32Seq);

        if(aframe_number > 255)
            aframe_number = 0;
        aframe_number++;
        packlen = pstFrm.u32Len;


        aframe_data[INFO_HEADER_SN] = aframe_number;
        if(pstFrm.u32Len%PACK_LEN > 0)
            *(HI_U16*)&aframe_data[INFO_HEADER_TP] = pstFrm.u32Len/PACK_LEN + 1;
        else
            *(HI_U16*)&aframe_data[INFO_HEADER_TP] = pstFrm.u32Len/PACK_LEN;

        aframe_data[INFO_HEADER_FD] = pstFrm.u64TimeStamp >> 56;

        *(HI_U16*)&aframe_data[INFO_HEADER_CP] = 0;
        char* bkup_data = (char*)pstFrm.pVirAddr[0];
        uint x=0;
        x++;
        if(x%10 == 0)
            printf("send multicast ip:  %s,port:  %d\n",aumcast_ip[AiDev],aumcast_port[AiDev]);
        while (packlen > 0){
            memcpy(&aframe_data[INFO_HEADER], bkup_data, min(PACK_LEN,packlen));
            bkup_data += min(PACK_LEN,packlen);
            MultiSendSocket->SendTo(
                        aumcast_ip[AiDev],
                    aumcast_port[AiDev],
                    (char*)aframe_data, min(PACK_LEN,packlen) + INFO_HEADER);           
            packlen -= min(PACK_LEN,packlen);
            (*(HI_U16*)&aframe_data[INFO_HEADER_CP]) ++;
        }

        s32Ret = HI_MPI_AI_ReleaseFrame(AiDev, AiChn, &pstFrm, &pstAecFrm);
        if (HI_SUCCESS != s32Ret )
        {
            printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                   __FUNCTION__, AiChn, s32Ret);
            //return NULL;
        }
    }

    return NULL;
}



 #if 1
bool HimppMaster::StartAudioEnc_1( AI_CHN AiChn_, AUDIO_DEV Aidev)
{
    HI_S32 s32Ret,packlen,i = 0;
    AUDIO_FRAME_S pstFrm,pstFrmright;
    AEC_FRAME_S pstAecFrm , pstAecFrm1;
    HI_S32 s32MilliSec = -1;
    byte* g711 = new byte[1024+10];
    HI_U8 *outBytes = new HI_U8[6144];
    HI_U32 numOutBytes;
    svAac[Aidev] = new HI_U8[6144];
    int aframe_number = 0;
    char aframe_data[INFO_HEADER + PACK_LEN];
    memset(aframe_data,0, (INFO_HEADER + PACK_LEN));
    aframe_data[INFO_HEADER_H0] = 'S';
    aframe_data[INFO_HEADER_H1] = 'L';
    aframe_data[INFO_HEADER_VA] = 'A';


   // aframe_data[INFO_HEADER_VE] = Aidev&0xF;
    int aumcast_sv_port[3] = {60123,60124,60125};

//    UDPSocket *MultiSendSocket = new UDPSocket();
//    int socket = MultiSendSocket->CreateUDPClient(nullptr,41235, false); //设为阻塞
//    MultiSendSocket->BindLocalAddr(ETH0);

  //  printf()
   UDPSocket * MultiSendSocket = new UDPSocket();
    int socket = MultiSendSocket->CreateUDPClient(nullptr,aumcast_sv_port[Aidev], false); //设为阻塞
    MultiSendSocket->BindLocalAddr(ETH0);

      setsockopt(socket, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl));
        setsockopt(socket, IPPROTO_IP, IP_TTL, (char *)&ttl, sizeof(ttl));
    //   setsockopt(socket, SOL_SOCKET, SO_SNDBUF,(char *)&value, sizeof(value));






    int send_len = 0;

    int value = (PACK_LEN + INFO_HEADER)*16;
    setsockopt(socket, SOL_SOCKET, SO_SNDBUF,(char *)&value, sizeof(value));


    AI_CHN_PARAM_S pstChnParam;
    HI_S32 j = 0;

    int chnai = 2;
    if(Aidev == 2)
        chnai =1;
     for(j = 0; j < chnai;j++)
   {
        s32Ret = HI_MPI_AI_GetChnParam(Aidev, AiChn_ +j , &pstChnParam);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%s: Get ai chn param failed:0x%x,dev=%d,chn=%d\n", __FUNCTION__,s32Ret,Aidev,AiChn_ +j);
       // return NULL;
    }

    pstChnParam.u32UsrFrmDepth = 2;

    s32Ret = HI_MPI_AI_SetChnParam(Aidev, AiChn_ + j, &pstChnParam);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%s: set ai chn param failed:0x%x aichn=%d\n", __FUNCTION__,s32Ret,AiChn_ +j);
        //return NULL;
    }

    }
    auto thisini = Singleton<SoftwareConfig>::getInstance();
//int i = 0;
   #if 1
    if( Aidev == 0) {
        i = 0;
        aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
        memset(aumcast_ip[i],0,IPSIZE);
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastIp).c_str() );
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastPort));


    }
    else if(Aidev == 1)
    {
        i = 1;
        aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
        memset(aumcast_ip[i],0,IPSIZE);
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastIp).c_str() );
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastPort));


    }
#endif

   // sleep(1);


if(((Aidev == 0)&&(Aechn[0] != 2))  || ( Aidev == 2) || ((Aidev == 1) && (Aechn[1] != 2))) {

    while( audio_running_ai[Aidev] ){


        s32Ret = HI_MPI_AI_GetFrame(Aidev, AiChn_, &pstFrm, &pstAecFrm, s32MilliSec);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_AI0_GetFrame err:0x%x\n", s32Ret);
            return false;
        }

//
        pstFrm.u64TimeStamp &= 0xFFFFFFFFFFFFFF;

         pstFrm.u64TimeStamp |= (HI_U64)glFrameID << 56;

         if( Aidev !=  2)
         {
             s32Ret = HI_MPI_AI_GetFrame(Aidev, AiChn_+1, &pstFrmright, &pstAecFrm1, s32MilliSec);
             if (HI_SUCCESS != s32Ret)
             {
                 printf("HI_MPI_AI1_GetFrame err:0x%x\n", s32Ret);
                 return false;
             }
            // audio_right = *(short*)pstFrmright.pVirAddr[0];
         }
         else if(Aidev == 2)
         {
             pstFrmright = pstFrm;
         }



       //    aumcast_enable[2] = HI_FALSE;

        if((aumcast_enable[Aidev]) && (audio_running_ai[Aidev] )){
            aframe_data[INFO_HEADER_H0] = 'S';
            aframe_data[INFO_HEADER_H1] = 'L';
            aframe_data[INFO_HEADER_VA] = 'A';
            if(aframe_number > 255)
                aframe_number = 0;
            aframe_number++;
            packlen = pstFrm.u32Len;


            aframe_data[INFO_HEADER_SN] = aframe_number;
            if(packlen%PACK_LEN > 0)
                *(HI_U16*)&aframe_data[INFO_HEADER_TP] = (packlen)/PACK_LEN + 1;
            else
                *(HI_U16*)&aframe_data[INFO_HEADER_TP] = (packlen)/PACK_LEN;

            aframe_data[INFO_HEADER_FD] = pstFrm.u64TimeStamp >> 56;

            *(HI_U16*)&aframe_data[INFO_HEADER_CP] = 0;
            char* bkup_data = (char*)pstFrm.pVirAddr[0];
            //            memcpy(bkup_data+2048, (char*)pstFrmright.pVirAddr[0], pstFrm.u32Len);





              if( ((aumcast_enable[Aidev])) && (audio_running_ai[Aidev] == true) ){

                while ((packlen > 0 )&& (audio_running_ai[Aidev]  == true)){


                  send_len = min(PACK_LEN,packlen);
                  memcpy(&aframe_data[INFO_HEADER], bkup_data, min(PACK_LEN,packlen));
                  bkup_data += min(PACK_LEN,packlen);
                  packlen -= send_len;
                  MultiSendSocket->SendTo(aumcast_ip[Aidev],  aumcast_port[Aidev],
                                          (char*)aframe_data,  send_len + INFO_HEADER);
             //   printf("Aidev=%d,packlen=%d aumcast_ip =%s    aumcast_port =%d,sendlen=%d\n",Aidev, packlen,aumcast_ip[Aidev],aumcast_port[Aidev ], send_len + INFO_HEADER);

                  (*(HI_U16*)&aframe_data[INFO_HEADER_CP]) ++;

                  //指向下一包
                }
            }









        }




//音频表
#if 1
        if( ((Aechn[0] == 0) && (Aidev == 0))  &&((coderFormat[0] == AUDIO_AAC) || (coderFormat[0] == AUDIO_G711A)) )
        {
        audio_left[Aidev] = *(short*)pstFrm.pVirAddr[0];
        audio_right[Aidev] = *(short*)pstFrm.pVirAddr[0];
      //  printf("Aidev=%d,left=%x,right=%x\n",Aidev,audio_left[Aidev],audio_right[Aidev]);
        }else if(( (Aechn[1] == 1) &&(Aidev == 1))&&((coderFormat[1] == AUDIO_AAC) || (coderFormat[1] == AUDIO_G711A)) )
        {
            audio_left[Aidev] = *(short*)pstFrm.pVirAddr[0];
            audio_right[Aidev] = *(short*)pstFrm.pVirAddr[0];
         //  printf("Aidev=%d,left=%x,right=%x\n",Aidev,audio_left[Aidev],audio_right[Aidev]);
        }
          if(((Aidev == 2) &&(Aechn[0] == 2) ) && ((coderFormat[0] == AUDIO_AAC) || (coderFormat[0] == AUDIO_G711A)) )
        {
            audio_left[0] = *(short*)pstFrm.pVirAddr[0];
            audio_right[0] = *(short*)pstFrm.pVirAddr[0];
          //   printf("3.5Aidev=0,left=%x,right=%x\n",audio_left[0],audio_right[0]);
        }
          if(((Aidev == 2) &&(Aechn[1] == 2) ) && ((coderFormat[1] == AUDIO_AAC) || (coderFormat[1] == AUDIO_G711A)) )
        {
            audio_left[1] = *(short*)pstFrm.pVirAddr[0];
            audio_right[1] = *(short*)pstFrm.pVirAddr[0];
          //    printf("3.5Aidev=1,left=%x,right=%x\n",audio_left[1],audio_right[1]);
        }
          if(((Aidev == 2) &&(Aechn[2] == 2) ) && ((coderFormat[2] == AUDIO_AAC) || (coderFormat[2] == AUDIO_G711A)) )
        {
            audio_left[2] = *(short*)pstFrm.pVirAddr[0];
            audio_right[2] = *(short*)pstFrm.pVirAddr[0];
        }

  #endif

        HI_U64 u64CurPts;
        HI_MPI_SYS_GetCurPts(&u64CurPts);
        HI_U32 pts1 = u64CurPts/1000;

        if((Aidev == 0) && (Aechn[0] == 0)) {     //输入0启用HDMI0


            if(coderFormat[0] == AUDIO_AAC) { //acc



             if(audio_running_ai[Aidev] == true)
            {
                 memset(outBytes,0,6144);

                s32Ret = g_audio_aac[0]->enc((unsigned char*)pstFrm.pVirAddr[0],
                        (unsigned char*)pstFrmright.pVirAddr[0],
                        pstFrm.u32Len,
                        outBytes,
                        &numOutBytes);
            }


                if(startFlag[0]) {
                    memset(svAac[0],0,6144);
                    memcpy(svAac[0],outBytes,numOutBytes);
                    svpts[0] = pts1;
                    svAacNum[0] = numOutBytes;
                    startFlag[0] = false;
                   // printf("aac-----------------svpts[0]=%d,svAacNum=%d\n",svpts[0],svAacNum[0]);

                }



                for( i =  (2 * Aidev) ;i < ( 2 * (Aidev + 1));i++){

                     // printf("crtsp_putdio1\n");
                    s32Ret = crtsps_pushaudio( g_stream[i],(void*)outBytes,numOutBytes,pts1 );
                  //  printf("crtsp_putdio2\n");

                    if (HI_SUCCESS > s32Ret)
                     {
                         printf(" i am is  StartAudioEnc  crtsps_pushaudio g_stream[%d]=%d   \n",i,g_stream[i]);

                         break;
                         //return s32Ret;
                     }





               }


            }
           else if((coderFormat[0] == AUDIO_G711A)&&(audio_running_ai[Aidev] == true)){


                if(audio_running_ai[Aidev] == true)
               {

                        g711_encode((byte*)pstFrm.pVirAddr[0], pstFrm.u32Len, g711);


                        for( i =  (2 * Aidev) ;i < ( 2 * (Aidev + 1));i++){
                         crtsps_pushaudio( g_stream[i],(void*)g711,pstFrm.u32Len/2, pts1 );
                        }
                }
           }
        }
        else if((Aidev == 1) && (Aechn[1] == 1))
        {
            if(coderFormat[Aidev] == AUDIO_AAC) { //acc


                memset(outBytes,0,6144);

                s32Ret = g_audio_aac[Aidev]->enc((unsigned char*)pstFrm.pVirAddr[0],
                        (unsigned char*)pstFrmright.pVirAddr[0],
                        pstFrm.u32Len,
                        outBytes,
                        &numOutBytes);
//
                if(startFlag[Aidev]) {
                    memset(svAac[Aidev],0,6144);
                    memcpy(svAac[Aidev],outBytes,numOutBytes);
                    svpts[Aidev] = pts1;
                    svAacNum[Aidev] = numOutBytes;
                    startFlag[Aidev] = false;
                    printf("aac-----------------svpts[1]=%d,svAacNum=%d\n",svpts[Aidev],svAacNum[Aidev]);

                }

                for( i =  (2 * Aidev) ;i < ( 2 * (Aidev + 1));i++){

                    s32Ret = crtsps_pushaudio( g_stream[i],(void*)outBytes,numOutBytes,pts1 );
                     if (HI_SUCCESS > s32Ret)
                     {
                         printf(" i am is  StartAudioEnc  crtsps_pushaudio g_stream[%d]=%d   \n",i,g_stream[i]);
                         //return s32Ret;
                     }





               }


            }
           else if(coderFormat[Aidev] == AUDIO_G711A){

                  //  printf("i am is dev 1 g711a g_stream =%d\n",g_stream[Aidev]);

                        g711_encode((byte*)pstFrm.pVirAddr[0], pstFrm.u32Len, g711);
                          for( i =  (2 * Aidev) ;i < ( 2 * (Aidev + 1));i++){
                         crtsps_pushaudio( g_stream[i],(void*)g711,pstFrm.u32Len/2, pts1 );
                     }
           }

        }
        else if(Aidev == 2)
        {

                memset(outBytes,0,6144);







            if((Aechn[0] == 2) &&(audio_running_ai[Aidev] == true) && (coderFormat[0] == AUDIO_AAC))
            {

                //  printf("i am is ai aac chn0\n");

                 memset(outBytes,0,6144);
                s32Ret = g_audio_aac[0]->enc((unsigned char*)pstFrm.pVirAddr[0],
                        (unsigned char*)pstFrm.pVirAddr[0],
                        pstFrm.u32Len,
                        outBytes,
                        &numOutBytes);




                for( i =  (2 * 0) ;i < ( 2 * (0 + 1));i++){

                    // printf("crtsps_pushaudio  000  ==== %d\n",i);
                    s32Ret = crtsps_pushaudio( g_stream[i],(void*)outBytes,numOutBytes,pts1 );





               }



              }

            if((Aechn[1] == 2)&&(audio_running_ai[Aidev] == true) && (coderFormat[1] == AUDIO_AAC))
            {

                s32Ret = g_audio_aac[1]->enc((unsigned char*)pstFrm.pVirAddr[0],
                        (unsigned char*)pstFrm.pVirAddr[0],
                        pstFrm.u32Len,
                        outBytes,
                        &numOutBytes);



                for( i =  (2 * 1) ;i < ( 2 * (1 + 1));i++){


                    s32Ret = crtsps_pushaudio( g_stream[i],(void*)outBytes,numOutBytes,pts1 );



               }


            }
            if((Aechn[2] == 2)&& (audio_running_ai[Aidev] == true)&&(coderFormat[2] == AUDIO_AAC) )
            {

               memset(outBytes,0,6144);

                s32Ret = g_audio_aac[2]->enc((unsigned char*)pstFrm.pVirAddr[0],
                        (unsigned char*)pstFrm.pVirAddr[0],
                        pstFrm.u32Len,
                        outBytes,
                        &numOutBytes);

                 // printf("crtsps_pushaudio aac 222  ==== %d,g_stream=%d\n",i,g_stream[i]);

                for( i =  (2 * 2) ;i < ( 2 * (2 + 1));i++){


                    s32Ret = crtsps_pushaudio( g_stream[i],(void*)outBytes,numOutBytes,pts1 );





               }

            }



              if((coderFormat[0] == AUDIO_G711A)|| (coderFormat[1] == AUDIO_G711A) || (coderFormat[2] == AUDIO_G711A)){


                g711_encode((byte*)pstFrm.pVirAddr[0], pstFrmright.u32Len, g711);


                if((Aechn[0] == 2) &&(coderFormat[0] == AUDIO_G711A)&&(audio_running_ai[Aidev] == true))
                {

                    //printf(" 3,5mmmmm g711\n");
                      for( i =  (2 * 0) ;i < ( 2 * (0 + 1));i++){
                     crtsps_pushaudio( g_stream[i],(void*)g711,pstFrm.u32Len/2, pts1 );
                    }

                }
                else if((Aechn[1] == 2)&&(coderFormat[1] == AUDIO_G711A)&&(audio_running_ai[Aidev] == true))
                {

                      for( i =  (2 * 1) ;i < ( 2 * (1 + 1));i++){
                     crtsps_pushaudio( g_stream[i],(void*)g711,pstFrm.u32Len/2, pts1 );
                    }

                }
                else if((Aechn[2] == 2)&&(coderFormat[2] == AUDIO_G711A)&&(audio_running_ai[Aidev] == true) && (edid_4k == 0))
                {
                    // printf("22ia sm id g711a\n");

                      for( i =  (2 * 2) ;i < ( 2 * (2 + 1));i++){
                     crtsps_pushaudio( g_stream[i],(void*)g711,pstFrm.u32Len/2, pts1 );
                    }

                }

            }



        }
        else
        {
            usleep(20000);
        }









        s32Ret = HI_MPI_AI_ReleaseFrame(Aidev, AiChn_, &pstFrm, &pstAecFrm);
        if (HI_SUCCESS != s32Ret )
        {
            printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                   __FUNCTION__, AiChn_, s32Ret);
            return NULL;
        }

       // printf("realseframe\n");
        if(Aidev != 2)
        {
          //  printf("relasefram--ai\n");
            s32Ret = HI_MPI_AI_ReleaseFrame(Aidev, AiChn_+1, &pstFrmright, &pstAecFrm1);
            if (HI_SUCCESS != s32Ret )
            {
                printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                       __FUNCTION__, AiChn_, s32Ret);
                return NULL;
            }
        }



      //  sem_post(&sem6);
    }
}
   // if(coderFormat[Aidev] == AUDIO_G711A)

delete g711;

   delete outBytes;
    delete MultiSendSocket;
printf("--------------------11\n");
   if((Aidev != 2) && (coderFormat[Aidev] != AUDIO_G711A))
    delete svAac[Aidev];

printf("thread ai=%d exit\n",Aidev);
    return NULL;
}
#endif



bool HimppMaster::ModeuleAiBind(AUDIO_DEV AiDev)
{
    /*
     * Ai(0) >--Aenc(0)
    */
    HI_S32   s32Ret;
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(AiDev, 0, AiDev);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }

    return true;
}


bool HimppMaster::AiBindAenc(AENC_CHN aeChn,AUDIO_DEV aiDev,AI_CHN aiChn)
{

//    this->AiBindAenc(0,0,0);
//    this->AiBindAenc(1,1,0);


    HI_S32   s32Ret;
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(aiDev, aiChn, aeChn);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return HI_FAILURE;
    }
    return HI_SUCCESS;
}




bool HimppMaster::AudioModuleDestroy()
{

HI_S32 s32Ret;
    HI_U32  i = 0, j = 0;
    for(i = 0;i<3;i++){
        for(j = 0 ; j < 2 ; j ++){
        s32Ret = HI35xx_COMM_AUDIO_StopAi(i, j, HI_FALSE, HI_FALSE);
        if (s32Ret != HI_SUCCESS)
        {
            COMMON_PRT("SAMPLE_COMM_AUDIO_StopAi failed with %x   chn=%d", s32Ret,j);
            return HI_FAILURE;
        }
        }

        s32Ret = HI_MPI_AI_Disable(i);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
            return s32Ret;
        }

    }
    COMMON_PRT("audio modle destroy ok\n");

    return true;
}
