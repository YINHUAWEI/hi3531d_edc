#ifndef HI35xx_COMM_FUN_H
#define HI35xx_COMM_FUN_H

#include "hi3531d_common/sample_comm.h"
#include "hi3531d_common/sample_comm_ivs.h"
#include <hifb.h>

#include "global.h"
#include "mytimer.h"
#include "audio_g711.h"
#include "../common/version.h"

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* Begin of #ifdef __cplusplus */

#define CUT_NUMBER   1
#define SAMPLE_VI_MODE      SAMPLE_VI_MODE_8_1080P
#define DMA_MIN_ROW     1080
 #define POOL_MAX   12
#define MIN(x,y)  x>=y?y:x
#define CHECK_POINTER_NO_RET(p) \
do{                      \
    if(HI_NULL == p){\
        printf("The pointer is null\n");       \
        return; \
    } \
}while(0)

#define CHECK_RET_SUCCESS_NO_RET(val) \
do{                      \
    if(HI_SUCCESS != val){\
        printf("return value is not HI_SUCCESS, 0x%x!\n", val);       \
        return ; \
    } \
}while(0)


extern HI_BOOL vo_size_vi;
extern HI_S32 SnapQuality[4];
extern PARAMETER viParam[4];
extern enum audio_type coderFormat[4];

typedef enum {
    kForceDVI = 0,
    kForceHDMI,
    kAUTO,
}OUTPUT_MODEL;




typedef struct hiHDMI_ARGS_S
{
    HI_HDMI_ID_E        enHdmi;
    HI_HDMI_VIDEO_FMT_E eForceFmt;
    AIO_ATTR_S          *pstAioAttr;
}HDMI_ARGS_S;


//typedef struct hiHDMI_ARGS_S
//{
//    HI_HDMI_ID_E        enHdmi;
//    HI_HDMI_VIDEO_FMT_E eForceFmt;
//    AIO_ATTR_S          stAioAttr;
//    OUTPUT_MODEL        enOutputModel;
//}HDMI_ARGS_S;







/*******************************************************
    function announce
*******************************************************/
HI_U32 HI35xx_COMM_SYS_CalcPicVbBlkSize(SIZE_S *stSize, PIXEL_FORMAT_E enPixFmt, HI_U32 u32AlignWidth,COMPRESS_MODE_E enCompFmt);
HI_S32 HI35xx_COMM_VI_Start_1(HI_BOOL vi_loop,HI_U32 did_4k,VI_DEV ViDev, VI_CHN ViChn, VIDEO_NORM_E enNorm, SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect,SIZE_S * pstTarSize,  HI_U32 SrcFrmRate,HI_U32 DrcFrmRate);
HI_S32 hi35_SAMPLE_COMM_VENC_Start_1(VENC_CHN venc_chn ,PARAMETER vencParam,HI_U32 slave,SIZE_S size_s0_s1);

HI_S32 HI35xx_COMM_VI_Start(VI_DEV ViDev, VI_CHN ViChn, SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type);
HI_S32 HI35xx_COMM_VI_Stop(VI_DEV ViDev, VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_BindVpss(VI_DEV ViDev, VPSS_GRP VpssGrp,VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_StartDev(VI_DEV ViDev, SAMPLE_VI_MODE_E enViMode);
HI_S32 HI35xx_COMM_VI_SetMask(VI_DEV ViDev, VI_DEV_ATTR_S *pstDevAttr);
HI_S32 HI35xx_COMM_VI_StartChn(VI_CHN ViChn, RECT_S *pstCapRect, SIZE_S *pstTarSize, HI_S32 input_type, SAMPLE_VI_CHN_SET_E enViChnSet);
HI_S32 HI35xx_COMM_VI_BindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_UnBindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_BindVo(VI_DEV ViDev, VI_CHN ViChn, VO_LAYER VoLayer, VO_CHN VoChn);

HI_S32 HI35xx_COMM_VENC_Start(VENC_CHN VencChn, PAYLOAD_TYPE_E enType, SIZE_S stMaxPicSize, SIZE_S stPicSize, SAMPLE_RC_E enRcMode, HI_U32  u32Profile,  HI_U32 u32Gop, HI_U32 u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 u32DstFrameRate, HI_U32 u32MinQp, HI_U32 u32MaxQp, HI_U32 u32MinIQp, HI_U32 u32IQp, HI_U32 u32PQp, HI_U32 u32BQp, HI_U32 u32MinIProp, HI_U32 u32MaxIProp, HI_BOOL setFlag);
HI_S32 SAMPLE_COMM_VENC_SaveStream(PAYLOAD_TYPE_E enType, FILE* pFd, VENC_STREAM_S* pstStream);
HI_S32 HI35xx_COMM_VENC_SnapStart(VENC_CHN VencChn, SIZE_S *pstSize,
                                  HI_BOOL bBindVpss = HI_FALSE, VPSS_GRP VpssGrp = 0, VPSS_CHN VpssChn = 0);
HI_S32 HI35xx_COMM_VENC_SnapStop(VENC_CHN VencChn,
                                 HI_BOOL bBindVpss = HI_FALSE, VPSS_GRP VpssGrp = 0, VPSS_CHN VpssChn = 0);
HI_S32 RSAMPLE_COMM_VENC_BindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn);
HI_S32 RSAMPLE_COMM_VENC_UnBindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn);


HI_S32 HI35xx_COMM_VO_StartChn(VO_LAYER VoLayer, VO_CHN VoChn);
HI_S32 HI35xx_COMM_VO_StopChn(VO_DEV VoDev, HI_U32 u32VoChn);
HI_S32 HI35xx_COMM_VO_BindVpss(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn);
HI_S32 HI35xx_COMM_VO_GetWH(VO_INTF_SYNC_E *enIntfSync, SIZE_S stVoSize, HI_U32 pu32Frm);
HI_S32 HI35xx_COMM_VO_StartDevLayer(HI_U32 edid_4k, VO_DEV VoDev, VO_INTF_SYNC_E intfSync, HI_U32 dispWidth, HI_U32 dispHeight, HI_U32 frameRt, HI_U32 bgColor );
HI_S32 HI35xx_COMM_VPSS_Startvo(HI_S32 s32Grpstart,HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr,HI_U32 edid4k);
HI_S32 HI35xx_COMM_VO_StartGraphicLayer(POINT_S stRes, POINT_S stResVirtual, POINT_S stOffset);
HI_S32 HI35xx_COMM_VO_PipStart(VO_DEV VoDev, HI_U32 u32Width, HI_U32 u32Height, HI_U32 u32DispFrmRt);
HI_VOID HI35xx_SMAPLE_VO_SIZE(SIZE_S Visize,VO_INTF_SYNC_E * VOintf);


HI_S32 HI35xx_COMM_VPSS_Start(HI_S32 s32Grpstart,HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr,HI_U32 edid4k);
HI_S32 HI35xx_COMM_SYS_MemConfig(HI_S32 s32VdChnCnt, HI_S32 s32VpssGrpCnt, HI_S32 s32VeChnCnt, VO_DEV VoDevId);
HI_S32 HI35xx_COMM_VPSS_Start_1(HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr);

HI_S32 RSAMPLE_COMM_VO_StartChn(VO_LAYER VoLayer, SAMPLE_VO_MODE_E enMode);

HI_S32 HI35xx_COMM_AUDIO_StartAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn, AIO_ATTR_S* pstAioAttr,
                                 AUDIO_SAMPLE_RATE_E enOutSampleRate, HI_BOOL bResampleEn, HI_VOID* pstAiVqeAttr, HI_U32 u32AiVqeType);
HI_S32 HI35xx_COMM_AUDIO_StopAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn, HI_BOOL bResampleEn, HI_BOOL bVqeEn);
HI_S32 HI35xx_COMM_VI_UnBindVpss(VI_DEV ViDev, VENC_CHN VpssGrp,VI_CHN ViChn);
char *getFileAll(char *pBuf, char *fname,int *length);
HI_BOOL HI35xx_IVE_MAP(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *stSrcFrame, POINT_S stDstPoint, RECT_S stCropRect, HI_BOOL bTimeOut, HI_BOOL uv_IVE);

HI_S32 Hi_SetReg(HI_U32 u32Addr, HI_U32 u32Value);
HI_S32 Hi_GetReg(HI_U32 u32Addr, HI_U32 *pu32Value);
int tim_subtract(struct timeval *result, struct timeval *begin, struct timeval *end );
HI_S32 HI35xx_COMM_VENC_SnapStart_1(VENC_CHN VencChn, SIZE_S pstSize,SIZE_S stMaxsize);
HI_S32 HI35xx_COMM_bind_vi_venc(VI_DEV ViDev,VI_CHN Vichn,VENC_CHN Vencchn);
HI_S32 HI35xx_VB_PoolInit(SIZE_S stPoolSize, HI_U32 u32BlkCnt);
HI_S32 RSAMPLE_COMM_VO_HdmiStart(VO_INTF_SYNC_E enIntfSync);





#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

#endif // HI35xx_COMM_FUN_H

