#融合4HDMI  4MIX输入，3.5mm音频测试完成，4MIX-HDMI音频未测试
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

##指定目标文件(obj)的存放目录
OBJECTS_DIR += ../obj
TARGET = hi3531d_edc_venc

BASE_PATH = /home/pubilc-tool-lib/Hisilicon/lib/lib_hisiv500
 HISI_SDK_VER = /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0
#HISI_SDK_VER_ko  = /home/hwyin/work/hi3531d_4in_venc/Hi3531DV100_SDK_V1.0.5.0/package/mpp
#HISI_SDK_VER = /home/pubilc-tool-lib/Hisilicon/sdk/Hi3531DV100_SDK_V1.0.4.0/mpp


CROSS_COMPILE = arm-hisiv500



#头文件包含路径

#/home/hwyin/work/hi3531d_4in_venc/beifen/hi3531d_4in_venc-master/hi3531d_venc/hi3531d_venc
#INCLUDEPATH += /home/hwyin/work/hi3531d_4in_venc/beifen/hi3531d_4in_venc-master/hi3531d_venc/common \
#               /home/hwyin/work/hi3531d_4in_venc/beifen/hi3531d_4in_venc-master/hi3531d_venc/hi3531d_venc/hi3531d_common

INCLUDEPATH  += /home/hwyin/yin_mk/new/hi3531d_venc_NSCP/common \
                /home/hwyin/yin_mk/new/hi3531d_venc_NSCP/hi3531d_venc/hi3531d_common




INCLUDEPATH += $${BASE_PATH}/audiodec/include \
               $${HISI_SDK_VER}/include \
               $${HISI_SDK_VER}/extdrv/nvp6134_ex \
               $${HISI_SDK_VER}/extdrv/tlv320aic31 \
               $${HISI_SDK_VER}/extdrv/tp2853c \
               $${HISI_SDK_VER}/extdrv/tp2823 \
               $${HISI_SDK_VER}/extdrv/tp2827 \
               $${BASE_PATH}/boost-1.59.0/include \
               $${BASE_PATH}/ffmpeg-2.8/include \
#               /home/hwyin/work/hi3531d_4in_venc/hi3531d-processor-low_latency_feat/processor/processor_III/rtspc/include \
               $${BASE_PATH}/pte \
               $${BASE_PATH}/freetype-2.4.4/include \
               $${BASE_PATH}/freetype-2.4.4/include/freetype2

LIBS += $${HISI_SDK_VER}/lib/libmpi.a \
        $${HISI_SDK_VER}/lib/libhdmi.a \
        $${HISI_SDK_VER}/lib/libive.a \
        $${HISI_SDK_VER}/lib/libjpeg6b.a \
        $${HISI_SDK_VER}/lib/libjpeg.a \
        $${HISI_SDK_VER}/lib/libpciv.a \
        $${HISI_SDK_VER}/lib/libtde.a \
#       $${HISI_SDK_VER}/lib/libaacenc.a \
        $${HISI_SDK_VER}/lib/libaacdec.a \
        $${HISI_SDK_VER}/lib/libVoiceEngine.a \
        $${HISI_SDK_VER}/lib/libupvqe.a \
        $${HISI_SDK_VER}/lib/libdnvqe.a \
        $${BASE_PATH}/ffmpeg-2.8/$${CROSS_COMPILE}/lib/libavformat.a\
        $${BASE_PATH}/ffmpeg-2.8/$${CROSS_COMPILE}/lib/libavcodec.a\
        $${BASE_PATH}/ffmpeg-2.8/$${CROSS_COMPILE}/lib/libavutil.a \
        $${BASE_PATH}/freetype-2.4.4/$${CROSS_COMPILE}/lib/libfreetype.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv300-aacenc/libaacenc.a


LIBS += ./libcrtsps.so
LIBS += -lpthread -lm -ldl -lrt \
        -lstdc++

#不检测定义后未使用的参数错误等
QMAKE_CXXFLAGS += -Wno-unused-parameter \
                  -Wno-unused-but-set-variable \
                  -Wno-unused-but-set-parameter \
                  -Wno-narrowing \
                  -Wno-literal-suffix \
                  -std=c++11
QMAKE_CFLAGS += -Wno-unused-parameter \
                -Wno-unused-but-set-variable \
                -Wno-unused-but-set-parameter
#定义编译选项
DEFINES += __LINUX__
DEFINES += __DEBUG__

SOURCES += \
    himpp_master.cpp \
    main.cpp \
    ../common/common.cpp \
    software_config.cpp \
    udpsocket/udpsocket.cpp \
    hi35xx_comm_fun.cpp \
    http_server.cpp \
    mongoose/mongoose.c \
    cJSON.c \
    hi3531d_common/loadbmp.c \
    hi3531d_common/sample_comm_ivs.c \
    hi3531d_common/sample_comm_sys.c \
    hi3531d_common/sample_comm_vda.c \
    hi3531d_common/sample_comm_vdec.c \
    hi3531d_common/sample_comm_venc.c \
    hi3531d_common/sample_comm_vi.c \
    hi3531d_common/sample_comm_vo.c \
    hi3531d_common/sample_comm_vpss.c \
    audio_g711.cpp \
    hi3531d_common/sample_comm_audio.c \
    commandmap.cpp \
    ../common/uuid/gen_uuid.c

HEADERS += \
    himpp_master.h \
    ../common/singleton.h \
    ../common/readini.h \
    ../common/common.h \
    software_config.h \
    global.h \
    mytimer.h \
    udpsocket/networkinfo.h \
    udpsocket/udpsocket.h \
    tiny_rtsp_server_i.h \
    hi35xx_comm_fun.h \
    mongoose/mongoose.h \
    cJSON.h \
    http_handler.h \
    http_server.h \
    hi3531d_common/loadbmp.h \
    hi3531d_common/sample_comm.h \
    hi3531d_common/sample_comm_ivs.h \
    ../common/version.h \
    audio_g711.h \
    librtsps_i.h \
    aac/aac_encode.h \
    aac/aacenc.h \
    commandmap.h \
    /home/9_my_code_tool/public/lib/hisilicon/easylogger/inc/elog.h \
    ../common/version.h \
    ../common/uuid/uuid.h \
    ../common/uuid/uuidd.h \
    ../common/uuid/uuidP.h \
    ../common/uuid/uuid_types.h \
    cnFreeType.h \
    bmptoyuv.h \
    def_gpio_reg.h
