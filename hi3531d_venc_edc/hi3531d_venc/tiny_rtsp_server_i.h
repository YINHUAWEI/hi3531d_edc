#ifndef _tiny_rtsp_server_i__
#define _tiny_rtsp_server_i__

enum video_type
{
	VIDEO_NULL = 0,
	VIDEO_H264,
};

enum audio_type
{
	AUDIO_NULL = 0,
	AUDIO_G711, 
};

class tiny_rtsp_server_i
{
public:
	virtual int start_rtsp_server( short port, const char* user_name, const char* user_pwd ) = 0;
	virtual int stop_rtsp_server ( void ) = 0;
	virtual int open_live_source ( video_type vt, audio_type at, int audio_chnnel, int audio_sample, const char* rui_path ) = 0;
	virtual int close_live_source( int id ) = 0;
	virtual int push_live_video_data( int id, void* data, int len, unsigned int ts_ms ) = 0;
	virtual int push_live_audio_data ( int id, void* data, int len, unsigned int ts_ms ) = 0;
};

tiny_rtsp_server_i* new_server( void );
void free_server( tiny_rtsp_server_i* p );

#endif
