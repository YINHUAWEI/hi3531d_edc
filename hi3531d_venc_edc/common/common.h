#ifndef COMMON_H
#define COMMON_H

#define SZ_IPADDR       (24)
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>        /* ftok() etc. */
#include <sys/msg.h>        /* msgget() etc. */
#include <pthread.h>
#include <errno.h>
#include <sstream>
#include <vector>



#include <fstream>

#include <vector>


#include <singleton.h>  // 线程安全的单例类实现

#include <version.h>    // 写回程序版本号
#include <mytimer.h>    // Liuxn下使用gettimeofday函数的微秒级精度计时函数




#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* Begin of #ifdef __cplusplus */

#define MESSAGE_BUF_MAX (5*1024*1024)
#define BUFF_SIZE 1024
#define COMMON_PRT(fmt...)   \
    do {\
    printf("[%s]-%d: ", __FUNCTION__, __LINE__);\
    printf(fmt);\
}while(0)

struct msg_ex{
    uint port;
    char ip[SZ_IPADDR];
    char msg[BUFF_SIZE+1];
};

struct msg_st{
    int           mtype;
    struct msg_ex mtext;
};

extern int c_netmsgid;
extern int c_osdmsgid;
extern int c_netmsgtype;
extern int c_osdmsgtype;
extern const char *c_netmsgpath;
extern const char *c_osdmsgpath;
extern const char *c_mainmsgpath;
extern int c_mainmsgid;
extern int c_mainmsgtype;


/*
 * 功能： 创建进程间的消息队列
 * 输入： int, const char*
 * 返回： bool
 * 日期： 2018.07.07
 * 作者： zh.sun
 */
int CreateMessageQueue(const char *msgpath);

/*
 * 功能： 销毁进程间的消息队列
 * 输入： int
 * 返回： bool
 * 日期： 2018.07.07
 * 作者： zh.sun
 */
bool DeleteMessageQueue(int msgid);
bool SetMessageQueue(int msgid);

std::string ReadSthFile(const char *filepath);


// 按照指定字符分离string字符串
void SplitString(const std::string& s, std::vector<std::string>& v, const std::string& c);

//template<typename T>
pthread_t CreateThread(void* (*pThreadFunc)(void*), uint priority, int schedpolicy, bool b_detached, void *threadin);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

int str2int(const std::string &string_temp);

template<typename T>
std::string int2str(const T &dst_temp)
{
    std::stringstream stream;
    stream << dst_temp;
    return stream.str();
}

#endif // COMMON_H
