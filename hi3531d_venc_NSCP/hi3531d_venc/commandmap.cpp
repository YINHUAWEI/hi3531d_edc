#define LOG_TAG    "COMMANDMAP"

#include <fstream>

#include "commandmap.h"
#include "common.h"
#include "def_gpio_reg.h"
#include "hi35xx_comm_fun.h"
//#include "/home/9_my_code_tool/public/lib/hisilicon/easylogger/inc/elog.h"

char gClientIP[20] = "";
int  gClientPort = 0;
char mcast_ip[8][IPSIZE];
int mcast_port[8];
int repeat_time[8];
video_type v_type[8];
unsigned int u32DstFrameRate[8];
char msg[5] = "ok";
 //RECT_S CapRect[4];
char aumcast_ip[3][IPSIZE];
int aumcast_port[4];
int diff_index = 0;
SNAPPARAM veSnap[4];


/*
 * 功能： 注册各种指令（关键字），对应的处理函数
 * 输入： 无
 * 返回： 无
 * 日期： 2018.1.25
 * 作者： zh.sun
 */
CommandMap::CommandMap()
{
    //创建回复socket

    echoudp_ = new UDPSocket();

    echoudp_->CreateUDPClient(nullptr, 0); //设为非阻塞


    //创建一级关键字指令处理队列
    pfuncmdmap_.insert(make_pair("Type",            &CommandMap::TypeHandler));
    pfuncmdmap_.insert(make_pair("Function",        &CommandMap::FuncHandler));
    pfuncmdmap_.insert(make_pair("Value",           &CommandMap::ValueHandler));

    //创建媒体配置相关指令处理队列
    mediafuncmdmap_.insert(make_pair("setNet",           &CommandMap::SetNetHandler));
    mediafuncmdmap_.insert(make_pair("setVenc",          &CommandMap::SetVencHandler));
    mediafuncmdmap_.insert(make_pair("setAudio",         &CommandMap::SetAudioHandler));
    mediafuncmdmap_.insert(make_pair("setMulticast",     &CommandMap::SetMulticastHandler));
    mediafuncmdmap_.insert(make_pair("setSnap",          &CommandMap::SetSnapHandler));
    mediafuncmdmap_.insert(make_pair("setSnapMulticast", &CommandMap::SetSnapMulticastHandler));
    mediafuncmdmap_.insert(make_pair("setBasicConfig",   &CommandMap::SetBasicConfigHandler));
    mediafuncmdmap_.insert(make_pair("setViCrop",        &CommandMap::SetViCropHandler));
    mediafuncmdmap_.insert(make_pair("setOsd",           &CommandMap::SetOsdHandler));
    mediafuncmdmap_.insert(make_pair("getSystemMsg",     &CommandMap::GetMsgHandler));
    mediafuncmdmap_.insert(make_pair("getLog",           &CommandMap::GetLogHandler));
    mediafuncmdmap_.insert(make_pair("deleteConfig",     &CommandMap::DeleteConfigHandler));
    mediafuncmdmap_.insert(make_pair("setAuMulticast",   &CommandMap::SetAuMulticastHandler));
    mediafuncmdmap_.insert(make_pair("removeFile",       &CommandMap::RemoveFileHandler));
    mediafuncmdmap_.insert(make_pair("setTickMaster",    &CommandMap::SetTickMasterHandler));
    mediafuncmdmap_.insert(make_pair("setHardwareName",  &CommandMap::SetHardwareNameHandler));
    mediafuncmdmap_.insert(make_pair("setGpioReg",       &CommandMap::SetSysGPIOHandler));
    mediafuncmdmap_.insert(make_pair("getShellMsg",      &CommandMap::GetShellHandler));
    mediafuncmdmap_.insert(make_pair("setVo",            &CommandMap::SetVoHandler));
}

CommandMap::~CommandMap()
{
    if(echoudp_ != NULL)
        delete echoudp_;
}

/*
 * 功能： 执行指令对应的处理函数
 * 输入： string key： 指令关键词
 *       cJSON *value： json字符串
 * 返回： bool
 * 日期： 2018.1.25
 * 作者： zh.sun
 */
bool CommandMap::ProcessCmd(string key, cJSON *value)
{
    auto iter = pfuncmdmap_.find(key);
    if(iter != pfuncmdmap_.end()){
        return (this->*(iter->second))(value); //调用对应的函数指针
    }
    else
        return false;
}

bool CommandMap::TypeHandler(cJSON *value)
{
    if( !strcmp(value->string, "Type") ){
        if (cJSON_IsString(value) && (value->valuestring != NULL))
            cmdtype_ = value->valuestring;
        else
            cmdtype_ = "null";
    }

    return true;
}




bool CommandMap::FuncHandler(cJSON *value)
{
    if( !strcmp(value->string, "Function") ){
        if (cJSON_IsString(value) && (value->valuestring != NULL))
            cmdfunc_ = value->valuestring;
        else
            cmdfunc_ = "null";
    }
    COMMON_PRT("FuncHandler %s\n",value->valuestring);
    return true;
}

bool CommandMap::ValueHandler(cJSON *value)
{
    auto iter = mediafuncmdmap_.find(cmdfunc_);
    if(iter != mediafuncmdmap_.end()){
        COMMON_PRT("%s: %s", cmdfunc_.c_str(), cJSON_Print(value));
        return (this->*(iter->second))(value); //调用对应的函数指针
    }
    else
        return false;

    return true;
}

bool CommandMap::SetNetHandler(cJSON *value)
{

    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    cJSON *net;
    cJSON_ArrayForEach(net, value){
        cJSON *name = cJSON_GetObjectItemCaseSensitive(net, "name");
        if (name && cJSON_IsString(name)) {
            if(!strcmp(name->valuestring, "eth0")){
                cJSON *ip = cJSON_GetObjectItemCaseSensitive(net, "ip");
                if(ip && cJSON_IsString(ip) ){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Ip, ip->valuestring) ){
                        COMMON_PRT("set net->ip set failed");
                    }
                }

                cJSON *mask = cJSON_GetObjectItemCaseSensitive(net, "mask");
                if(mask && cJSON_IsString(mask)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Mask, mask->valuestring) ){
                        COMMON_PRT("set net->mask set failed");
                    }
                }

                cJSON *gateway = cJSON_GetObjectItemCaseSensitive(net, "gateway");
                if(gateway && cJSON_IsString(gateway)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Gateway, gateway->valuestring) ){
                        COMMON_PRT("set net->gateway set failed");
                    }
                }

                cJSON *dns = cJSON_GetObjectItemCaseSensitive(net, "dns");
                if(dns && cJSON_IsString(dns)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Dns, dns->valuestring) ){
                        COMMON_PRT("set net->dns set failed");
                    }
                }

                cJSON *mac = cJSON_GetObjectItemCaseSensitive(net, "mac");
                if(mac && cJSON_IsString(mac)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Mac, mac->valuestring) ){
                        COMMON_PRT("set net->mac set failed");
                    }
                }

                char cmdline[126] = "";
               // sprintf(cmdline, "ethtool -s eth0 autoneg off speed %d\n", speed->valueint);

//                sprintf(cmdline, "ifconfig eth0  %s netmask 255.255.0.0\n",ip->valuestring );
//                system(cmdline);


//                cJSON *speed = cJSON_GetObjectItemCaseSensitive(net, "speed");
//                if(speed && cJSON_IsNumber(speed)){
//                    char cmdline[126] = "";
//                    sprintf(cmdline, "ethtool -s eth0 autoneg off speed %d\n", speed->valueint);
//                    system(cmdline);
//                }
            }
            /*else if(!strcmp(name->valuestring, "eth1")){
                cJSON *ip = cJSON_GetObjectItemCaseSensitive(net, "ip");
                if(ip && cJSON_IsString(ip)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth1Ip, ip->valuestring) ){
                        COMMON_PRT("set net->ip set failed");
                    }
                }

                cJSON *mask = cJSON_GetObjectItemCaseSensitive(net, "mask");
                if(mask && cJSON_IsString(mask)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth1Mask, mask->valuestring) ){
                        COMMON_PRT("set net->mask set failed");
                    }
                }

                cJSON *gateway = cJSON_GetObjectItemCaseSensitive(net, "gateway");
                if(gateway && cJSON_IsString(gateway)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth1Gateway, gateway->valuestring) ){
                        COMMON_PRT("set net->gateway set failed");
                    }
                }

                cJSON *dns = cJSON_GetObjectItemCaseSensitive(net, "dns");
                if(dns && cJSON_IsString(dns)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth1Dns, dns->valuestring) ){
                        COMMON_PRT("set net->dns set failed");
                    }
                }

                cJSON *mac = cJSON_GetObjectItemCaseSensitive(net, "mac");
                if(mac && cJSON_IsString(mac)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth1Mac, mac->valuestring) ){
                        COMMON_PRT("set net->mac set failed");
                    }
                }
            }*/
            else{
                COMMON_PRT("set net->name Invalid format");
                continue;
            }
        }
        else{
            COMMON_PRT("set net-> name Invalid format");
            continue;
        }
    }

//#ifdef __DEBUG__ // 在*.pro中定义,即 Makefile中指定
//    softwareconfig->PrintConfig();
//#endif // __DEBUG__

    softwareconfig->SaveNetConfig();
    system("cp /data/net.ini /data/net_old.ini\n");

   // softwareconfig->SaveNetConfig();
    this->LetMeTellYou(msg);


  //  this->ACK(CODE_SUCCEED, cmdfunc_.c_str());

    return true;
}

void* input1proc_(void *arg)
{
   // uint in = *(uint *)arg;
     HI_S32 in = *(HI_S32*)arg;
    printf(" input number:%d\n",in);
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    //himppmaster->StartLoop(VencStreamCnt*in, VencStreamCnt*(in+1), in);    //1-VENC
     himppmaster->StartLoop(2*in,2*(in+1),in);
    return NULL;
}


void* input_ai(void *arg)
{
   // uint in = *(uint *)arg;
     HI_S32 in = *(HI_S32*)arg;
    printf(" input number:%d\n",in);
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    //himppmaster->StartLoop(VencStreamCnt*in, VencStreamCnt*(in+1), in);    //1-VENC
     himppmaster->StartAudioEnc_1(0,in);
    return NULL;
}

/*
 * 统一UMP版本适用
 * */
bool swAudioFormat(int port)
{
    switch (port) {
        case 0:
            if(coderFormat[0] == AUDIO_NULL)
                coderFormat[0] = AUDIO_AAC;
            else if(coderFormat[0] == AUDIO_AAC)
                coderFormat[0] = AUDIO_G711A;
            else
                coderFormat[0] = AUDIO_NULL;
            break;
        case 1:
            if(coderFormat[1] == AUDIO_NULL)
                coderFormat[1] = AUDIO_AAC;
            else if(coderFormat[1] == AUDIO_AAC)
                coderFormat[1] = AUDIO_G711A;
            else
                coderFormat[1] = AUDIO_NULL;
            break;
        case 2:
            if(coderFormat[2] == AUDIO_NULL)
                coderFormat[2] = AUDIO_AAC;
            else if(coderFormat[2] == AUDIO_AAC)
                coderFormat[2] = AUDIO_G711A;
            else
                coderFormat[2] = AUDIO_NULL;
            break;
        case 3:
            if(coderFormat[3] == AUDIO_NULL)
                coderFormat[3] = AUDIO_AAC;
            else if(coderFormat[3] == AUDIO_AAC)
                coderFormat[3] = AUDIO_G711A;
            else
                coderFormat[3] = AUDIO_NULL;
            break;

        default:
            break;
    }
    return true;
}


bool CommandMap::SetVencHandler(cJSON *value)
{
    this->LoadEchoUdp();
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    int viport = 0;
    SIZE_S viSize;
    HI_U32 chnWho;


    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[0].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[0].stVencSize.u32Width = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stVencSize.u32Width = viParam[0].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stVencSize.u32Height = viParam[0].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[0].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[0].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[0].u32DstFrameRate[0] = viParam[0].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[0].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[0].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[0].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint ;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[0].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[0].u32MinQp[0] = min_qp->valueint;
                        }

                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[0].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[0].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[0].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32BQp[0] = b_qp->valueint;
                        }
                    }

                }else if(!strcmp(name->valuestring, "sub0")){         //次码流
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[0].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[0].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stMinorSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stMinorSize.u32Width = viParam[0].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stMinorSize.u32Height = viParam[0].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[0].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[0].enType[1]  = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[0].u32DstFrameRate[1] = viParam[0].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[0].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[0].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[0].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[0].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[0].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[0].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[0].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[0].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn1")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流1
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[1].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[1].stVencSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[1].stVencSize.u32Width = viParam[1].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[1].stVencSize.u32Height = viParam[1].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[1].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[1].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[1].u32DstFrameRate[0] = viParam[1].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[1].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[1].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[1].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[1].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[1].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[1].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[1].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[1].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流1
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[1].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[1].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stMinorSize.u32Height = viSize.u32Height;
                            }
 printf("111????????????????????????????????h=%d,w=%d\n",viParam[1].stMinorSize.u32Height,viParam[1].stMinorSize.u32Width);
                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    //if(viParam[1].u32W  <= viSize.u32Width)
                                    viParam[1].stMinorSize.u32Width = viParam[1].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    //if(viParam[1])
                                    viParam[1].stMinorSize.u32Height = viParam[1].u32H;
                                }
                            }

                            printf("????????????????????????????????h=%d,w=%d\n",viParam[1].stMinorSize.u32Height,viParam[1].stMinorSize.u32Width);

                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[1].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[1].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[1].u32DstFrameRate[1] = viParam[1].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[1].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[1].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[1].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[1].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[1].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[1].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[1].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[1].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn2")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流2
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[2].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[2].stVencSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stVencSize.u32Width = viParam[2].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stVencSize.u32Height = viParam[2].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[2].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[2].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[2].u32DstFrameRate[0] = viParam[2].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[2].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[2].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[2].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[2].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[2].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[2].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[2].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[2].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流2
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[2].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[2].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stMinorSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stMinorSize.u32Width = viParam[2].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stMinorSize.u32Height = viParam[2].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[2].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[2].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[2].u32DstFrameRate[1] = viParam[2].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[2].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[2].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[2].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[2].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[2].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[2].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[2].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[2].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn3")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流3
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[3].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[3].stVencSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stVencSize.u32Width = viParam[3].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stVencSize.u32Height = viParam[3].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[3].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        viParam[3].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[3].u32DstFrameRate[0] = viParam[3].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[3].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[3].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[3].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[3].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[3].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[3].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[3].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[3].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流3
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[3].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[3].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stMinorSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stMinorSize.u32Width = viParam[3].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stMinorSize.u32Height = viParam[3].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[3].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[3].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[3].u32DstFrameRate[1] = viParam[3].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[3].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[3].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[3].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[3].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[3].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[3].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[3].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[3].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }
    }
    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);

    auto thisini = Singleton<SoftwareConfig>::getInstance();



int i = 0;
    if(!strcmp(channel->valuestring, "chn0"))
    {

         chnWho = 0;

         for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
             crtsps_closestream(g_stream[i]);
              printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
         }

       // audio_running_ai[0] = false;


           if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
                return false;

             l_startloop[chnWho] = false;

             usleep(30000);

             himppmaster->VencModuleDestroy_1(chnWho* 2,chnWho* 2+2,-1);






               viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_VENC_W)),
                                         str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_VENC_H))};
               viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_W)),
                                         str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_H))};









            viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_VENC_Type));
            viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_Type));
            viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_BitRate));
            viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_BitRate));
            viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_B_QP));
            viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_B_QP));
            viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_DstFrmRate));
            viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_DstFrmRate));
            viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_Gop));
            viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_Gop));
            viParam[chnWho].u32IQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_I_QP));
            viParam[chnWho].u32IQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_I_QP));
            viParam[chnWho].u32PQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_P_QP));
            viParam[chnWho].u32PQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_P_QP));



            if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
            {
                if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                        viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                         viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

               if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                             viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                         viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



            }
            if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
            {

                if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                     (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                                viParam[chnWho].stVencSize  = {1920,1080} ;

                if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                     (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                                viParam[chnWho].stMinorSize  = {1920,1080} ;
            }

            if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
            {
                if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                  viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
                if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                  viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};
            }


            printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
           ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

            if(viParam[chnWho].enType[0] == PT_H264)
                v_type[chnWho*2] = VIDEO_H264;
            else if(viParam[chnWho].enType[0] == PT_H265)
                v_type[chnWho*2] = VIDEO_H265;
            if(viParam[chnWho].enType[1] == PT_H264)
                v_type[chnWho *2 +1] = VIDEO_H264;
            else if(viParam[chnWho].enType[1] == PT_H265)
                v_type[chnWho *2 +1] = VIDEO_H265;


            AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
            int param[2] = { 1, enAiSampleRate };
            coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_AiCoderFormat));
             HI_S32 bitRate = str2int(softwareconfig->GetConfig(SoftwareConfig::kAiBitRate));
            swAudioFormat(0);

            if(coderFormat[chnWho] == AUDIO_NULL){
                g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/0",NULL);
                g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/1",NULL);
            }else{
                g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/0",param);
                g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/1",param);
            }

            if(AUDIO_AAC ==  coderFormat[chnWho]) {
                for(int i = (2*chnWho); i <((chnWho +1) *2)  ; i++) {
                    crtsps_pushaudio( g_stream[i],(void*)svAac[0],svAacNum[0],svpts[0] );

                }
            }


            printf("crtsps_openstream = %d\n",g_stream[chnWho]);
            l_startloop[chnWho] = true;
           // audio_running_ai[0] = true;;
            himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);
            input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
            if(input_tid1[chnWho] == 0){
                SAMPLE_PRT("pthread_create() failed: videv1\n");
            }








            usleep(10000);

    }
    else if(!strcmp(channel->valuestring, "chn1"))
    {
        chnWho = 1;
        for(int i = chnWho * 2;i<(chnWho * 2 + 2);i++){
            crtsps_closestream(g_stream[i]);
        }


         if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
              return false;



         l_startloop[chnWho] = false;

//       if(((viParam[0].u32ViHeigth == 2160)||( viParam[0].u32ViWidth == 3840)) && ((viParam[1].u32ViHeigth == 2160)||( viParam[1].u32ViWidth == 3840) ))
//       {
//           viParam[chnWho].stVencSize = {3840,2160};
//           thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_W,viParam[chnWho].stVencSize.u32Width);
//           thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_H,viParam[chnWho].stVencSize.u32Height );
//                thisini->SaveConfig();
//                usleep(2000);
//       }
//       else
//       {
           viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_VENC_W)),
                                     str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_VENC_H))};
           viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_W)),
                                     str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_H))};
     //  }



           usleep(30000);



            himppmaster->VencModuleDestroy_1(chnWho*2,chnWho*2+2,-1);








           viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_VENC_Type));
           viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_Type));
           viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_BitRate));
           viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_BitRate));
           viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_B_QP));
           viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_B_QP));
           viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_DstFrmRate));
           viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_DstFrmRate));
           viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_Gop));
           viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_Gop));
           viParam[chnWho].u32IQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_I_QP));
           viParam[chnWho].u32IQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_I_QP));
           viParam[chnWho].u32PQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_P_QP));
           viParam[chnWho].u32PQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_P_QP));





           if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
           {
               if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                       viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

              if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                            viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



           }
           if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
           {

               if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                    (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                               viParam[chnWho].stVencSize  = {1920,1080} ;

               if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                    (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                               viParam[chnWho].stMinorSize  = {1920,1080} ;
           }

           if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
           {
               if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
               if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};

           }



   printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
          ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

           if(viParam[chnWho].enType[0] == PT_H264)
               v_type[chnWho*2] = VIDEO_H264;
           else if(viParam[chnWho].enType[0] == PT_H265)
               v_type[chnWho*2] = VIDEO_H265;
           if(viParam[chnWho].enType[1] == PT_H264)
               v_type[chnWho *2 +1] = VIDEO_H264;
           else if(viParam[chnWho].enType[1] == PT_H265)
               v_type[chnWho *2 +1] = VIDEO_H265;


           AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
           int param[2] = { 1, enAiSampleRate };
           coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_AiCoderFormat));
           swAudioFormat(chnWho);


           if(coderFormat[chnWho] == AUDIO_NULL){
               g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/2",NULL);
               g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/3",NULL);
           }else{
               g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/2",param);
               g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/3",param);
           }


           if(AUDIO_AAC ==  coderFormat[chnWho]) {
               for(int i = (2*chnWho); i <((chnWho +1) *2)  ; i++) {
                   crtsps_pushaudio( g_stream[i],(void*)svAac[chnWho],svAacNum[chnWho],svpts[chnWho] );

               }
           }


           l_startloop[chnWho] = true;


            himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);
           input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
           if(input_tid1[chnWho] == 0){
               SAMPLE_PRT("pthread_create() failed: videv1\n");
           }

//           switch (coderFormat[chnWho]) {
//               case AUDIO_NULL:
//                   break;
//                case AUDIO_G711A:
//                     g711_init();
//                   break;
//               default:
//                   break;
//           }

      // if(G711Init)
    //       g711_init();


//           if(coderFormat[chnWho] != AUDIO_NULL)
//                 CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);




           usleep(10000);



    }
#if 1
    else if(!strcmp(channel->valuestring, "chn2"))
    {
if(edid_4k == HI_FALSE)
{
        chnWho = 2;

        for(int i = chnWho * 2;i<(chnWho * 2 + 2);i++){
            crtsps_closestream(g_stream[i]);
        }

       //audio_running_ai[2] = false;

        if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
             return false;

         l_startloop[chnWho] = false;

          usleep(30000);

        himppmaster->VencModuleDestroy_1(chnWho * 2,chnWho*2 +2,-1);

            viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_H));
            viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_W));
           viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_W)),
                                     str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_H))};
           viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_W)),
                                     str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_H))};
           viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_VENC_Type));
           viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_Type));
           viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_BitRate));
           viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_BitRate));
           viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_B_QP));
           viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_B_QP));
           viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_DstFrmRate));
           viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_DstFrmRate));
           viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_Gop));
           viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_Gop));
           viParam[chnWho].u32IQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_I_QP));
           viParam[chnWho].u32IQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_I_QP));
           viParam[chnWho].u32PQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_P_QP));
           viParam[chnWho].u32PQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_P_QP));







           if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
           {
               if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                       viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

              if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                            viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



           }
           if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
           {

               if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                    (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                               viParam[chnWho].stVencSize  = {1920,1080} ;

               if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                    (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                               viParam[chnWho].stMinorSize  = {1920,1080} ;
           }
           if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
           {
               if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
               if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};


           }



   printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
          ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

           if(viParam[chnWho].enType[0] == PT_H264)
               v_type[chnWho*2] = VIDEO_H264;
           else if(viParam[chnWho].enType[0] == PT_H265)
               v_type[chnWho*2] = VIDEO_H265;
           if(viParam[chnWho].enType[1] == PT_H264)
               v_type[chnWho *2 +1] = VIDEO_H264;
           else if(viParam[chnWho].enType[1] == PT_H265)
               v_type[chnWho *2 +1] = VIDEO_H265;


           AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
           int param[2] = { 1, enAiSampleRate };
           coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_AiCoderFormat));
           swAudioFormat(chnWho);

           if(coderFormat[chnWho] == AUDIO_NULL){
               g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/4",NULL);
               g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/5",NULL);
           }else{
               g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/4",param);
               g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/5",param);
           }

           l_startloop[chnWho] = true;
       //    audio_running_ai[2] = true;
           himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);
           input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
           if(input_tid1[chnWho] == 0){
               SAMPLE_PRT("pthread_create() failed: videv1\n");
           }
           usleep(10000);
}
    }
#endif
     else if(!strcmp(channel->valuestring, "chnxx"))
    {
        chnWho = 3;

        for(int i = chnWho * 2;i<(chnWho * 2 + 2);i++){
            crtsps_closestream(g_stream[i]);
        }

       // audio_running_ai[3] = false;

        if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
             return false;


             l_startloop[chnWho] = false;
          usleep(30000);
        himppmaster->VencModuleDestroy_1(chnWho* 2,chnWho*2+2,-1);

            viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_H));
            viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_W));
           viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_VENC_W)),
                                     str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_VENC_H))};
           viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_W)),
                                     str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_H))};
           viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_VENC_Type));
           viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_Type));
           viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_BitRate));
           viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_BitRate));
           viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_B_QP));
           viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_B_QP));
           viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_DstFrmRate));
           viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_DstFrmRate));
           viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_Gop));
           viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_Gop));
           viParam[chnWho].u32IQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_I_QP));
           viParam[chnWho].u32IQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_I_QP));
           viParam[chnWho].u32PQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_P_QP));
           viParam[chnWho].u32PQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_P_QP));



           if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
           {
               if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                       viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

              if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                            viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



           }





           if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
           {

               if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                    (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                               viParam[chnWho].stVencSize  = {1920,1080} ;

               if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                    (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                               viParam[chnWho].stMinorSize  = {1920,1080} ;
           }
           if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
           {
               if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
               if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};


           }


   printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
          ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

           if(viParam[chnWho].enType[0] == PT_H264)
               v_type[chnWho*2] = VIDEO_H264;
           else if(viParam[chnWho].enType[0] == PT_H265)
               v_type[chnWho*2] = VIDEO_H265;
           if(viParam[chnWho].enType[1] == PT_H264)
               v_type[chnWho *2 +1] = VIDEO_H264;
           else if(viParam[chnWho].enType[1] == PT_H265)
               v_type[chnWho *2 +1] = VIDEO_H265;


           AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
           int param[2] = { 1, enAiSampleRate };
           coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_AiCoderFormat));
          // swAudioFormat(0);

           if(coderFormat[chnWho] == AUDIO_NULL){
               g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/6",NULL);
               g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/7",NULL);
           }else{
               g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/6",param);
               g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/7",param);
           }

           l_startloop[chnWho] = true;
         //  audio_running_ai[3] = true;
           himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);
           input_tid1[0] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
           if(input_tid1[0] == 0){
               SAMPLE_PRT("pthread_create() failed: videv1\n");
           }
           usleep(10000);



    }
    else
    {
        printf("perror    setvenc \n");
    }




    return true;
}


#if 0

// 3.25
bool CommandMap::SetVencHandler(cJSON *value)
{
    this->LoadEchoUdp();
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    int viport = 0;
    SIZE_S viSize;
    HI_U32 chnWho;
    printf("*******************************************SetVencHandler\n");

    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[0].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[0].stVencSize.u32Width = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stVencSize.u32Width = viParam[0].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stVencSize.u32Height = viParam[0].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[0].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[0].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[0].u32DstFrameRate[0] = viParam[0].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[0].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[0].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[0].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint ;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[0].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[0].u32MinQp[0] = min_qp->valueint;
                        }

                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[0].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[0].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[0].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32BQp[0] = b_qp->valueint;
                        }
                    }

                }else if(!strcmp(name->valuestring, "sub0")){         //次码流
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[0].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[0].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stMinorSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stMinorSize.u32Width = viParam[0].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[0].stMinorSize.u32Height = viParam[0].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[0].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[0].enType[1]  = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[0].u32DstFrameRate[1] = viParam[0].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[0].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[0].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[0].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[0].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[0].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[0].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[0].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[0].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn1")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流1
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[1].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[1].stVencSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[1].stVencSize.u32Width = viParam[1].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[1].stVencSize.u32Height = viParam[1].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[1].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[1].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[1].u32DstFrameRate[0] = viParam[1].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[1].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[1].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[1].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[1].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[1].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[1].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[1].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[1].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流1
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[1].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[1].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stMinorSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[1].stMinorSize.u32Width = viParam[1].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[1].stMinorSize.u32Height = viParam[1].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[1].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[1].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[1].u32DstFrameRate[1] = viParam[1].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[1].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[1].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[1].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[1].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[1].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[1].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[1].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[1].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn2")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流2
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[2].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[2].stVencSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stVencSize.u32Width = viParam[2].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stVencSize.u32Height = viParam[2].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[2].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[2].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[2].u32DstFrameRate[0] = viParam[2].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[2].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[2].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[2].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[2].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[2].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[2].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[2].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[2].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流2
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[2].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[2].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stMinorSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stMinorSize.u32Width = viParam[2].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[2].stMinorSize.u32Height = viParam[2].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[2].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[2].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[2].u32DstFrameRate[1] = viParam[2].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[2].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[2].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[2].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[2].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[2].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[2].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[2].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[2].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn3")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流3
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[3].stVencSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[3].stVencSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {   //VI端无信号，插入1080P无信号地图，帧率30
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W,viSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stVencSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, viSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stVencSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable))){   //使能VI裁剪，编码分辨率等于裁剪后的分辨率
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stVencSize.u32Width = viParam[3].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stVencSize.u32Height = viParam[3].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[3].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        viParam[3].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[3].u32DstFrameRate[0] = viParam[3].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[3].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[3].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[3].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[3].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[3].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[3].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[3].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[3].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流3
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W, venc_w->valueint) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[3].stMinorSize.u32Width = venc_w->valueint;
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H, venc_h->valueint) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[3].stMinorSize.u32Height = venc_h->valueint;
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            viSize.u32Width = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_W));
                            viSize.u32Height = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_H));
                            if((viSize.u32Width == 0) || (viSize.u32Height == 0)) {
                                viSize.u32Width = 1920;
                                viSize.u32Height = 1080;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W,viSize.u32Width)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stMinorSize.u32Width = viSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H,viSize.u32Height )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stMinorSize.u32Height = viSize.u32Height;
                            }

                            if(str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable).c_str())) {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Width)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stMinorSize.u32Width = viParam[3].u32W;
                                }
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Height)))){
                                    COMMON_PRT("set venc height set failed");
                                }
                                else {
                                    viParam[3].stMinorSize.u32Height = viParam[3].u32H;
                                }
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            viParam[3].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[3].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_DstFrmRate, min(venc_dstframerate->valueint, str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_SrcFrmRate))))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[3].u32DstFrameRate[1] = viParam[3].u32SrcFrmRate;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[3].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[3].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[3].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[3].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[3].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[3].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[3].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[3].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }
    }
    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);

    auto thisini = Singleton<SoftwareConfig>::getInstance();

    VIDEO_NORM_E gEnNorm = VIDEO_ENCODING_MODE_NTSC ;

int i = 0;


 usleep(2000);


    if(!strcmp(channel->valuestring, "chn0"))
    {

         chnWho = 0;
         VI_DEV viDev = 1;    ;      //vi dev 1 3 5 7 chn 4 12 20 28
       VI_CHN  viChn = (4 * viDev);


        printf( "  vi _h=%d\n",viParam[chnWho].u32ViHeigth);
  if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
      return true;



  if(((viParam[0].u32ViHeigth == 2160)||( viParam[0].u32ViWidth == 3840)) && ((viParam[1].u32ViHeigth == 2160)||( viParam[1].u32ViWidth == 3840) ))
  {
      viParam[chnWho].stVencSize = {3840,2160};
      thisini->SetConfig(SoftwareConfig::kChn0_M_VENC_W,viParam[chnWho].stVencSize.u32Width);
      thisini->SetConfig(SoftwareConfig::kChn0_M_VENC_H,viParam[chnWho].stVencSize.u32Height );
           thisini->SaveConfig();
           usleep(2000);
  }
  else
  {
      viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_VENC_W)),
                                str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_VENC_H))};
      viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_W)),
                                str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_H))};
  }


         for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
             crtsps_closestream(g_stream[i]);
              printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
         }
          // l_startloop[chnWho] = false;
       // audio_running_ai[0] = false;

//         viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_H));
//         viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_W));

//         if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
//         usleep(30000);
//         else
            // himppmaster->StopViModule(viDev,viChn);

             himppmaster->VencModuleDestroy_1(chnWho* 2, chnWho * 2+2,-1);

            //sleep(1);
             l_startloop[chnWho] = false;
             snap_runlag[chnWho] = false;

             usleep(50000);


//             viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn0_VI_H));
//             viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn0_VI_W));

            viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_VENC_Type));
            viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_Type));
            viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_BitRate));
            viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_BitRate));
            viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_B_QP));
            viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_B_QP));
            viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_DstFrmRate));
            viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_DstFrmRate));
            viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_Gop));
            viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_Gop));
            viParam[chnWho].u32IQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_I_QP));
            viParam[chnWho].u32IQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_I_QP));

             viParam[chnWho].u32PQp[0] =  str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_P_QP));

             viParam[chnWho].u32PQp[1] =  str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_P_QP));

             SIZE_S stTargetSize ={viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};





            if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
            {
                if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                        viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                         viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

               if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                             viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                         viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

               if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                             viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                         viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};






            }
            if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
            {

                if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                     (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                                viParam[chnWho].stVencSize  = {1920,1080} ;

                if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                     (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                                viParam[chnWho].stMinorSize  = {1920,1080} ;
            }

            if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
            {
                if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                  viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
                if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                  viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};
            }


            printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
           ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

            if(viParam[chnWho].enType[0] == PT_H264)
                v_type[chnWho*2] = VIDEO_H264;
            else if(viParam[chnWho].enType[0] == PT_H265)
                v_type[chnWho*2] = VIDEO_H265;
            if(viParam[chnWho].enType[1] == PT_H264)
                v_type[chnWho *2 +1] = VIDEO_H264;
            else if(viParam[chnWho].enType[1] == PT_H265)
                v_type[chnWho *2 +1] = VIDEO_H265;
#if 1

            AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
            int param[2] = { 1, enAiSampleRate };
            coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_AiCoderFormat));
             HI_S32 bitRate = str2int(softwareconfig->GetConfig(SoftwareConfig::kAiBitRate));
            swAudioFormat(0);

            if(coderFormat[chnWho] == AUDIO_NULL){
                g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/0",NULL);
                g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/1",NULL);
            }else{
                g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/0",param);
                g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/1",param);
            }


            printf("crtsps_openstream = %d\n",g_stream[chnWho]);
            l_startloop[chnWho] = true;
          //  audio_running_ai[0] = true;;
            snap_runlag[chnWho] = true;
            RECT_S stCapRect = {viParam[chnWho].u32X,viParam[chnWho].u32Y, viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};


          //  himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate );


            himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);
            input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
            if(input_tid1[chnWho] == 0){
                SAMPLE_PRT("pthread_create() failed: videv1\n");
            }





#endif


            usleep(10000);

    }
    else if(!strcmp(channel->valuestring, "chn1"))
    {
        chnWho = 1;
        if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
            return true;
//printf("___________0 W=%d_________w__%d\n", viParam[0].u32ViWidth,viParam[1].u32ViWidth);

        if(((viParam[0].u32ViHeigth == 2160)||( viParam[0].u32ViWidth == 3840)) && ((viParam[1].u32ViHeigth == 2160)||( viParam[1].u32ViWidth == 3840) ))
        {

            viParam[chnWho].stVencSize = {3840,2160};
            thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_W,viParam[chnWho].stVencSize.u32Width);
            thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_H,viParam[chnWho].stVencSize.u32Height );
                 thisini->SaveConfig();
                 usleep(2000);

        }
            else
        {
            viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_VENC_W)),
                                      str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_VENC_H))};
            viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_W)),
                                      str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_H))};
        }






        for(int i = chnWho * 2;i<(chnWho * 2 + 2);i++){
            crtsps_closestream(g_stream[i]);
        }







         l_startloop[chnWho] = false;
      //  audio_running_ai[1] = false;



//         if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
//         usleep(30000);
//         else
           usleep(30000);


//        if(input_tid[chnWho] != 0)
//         {    pthread_cancel(input_tid[chnWho]);
//              input_tid[chnWho] = 0;
//         }
//            else if(input_tid1[chnWho] != 0)
//             pthread_cancel(input_tid1[chnWho]);


            himppmaster->VencModuleDestroy_1(chnWho*2,chnWho*2+2,-1);




//          viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_H));
//          viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_W));



//            viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_H));
//            viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_W));

           viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_VENC_Type));
           viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_Type));
           viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_BitRate));
           viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_BitRate));
           viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_B_QP));
           viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_B_QP));
           viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_DstFrmRate));
           viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_DstFrmRate));
           viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_Gop));
           viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_Gop));



           if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
           {
               if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                       viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

              if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                            viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



           }
           if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
           {

               if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                    (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                               viParam[chnWho].stVencSize  = {1920,1080} ;

               if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                    (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                               viParam[chnWho].stMinorSize  = {1920,1080} ;
           }

           if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
           {
               if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
               if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};

           }



   printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
          ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

           if(viParam[chnWho].enType[0] == PT_H264)
               v_type[chnWho*2] = VIDEO_H264;
           else if(viParam[chnWho].enType[0] == PT_H265)
               v_type[chnWho*2] = VIDEO_H265;
           if(viParam[chnWho].enType[1] == PT_H264)
               v_type[chnWho *2 +1] = VIDEO_H264;
           else if(viParam[chnWho].enType[1] == PT_H265)
               v_type[chnWho *2 +1] = VIDEO_H265;


           AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
           int param[2] = { 1, enAiSampleRate };
           coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_AiCoderFormat));
           swAudioFormat(chnWho);


           if(coderFormat[chnWho] == AUDIO_NULL){
               g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/2",NULL);
               g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/3",NULL);
           }else{
               g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/2",param);
               g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/3",param);
           }

           l_startloop[chnWho] = true;
           // audio_running_ai[1] = true;



            himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);


           input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
           if(input_tid1[chnWho] == 0){
               SAMPLE_PRT("pthread_create() failed: videv1\n");
           }

//           switch (coderFormat[chnWho]) {
//               case AUDIO_NULL:
//                   break;
//                case AUDIO_G711A:
//                     g711_init();
//                   break;
//               default:
//                   break;
//           }

      // if(G711Init)
    //       g711_init();


//           if(coderFormat[chnWho] != AUDIO_NULL)
//                 CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);




           usleep(10000);



    }
#if 1
    else if(!strcmp(channel->valuestring, "chn2"))
    {

        if(edid_4k == HI_FALSE)
        {

        chnWho = 2;
        if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
            return true;

        for(int i = chnWho * 2;i<(chnWho * 2 + 2);i++){
            crtsps_closestream(g_stream[i]);
        }
        l_startloop[chnWho] = false;
       //audio_running_ai[2] = false;

        viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_H));
        viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_W));

           printf("??????????????????viParam--vi h=%d    w=%d\n",viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth);



//        if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
//        usleep(30000);
//        else
          usleep(30000);

        himppmaster->VencModuleDestroy_1(chnWho * 2,chnWho*2 +2,-1);

            viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_H));
            viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_W));
           viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_W)),
                                     str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_H))};
           viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_W)),
                                     str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_H))};
           viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_VENC_Type));
           viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_Type));
           viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_BitRate));
           viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_BitRate));
           viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_B_QP));
           viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_B_QP));
           viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_DstFrmRate));
           viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_DstFrmRate));
           viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_Gop));
           viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_Gop));

           if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
           {
               if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                       viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

              if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                            viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



           }
           if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
           {

               if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                    (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                               viParam[chnWho].stVencSize  = {1920,1080} ;

               if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                    (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                               viParam[chnWho].stMinorSize  = {1920,1080} ;
           }
           if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
           {
               if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
               if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};


           }



   printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
          ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

           if(viParam[chnWho].enType[0] == PT_H264)
               v_type[chnWho*2] = VIDEO_H264;
           else if(viParam[chnWho].enType[0] == PT_H265)
               v_type[chnWho*2] = VIDEO_H265;
           if(viParam[chnWho].enType[1] == PT_H264)
               v_type[chnWho *2 +1] = VIDEO_H264;
           else if(viParam[chnWho].enType[1] == PT_H265)
               v_type[chnWho *2 +1] = VIDEO_H265;


           AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
           int param[2] = { 1, enAiSampleRate };
           coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_AiCoderFormat));
           swAudioFormat(chnWho);

           if(coderFormat[chnWho] == AUDIO_NULL){
               g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/4",NULL);
               g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/5",NULL);
           }else{
               g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/4",param);
               g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/5",param);
           }

           l_startloop[chnWho] = true;
       //    audio_running_ai[2] = true;
           himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);
           input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
           if(input_tid1[chnWho] == 0){
               SAMPLE_PRT("pthread_create() failed: videv1\n");
           }
        }
           usleep(10000);

    }
#endif
     else if(!strcmp(channel->valuestring, "chn3"))
    {
        chnWho = 3;
        if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
            return true;
        for(int i = chnWho * 2;i<(chnWho * 2 + 2);i++){
            crtsps_closestream(g_stream[i]);
        }
        l_startloop[chnWho] = false;
        audio_running_ai[3] = false;
        viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_H));
        viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_W));

//        if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
//        usleep(30000);
//        else
          usleep(30000);
        himppmaster->VencModuleDestroy_1(chnWho* 2,chnWho*2+2,-1);

            viParam[chnWho].u32ViHeigth = str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_H));
            viParam[chnWho].u32ViWidth = str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_W));
           viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_VENC_W)),
                                     str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_VENC_H))};
           viParam[chnWho].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_W)),
                                     str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_H))};
           viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_VENC_Type));
           viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_Type));
           viParam[chnWho].u32BitRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_BitRate));
           viParam[chnWho].u32BitRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_BitRate));
           viParam[chnWho].u32BQp[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_B_QP));
           viParam[chnWho].u32BQp[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_B_QP));
           viParam[chnWho].u32DstFrameRate[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_DstFrmRate));
           viParam[chnWho].u32DstFrameRate[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_DstFrmRate));
           viParam[chnWho].u32Gop[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_Gop));
           viParam[chnWho].u32Gop[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_Gop));


           if((viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0))
           {
               if(viParam[chnWho].stVencSize.u32Height <= 0 || viParam[chnWho].stVencSize.u32Width <= 0  ||
                       viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stVencSize  = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

              if(viParam[chnWho].stMinorSize.u32Height <= 0 || viParam[chnWho].stMinorSize.u32Width <= 0  ||
                            viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32ViWidth || viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32ViHeigth )
                        viParam[chnWho].stMinorSize= {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



           }
           if(viParam[chnWho].u32ViWidth <= 0 || viParam[chnWho].u32ViWidth <= 0)
           {

               if( (viParam[chnWho].stVencSize.u32Height >1080 ) || ( viParam[chnWho].stVencSize.u32Width >1920) ||
                    (viParam[chnWho].stVencSize.u32Height <= 0) || (viParam[chnWho].stVencSize.u32Width <= 0) )
                               viParam[chnWho].stVencSize  = {1920,1080} ;

               if( (viParam[chnWho].stMinorSize.u32Height >1080 ) || ( viParam[chnWho].stMinorSize.u32Width >1920) ||
                    (viParam[chnWho].stMinorSize.u32Height <= 0) || (viParam[chnWho].stMinorSize.u32Width <= 0) )
                               viParam[chnWho].stMinorSize  = {1920,1080} ;
           }
           if((viParam[chnWho].vi_crop_enable == HI_TRUE) && (viParam[chnWho].u32ViHeigth != 0 ) && (viParam[chnWho].u32ViWidth != 0 ) )
           {
               if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stVencSize  = {viParam[chnWho].u32W,viParam[chnWho].u32H};
               if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H ) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) )
                 viParam[chnWho].stMinorSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};


           }


   printf("vi h=%d vi_w=%d venc_w=%d venc_h=%d\n", viParam[chnWho].u32ViHeigth,viParam[chnWho].u32ViWidth
          ,viParam[chnWho].stVencSize.u32Width ,viParam[chnWho].stVencSize.u32Height);

           if(viParam[chnWho].enType[0] == PT_H264)
               v_type[chnWho*2] = VIDEO_H264;
           else if(viParam[chnWho].enType[0] == PT_H265)
               v_type[chnWho*2] = VIDEO_H265;
           if(viParam[chnWho].enType[1] == PT_H264)
               v_type[chnWho *2 +1] = VIDEO_H264;
           else if(viParam[chnWho].enType[1] == PT_H265)
               v_type[chnWho *2 +1] = VIDEO_H265;


           AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
           int param[2] = { 1, enAiSampleRate };
           coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_AiCoderFormat));
          // swAudioFormat(0);

           if(coderFormat[chnWho] == AUDIO_NULL){
               g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/6",NULL);
               g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/7",NULL);
           }else{
               g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/6",param);
               g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/7",param);
           }

           l_startloop[chnWho] = true;
           audio_running_ai[3] = true;
           himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);
           input_tid1[0] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
           if(input_tid1[0] == 0){
               SAMPLE_PRT("pthread_create() failed: videv1\n");
           }
           usleep(10000);



    }
    else
    {
        printf("perror    setvenc \n");
    }

//}


    return true;
}
#endif

/*
 *重启生效
 * 设置RTSP音频
 * chnx---  4路VI RTSP视频分别和HDMI0 HDIM1 3.5MM 音频MIX
 * ai_mode--- HDMI0 HDMI1 3.5MM 音频
 * ai_codeformat--- 音频编码格式 0 ---AAC  1--- G711A  3 ---NULL（无音频）
*/



bool CommandMap::SetAudioHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    this->LoadEchoUdp();
    audio_type AAC_or_g71[4] ;
    AAC_or_g71[0] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_AiCoderFormat));

            AAC_or_g71[1] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_AiCoderFormat));
            swAudioFormat(1);
            coderFormat[2] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_AiCoderFormat));
            swAudioFormat(2);


    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }
            }

            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_AiDev, aimode->valuestring) ){
                    COMMON_PRT("set audio->mode set failed");
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }
            }

            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_AiDev, aimode->valuestring) ){
                    COMMON_PRT("set audio->mode set failed");
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }
            }

            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_AiDev, aimode->valuestring) ){
                    COMMON_PRT("set audio->mode set failed");
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }
            }

            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_AiDev, aimode->string) ){
                    COMMON_PRT("set audio->mode set failed");
                }
            }
        }

    }

    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    usleep(5000);
#if 1

int chnWho = 0, i = 0;
    if (channel && cJSON_IsString(channel)) {


        if(!strcmp(softwareconfig->GetConfig(SoftwareConfig::kChn0_AiDev).c_str(),"HDMI"))
            Aechn[0] = 0;
        else if(!strcmp(softwareconfig->GetConfig(SoftwareConfig::kChn0_AiDev).c_str(),"3.5mm"))
            Aechn[0] = 2;

        if(!strcmp(softwareconfig->GetConfig(SoftwareConfig::kChn1_AiDev).c_str(),"HDMI"))
            Aechn[1] = 1;
        else if(!strcmp(softwareconfig->GetConfig(SoftwareConfig::kChn1_AiDev).c_str(),"3.5mm"))
            Aechn[1] = 2;

        //if(!strcmp(thisini->GetConfig(SoftwareConfig::kChn2_AiDev).c_str(),"3.5mm"))
            Aechn[2] = 2;


            coderFormat[0] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_AiCoderFormat));
            swAudioFormat(0);
            coderFormat[1] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_AiCoderFormat));
            swAudioFormat(1);
            coderFormat[2] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_AiCoderFormat));
            swAudioFormat(2);

         printf("++++++++++++++++++++aichn0===%d,chn1===%d,coderfirmat=%d,edid--%d\n",Aechn[0],Aechn[1],coderFormat[2],edid_4k);
     //    audio_running_ai[0] = false;


        if(!strcmp(channel->valuestring, "chn0"))
        {


             chnWho = 0;


//             if(Aechn[chnWho] != 0)
//                 return false;

             for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
                 crtsps_closestream(g_stream[i]);
                  printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
             }

            audio_running_ai[0] = false;
            if(Aechn[0] == 2)
                audio_running_ai[2] = false;



             if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
             usleep(30000);
             else
               usleep(10000);





                if(viParam[chnWho].enType[0] == PT_H264)
                    v_type[chnWho*2] = VIDEO_H264;
                else if(viParam[chnWho].enType[0] == PT_H265)
                    v_type[chnWho*2] = VIDEO_H265;
                if(viParam[chnWho].enType[1] == PT_H264)
                    v_type[chnWho *2 +1] = VIDEO_H264;
                else if(viParam[chnWho].enType[1] == PT_H265)
                    v_type[chnWho *2 +1] = VIDEO_H265;


                AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
                int param[2] = { 1, enAiSampleRate };
                coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_AiCoderFormat));
                 HI_S32 bitRate = str2int(softwareconfig->GetConfig(SoftwareConfig::kAiBitRate));
                swAudioFormat(0);

                if(coderFormat[chnWho] == AUDIO_NULL){
                    g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/0",NULL);
                    g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/1",NULL);
                }else{
                    g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/0",param);
                    g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/1",param);



                }
                printf("crtsps_openstream = %d\n",g_stream[chnWho]);
              //  l_startloop[chnWho] = true;
                audio_running_ai[0] = true;
               // himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,chnWho);
//                input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
//                if(input_tid1[chnWho] == 0){
//                    SAMPLE_PRT("pthread_create() failed: videv1\n");
//                }
                switch (coderFormat[chnWho]) {
                    case AUDIO_NULL:
                        break;
                    case AUDIO_AAC:
                        startFlag[chnWho] = true;
                        g_audio_aac[chnWho] = new aacenc();
                        g_audio_aac[chnWho]->open(bitRate, enAiSampleRate, AACLC);
                    break;
                     case AUDIO_G711A:
                          g711_init();
                        break;
                    default:
                        break;
                }






                if(Aechn[0] != 2)
                {
                     audio_running_ai[0] = true;
                   if(coderFormat[chnWho] != AUDIO_NULL)
                      CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);
                }
                else if (Aechn[0] == 2)
                {
                    chnWho = 2;
                    audio_running_ai[2] = true;
                     CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);
                }

                 //    himppmaster->StartAudioEnc_1(0,0);



                usleep(10000);

        }
        else if(!strcmp(channel->valuestring, "chn1"))
        {

             chnWho = 1;
//              if(Aechn[chnWho] != 1)
//                  return false;

             for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
                 crtsps_closestream(g_stream[i]);
                  printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
             }



            audio_running_ai[1] = false;
            if(Aechn[1] == 2)
                audio_running_ai[2] = false;



             if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
             usleep(30000);
             else
               usleep(10000);





                if(viParam[chnWho].enType[0] == PT_H264)
                    v_type[chnWho*2] = VIDEO_H264;
                else if(viParam[chnWho].enType[0] == PT_H265)
                    v_type[chnWho*2] = VIDEO_H265;
                if(viParam[chnWho].enType[1] == PT_H264)
                    v_type[chnWho *2 +1] = VIDEO_H264;
                else if(viParam[chnWho].enType[1] == PT_H265)
                    v_type[chnWho *2 +1] = VIDEO_H265;


                AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
                int param[2] = { 1, enAiSampleRate };
                coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_AiCoderFormat));
                 HI_S32 bitRate = str2int(softwareconfig->GetConfig(SoftwareConfig::kAiBitRate));
                swAudioFormat(1);

                if(coderFormat[chnWho] == AUDIO_NULL){
                    g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/2",NULL);
                    g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/3",NULL);
                }else{
                    g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/2",param);
                    g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/3",param);



                }
                printf("crtsps_openstream = %d\n",g_stream[chnWho]);

                audio_running_ai[chnWho] = true;;

                switch (coderFormat[chnWho]) {
                    case AUDIO_NULL:
                        break;
                    case AUDIO_AAC:
                        startFlag[chnWho] = true;
                        g_audio_aac[chnWho] = new aacenc();
                        g_audio_aac[chnWho]->open(bitRate, enAiSampleRate, AACLC);
                    break;
                     case AUDIO_G711A:

                         g711_init();
                        break;
                    default:
                        break;
                }




                if(Aechn[1] != 2)
                {
                    if(coderFormat[chnWho] != AUDIO_NULL)
                        CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);
                }
                else
                {
                    chnWho = 2;
                    audio_running_ai[2] = true;
                    CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);
                }


                 //    himppmaster->StartAudioEnc_1(0,0);



                usleep(10000);

        }
        else if(!strcmp(channel->valuestring, "chn2"))
        {

            if(edid_4k  ==  1)
                return false;

             chnWho = 2;
//              if(Aechn[chnWho] != 1)
//                  return false;
             audio_running_ai[chnWho] = false;


   #if 1
             for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
                 crtsps_closestream(g_stream[i]);
                  printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
             }

            audio_running_ai[chnWho] = false;



             if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
             usleep(30000);
             else
               usleep(10000);





                if(viParam[chnWho].enType[0] == PT_H264)
                    v_type[chnWho*2] = VIDEO_H264;
                else if(viParam[chnWho].enType[0] == PT_H265)
                    v_type[chnWho*2] = VIDEO_H265;
                if(viParam[chnWho].enType[1] == PT_H264)
                    v_type[chnWho *2 +1] = VIDEO_H264;
                else if(viParam[chnWho].enType[1] == PT_H265)
                    v_type[chnWho *2 +1] = VIDEO_H265;


                AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
                int param[2] = { 1, enAiSampleRate };
              //  coderFormat[chnWho] = (enum audio_type)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_AiCoderFormat));
                 HI_S32 bitRate = str2int(softwareconfig->GetConfig(SoftwareConfig::kAiBitRate));
               // swAudioFormat(1);

                if(coderFormat[chnWho] == AUDIO_NULL){
                    g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/4",NULL);
                    g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/5",NULL);
                }else{
                    g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/4",param);
                    g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/5",param);



                }
                printf("crtsps_openstream = --------------------------------------%d\n",g_stream[chnWho * 2]);

                audio_running_ai[chnWho] = true;;


                switch (coderFormat[chnWho]) {
                    case AUDIO_NULL:
                        break;
                    case AUDIO_AAC:
                        startFlag[chnWho] = true;
                        g_audio_aac[chnWho] = new aacenc();
                        g_audio_aac[chnWho]->open(bitRate, enAiSampleRate, AACLC);
                    break;
                     case AUDIO_G711A:

                         g711_init();
                        break;
                    default:
                        break;
                }




             //   if(coderFormat[chnWho] != AUDIO_NULL)
                     CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);


#endif
                //     himppmaster->StartAudioEnc_1(0,0);



                usleep(10000);

        }


#if 0
        if((Aechn[0] == 2) || (Aechn[1] == 2) || (Aechn[2] == 2) )
        {




                 chnWho = 2;
    //              if(Aechn[chnWho] != 1)
    //                  return false;

               audio_running_ai[chnWho] = false;



                 if((viParam[chnWho].u32ViHeigth  <= 0 ) || (viParam[chnWho].u32ViWidth <= 0))
                 usleep(30000);
                 else
                   usleep(10000);












   audio_running_ai[chnWho] = true;
                  //  if(coderFormat[chnWho] != AUDIO_NULL)
                          CreateThread(input_ai, 0, SCHED_FIFO, true, &chnWho);


                         //himppmaster->StartAudioEnc_1(0,0);



                    usleep(10000);







        }




#endif


 }



#endif



    return true;
}
#if 1





bool CommandMap::SetBasicConfigHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    //节点名称
     printf("   i am  SetBasicConfigHandler\n");
    cJSON *node_name = cJSON_GetObjectItemCaseSensitive(value, "node_name");
    if (node_name && cJSON_IsString(node_name)) {
        char namefile[30] = "";
        sprintf(namefile, "%s/%s", DATA_FILE, NODE_NAME_FILE);
        FILE *f = fopen( namefile, "wb" );
        if(f){
              printf("   i am  SetBasicConfigHandler---file\n");
            fprintf(f, "%s\n", node_name->valuestring);
            fflush(f);
            fclose(f);
            f = NULL;
        }
    }

    //#ifdef __HI3531D_VENC__
    cJSON *edid = cJSON_GetObjectItemCaseSensitive(value, "edid");
    if (edid && cJSON_IsNumber(edid)) {

        if( !softwareconfig->SetConfig(SoftwareConfig::kEdid, edid->valueint) ){
            COMMON_PRT("set edid set failed");
        }
    }
    //#endif

    cJSON *mcast_diff = cJSON_GetObjectItemCaseSensitive(value, "mcast_diff");
    if (mcast_diff && cJSON_IsNumber(mcast_diff)) {


        diff_index = mcast_diff->valueint;
    }


    cJSON *vi_module = cJSON_GetObjectItemCaseSensitive(value, "vi_module");
    if (vi_module && cJSON_IsNumber(vi_module)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Module, vi_module->valueint) ){
            COMMON_PRT("set vi_module set failed");
        }
    }





        softwareconfig->SaveConfig();

    this->LetMeTellYou(msg);

    HI_U32   edid_4k_new =   str2int(softwareconfig->GetConfig(SoftwareConfig::kEdid));
       // edid_4k_new









    auto himppmaster = Singleton<HimppMaster>::getInstance();



//        int i = 0;
//        if(edid_4k  != edid_4k_new     )
//        {

//               for( i = 0;i< 8;i++){
//            crtsps_closestream(g_stream[i]);


//            }

//                system("reboot");


//        }








    return true;
}
#endif

#if 1
bool CommandMap::SetAuMulticastHandler(cJSON *value)
{
    bool nspc = false;
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    cJSON *cmd = cJSON_GetObjectItemCaseSensitive(value, "cmd_frome");
    if(cmd && cJSON_IsString(cmd)) {
        if(!strcmp(cmd->valuestring,"nspc")) {
            nspc = true;
        }
    }

    cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
    if (name && cJSON_IsString(name)) {
        if(!strcmp(name->valuestring, "hdmi0")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable) {
                        aumcast_enable[0] = HI_FALSE;
                    }
                    else {
                        aumcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    aumcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
                }
            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
               // sprintf(viParam.Amcast_ip, "%s", string(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastIp)).c_str());
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
              //  viParam.Amcast_port = atoi(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastPort).c_str());
            }
        }
       else  if(!strcmp(name->valuestring, "hdmi1")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable) {
                        aumcast_enable[1] = HI_FALSE;
                    }
                    else {
                        aumcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    aumcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
                }
            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
              //  sprintf(viParam.Amcast_ip, "%s", string(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastIp)).c_str());
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
             //   viParam.Amcast_port = atoi(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastPort).c_str());
            }
        }
        else if(!strcmp(name->valuestring, "3.5mm")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        aumcast_enable[2] = HI_FALSE;
                    }
                    else {
                        aumcast_enable[2]= (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    aumcast_enable[2] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
                }

            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
                //sprintf(viParam.Amcast_ip, "%s", string(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastIp)).c_str());
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
                //viParam.Amcast_port = atoi(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastPort).c_str());
            }
        }
    }
    softwareconfig->SaveConfig();
#if 1
    auto thisini = Singleton<SoftwareConfig>::getInstance();


    int i = 0;



    if(!strcmp(name->valuestring,"hdmi0")) {
        i = 0;
       // aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastPort));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastIp).c_str());
    }
    if(!strcmp(name->valuestring,"hdmi1")) {
        i = 1;
       // aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastPort));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastIp).c_str());

    }
    else if(!strcmp(name->valuestring,"3.5mm")) {
        i = 2;
      //  aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kAnalog_Au_McastPort));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kAnalog_Au_McastIp).c_str());

    }




#endif





    return true;
}


#endif





#if 0
bool CommandMap::SetAuMulticastHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "name");
    if(channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring,"hdmi0")) {      //HDMI0组播设置
            cJSON *au0multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (au0multicast_enable && cJSON_IsNumber(au0multicast_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastEnable, au0multicast_enable->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                aumcast_enable[0] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
            }
            cJSON *au0multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if (au0multicast_ip && cJSON_IsString(au0multicast_ip)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastIp, au0multicast_ip->valuestring) ){
                    COMMON_PRT("set aumcast_ip set failed");
                }
                memset(aumcast_ip[0],0,IPSIZE);
               // sprintf(aumcast_ip[0],"%s",au0multicast_ip->valuestring);
            }
            cJSON *au0multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (au0multicast_port && cJSON_IsNumber(au0multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastPort, au0multicast_port->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                aumcast_port[0] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastPort));
            }
        }
        else if(!strcmp(channel->valuestring,"hdmi1")) { //HDMI1组播设置
            cJSON *au1multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (au1multicast_enable && cJSON_IsNumber(au1multicast_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastEnable, au1multicast_enable->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                aumcast_enable[1] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
            }
            cJSON *au1multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if (au1multicast_ip && cJSON_IsString(au1multicast_ip)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastIp, au1multicast_ip->valuestring) ){
                    COMMON_PRT("set aumcast_ip set failed");
                }
                memset(aumcast_ip[1],0,IPSIZE);
               // sprintf(aumcast_ip[1],"%s",au1multicast_ip->valuestring);
            }
            cJSON *au1multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (au1multicast_port && cJSON_IsNumber(au1multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastPort, au1multicast_port->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                aumcast_port[1] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastPort));
            }
        }
        else if(!strcmp(channel->valuestring,"3.5mm")) { //3.5mm组播设置
            cJSON *au2multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (au2multicast_enable && cJSON_IsNumber(au2multicast_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastEnable, au2multicast_enable->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                aumcast_enable[2] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
            }
            cJSON *au2multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if (au2multicast_ip && cJSON_IsString(au2multicast_ip)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastIp, au2multicast_ip->valuestring) ){
                    COMMON_PRT("set aumcast_ip set failed");
                }
                memset(aumcast_ip[2],0,IPSIZE);
               // sprintf(aumcast_ip[2],"%s",au2multicast_ip->valuestring);
            }
            cJSON *au2multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (au2multicast_port && cJSON_IsNumber(au2multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastPort, au2multicast_port->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                aumcast_port[2] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastPort));
            }
        }
    }
    softwareconfig->SaveConfig();

#if 1
    auto thisini = Singleton<SoftwareConfig>::getInstance();


    int i = 0;



    if(!strcmp(channel->valuestring,"hdmi0")) {
        i = 0;
        aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastPort));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastIp).c_str());
    }
    if(!strcmp(channel->valuestring,"hdmi1")) {
        i = 1;
        aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastPort));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastIp).c_str());

    }
    else if(!strcmp(channel->valuestring,"3.5mm")) {
        i = 2;
        aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
        bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kAnalog_Au_McastPort));
        sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kAnalog_Au_McastIp).c_str());

    }




#endif





    return true;
}
#endif


bool CommandMap::SetMulticastHandler(cJSON *value)
{


    bool nspc = false;
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
//printf("************************************SetMulticastHandler\n");
    cJSON *cmd = cJSON_GetObjectItemCaseSensitive(value, "cmd_frome");
    if(cmd && cJSON_IsString(cmd)) {
        if(!strcmp(cmd->valuestring,"nspc")) {
            nspc = true;
        }
    }




    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){

            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
           // cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");


            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){



                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                               mcast_enable[0] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                mcast_enable[0] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            }
                        }
                       else {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastEnable, multicast_enable->valueint) ){
                            COMMON_PRT("set mcast_enable set failed");
                        }
                        mcast_enable[0] =  multicast_enable->valueint; // str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                        viParam[0].mcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                     }
                   }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }   
                        sprintf(mcast_ip[0], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastIp)).c_str());
                        memset(viParam[0].mcast_ip[0],0,IPSIZE);
                        sprintf(viParam[0].mcast_ip[0],"%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        mcast_port[0] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastPort).c_str());
                        viParam[0].mcast_port[0] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }



                    }

                    cJSON *mcast_num = cJSON_GetObjectItemCaseSensitive(value, "mcast_num");
                    if (mcast_num && cJSON_IsNumber(mcast_num)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastNum, mcast_num->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                        repeat_time[0] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastNum));
                        repeat_time[1] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastNum));
                    }


                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                               mcast_enable[1] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                mcast_enable[1] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                            }
                        }
                       else {
                                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastEnable, multicast_enable->valueint) ){
                            COMMON_PRT("set mcast_enable set failed");
                        }
                        mcast_enable[1] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                        viParam[0].mcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                     }



                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        sprintf(mcast_ip[1], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastIp)).c_str());
                        memset(viParam[0].mcast_ip[1],0,IPSIZE);
                        sprintf(viParam[0].mcast_ip[1], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        mcast_port[1] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastPort).c_str());
                        viParam[0].mcast_port[1] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                    /*cJSON *mcast_num = cJSON_GetObjectItemCaseSensitive(value, "mcast_num");
            if (mcast_num && cJSON_IsNumber(mcast_num)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastNum, mcast_num->valueint) ){
                    COMMON_PRT("set mcast_delay set failed");
                }
                repeat_time[1] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastNum));
            }*/
                }
            }
        }else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){
                        cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                   if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
                        if(nspc) {      //来自NSPC命令
                        if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                           mcast_enable[2] = HI_FALSE;
                        }
                        else {                                      //NSPC打开编码组播，读取UMP设置的状态
                            mcast_enable[2] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastEnable));
                        }
                    }
                   else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastEnable, multicast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    mcast_enable[2] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastEnable));
                  //  viParam[0].mcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                 }

            }





//                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
//                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastEnable, multicast_enable->valueint) ){
//                            COMMON_PRT("set mcast_enable set failed");
//                        }
//                        mcast_enable[2] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastEnable));
//                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }
                        sprintf(mcast_ip[2], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        mcast_port[2] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }

                    cJSON *mcast_num = cJSON_GetObjectItemCaseSensitive(value, "mcast_num");
                    if (mcast_num && cJSON_IsNumber(mcast_num)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastNum, mcast_num->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                        repeat_time[2] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastNum));
                        repeat_time[3] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastNum));
                    }
                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
                        if(nspc) {      //来自NSPC命令
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                mcast_enable[3] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                mcast_enable[3] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            mcast_enable[3] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                            //  viParam[0].mcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                        }

                    }

//                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
//                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastEnable, multicast_enable->valueint) ){
//                            COMMON_PRT("set mcast_enable set failed");
//                        }
//                        mcast_enable[3] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
//                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        sprintf(mcast_ip[3], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        mcast_port[3] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                    /*cJSON *mcast_num = cJSON_GetObjectItemCaseSensitive(value, "mcast_num");
            if (mcast_num && cJSON_IsNumber(mcast_num)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastNum, mcast_num->valueint) ){
                    COMMON_PRT("set mcast_delay set failed");
                }
                repeat_time[1] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastNum));
            }*/
                }
            }
        }else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){

                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
                    if(nspc) {      //来自NSPC命令
                        if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                           mcast_enable[4] = HI_FALSE;
                        }
                        else {                                      //NSPC打开编码组播，读取UMP设置的状态
                            mcast_enable[4] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastEnable));
                        }
                    }
                   else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastEnable, multicast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    mcast_enable[4] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastEnable));
                  //  viParam[0].mcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                 }
                  }
//                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
//                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastEnable, multicast_enable->valueint) ){
//                            COMMON_PRT("set mcast_enable set failed");
//                        }
//                        mcast_enable[4] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastEnable));
//                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }
                        sprintf(mcast_ip[4], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        mcast_port[4] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }

                    cJSON *mcast_num = cJSON_GetObjectItemCaseSensitive(value, "mcast_num");
                    if (mcast_num && cJSON_IsNumber(mcast_num)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastNum, mcast_num->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                        repeat_time[4] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastNum));
                        repeat_time[5] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastNum));
                    }
                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
                        if(nspc) {      //来自NSPC命令
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                mcast_enable[5] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                mcast_enable[5] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            mcast_enable[5] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastEnable));
                            //  viParam[0].mcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                        }

                    }
//                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
//                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastEnable, multicast_enable->valueint) ){
//                            COMMON_PRT("set mcast_enable set failed");
//                        }
//                        mcast_enable[5] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastEnable));
//                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        sprintf(mcast_ip[5], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        mcast_port[5] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                    /*cJSON *mcast_num = cJSON_GetObjectItemCaseSensitive(value, "mcast_num");
            if (mcast_num && cJSON_IsNumber(mcast_num)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastNum, mcast_num->valueint) ){
                    COMMON_PRT("set mcast_delay set failed");
                }
                repeat_time[2] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastNum));
            }*/

                }
            }
        }else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){

                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
                    if(nspc) {      //来自NSPC命令
                        if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                           mcast_enable[6] = HI_FALSE;
                        }
                        else {                                      //NSPC打开编码组播，读取UMP设置的状态
                            mcast_enable[6] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastEnable));
                        }
                    }
                   else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastEnable, multicast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    mcast_enable[6] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastEnable));
                  //  viParam[0].mcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                 }
                }

//                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
//                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastEnable, multicast_enable->valueint) ){
//                            COMMON_PRT("set mcast_enable set failed");
//                        }
//                        mcast_enable[6] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastEnable));
//                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }
                        sprintf(mcast_ip[6], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        mcast_port[6] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }

                    cJSON *mcast_num = cJSON_GetObjectItemCaseSensitive(value, "mcast_num");
                    if (mcast_num && cJSON_IsNumber(mcast_num)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastNum, mcast_num->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                        repeat_time[6] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastNum));
                        repeat_time[7] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastNum));
                    }
                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");

                     if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
                    if(nspc) {      //来自NSPC命令
                        if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                           mcast_enable[7] = HI_FALSE;
                        }
                        else {                                      //NSPC打开编码组播，读取UMP设置的状态
                            mcast_enable[7] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastEnable));
                        }
                    }
                   else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastEnable, multicast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    mcast_enable[7] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastEnable));
                  //  viParam[0].mcast_enable[1] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                 }
                 }



//                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
//                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastEnable, multicast_enable->valueint) ){
//                            COMMON_PRT("set mcast_enable set failed");
//                        }
//                        mcast_enable[7] =  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastEnable));
//                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        sprintf(mcast_ip[7], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        mcast_port[7] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }
            }
        }
    }

    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);

    usleep(2000);
    auto thisini = Singleton<SoftwareConfig>::getInstance();

   int i = 0;
#if 1

   if(!strcmp(channel->valuestring, "chn0")){
        i = 0;
      //  aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
       bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));
        aumcast_port[i] = str2int(thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastPort));
       sprintf(aumcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kHdmi0_Au_McastIp).c_str());


       sprintf(mcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kChn0_M_McastIp).c_str());
      mcast_port[i] =  str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_McastPort));

      sprintf(mcast_ip[i +1],"%s",thisini->GetConfig(SoftwareConfig::kChn0_S0_McastIp).c_str());
      mcast_port[i +1] =  str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_McastPort));

       mcast_delay[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_McastDelay));
       mcast_delay[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_McastDelay));

    }else if(!strcmp(channel->valuestring, "chn1")){

        i = 2;
       // aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
       // memset(aumcast_ip[i],0,IPSIZE);
         bzero(aumcast_ip[i],sizeof(aumcast_ip[i]));


         sprintf(mcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kChn1_M_McastIp).c_str());
        mcast_port[i] =  str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_McastPort));

        sprintf(mcast_ip[i +1],"%s",thisini->GetConfig(SoftwareConfig::kChn1_S0_McastIp).c_str());
        mcast_port[i +1] =  str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_McastPort));




         aumcast_port[i] = mcast_port[i];
         sprintf(aumcast_ip[i],"%s",mcast_ip[i]);
         mcast_delay[2] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_McastDelay));
         mcast_delay[3] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_McastDelay));

      //   printf("1--------venc--aumcast_ip =%s   aumcast_port =%d\n",aumcast_ip[i],mcast_port[i]);
        //  printf("s0-------venc--aumcast_ip =%s   aumcast_port =%d\n",aumcast_ip[i+1],mcast_port[i+1]);

    }
     else if(!strcmp(channel->valuestring, "chn2")){

          i = 4;

           memset(mcast_ip[i],0,IPSIZE);

           sprintf(mcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kChn2_M_McastIp).c_str());
          mcast_port[i] =  str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_McastPort));
          sprintf(mcast_ip[i +1],"%s",thisini->GetConfig(SoftwareConfig::kChn2_S0_McastIp).c_str());
          mcast_port[i +1] =  str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_McastPort));

          mcast_delay[4] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_McastDelay));
          mcast_delay[5] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_McastDelay));




   }



   else if(!strcmp(channel->valuestring, "chn3")){

       i = 6;

        memset(mcast_ip[i],0,IPSIZE);

        sprintf(mcast_ip[i],"%s",thisini->GetConfig(SoftwareConfig::kChn3_M_McastIp).c_str());
       mcast_port[i] =  str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_McastPort));

       sprintf(mcast_ip[i +1],"%s",thisini->GetConfig(SoftwareConfig::kChn3_S0_McastIp).c_str());
       mcast_port[i +1] =  str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_McastPort));

       mcast_delay[6] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_McastDelay));
       mcast_delay[7] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_McastDelay));



   }
#endif








    return true;
}

/*
 * 重启生效抓图设置
 * */
bool CommandMap::SetSnapMulticastHandler(cJSON *value)
{

    bool nspc = false;
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    cJSON *cmd = cJSON_GetObjectItemCaseSensitive(value, "cmd_frome");
    if(cmd && cJSON_IsString(cmd)) {
        if(!strcmp(cmd->valuestring,"nspc")) {
            nspc = true;
        }
    }


#if 1
    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");





            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        veSnap[0].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        veSnap[0].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    veSnap[0].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                }
            }




//                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastEnable, mcast_enable->valueint) ){
//                    COMMON_PRT("set mcast_enable set failed");
//                }
//            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[0].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[0].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");


            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        veSnap[1].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        veSnap[1].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    veSnap[1].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                }
            }



//            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
//                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastEnable, mcast_enable->valueint) ){
//                    COMMON_PRT("set mcast_enable set failed");
//                }
//            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[1].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[1].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");

            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        veSnap[2].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        veSnap[2].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    veSnap[2].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                }
            }



//            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
//                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastEnable, mcast_enable->valueint) ){
//                    COMMON_PRT("set mcast_enable set failed");
//                }
//            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[2].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[2].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");

            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        veSnap[3].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        veSnap[3].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    veSnap[3].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                }
            }




//            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
//                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastEnable, mcast_enable->valueint) ){
//                    COMMON_PRT("set mcast_enable set failed");
//                }
//            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[3].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[3].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
            }
        }
    }
    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);
    pthread_t tid_snap[4];
    HI_U32 vi = 0,snap_port[4];
    HI_U32 snap_[4] = {0,1,2,3};
    bool start_mac[4];
    bool b_detached = true;
    usleep(2000);
    if(!strcmp(channel->valuestring, "chn0")){
        vi = 0;



        start_mac[vi] =  veSnap[vi].Snapmcast_enable;
     //   veSnap[vi].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastEnable));
        sprintf(veSnap[vi].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastIp)).c_str());
        veSnap[vi].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastPort).c_str());
        veSnap[vi].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastFreq).c_str());


//        if((start_mac[vi] != veSnap[vi].Snapmcast_enable ) && (veSnap[vi].Snapmcast_enable == HI_TRUE ))
//            mult_snap[vi] = true;





    }
    else if(!strcmp(channel->valuestring, "chn1")){

        vi = 1;



        start_mac[vi] =  veSnap[vi].Snapmcast_enable;
     //   veSnap[vi].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastEnable));
        sprintf(veSnap[vi].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastIp)).c_str());
        veSnap[vi].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastPort).c_str());
        veSnap[vi].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastFreq).c_str());


//        if((start_mac[vi] != veSnap[vi].Snapmcast_enable ) && (veSnap[vi].Snapmcast_enable == HI_TRUE ))
//            mult_snap[vi] = true;



    }
    else if(!strcmp(channel->valuestring, "chn2")){
        if(edid_4k == 0)
        {
            vi = 2;



            start_mac[vi] =  veSnap[vi].Snapmcast_enable;
          //  veSnap[vi].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastEnable));
            sprintf(veSnap[vi].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastIp)).c_str());
            veSnap[vi].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastPort).c_str());
            veSnap[vi].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastFreq).c_str());


//            if((start_mac[vi] != veSnap[vi].Snapmcast_enable ) && (veSnap[vi].Snapmcast_enable == HI_TRUE ))
//                mult_snap[vi] = true;


        }
    }
     else if(!strcmp(channel->valuestring, "chnxx")){

        vi = 3;

        start_mac[vi] =  veSnap[vi].Snapmcast_enable;
      //  veSnap[vi].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastEnable));
        sprintf(veSnap[vi].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastIp)).c_str());
        veSnap[vi].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastPort).c_str());
        veSnap[vi].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastFreq).c_str());


//        if((start_mac[vi] != veSnap[vi].Snapmcast_enable ) && (veSnap[vi].Snapmcast_enable == HI_TRUE ))
//            mult_snap[vi] = true;

     }


     //    softwareconfig->SaveConfig();
     //    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());
#endif
     return true;
}



bool CommandMap::SetSnapHandler(cJSON *value)
{
    bool udp_reflag = false;
    uint currentPort = 0;
    VENC_CHN  snapChn = 0;
    HI_S32 snapPort = 0;
    SIZE_S viSize;
    SNAPPARAM star_veSnap[4];
    pthread_t snap_id1;
    int i = 0;
    HI_U32 snap_port_1;
    VENC_CHN  chnwho = 0;
    HI_BOOL macsnap[4];
    star_veSnap[0] = veSnap[0];
    star_veSnap[1] = veSnap[1];
    star_veSnap[2] = veSnap[2];
    star_veSnap[3] = veSnap[3];

    this->LoadEchoUdp();

    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");

    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");

            int chn_snap = 0;
            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_w->valueint =   min(snap_w->valueint,(int)viParam[chn_snap].u32W);




            if (snap_w && cJSON_IsNumber(snap_w)) {

                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    veSnap[0].snapSize.u32Width = snap_w->valueint;
                }
            }

            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");

            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_h->valueint =   min(snap_h->valueint,(int)viParam[chn_snap].u32H);



            if (snap_h && cJSON_IsNumber(snap_h)) {



                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                     veSnap[0].snapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
                else {
                    if(veSnap[0].snapPort != snap_port->valueint) {
                        currentPort = veSnap[0].snapPort;
                        veSnap[0].snapPort = snap_port->valueint;
                        udp_reflag = true;
                    }

                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_SnapLen, snap_len->valueint) ){
                    COMMON_PRT("set snap_len set failed");
                }
                else {
                    veSnap[0].snapLeng = snap_len->valueint;
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                    veSnap[0].snapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    veSnap[0].snapQuality = snap_quality->valueint;
                }
            }

            softwareconfig->SaveConfig();
            this->LetMeTellYou(msg);
            usleep(20000);
            if(1){



                 chnwho =0;


                macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;

              //  star_veSnap[chnwho] = veSnap[chnwho];

                 printf("+++++++++++++++snapsizeu32+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

                 veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastEnable));
        //         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

        //       {
        //        mult_snap[chnwho] = false;
        //        snap_runlag[chnwho] = true;
        //       }
        //        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
        //              snap_runlag[chnwho] = true;


         #if 1
                 veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastEnable));

                 veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_Quality));
                 veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapPort).c_str());
                 //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
                 veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_Delay).c_str());

                 veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_W)),
                                          str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_H))};

                 sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastIp)).c_str());
                 veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastPort).c_str());
                 veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastFreq).c_str());

                 printf("+++++++++++++++2++star-h%d+++size-w=%d++++enable==%d\n",star_veSnap[chnwho].snapSize.u32Height,veSnap[chnwho].snapSize.u32Height, veSnap[chnwho].Snapmcast_enable);

             //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
               if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
                   ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
               {

                   return false;
               }
               if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
                 {

                    return false;

                 }


                if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
                   {     snap_runlag[chnwho] = false;
                        mult_snap_runlag[chnwho] = true;
                  }
                  else
                   {
                    snap_runlag[chnwho] = true;
                     mult_snap_runlag[chnwho] = false;
                   }
                   snap_loop[chnwho] = HI_FALSE;
                   usleep(20000);

                  himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


                //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

              //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

                 usleep(10000);


                 if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
                 {

                     if((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) || (veSnap[chnwho].snapSize.u32Height <= 0) ||  (veSnap[chnwho].snapSize.u32Width <= 0))
                     {
                      //  printf("++++++++++++++++++++++++++++a\n");
                     veSnap[chnwho].snapSize={320,180};
                     }
                 }
                 else
                 {
                     if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                             (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                         // printf("++++++++++++++++++++++++++++b\n");

                         veSnap[chnwho].snapSize={320,180};

                     }
                 }
                 if(viParam[chnwho].vi_crop_enable == HI_TRUE)
                 {
                     if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
                     {    veSnap[chnwho].snapSize = {320,180};
                        // printf("++++++++++++++++++++++++++++c\n");

                     }
                 }




                 snap_port_1 = veSnap[chnwho].snapPort;
                 snap_loop[chnwho] = HI_TRUE;
             //    himppmaster->snap_running[chnwho] = HI_TRUE;

           himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

                 if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
                 {
                     snap_runlag[chnwho] = true;
                      mult_snap_runlag[chnwho] = false;
                     snap_id1 = CreateThread(snap0ServerProc, 0, SCHED_FIFO, true, &snap_port_1);
                   if(snap_id1 == 0){
                     COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                   }
                 }
                 else
                 {
                     if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
                     {
                         mult_snap[chnwho] = true;
                         snap_runlag[chnwho] = false;
                        mult_snap_runlag[chnwho] = true;

                         snap_tid[chnwho] = CreateThread(multiSnapProc, 0, SCHED_FIFO, true, &chnwho);
                         if(snap_tid[chnwho] == 0){
                             COMMON_PRT("pthread_create() failed: snapServerProc\n");
                         }
                     }
                 }

                 softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
                 softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
                 softwareconfig->SaveConfig();


                 usleep(2000);
         #endif
             }


        }
        if(!strcmp(channel->valuestring, "chn1")){


            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");

         //   int chn_i = 1;

            int chn_snap = 1;
            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_w->valueint =   min(snap_w->valueint,(int)viParam[chn_snap].u32W);



            if((viParam[chn_snap].u32ViWidth == 0) || (viParam[chn_snap].u32ViHeigth == 0))
                 snap_w->valueint = min(snap_w->valueint,1920);
            else
                  snap_w->valueint = min(snap_w->valueint,(int)viParam[chn_snap].u32ViWidth);

            if (snap_w && cJSON_IsNumber(snap_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    veSnap[1].snapSize.u32Width = snap_w->valueint;
                }
            }

            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");

            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_h->valueint =   min(snap_h->valueint,(int)viParam[chn_snap].u32H);


            if((viParam[chn_snap].u32ViWidth == 0) || (viParam[chn_snap].u32ViHeigth == 0))
               snap_h->valueint = min(snap_h->valueint,1080);
            else
                  snap_h->valueint = min(snap_h->valueint,(int)viParam[chn_snap].u32ViHeigth);


            if (snap_h && cJSON_IsNumber(snap_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                    veSnap[1].snapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
                else {
                    if(veSnap[1].snapPort != snap_port->valueint) {
                        currentPort = veSnap[1].snapPort;
                        veSnap[1].snapPort = snap_port->valueint;
                        udp_reflag = true;
                    }

                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_SnapLen, snap_len->valueint) ){
                    COMMON_PRT("set snap_len set failed");
                }
                else {
                    veSnap[1].snapLeng = snap_len->valueint;
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                    veSnap[1].snapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    veSnap[1].snapQuality = snap_quality->valueint;
                }
            }

            softwareconfig->SaveConfig();
            this->LetMeTellYou(msg);
            usleep(20000);

            if(1){



                  chnwho =1;


                 macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;


            //star_veSnap[chnwho] = veSnap[chnwho];
                  printf("+++++++++++++++snapsizeu32++++++chnwho=%d+++enable==%d\n", chnwho,veSnap[chnwho].Snapmcast_enable);

                  veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastEnable));
         //         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

         //       {
         //        mult_snap[chnwho] = false;
         //        snap_runlag[chnwho] = true;
         //       }
         //        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
         //              snap_runlag[chnwho] = true;


          #if 1
                  veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastEnable));

                  veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_Quality));
                  veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn1_SnapPort).c_str());
                  //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
                  veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_Delay).c_str());

                  veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_W)),
                                           str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_H))};

                  sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastIp)).c_str());
                  veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastPort).c_str());
                  veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastFreq).c_str());

          printf("+++++++++++++++1+++++++++snap_h==%d------star-h=%d\n", veSnap[chnwho].snapSize.u32Height,star_veSnap[chnwho].snapSize.u32Height);

              //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
                if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
                    ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
                {

                    return false;
                }
                if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
                  {

                     return false;

                  }


                 if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
                    {     snap_runlag[chnwho] = false;
                         mult_snap_runlag[chnwho] = true;
                   }
                   else
                    {
                     snap_runlag[chnwho] = true;
                      mult_snap_runlag[chnwho] = false;
                    }
                    snap_loop[chnwho] = HI_FALSE;
                    usleep(20000);

                   himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


                 //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

               //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

                  usleep(10000);

           printf("+++++++++++++++2+++++++++snap_h==%d----w=%d\n", veSnap[chnwho].snapSize.u32Height,veSnap[chnwho].snapSize.u32Width);

                 HI_U32 snap_0  = 0;
                  if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
                  {




                      if(((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) ))//|| ((veSnap[chnwho].snapSize.u32Height < snap_0) ||  (veSnap[chnwho].snapSize.u32Width < snap_0)));
                      {
                          printf("++++++++++++++++++++++++++++a\n");
                         veSnap[chnwho].snapSize={320,180};
                      }
                     if(((veSnap[chnwho].snapSize.u32Height == 0 )|| (veSnap[chnwho].snapSize.u32Width ==0 ) ))
                     {
                         veSnap[chnwho].snapSize={320,180};

                     }
                   }
                  else
                  {
                      if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                              (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                           printf("++++++++++++++++++++++++++++b\n");

                          veSnap[chnwho].snapSize={320,180};

                      }
                  }
                  if(viParam[chnwho].vi_crop_enable == HI_TRUE)
                  {
                      if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
                      {    veSnap[chnwho].snapSize = {320,180};

                      }
                  }




                  snap_port_1 = veSnap[chnwho].snapPort;
                  snap_loop[chnwho] = HI_TRUE;
              //    himppmaster->snap_running[chnwho] = HI_TRUE;

            himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

                  if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
                  {
                      printf("????????????????????????a\n");
                      snap_runlag[chnwho] = true;
                       mult_snap_runlag[chnwho] = false;
                      snap_id1 = CreateThread(snap1ServerProc, 0, SCHED_FIFO, true, &snap_port_1);
                    if(snap_id1 == 0){
                      COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                    }
                  }
                  else
                  {
                      if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
                      {
                                 printf("????????????????????????b\n");
                          mult_snap[chnwho] = true;
                          snap_runlag[chnwho] = false;
                         mult_snap_runlag[chnwho] = true;

                          snap_tid[chnwho] = CreateThread(multiSnapProc1, 0, SCHED_FIFO, true, &chnwho);
                          if(snap_tid[chnwho] == 0){
                              COMMON_PRT("pthread_create() failed: snapServerProc\n");
                          }
                      }
                  }

                  softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
                  softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
                  softwareconfig->SaveConfig();



                  usleep(2000);
          #endif
              }


        }
        if(!strcmp(channel->valuestring, "chn2")){
            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");


            int chn_snap = 2;
            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_w->valueint =   min(snap_w->valueint,(int)viParam[chn_snap].u32W);



            if((viParam[chn_snap].u32ViWidth == 0) || (viParam[chn_snap].u32ViHeigth == 0))
                 snap_w->valueint = min(snap_w->valueint,1920);
            else
                  snap_w->valueint = min(snap_w->valueint,(int)viParam[chn_snap].u32ViWidth);



            if (snap_w && cJSON_IsNumber(snap_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    veSnap[2].snapSize.u32Width = snap_w->valueint;
                }
            }

            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");

            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_h->valueint =   min(snap_h->valueint,(int)viParam[chn_snap].u32H);


            if((viParam[chn_snap].u32ViWidth == 0) || (viParam[chn_snap].u32ViHeigth == 0))
               snap_h->valueint = min(snap_h->valueint,1080);
            else
                  snap_h->valueint = min(snap_h->valueint,(int)viParam[chn_snap].u32ViHeigth);



            if (snap_h && cJSON_IsNumber(snap_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                    veSnap[2].snapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
                else {
                    if(veSnap[2].snapPort != snap_port->valueint) {
                        currentPort = veSnap[2].snapPort;
                        veSnap[2].snapPort = snap_port->valueint;
                        udp_reflag = true;
                    }

                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_SnapLen, snap_len->valueint) ){
                    COMMON_PRT("set snap_len set failed");
                }
                else {
                    veSnap[2].snapLeng = snap_len->valueint;
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                    veSnap[2].snapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    veSnap[2].snapQuality = snap_quality->valueint;
                }
            }

            softwareconfig->SaveConfig();
            this->LetMeTellYou(msg);
             if(1){


                if(edid_4k == 1)
                    return false;


                 chnwho = 2;


                macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;



                // printf("+++++++++++++++snapsizeu32+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

                 veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastEnable));
        //         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

        //       {
        //        mult_snap[chnwho] = false;
        //        snap_runlag[chnwho] = true;
        //       }
        //        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
        //              snap_runlag[chnwho] = true;


         #if 1
                 veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastEnable));

                 veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_Quality));
                 veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn2_SnapPort).c_str());
                 //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
                 veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_Delay).c_str());

                 veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_W)),
                                          str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_H))};

                 sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastIp)).c_str());
                 veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastPort).c_str());
                 veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastFreq).c_str());

                 //printf("+++++++++++++++2+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

             //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
               if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
                   ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
               {

                   return false;
               }
               if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
                 {

                    return false;

                 }


                if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
                   {     snap_runlag[chnwho] = false;
                        mult_snap_runlag[chnwho] = true;
                  }
                  else
                   {
                    snap_runlag[chnwho] = true;
                     mult_snap_runlag[chnwho] = false;
                   }
                   snap_loop[chnwho] = HI_FALSE;
                   usleep(20000);

                  himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


                //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

              //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

                 usleep(10000);


                 if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
                 {

                     if((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) || (veSnap[chnwho].snapSize.u32Height <= 0) ||  (veSnap[chnwho].snapSize.u32Width <= 0))
                     {
                      //   printf("++++++++++++++++++++++++++++a\n");
                     veSnap[chnwho].snapSize={320,180};
                     }
                 }
                 else
                 {
                     if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                             (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                          printf("++++++++++++++++++++++++++++b\n");

                         veSnap[chnwho].snapSize={320,180};

                     }
                 }
                 if(viParam[chnwho].vi_crop_enable == HI_TRUE)
                 {
                     if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
                     {    veSnap[chnwho].snapSize = {320,180};

                     }
                 }




                 snap_port_1 = veSnap[chnwho].snapPort;
                 snap_loop[chnwho] = HI_TRUE;
             //    himppmaster->snap_running[chnwho] = HI_TRUE;

           himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

                 if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
                 {
                     snap_runlag[chnwho] = true;
                      mult_snap_runlag[chnwho] = false;
                     snap_id1 = CreateThread(snap2ServerProc, 0, SCHED_FIFO, true, &snap_port_1);
                   if(snap_id1 == 0){
                     COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                   }
                 }
                 else
                 {
                     if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
                     {
                         mult_snap[chnwho] = true;
                         snap_runlag[chnwho] = false;
                        mult_snap_runlag[chnwho] = true;

                         snap_tid[chnwho] = CreateThread(multiSnapProc2, 0, SCHED_FIFO, true, &chnwho);
                         if(snap_tid[chnwho] == 0){
                             COMMON_PRT("pthread_create() failed: snapServerProc\n");
                         }
                     }
                 }

                 softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
                 softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
                 softwareconfig->SaveConfig();




                 usleep(2000);
         #endif
             }



        }
        if(!strcmp(channel->valuestring, "chn3")){
            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");

            int chn_snap = 3;
            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_w->valueint =   min(snap_w->valueint,(int)viParam[chn_snap].u32W);



            if((viParam[chn_snap].u32ViWidth == 0) || (viParam[chn_snap].u32ViHeigth == 0))
                 snap_w->valueint = min(snap_w->valueint,1920);
            else
                  snap_w->valueint = min(snap_w->valueint,(int)viParam[chn_snap].u32ViWidth);



            if (snap_w && cJSON_IsNumber(snap_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    veSnap[3].snapSize.u32Width = snap_w->valueint;
                }
            }

            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");

            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_h->valueint =   min(snap_h->valueint,(int)viParam[chn_snap].u32H);


            if((viParam[chn_snap].u32ViWidth == 0) || (viParam[chn_snap].u32ViHeigth == 0))
               snap_h->valueint = min(snap_h->valueint,1080);
            else
                  snap_h->valueint = min(snap_h->valueint,(int)viParam[chn_snap].u32ViHeigth);



            if(viParam[chn_snap].vi_crop_enable == HI_TRUE)
              snap_h->valueint =   min(snap_h->valueint,(int)viParam[chn_snap].u32H);


            if((viParam[chn_snap].u32ViWidth == 0) || (viParam[chn_snap].u32ViHeigth == 0))
               snap_h->valueint = min(snap_h->valueint,1080);
            else
                  snap_h->valueint = min(snap_h->valueint,(int)viParam[chn_snap].u32ViHeigth);


            if (snap_h && cJSON_IsNumber(snap_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                    veSnap[3].snapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
                else {
                    if(veSnap[3].snapPort != snap_port->valueint) {
                        currentPort = veSnap[3].snapPort;
                        veSnap[3].snapPort = snap_port->valueint;
                        udp_reflag = true;
                    }

                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_SnapLen, snap_len->valueint) ){
                    COMMON_PRT("set snap_len set failed");
                }
                else {
                    veSnap[3].snapLeng = snap_len->valueint;
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                    veSnap[3].snapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    veSnap[3].snapQuality = snap_quality->valueint;
                }
            }

            softwareconfig->SaveConfig();
            this->LetMeTellYou(msg);
            usleep(20000);
             if(-1){



                 chnwho =3;


                macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;

               // star_veSnap[chnwho] = veSnap[chnwho];

                // printf("+++++++++++++++snapsizeu32+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

                 veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastEnable));
        //         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

        //       {
        //        mult_snap[chnwho] = false;
        //        snap_runlag[chnwho] = true;
        //       }
        //        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
        //              snap_runlag[chnwho] = true;


         #if 1


                 veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_Quality));
                 veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn3_SnapPort).c_str());
                 //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
                 veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_Delay).c_str());

                 veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_W)),
                                          str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_H))};

                 sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastIp)).c_str());
                 veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastPort).c_str());
                 veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastFreq).c_str());

                 //printf("+++++++++++++++2+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

             //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
               if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
                   ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
               {

                   return false;
               }
               if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
                 {

                    return false;

                 }


                if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
                   {     snap_runlag[chnwho] = false;
                        mult_snap_runlag[chnwho] = true;
                  }
                  else
                   {
                    snap_runlag[chnwho] = true;
                     mult_snap_runlag[chnwho] = false;
                   }
                   snap_loop[chnwho] = HI_FALSE;
                   usleep(20000);

                  himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


                //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

              //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

                 usleep(10000);


                 if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
                 {

                     if((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) || (veSnap[chnwho].snapSize.u32Height <= 0) ||  (veSnap[chnwho].snapSize.u32Width <= 0))
                     {
                      //   printf("++++++++++++++++++++++++++++a\n");
                     veSnap[chnwho].snapSize={320,180};
                     }
                 }
                 else
                 {
                     if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                             (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                         // printf("++++++++++++++++++++++++++++b\n");

                         veSnap[chnwho].snapSize={320,180};

                     }
                 }
                 if(viParam[chnwho].vi_crop_enable == HI_TRUE)
                 {
                     if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
                     {    veSnap[chnwho].snapSize = {320,180};

                     }
                 }




                 snap_port_1 = veSnap[chnwho].snapPort;
                 snap_loop[chnwho] = HI_TRUE;
             //    himppmaster->snap_running[chnwho] = HI_TRUE;

           himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

                 if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
                 {
                     snap_runlag[chnwho] = true;
                      mult_snap_runlag[chnwho] = false;
                     snap_id1 = CreateThread(snap3ServerProc, 0, SCHED_FIFO, true, &snap_port_1);
                   if(snap_id1 == 0){
                     COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                   }
                 }
                 else
                 {
                     if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
                     {
                         mult_snap[chnwho] = true;
                         snap_runlag[chnwho] = false;
                        mult_snap_runlag[chnwho] = true;

                         snap_tid[chnwho] = CreateThread(multiSnapProc3, 0, SCHED_FIFO, true, &chnwho);
                         if(snap_tid[chnwho] == 0){
                             COMMON_PRT("pthread_create() failed: snapServerProc\n");
                         }
                     }
                 }

                 softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
                 softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
                 softwareconfig->SaveConfig();


                 usleep(2000);
         #endif
             }


        }
    }



    usleep(2000);



#if 0

    if(!strcmp(channel->valuestring, "chn0")){



         chnwho =0;


        macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;

      //  star_veSnap[chnwho] = veSnap[chnwho];

         printf("+++++++++++++++snapsizeu32+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

         veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastEnable));
//         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

//       {
//        mult_snap[chnwho] = false;
//        snap_runlag[chnwho] = true;
//       }
//        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
//              snap_runlag[chnwho] = true;


 #if 1
         veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastEnable));

         veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_Quality));
         veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapPort).c_str());
         //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
         veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_Delay).c_str());

         veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_W)),
                                  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_H))};

         sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastIp)).c_str());
         veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastPort).c_str());
         veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastFreq).c_str());

         printf("+++++++++++++++2++star-h%d+++size-w=%d++++enable==%d\n",star_veSnap[chnwho].snapSize.u32Height,veSnap[chnwho].snapSize.u32Height, veSnap[chnwho].Snapmcast_enable);

     //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
       if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
           ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
       {

           return false;
       }
       if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
         {

            return false;

         }


        if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
           {     snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;
          }
          else
           {
            snap_runlag[chnwho] = true;
             mult_snap_runlag[chnwho] = false;
           }
           snap_loop[chnwho] = HI_FALSE;
           usleep(20000);

          himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


        //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

      //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

         usleep(10000);


         if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
         {

             if((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) || (veSnap[chnwho].snapSize.u32Height <= 0) ||  (veSnap[chnwho].snapSize.u32Width <= 0))
             {
                printf("++++++++++++++++++++++++++++a\n");
             veSnap[chnwho].snapSize={320,180};
             }
         }
         else
         {
             if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                     (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                  printf("++++++++++++++++++++++++++++b\n");

                 veSnap[chnwho].snapSize={320,180};

             }
         }
         if(viParam[chnwho].vi_crop_enable == HI_TRUE)
         {
             if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
             {    veSnap[chnwho].snapSize = {320,180};
                 printf("++++++++++++++++++++++++++++c\n");

             }
         }




         snap_port = veSnap[chnwho].snapPort;
         snap_loop[chnwho] = HI_TRUE;
     //    himppmaster->snap_running[chnwho] = HI_TRUE;

   himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

         if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
         {
             snap_runlag[chnwho] = true;
              mult_snap_runlag[chnwho] = false;
             snap_id1 = CreateThread(snap0ServerProc, 0, SCHED_FIFO, true, &snap_port);
           if(snap_id1 == 0){
             COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
           }
         }
         else
         {
             if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
             {
                 mult_snap[chnwho] = true;
                 snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;

                 snap_tid[chnwho] = CreateThread(multiSnapProc, 0, SCHED_FIFO, true, &chnwho);
                 if(snap_tid[chnwho] == 0){
                     COMMON_PRT("pthread_create() failed: snapServerProc\n");
                 }
             }
         }

         softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
         softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
         softwareconfig->SaveConfig();


         usleep(2000);
 #endif
     }
   else if(!strcmp(channel->valuestring, "chn1")){



         chnwho =1;


        macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;


   //star_veSnap[chnwho] = veSnap[chnwho];
         printf("+++++++++++++++snapsizeu32++++++chnwho=%d+++enable==%d\n", chnwho,veSnap[chnwho].Snapmcast_enable);

         veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastEnable));
//         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

//       {
//        mult_snap[chnwho] = false;
//        snap_runlag[chnwho] = true;
//       }
//        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
//              snap_runlag[chnwho] = true;


 #if 1
         veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastEnable));

         veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_Quality));
         veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn1_SnapPort).c_str());
         //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
         veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_Delay).c_str());

         veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_W)),
                                  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_H))};

         sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastIp)).c_str());
         veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastPort).c_str());
         veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastFreq).c_str());

 printf("+++++++++++++++1+++++++++snap_h==%d------star-h=%d\n", veSnap[chnwho].snapSize.u32Height,star_veSnap[chnwho].snapSize.u32Height);

     //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
       if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
           ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
       {

           return false;
       }
       if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
         {

            return false;

         }


        if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
           {     snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;
          }
          else
           {
            snap_runlag[chnwho] = true;
             mult_snap_runlag[chnwho] = false;
           }
           snap_loop[chnwho] = HI_FALSE;
           usleep(20000);

          himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


        //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

      //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

         usleep(10000);

  printf("+++++++++++++++2+++++++++snap_h==%d----w=%d\n", veSnap[chnwho].snapSize.u32Height,veSnap[chnwho].snapSize.u32Width);

        HI_U32 snap_0  = 0;
         if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
         {




             if(((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) ))//|| ((veSnap[chnwho].snapSize.u32Height < snap_0) ||  (veSnap[chnwho].snapSize.u32Width < snap_0)));
             {
                 printf("++++++++++++++++++++++++++++a\n");
                veSnap[chnwho].snapSize={320,180};
             }
            if(((veSnap[chnwho].snapSize.u32Height == 0 )|| (veSnap[chnwho].snapSize.u32Width ==0 ) ))
            {
                veSnap[chnwho].snapSize={320,180};

            }
          }
         else
         {
             if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                     (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                  printf("++++++++++++++++++++++++++++b\n");

                 veSnap[chnwho].snapSize={320,180};

             }
         }
         if(viParam[chnwho].vi_crop_enable == HI_TRUE)
         {
             if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
             {    veSnap[chnwho].snapSize = {320,180};

             }
         }




         snap_port = veSnap[chnwho].snapPort;
         snap_loop[chnwho] = HI_TRUE;
     //    himppmaster->snap_running[chnwho] = HI_TRUE;

   himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

         if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
         {
             printf("????????????????????????a\n");
             snap_runlag[chnwho] = true;
              mult_snap_runlag[chnwho] = false;
             snap_id1 = CreateThread(snap1ServerProc, 0, SCHED_FIFO, true, &snap_port);
           if(snap_id1 == 0){
             COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
           }
         }
         else
         {
             if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
             {
                        printf("????????????????????????b\n");
                 mult_snap[chnwho] = true;
                 snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;

                 snap_tid[chnwho] = CreateThread(multiSnapProc1, 0, SCHED_FIFO, true, &chnwho);
                 if(snap_tid[chnwho] == 0){
                     COMMON_PRT("pthread_create() failed: snapServerProc\n");
                 }
             }
         }

         softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
         softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
         softwareconfig->SaveConfig();



         usleep(2000);
 #endif
     }
    else if(!strcmp(channel->valuestring, "chn2")){


        if(edid_4k == 1)
            return false;


         chnwho = 2;


        macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;



        // printf("+++++++++++++++snapsizeu32+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

         veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastEnable));
//         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

//       {
//        mult_snap[chnwho] = false;
//        snap_runlag[chnwho] = true;
//       }
//        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
//              snap_runlag[chnwho] = true;


 #if 1
         veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastEnable));

         veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_Quality));
         veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn2_SnapPort).c_str());
         //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
         veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_Delay).c_str());

         veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_W)),
                                  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_H))};

         sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastIp)).c_str());
         veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastPort).c_str());
         veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastFreq).c_str());

         //printf("+++++++++++++++2+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

     //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
       if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
           ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
       {

           return false;
       }
       if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
         {

            return false;

         }


        if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
           {     snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;
          }
          else
           {
            snap_runlag[chnwho] = true;
             mult_snap_runlag[chnwho] = false;
           }
           snap_loop[chnwho] = HI_FALSE;
           usleep(20000);

          himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


        //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

      //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

         usleep(10000);


         if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
         {

             if((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) || (veSnap[chnwho].snapSize.u32Height <= 0) ||  (veSnap[chnwho].snapSize.u32Width <= 0))
             {
              //   printf("++++++++++++++++++++++++++++a\n");
             veSnap[chnwho].snapSize={320,180};
             }
         }
         else
         {
             if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                     (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                  printf("++++++++++++++++++++++++++++b\n");

                 veSnap[chnwho].snapSize={320,180};

             }
         }
         if(viParam[chnwho].vi_crop_enable == HI_TRUE)
         {
             if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
             {    veSnap[chnwho].snapSize = {320,180};

             }
         }




         snap_port = veSnap[chnwho].snapPort;
         snap_loop[chnwho] = HI_TRUE;
     //    himppmaster->snap_running[chnwho] = HI_TRUE;

   himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

         if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
         {
             snap_runlag[chnwho] = true;
              mult_snap_runlag[chnwho] = false;
             snap_id1 = CreateThread(snap2ServerProc, 0, SCHED_FIFO, true, &snap_port);
           if(snap_id1 == 0){
             COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
           }
         }
         else
         {
             if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
             {
                 mult_snap[chnwho] = true;
                 snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;

                 snap_tid[chnwho] = CreateThread(multiSnapProc2, 0, SCHED_FIFO, true, &chnwho);
                 if(snap_tid[chnwho] == 0){
                     COMMON_PRT("pthread_create() failed: snapServerProc\n");
                 }
             }
         }

         softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
         softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
         softwareconfig->SaveConfig();




         usleep(2000);
 #endif
     }
    else if(!strcmp(channel->valuestring, "chn3")){



         chnwho =3;


        macsnap[chnwho] = veSnap[chnwho].Snapmcast_enable;

       // star_veSnap[chnwho] = veSnap[chnwho];

        // printf("+++++++++++++++snapsizeu32+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

         veSnap[chnwho].Snapmcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastEnable));
//         if((veSnap[chnwho].Snapmcast_enable == HI_TRUE)  &&(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable ))

//       {
//        mult_snap[chnwho] = false;
//        snap_runlag[chnwho] = true;
//       }
//        else if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
//              snap_runlag[chnwho] = true;


 #if 1


         veSnap[chnwho].snapQuality= str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_Quality));
         veSnap[chnwho].snapPort = str2int( softwareconfig->GetConfig(SoftwareConfig::kChn3_SnapPort).c_str());
         //veSnap[chnwho].snapLeng = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_SnapLen).c_str());
         veSnap[chnwho].snapDelay = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_Delay).c_str());

         veSnap[chnwho].snapSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_W)),
                                  str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_H))};

         sprintf(veSnap[chnwho].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastIp)).c_str());
         veSnap[chnwho].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastPort).c_str());
         veSnap[chnwho].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastFreq).c_str());

         //printf("+++++++++++++++2+++++++++enable==%d\n", veSnap[chnwho].Snapmcast_enable);

     //    if((star_veSnap[chnwho].snapDelay == veSnap[chnwho].snapDelay)&&(star_veSnap[chnwho].snapLeng))
       if((star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) &&
           ( star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) &&(veSnap[chnwho].Snapmcast_enable == HI_TRUE))
       {

           return false;
       }
       if(  (star_veSnap[chnwho].Snapmcast_enable == veSnap[chnwho].Snapmcast_enable) && (veSnap[chnwho].Snapmcast_enable == HI_FALSE) &&(star_veSnap[chnwho].snapSize.u32Height == veSnap[chnwho].snapSize.u32Height)&&(star_veSnap[chnwho].snapSize.u32Width == veSnap[chnwho].snapSize.u32Width) )
         {

            return false;

         }


        if(veSnap[chnwho].Snapmcast_enable == HI_TRUE)
           {     snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;
          }
          else
           {
            snap_runlag[chnwho] = true;
             mult_snap_runlag[chnwho] = false;
           }
           snap_loop[chnwho] = HI_FALSE;
           usleep(20000);

          himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );


        //himppmaster->VencModuleDestroy( chnwho + 8,chnwho + 9);

      //   SAMPLE_COMM_VENC_UnBindVpss(chnwho +8, chnwho,0);

         usleep(10000);


         if((viParam[chnwho].u32ViHeigth == 0 )|| (viParam[chnwho].u32ViWidth == 0))
         {

             if((veSnap[chnwho].snapSize.u32Height > 1080 )|| (veSnap[chnwho].snapSize.u32Width > 1920 ) || (veSnap[chnwho].snapSize.u32Height <= 0) ||  (veSnap[chnwho].snapSize.u32Width <= 0))
             {
              //   printf("++++++++++++++++++++++++++++a\n");
             veSnap[chnwho].snapSize={320,180};
             }
         }
         else
         {
             if((veSnap[chnwho].snapSize.u32Height > viParam[chnwho].u32ViHeigth ) || (veSnap[chnwho].snapSize.u32Width > viParam[chnwho].u32ViWidth ) ||
                     (veSnap[chnwho].snapSize.u32Height <= 180) || (veSnap[chnwho].snapSize.u32Width <=320 )){
                 // printf("++++++++++++++++++++++++++++b\n");

                 veSnap[chnwho].snapSize={320,180};

             }
         }
         if(viParam[chnwho].vi_crop_enable == HI_TRUE)
         {
             if((viParam[chnwho].u32W < veSnap[chnwho].snapSize.u32Width)  || (viParam[chnwho].u32H < veSnap[chnwho].snapSize.u32Height))
             {    veSnap[chnwho].snapSize = {320,180};

             }
         }




         snap_port = veSnap[chnwho].snapPort;
         snap_loop[chnwho] = HI_TRUE;
     //    himppmaster->snap_running[chnwho] = HI_TRUE;

   himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnwho].snapSize,chnwho);

         if(veSnap[chnwho].Snapmcast_enable == HI_FALSE)
         {
             snap_runlag[chnwho] = true;
              mult_snap_runlag[chnwho] = false;
             snap_id1 = CreateThread(snap3ServerProc, 0, SCHED_FIFO, true, &snap_port);
           if(snap_id1 == 0){
             COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
           }
         }
         else
         {
             if(macsnap[chnwho] != veSnap[chnwho].Snapmcast_enable)
             {
                 mult_snap[chnwho] = true;
                 snap_runlag[chnwho] = false;
                mult_snap_runlag[chnwho] = true;

                 snap_tid[chnwho] = CreateThread(multiSnapProc3, 0, SCHED_FIFO, true, &chnwho);
                 if(snap_tid[chnwho] == 0){
                     COMMON_PRT("pthread_create() failed: snapServerProc\n");
                 }
             }
         }

         softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_W, int2str(veSnap[chnwho].snapSize.u32Width));
         softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_H, int2str(veSnap[chnwho].snapSize.u32Height));
         softwareconfig->SaveConfig();


         usleep(2000);
 #endif
     }
     else
         printf("have'nt \n");

#endif







    return true;
}
#if 0
bool CommandMap::SetBasicConfigHandler(cJSON *value)
{

    //节点名称
    cJSON *node_name = cJSON_GetObjectItemCaseSensitive(value, "node_name");
    if (node_name && cJSON_IsString(node_name)) {
        char namefile[30] = "";
        sprintf(namefile, "%s/%s", DATA_FILE, NODE_NAME_FILE);
        FILE *f = fopen( namefile, "wb" );
        if(f){
            fprintf(f, "%s\n", node_name->valuestring);
            fflush(f);
            fclose(f);
            f = NULL;
        }
    }

    //组播PTS  UMP指定值
    cJSON *mcast_diff = cJSON_GetObjectItemCaseSensitive(value, "mcast_diff");
        if (mcast_diff && cJSON_IsNumber(mcast_diff)) {
            diff_index = mcast_diff->valueint;
        }

    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    /*cJSON *venc_num = cJSON_GetObjectItemCaseSensitive(value, "venc_num");
    if (venc_num && cJSON_IsNumber(venc_num)) {
        if(venc_num->valueint < 1)
            venc_num->valueint = 1;
        else if(venc_num->valueint > 3)
            venc_num->valueint = 3;
        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_NUM, venc_num->valueint) ){
            COMMON_PRT("set venc_num set failed");
        }
    }*/

//    cJSON *edid_4k = cJSON_GetObjectItemCaseSensitive(value, "4k_edid_enable");
//    if (edid_4k && cJSON_IsNumber(edid_4k)) {
//        if( !softwareconfig->SetConfig(SoftwareConfig::kEdid_4k, edid_4k->valueint) ){
//            COMMON_PRT("set edid_4k set failed");
//        }
//    }
    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);

    return true;
}
#endif
bool CommandMap::SetViCropHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");

            int i = 0;

            if(crop_w->valueint > viParam[i].u32ViWidth)
                crop_w->valueint = viParam[i].u32ViWidth;


            int Align_4 = crop_w->valueint % 2;
             if(Align_4 != 0)
                crop_w->valueint = crop_w->valueint + Align_4;



            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
            }

            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");

            if(crop_h->valueint > viParam[i].u32ViHeigth)
            crop_h->valueint = viParam[i].u32ViHeigth;


              Align_4 = crop_h->valueint % 2;
             if(Align_4 != 0)
                crop_h->valueint = crop_h->valueint + Align_4;



            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
            }
        }else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");
            int i = 1;

            if(crop_w->valueint > viParam[i].u32ViWidth)
                crop_w->valueint = viParam[i].u32ViWidth;


            int Align_4 = crop_w->valueint % 2;
             if(Align_4 != 0)
                crop_w->valueint = crop_w->valueint + Align_4;


            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
            }




            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");

            if(crop_h->valueint > viParam[i].u32ViHeigth)
            crop_h->valueint = viParam[i].u32ViHeigth;


            Align_4 = crop_h->valueint % 2;
           if(Align_4 != 0)
              crop_h->valueint = crop_h->valueint + Align_4;

            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
            }
        }else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");
            int i = 2;

            if(crop_w->valueint > viParam[i].u32ViWidth)
                crop_w->valueint = viParam[i].u32ViWidth;

            int Align_4 = crop_w->valueint % 2;
             if(Align_4 != 0)
                crop_w->valueint = crop_w->valueint + Align_4;



            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
            }

            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");
            if(crop_h->valueint > viParam[i].u32ViHeigth)
                crop_h->valueint = viParam[i].u32ViHeigth;

            Align_4 = crop_h->valueint % 2;
           if(Align_4 != 0)
              crop_h->valueint = crop_h->valueint + Align_4;

            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
            }
        }else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");
            int i = 3;

            if(crop_w->valueint > viParam[i].u32ViWidth)
                crop_w->valueint = viParam[i].u32ViWidth;

            int Align_4 = crop_w->valueint % 2;
             if(Align_4 != 0)
                crop_w->valueint = crop_w->valueint + Align_4;


            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
            }

            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");
            if(crop_h->valueint > viParam[i].u32ViHeigth)
                crop_h->valueint = viParam[i].u32ViHeigth;

            Align_4 = crop_h->valueint % 2;
           if(Align_4 != 0)
              crop_h->valueint = crop_h->valueint + Align_4;

            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
            }
        }
    }




  auto himppmaster = Singleton<HimppMaster>::getInstance();

    softwareconfig->SaveConfig();

    this->LetMeTellYou(msg);

   usleep(2000);

    VI_DEV viDev,videv;
    VI_CHN viChn ;
    RECT_S CapRect ,CapRect1 ;
    SIZE_S viRect ,viRect1 ,CmpSize;
   // SAMPLE_VI_MODE_E gEnViMode = SAMPLE_VI_MODE_4_1080P;

     int Cmp_h,Cmp_w;

    VIDEO_NORM_E gEnNorm = VIDEO_ENCODING_MODE_NTSC ;





    HI_S32 j = 0,chnWho = 0 ,i = 0;;

    if(!strcmp(channel->valuestring, "chn0")){


        chnWho = 0;
        viDev =  1;
        viChn =  (viDev * 4);
         viParam[chnWho].vi_crop_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Crop_Enable));



         if((viParam[chnWho].u32ViHeigth != 0 ) &&  (viParam[chnWho].u32ViWidth != 0))
         {

             viParam[chnWho].u32ViWidth   =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn0_VI_W));
             viParam[chnWho].u32ViHeigth  =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn0_VI_H));
             viParam[chnWho].u32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_X));
             viParam[chnWho].u32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Y));
             viParam[chnWho].u32W = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Width));
             viParam[chnWho].u32H = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_VI_Height));
             viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_VENC_W)),
                                            str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_VENC_H))};
             viParam[chnWho].stMinorSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_VENC_W)),
                                            str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_VENC_H))};

             viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_VENC_Type));
             viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_VENC_Type));
             viParam[chnWho].u32DstFrameRate[0] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_DstFrmRate));


             if((viParam[chnWho].u32X == 0) && (viParam[chnWho].u32Y == 0)&&(viParam[chnWho].u32W == 0)&& (viParam[chnWho].u32H == 0))
             {
                 return  false;
             }



                for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
                    crtsps_closestream(g_stream[i]);
                     printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
                }
                   l_startloop[chnWho] = false;


          usleep(30000);


          himppmaster->VencModuleDestroy_1(chnWho* 2,chnWho* 2+2,-1);
             himppmaster->StopViModule(viDev,viChn);






        printf("viparam--w=%d h=%d ---u32w=%d h=%d \n",viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth,
               viParam[chnWho].u32W,viParam[chnWho].u32H);



        if( viParam[chnWho].vi_crop_enable == HI_TRUE) {




            if((viParam[chnWho].u32X + viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth)&&(viParam[chnWho].u32Y + viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
            {
                if((viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32W   ) ||
                   (viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32H))
                    viParam[chnWho].stVencSize = {viParam[chnWho].u32W, viParam[chnWho].u32H};
                if((viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32W) ||
                   (viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32H))
                    viParam[chnWho].stMinorSize = {viParam[chnWho].u32W, viParam[chnWho].u32H};

//                if((viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) ||
//                   (viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H))
//                    viParam[chnWho].stMinorSize = {320,180};




                 CapRect = {viParam[chnWho].u32X, viParam[chnWho].u32Y, viParam[chnWho].u32W, viParam[chnWho].u32H};
                 viRect = {viParam[chnWho].u32W,viParam[chnWho].u32H};

            }
            else
            {
                if((viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth) && (viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
                {
                    CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
                    viRect.u32Height =  viParam[chnWho].u32ViHeigth;
                    viRect.u32Width =   viParam[chnWho].u32ViWidth;

                  }

            }

         }
         else if(viParam[chnWho].vi_crop_enable == HI_FALSE)
         {

            CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
            viRect.u32Height =  viParam[chnWho].u32ViHeigth;
            viRect.u32Width =   viParam[chnWho].u32ViWidth;


            if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32ViWidth))
                viParam[chnWho].stMinorSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

            if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32ViWidth))
                viParam[chnWho].stVencSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};



         }
         else
         {
            printf("err find \n");
         }




        printf(" %d venc-w=%d  minorisze-w=%d      vi--w==%d vi--h==%d  viret-w=%d   h==%d  \n",viParam[chnWho].u32SrcFrmRate,viParam[chnWho].stVencSize.u32Width,
               viParam[chnWho].stMinorSize.u32Width,CapRect .u32Width,CapRect.u32Height,viRect.u32Width,viRect.u32Height);



        if(viParam[chnWho].enType[0] == PT_H264)
            v_type[chnWho*2] = VIDEO_H264;
        else if(viParam[chnWho].enType[0] == PT_H265)
            v_type[chnWho*2] = VIDEO_H265;
        if(viParam[chnWho].enType[1] == PT_H264)
            v_type[chnWho *2 +1] = VIDEO_H264;
        else if(viParam[chnWho].enType[1] == PT_H265)
            v_type[chnWho *2 +1] = VIDEO_H265;
        AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
        int param[2] = { 1, enAiSampleRate };

                if(coderFormat[chnWho] == AUDIO_NULL){
                    g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/0",NULL);
                    g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/1",NULL);
                }else{
                    g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/0",param);
                    g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/1",param);
                }
                             l_startloop[chnWho] = true;

                             himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,viRect,CapRect,viParam[chnWho].u32SrcFrmRate ,viParam[chnWho].u32DstFrameRate[0],viParam[chnWho].u32DstFrameRate[1]);
                             himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);



                         if((viParam[chnWho].vi_crop_enable == HI_TRUE) && ((viParam[chnWho].u32H < veSnap[chnWho].snapSize.u32Height) ||(viParam[chnWho].u32W < veSnap[chnWho].snapSize.u32Width ))  )
                         {

                             veSnap[chnWho].snapSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};

                             printf("__________________________________________ vicrop_snap\n");
                             if(veSnap[chnWho].Snapmcast_enable == HI_TRUE)
                                {     snap_runlag[chnWho] = false;
                                     mult_snap_runlag[chnWho] = true;
                               }
                               else
                                {
                                 snap_runlag[chnWho] = true;
                                  mult_snap_runlag[chnWho] = false;
                                }
                                snap_loop[chnWho] = HI_FALSE;

                                usleep(20000);

                               himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnWho );


                           HI_U32    snap_port = veSnap[chnWho].snapPort;
                               snap_loop[chnWho] = HI_TRUE;
                           //    himppmaster->snap_running[chnwho] = HI_TRUE;

                         himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnWho].snapSize,chnWho);

                               if(veSnap[chnWho].Snapmcast_enable == HI_FALSE)
                               {
                                   snap_runlag[chnWho] = true;
                                    mult_snap_runlag[chnWho] = false;
                                 pthread_t  snap_id1 = CreateThread(snap0ServerProc, 0, SCHED_FIFO, true, &snap_port);
                                 if(snap_id1 == 0){
                                   COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                                 }
                               }
                               else
                               {
//                                   if(macsnap[chnWho] != veSnap[chnWho].Snapmcast_enable)
//                                   {
                                       mult_snap[chnWho] = true;
                                       snap_runlag[chnWho] = false;
                                      mult_snap_runlag[chnWho] = true;

                                       snap_tid[chnWho] = CreateThread(multiSnapProc, 0, SCHED_FIFO, true, &chnWho);
                                       if(snap_tid[chnWho] == 0){
                                           COMMON_PRT("pthread_create() failed: snapServerProc\n");
                                       }
                                 //  }
                               }

                               softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_W, int2str(veSnap[chnWho].snapSize.u32Width));
                               softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_H, int2str(veSnap[chnWho].snapSize.u32Height));

                               softwareconfig->SaveConfig();
                               usleep(2000);






                          }







                             input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
                             if(input_tid1[chnWho] == 0){
                                 SAMPLE_PRT("pthread_create() failed: videv1\n");
                             }
        }
       usleep(10000);

    }
    else if(!strcmp(channel->valuestring, "chn1")){


        chnWho = 1;
        viDev =  3;
        viChn =  (viDev * 4);
         viParam[chnWho].vi_crop_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Crop_Enable));
        if((viParam[chnWho].u32ViHeigth != 0 ) &&  (viParam[chnWho].u32ViWidth != 0))
        {

            viParam[chnWho].u32ViWidth   =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn1_VI_W));
            viParam[chnWho].u32ViHeigth  =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn1_VI_H));
            viParam[chnWho].u32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_X));
            viParam[chnWho].u32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Y));
            viParam[chnWho].u32W = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Width));
            viParam[chnWho].u32H = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_VI_Height));
            viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_VENC_W)),
                                      str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_VENC_H))};
            viParam[chnWho].stMinorSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_VENC_W)),
                                      str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_VENC_H))};

            viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_VENC_Type));
            viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_VENC_Type));
            viParam[chnWho].u32DstFrameRate[0] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_DstFrmRate));


         printf("viparam--w=%d h=%d ---u32w=%d h=%d \n",viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth,
                viParam[chnWho].u32W,viParam[chnWho].u32H);

         if((viParam[chnWho].u32X == 0) && (viParam[chnWho].u32Y == 0)&&(viParam[chnWho].u32W == 0)&& (viParam[chnWho].u32H == 0))
         {
             return  false;
         }




                for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
                    crtsps_closestream(g_stream[i]);
                     printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
                }


          l_startloop[chnWho] = false;


          usleep(30000);


          himppmaster->VencModuleDestroy_1(chnWho* 2,chnWho* 2+2,-1);
             himppmaster->StopViModule(viDev,viChn);




        if( viParam[chnWho].vi_crop_enable == HI_TRUE) {




            if((viParam[chnWho].u32X + viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth)&&(viParam[chnWho].u32Y + viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
            {
                if((viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32W   ) ||
                   (viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32H))
                    viParam[chnWho].stVencSize = {viParam[chnWho].u32W, viParam[chnWho].u32H};
                if((viParam[chnWho].stMinorSize.u32Width > viParam[chnWho].u32W) ||
                   (viParam[chnWho].stMinorSize.u32Height > viParam[chnWho].u32H))
                    viParam[chnWho].stMinorSize = {viParam[chnWho].u32W, viParam[chnWho].u32H};

//                if((viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) ||
//                   (viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H))
//                    viParam[chnWho].stMinorSize = {320,180};




                 CapRect = {viParam[chnWho].u32X, viParam[chnWho].u32Y, viParam[chnWho].u32W, viParam[chnWho].u32H};
                 viRect = {viParam[chnWho].u32W,viParam[chnWho].u32H};

            }
            else
            {
                if((viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth) && (viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
                {
                    CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
                    viRect.u32Height =  viParam[chnWho].u32ViHeigth;
                    viRect.u32Width =   viParam[chnWho].u32ViWidth;

                }

            }

         }
         else if(viParam[chnWho].vi_crop_enable == HI_FALSE)
         {

            CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
            viRect.u32Height =  viParam[chnWho].u32ViHeigth;
            viRect.u32Width =   viParam[chnWho].u32ViWidth;


            if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32ViWidth))
                viParam[chnWho].stMinorSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

            if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32ViWidth))
                viParam[chnWho].stVencSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

           // if(viParam[chnWho].venc_SAME_INPUT[0] ==1 )
//            {
//              viParam[chnWho].stVencSize.u32Width   =  viCapRect->u32Width;
//              viParam[chnWho].stVencSize.u32Height  =   viCapRect->u32Height;
//            }
//            if(viParam[chnWho].venc_SAME_INPUT[1] ==1 )
//            {
//              viParam[chnWho].stMinorSize.u32Width  = viCapRect->u32Width;
//              viParam[chnWho].stMinorSize.u32Height = viCapRect->u32Height;
//            }





         }
         else
         {
            printf("err find \n");
         }




        printf(" %d venc-w=%d  minorisze-w=%d      vi--w==%d vi--h==%d  viret-w=%d   h==%d  \n",viParam[chnWho].u32SrcFrmRate,viParam[chnWho].stVencSize.u32Width,
               viParam[chnWho].stMinorSize.u32Width,CapRect .u32Width,CapRect.u32Height,viRect.u32Width,viRect.u32Height);



        if(viParam[chnWho].enType[0] == PT_H264)
            v_type[chnWho*2] = VIDEO_H264;
        else if(viParam[chnWho].enType[0] == PT_H265)
            v_type[chnWho*2] = VIDEO_H265;
        if(viParam[chnWho].enType[1] == PT_H264)
            v_type[chnWho *2 +1] = VIDEO_H264;
        else if(viParam[chnWho].enType[1] == PT_H265)
            v_type[chnWho *2 +1] = VIDEO_H265;
        AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
        int param[2] = { 1, enAiSampleRate };

                if(coderFormat[chnWho] == AUDIO_NULL){
                    g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/2",NULL);
                    g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/3",NULL);
                }else{
                    g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/2",param);
                    g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/3",param);
                }
                             l_startloop[chnWho] = true;

                             himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode2,viRect,CapRect,viParam[chnWho].u32SrcFrmRate,viParam[chnWho].u32DstFrameRate[0],viParam[chnWho].u32DstFrameRate[1] );
                             himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);


                             if((viParam[chnWho].vi_crop_enable == HI_TRUE) && ((viParam[chnWho].u32H < veSnap[chnWho].snapSize.u32Height) ||(viParam[chnWho].u32W < veSnap[chnWho].snapSize.u32Width ))  )
                             {

                                 veSnap[chnWho].snapSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};

                                 printf("__________________________________________ vicrop_snap\n");
                                 if(veSnap[chnWho].Snapmcast_enable == HI_TRUE)
                                    {     snap_runlag[chnWho] = false;
                                         mult_snap_runlag[chnWho] = true;
                                   }
                                   else
                                    {
                                     snap_runlag[chnWho] = true;
                                      mult_snap_runlag[chnWho] = false;
                                    }
                                    snap_loop[chnWho] = HI_FALSE;

                                    usleep(20000);

                                   himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnWho );


                               HI_U32    snap_port = veSnap[chnWho].snapPort;
                                   snap_loop[chnWho] = HI_TRUE;
                               //    himppmaster->snap_running[chnwho] = HI_TRUE;

                             himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnWho].snapSize,chnWho);

                                   if(veSnap[chnWho].Snapmcast_enable == HI_FALSE)
                                   {
                                       snap_runlag[chnWho] = true;
                                        mult_snap_runlag[chnWho] = false;
                                     pthread_t  snap_id1 = CreateThread(snap1ServerProc, 0, SCHED_FIFO, true, &snap_port);
                                     if(snap_id1 == 0){
                                       COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                                     }
                                   }
                                   else
                                   {
    //                                   if(macsnap[chnWho] != veSnap[chnWho].Snapmcast_enable)
    //                                   {
                                           mult_snap[chnWho] = true;
                                           snap_runlag[chnWho] = false;
                                          mult_snap_runlag[chnWho] = true;

                                           snap_tid[chnWho] = CreateThread(multiSnapProc1, 0, SCHED_FIFO, true, &chnWho);
                                           if(snap_tid[chnWho] == 0){
                                               COMMON_PRT("pthread_create() failed: snapServerProc\n");
                                           }
                                     //  }
                                   }

                                   softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_W, int2str(veSnap[chnWho].snapSize.u32Width));
                                   softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_H, int2str(veSnap[chnWho].snapSize.u32Height));

                                   softwareconfig->SaveConfig();
                                   usleep(2000);






                              }




                             input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
                             if(input_tid1[chnWho] == 0){
                                 SAMPLE_PRT("pthread_create() failed: videv1\n");
                             }
        }
       usleep(10000);

    }
    else  if(!strcmp(channel->valuestring, "chn2")){

       if(edid_4k == HI_FALSE)
      {
        chnWho = 2 ;
        viDev =  5;
        viChn =  (viDev * 4);
        viParam[chnWho].vi_crop_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable));
        if((viParam[chnWho].u32ViHeigth != 0 )&&  (viParam[chnWho].u32ViWidth != 0))
        {

            viParam[chnWho].u32ViWidth   =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn2_VI_W));
            viParam[chnWho].u32ViHeigth  =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn2_VI_H));
            viParam[chnWho].u32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_X));
            viParam[chnWho].u32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Y));
            viParam[chnWho].u32W = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Width));
            viParam[chnWho].u32H = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Height));
            viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_W)),
                                           str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_H))};
            viParam[chnWho].stMinorSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_VENC_W)),
                                           str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_VENC_H))};

            viParam[chnWho].u32DstFrameRate[0] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_DstFrmRate));

            viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_Type));
            viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_VENC_Type));
            if((viParam[chnWho].u32X == 0) && (viParam[chnWho].u32Y == 0)&&(viParam[chnWho].u32W == 0)&& (viParam[chnWho].u32H == 0))
            {
                return  false;
            }





            for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
                crtsps_closestream(g_stream[i]);
                printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
            }
            l_startloop[chnWho] = false;


            usleep(30000);


            himppmaster->VencModuleDestroy_1(chnWho* 2,chnWho* 2+2,-1);
            himppmaster->StopViModule(viDev,viChn);


            printf("viparam--w=%d h=%d ---u32w=%d h=%d \n",viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth,
                   viParam[chnWho].u32W,viParam[chnWho].u32H);

            viParam[chnWho].vi_crop_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable));

            if( viParam[chnWho].vi_crop_enable == HI_TRUE) {

                if((viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32W) ||
                        (viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32H))
                    viParam[chnWho].stVencSize = {viParam[chnWho].u32W, viParam[chnWho].u32H};
                if((viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) ||
                        (viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H))
                    viParam[chnWho].stMinorSize ={viParam[chnWho].u32W, viParam[chnWho].u32H};

                if((viParam[chnWho].u32X + viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth)&&(viParam[chnWho].u32Y + viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
                {

                    CapRect = {viParam[chnWho].u32X, viParam[chnWho].u32Y, viParam[chnWho].u32W, viParam[chnWho].u32H};
                    viRect = {viParam[chnWho].u32W,viParam[chnWho].u32H};

                }
                else
                {
                    if((viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth) && (viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
                    {
                        CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
                        viRect.u32Height =  viParam[chnWho].u32ViHeigth;
                        viRect.u32Width =   viParam[chnWho].u32ViWidth;


                        viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_W)),
                                                       str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_VENC_H))};
                        viParam[chnWho].stMinorSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_VENC_W)),
                                                       str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_VENC_H))};

                    }

                }


            }
            else if(viParam[chnWho].vi_crop_enable == HI_FALSE)
            {

                CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
                viRect.u32Height =  viParam[chnWho].u32ViHeigth;
                viRect.u32Width =   viParam[chnWho].u32ViWidth;


                if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32ViWidth))
                    viParam[chnWho].stMinorSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

                if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32ViWidth))
                    viParam[chnWho].stVencSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

            }
            else
            {
                printf("err find \n");
            }

            printf("venc-w=%d  minorisze-w=%d      vi--w==%d vi--h==%d  viret-w=%d   h==%d  \n",viParam[chnWho].stVencSize.u32Width,
                   viParam[chnWho].stMinorSize.u32Width,CapRect .u32Width,CapRect.u32Height,viRect.u32Width,viRect.u32Height);



            if(viParam[chnWho].enType[0] == PT_H264)
                v_type[chnWho*2] = VIDEO_H264;
            else if(viParam[chnWho].enType[0] == PT_H265)
                v_type[chnWho*2] = VIDEO_H265;
            if(viParam[chnWho].enType[1] == PT_H264)
                v_type[chnWho *2 +1] = VIDEO_H264;
            else if(viParam[chnWho].enType[1] == PT_H265)
                v_type[chnWho *2 +1] = VIDEO_H265;
            AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
            int param[2] = { 1, enAiSampleRate };

            if(coderFormat[chnWho] == AUDIO_NULL){
                g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/4",NULL);
                g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/5",NULL);
            }else{
                g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/4",param);
                g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/5",param);
            }
            l_startloop[chnWho] = true;
            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,viRect,CapRect,viParam[chnWho].u32SrcFrmRate,viParam[chnWho].u32DstFrameRate[0],viParam[chnWho].u32DstFrameRate[1] );
            himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);

            if((viParam[chnWho].vi_crop_enable == HI_TRUE) && ((viParam[chnWho].u32H < veSnap[chnWho].snapSize.u32Height) ||(viParam[chnWho].u32W < veSnap[chnWho].snapSize.u32Width ))  )
            {

                veSnap[chnWho].snapSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};

                printf("__________________________________________ vicrop_snap\n");
                if(veSnap[chnWho].Snapmcast_enable == HI_TRUE)
                {     snap_runlag[chnWho] = false;
                    mult_snap_runlag[chnWho] = true;
                }
                else
                {
                    snap_runlag[chnWho] = true;
                    mult_snap_runlag[chnWho] = false;
                }
                snap_loop[chnWho] = HI_FALSE;

                usleep(20000);

                himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnWho );


                HI_U32    snap_port = veSnap[chnWho].snapPort;
                snap_loop[chnWho] = HI_TRUE;
                //    himppmaster->snap_running[chnwho] = HI_TRUE;

                himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnWho].snapSize,chnWho);

                if(veSnap[chnWho].Snapmcast_enable == HI_FALSE)
                {
                    snap_runlag[chnWho] = true;
                    mult_snap_runlag[chnWho] = false;
                    pthread_t  snap_id1 = CreateThread(snap2ServerProc, 0, SCHED_FIFO, true, &snap_port);
                    if(snap_id1 == 0){
                        COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                    }
                }
                else
                {
                    //                                   if(macsnap[chnWho] != veSnap[chnWho].Snapmcast_enable)
                    //                                   {
                    mult_snap[chnWho] = true;
                    snap_runlag[chnWho] = false;
                    mult_snap_runlag[chnWho] = true;

                    snap_tid[chnWho] = CreateThread(multiSnapProc2, 0, SCHED_FIFO, true, &chnWho);
                    if(snap_tid[chnWho] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                    //  }
                }

                softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_W, int2str(veSnap[chnWho].snapSize.u32Width));
                softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_H, int2str(veSnap[chnWho].snapSize.u32Height));

                softwareconfig->SaveConfig();
                usleep(2000);






            }








            input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
            if(input_tid1[chnWho] == 0){
                SAMPLE_PRT("pthread_create() failed: videv1\n");
            }
        }
        usleep(2000);
       }
    }
    else  if(!strcmp(channel->valuestring, "chnxx")){
          chnWho = 3;
        viDev =  7;
        viChn =  (viDev * 4);

        viParam[chnWho].vi_crop_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable));
       if((viParam[chnWho].u32ViHeigth != 0 )&&  (viParam[chnWho].u32ViWidth != 0))
       {
           himppmaster->VencModuleDestroy_1(chnWho* 2,chnWho* 2+2,-1);
           himppmaster->StopViModule(viDev,viChn);
            viParam[chnWho].u32ViWidth   =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn3_VI_W));
            viParam[chnWho].u32ViHeigth  =   str2int(softwareconfig ->GetConfig(SoftwareConfig::kChn3_VI_H));
            viParam[chnWho].u32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_X));
            viParam[chnWho].u32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Y));
            viParam[chnWho].u32W = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Width));
            viParam[chnWho].u32H = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Height));
            viParam[chnWho].stVencSize =  {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_VENC_W)),
                                      str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_VENC_H))};
            viParam[chnWho].stMinorSize = {str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_VENC_W)),
                                      str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_VENC_H))};

            viParam[chnWho].enType[0] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_VENC_Type));
            viParam[chnWho].enType[1] = (PAYLOAD_TYPE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_VENC_Type));
            if((viParam[chnWho].u32X == 0) && (viParam[chnWho].u32Y == 0)&&(viParam[chnWho].u32W == 0)&& (viParam[chnWho].u32H == 0))
            {
                return  false;
            }



        for( i = (chnWho * 2);i<(chnWho * 2 + 2);i++){
            crtsps_closestream(g_stream[i]);
             printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
        }
           l_startloop[chnWho] = false;


          usleep(30000);






        printf("viparam--w=%d h=%d ---u32w=%d h=%d \n",viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth,
               viParam[chnWho].u32W,viParam[chnWho].u32H);

        viParam[chnWho].vi_crop_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable));

        if( viParam[chnWho].vi_crop_enable == HI_TRUE) {

            if((viParam[chnWho].stVencSize.u32Width > viParam[chnWho].u32W) ||
               (viParam[chnWho].stVencSize.u32Height > viParam[chnWho].u32H))
                viParam[chnWho].stVencSize = {viParam[chnWho].u32W, viParam[chnWho].u32H};
            if((viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32W) ||
               (viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32H))
                viParam[chnWho].stMinorSize = {320,180};


            if((viParam[chnWho].u32X + viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth)&&(viParam[chnWho].u32Y + viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
            {

                 CapRect = {viParam[chnWho].u32X, viParam[chnWho].u32Y, viParam[chnWho].u32W, viParam[chnWho].u32H};
                 viRect = {viParam[chnWho].u32W,viParam[chnWho].u32H};

            }
            else
            {
                if((viParam[chnWho].u32W <= viParam[chnWho].u32ViWidth) && (viParam[chnWho].u32H <= viParam[chnWho].u32ViHeigth))
                {
                    CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
                    viRect.u32Height =  viParam[chnWho].u32ViHeigth;
                    viRect.u32Width =   viParam[chnWho].u32ViWidth;



                  }

            }


         }
         else if(viParam[chnWho].vi_crop_enable == HI_FALSE)
         {

            CapRect  = {0,0,  viParam[chnWho].u32ViWidth, viParam[chnWho].u32ViHeigth};
            viRect.u32Height =  viParam[chnWho].u32ViHeigth;
            viRect.u32Width =   viParam[chnWho].u32ViWidth;


            if((viParam[chnWho].stMinorSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stMinorSize.u32Width >= viParam[chnWho].u32ViWidth))
                viParam[chnWho].stMinorSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

            if((viParam[chnWho].stVencSize.u32Height >= viParam[chnWho].u32ViHeigth) || (viParam[chnWho].stVencSize.u32Width >= viParam[chnWho].u32ViWidth))
                viParam[chnWho].stVencSize = {viParam[chnWho].u32ViWidth,viParam[chnWho].u32ViHeigth};

         }
        else
        {
            printf("err find \n");
        }

      //  printf("venc-w=%d  minorisze-w=%d      vi--w==%d vi--h==%d  viret-w=%d   h==%d  \n",viParam[chnWho].stVencSize.u32Width,
     //          viParam[chnWho].stMinorSize.u32Width,CapRect .u32Width,CapRect.u32Height,viRect.u32Width,viRect.u32Height);



        if(viParam[chnWho].enType[0] == PT_H264)
            v_type[chnWho*2] = VIDEO_H264;
        else if(viParam[chnWho].enType[0] == PT_H265)
            v_type[chnWho*2] = VIDEO_H265;
        if(viParam[chnWho].enType[1] == PT_H264)
            v_type[chnWho *2 +1] = VIDEO_H264;
        else if(viParam[chnWho].enType[1] == PT_H265)
            v_type[chnWho *2 +1] = VIDEO_H265;
        AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(softwareconfig->GetConfig(SoftwareConfig::kAiSampleRate));
        int param[2] = { 1, enAiSampleRate };

            if(coderFormat[chnWho] == AUDIO_NULL){
                g_stream[chnWho*2   ] = crtsps_openstream( v_type[chnWho *2 ],coderFormat[chnWho],"/6",NULL);
                g_stream[chnWho * 2 +1] = crtsps_openstream( v_type[chnWho *2 +1 ],coderFormat[chnWho],"/7",NULL);
            }else{
                g_stream[chnWho*2 ]= crtsps_openstream( v_type[chnWho *2],coderFormat[chnWho],"/6",param);
                g_stream[chnWho* 2 + 1] = crtsps_openstream( v_type[chnWho *2 +1],coderFormat[chnWho],"/7",param);
            }
                         l_startloop[chnWho] = true;
                         himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,viRect,CapRect,viParam[chnWho].u32SrcFrmRate ,viParam[chnWho].u32DstFrameRate[0],viParam[chnWho].u32DstFrameRate[1] );
                         himppmaster->VencModuleInit_1(chnWho,2,veSnap[chnWho].snapSize,-1);


                         if((viParam[chnWho].vi_crop_enable == HI_TRUE) && ((viParam[chnWho].u32H < veSnap[chnWho].snapSize.u32Height) ||(viParam[chnWho].u32W < veSnap[chnWho].snapSize.u32Width ))  )
                         {

                             veSnap[chnWho].snapSize = {viParam[chnWho].u32W,viParam[chnWho].u32H};


                             if(veSnap[chnWho].Snapmcast_enable == HI_TRUE)
                                {     snap_runlag[chnWho] = false;
                                     mult_snap_runlag[chnWho] = true;
                               }
                               else
                                {
                                 snap_runlag[chnWho] = true;
                                  mult_snap_runlag[chnWho] = false;
                                }
                                snap_loop[chnWho] = HI_FALSE;

                                usleep(20000);

                               himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnWho );


                           HI_U32    snap_port = veSnap[chnWho].snapPort;
                               snap_loop[chnWho] = HI_TRUE;
                           //    himppmaster->snap_running[chnwho] = HI_TRUE;

                         himppmaster->VencModuleInit_1(HI_FALSE,HI_FALSE,veSnap[chnWho].snapSize,chnWho);

                               if(veSnap[chnWho].Snapmcast_enable == HI_FALSE)
                               {
                                   snap_runlag[chnWho] = true;
                                    mult_snap_runlag[chnWho] = false;
                                 pthread_t  snap_id1 = CreateThread(snap3ServerProc, 0, SCHED_FIFO, true, &snap_port);
                                 if(snap_id1 == 0){
                                   COMMON_PRT("pthread_create() failed: snap0ServerProc\n");
                                 }
                               }
                               else
                               {
//                                   if(macsnap[chnWho] != veSnap[chnWho].Snapmcast_enable)
//                                   {
                                       mult_snap[chnWho] = true;
                                       snap_runlag[chnWho] = false;
                                      mult_snap_runlag[chnWho] = true;

                                       snap_tid[chnWho] = CreateThread(multiSnapProc3, 0, SCHED_FIFO, true, &chnWho);
                                       if(snap_tid[chnWho] == 0){
                                           COMMON_PRT("pthread_create() failed: snapServerProc\n");
                                       }
                                 //  }
                               }

                               softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_W, int2str(veSnap[chnWho].snapSize.u32Width));
                               softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_H, int2str(veSnap[chnWho].snapSize.u32Height));

                               softwareconfig->SaveConfig();
                               usleep(2000);






                          }










                         input_tid1[chnWho] = CreateThread(input1proc_, 0, SCHED_FIFO, true, &chnWho);
                         if(input_tid1[chnWho] == 0){
                             SAMPLE_PRT("pthread_create() failed: videv1\n");
                         }
        }
        usleep(2000);

    }
    else
        printf("havent err\n");


    return true;
}

bool CommandMap::SetOsdHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
     //auto thisini = Singleton<SoftwareConfig>::getInstance();
    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            osd_reset[0] = 1;
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
        }else if(!strcmp(channel->valuestring, "chn1")){
              osd_reset[1] = 1;
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
        }else if(!strcmp(channel->valuestring, "chn2")){
              osd_reset[2] = 1;
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
        }else if(!strcmp(channel->valuestring, "chn3")){
              osd_reset[3] = 1;
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
        }
    }

     softwareconfig->SaveConfig();


     osd_enable[0] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_Enable));
     osd_enable[1] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Osd_Enable));
     osd_enable[2] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Osd_Enable));
     osd_enable[3] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Osd_Enable));
    //




    //printf("SetOsdHandler-----==%d\n",osd_enable[0]);


    this->LetMeTellYou(msg);

    return true;
}

bool CommandMap::GetLogHandler(cJSON *value)
{
    bool ret = false;

    cJSON *sys_log = cJSON_GetObjectItemCaseSensitive(value, "get_sys_log");
    if (sys_log) {
        ret = this->SendFileMsg(sys_log, "/proc/umap/sys", "[sys_log]");
        if(!ret){
            COMMON_PRT("GetSysLogHandler() failed!");
        }
    }

    cJSON *vdec_log = cJSON_GetObjectItemCaseSensitive(value, "get_vdec_log");
    if (vdec_log) {
        ret = this->SendFileMsg(vdec_log, "/proc/umap/vdec", "[vdec_log]");
        if(!ret){
            COMMON_PRT("GetVdecLogHandler() failed!");
        }
    }

    cJSON *vo_log = cJSON_GetObjectItemCaseSensitive(value, "get_vo_log");
    if (vo_log) {
        ret = this->SendFileMsg(vo_log, "/proc/umap/vo", "[vo_log]");
        if(!ret){
            COMMON_PRT("GetVoLogHandler() failed!");
        }
    }

    cJSON *venc_log = cJSON_GetObjectItemCaseSensitive(value, "get_venc_log");
    if (venc_log) {
        ret = this->SendFileMsg(venc_log, "/proc/umap/venc", "[venc_log]");
        if(!ret){
            COMMON_PRT("GetVencLogHandler() failed!");
        }
    }

    cJSON *vi_log = cJSON_GetObjectItemCaseSensitive(value, "get_vi_log");
    if (vi_log) {
        ret = this->SendFileMsg(vi_log, "/proc/umap/vi", "[vi_log]");
        if(!ret){
            COMMON_PRT("GetViLogHandler() failed!");
        }
    }

    cJSON *vpss_log = cJSON_GetObjectItemCaseSensitive(value, "get_vpss_log");
    if (vpss_log) {
        ret = this->SendFileMsg(vpss_log, "/proc/umap/vpss", "[vpss_log]");
        if(!ret){
            COMMON_PRT("GetVpssLogHandler() failed!");
        }
    }

    cJSON *hdmi_log = cJSON_GetObjectItemCaseSensitive(value, "get_hdmi_log");
    if (hdmi_log) {
        ret = this->SendFileMsg(hdmi_log, "/proc/umap/hdmi0", "[hdmi_log]");
        if(!ret){
            COMMON_PRT("GetHdmiLogHandler() failed!");
        }
    }

    cJSON *ao_log = cJSON_GetObjectItemCaseSensitive(value, "get_ao_log");
    if (ao_log) {
        ret = this->SendFileMsg(ao_log, "/proc/umap/ao", "[ao_log]");
        if(!ret){
            COMMON_PRT("GetAoLogHandler() failed!");
        }
    }

    cJSON *ai_log = cJSON_GetObjectItemCaseSensitive(value, "get_ai_log");
    if (ai_log) {
        ret = this->SendFileMsg(ai_log, "/proc/umap/ai", "[ai_log]");
        if(!ret){
            COMMON_PRT("GetAiLogHandler() failed!");
        }
    }

    cJSON * js_ret;
         js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_patch_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/data/log/out_patch.log", "[patch_log]");
        if(!ret){
            COMMON_PRT("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_bus_input");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/bus/input/devices", "[bus_input]");
        if(!ret){
            COMMON_PRT("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_up_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/data/log/out_up.log", "[up_log]");
        if(!ret){
            COMMON_PRT("GetSysMsgHandler() failed!");
        }
    }
    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_debug_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/data/log/abnormal_long.log", "[debug_log]");
        if(!ret){
            COMMON_PRT("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_media-mem");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/media-mem", "[media-mem]");
        if(!ret){
            COMMON_PRT("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_file_modify_time");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/tmp/output_file_modifytime.log", "[file_modify_time]");
        if(!ret){
            COMMON_PRT("GetSysMsgHandler() failed!");
        }
    }


      js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_sys_msg");

    if (  js_ret) {
         string content;
         string cmd = "echo -e \"";
         string c;


         /** NOTE: CPU空闲率说明
         master@jay-intel:~$ cat /proc/uptime
         6447032.12 48185264.69
         master@jay-intel:~$ cat /proc/cpuinfo  | grep processor | wc -l
         8

         第一列输出的是，系统启动到现在的时间（以秒为单位），这里简记为num1；
         第二列输出的是，系统空闲的时间（以秒为单位）,这里简记为num2。

         注意，很多很多人都知道第二个是系统空闲的时间，但是可能你不知道是，在SMP系统里，系统空闲的时间有时会是系统运行时间的几倍，这是怎么回事呢？
         因为系统空闲时间的计算，是把SMP算进去的，就是所你有几个逻辑的CPU（包括超线程）。

         系统的空闲率(%) = num2/(num1*N) 其中N是SMP系统中的CPU个数。
         **/
         /*NOTE: CPU + runingtime + idleness(空闲率)
         */
         c = "iostat -c | awk '{printf $1}' | awk -F: '{print $2}' > /tmp/cmd.tmp"; system(c.c_str());
         cmd += "CPU: " + ReadSthFile("/tmp/cmd.tmp") + "%\n";
         c = "cat /proc/uptime | awk -F. '{run_days=$1 / 86400;run_hour=($1 % 86400)/3600;run_minute=($1 % 3600)/60;run_second=$1 % 60;printf(\"%dd.%dh:%dm:%ds\",run_days,run_hour,run_minute,run_second)}' > /tmp/cmd.tmp"; system(c.c_str());
         cmd += "Runing time: " + ReadSthFile("/tmp/cmd.tmp") + '\n';
         c = "cat /proc/cpuinfo  | grep processor | wc -l > /tmp/cmd.tmp"; system(c.c_str());
         c = "cat /proc/uptime | awk '{print $2/$1/" + ReadSthFile("/tmp/cmd.tmp") + "*100}' > /tmp/cmd.tmp"; system(c.c_str());
         cmd += "Iidleness: " + ReadSthFile("/tmp/cmd.tmp") + "%\n";

         /*NOTE:  sys cmdline
          * 样式
          * mem=128M console=ttyAMA0,115200 root=/dev/mtdblock2 rw rootfstype=yaffs2 mtdparts=hinand:1M(boot),4M(kernel),-(rootfs) ver=20191223
         */
         content = ReadSthFile("/proc/cmdline");
         cmd += "OS Mem: " + content.substr(content.find("mem=") + strlen("mem="), content.find(' ') - content.find("mem=") - strlen("mem=")) + '\n';

         c = "free -m | grep 'Mem' | awk '{print \"total \"$2\"M | used \"$3\"M | free \"$4\"M\"}' > /tmp/cmd.tmp"; system(c.c_str());
         cmd += "Memory Status: " + ReadSthFile("/tmp/cmd.tmp") + '\n';

         c = "df -h | grep 'root' | awk '{print \"total \"$2\" | used \"$5}' > /tmp/cmd.tmp"; system(c.c_str());
         cmd += "Flash Size: rootfs " + content.substr(content.rfind(",", content.rfind("(rootfs)")) + 1, content.rfind("(rootfs)") - content.rfind(",", content.find("(rootfs)")) - 1) + " | " + ReadSthFile("/tmp/cmd.tmp") + '\n';

         if(content.find("ver=") != string::npos)
            cmd += "U-Boot Version: " + content.substr(content.find("ver=") + strlen("ver=")) + '\n';
         else
            cmd += "U-Boot Version: <null>\n";

         /*NOTE: kernel version
          * 样式
          * Linux version 3.18.20 (lwx@lwx-VirtualBox) (gcc version 4.9.4 20150629 (prerelease) (Hisilicon_v500_20180120) ) #5 SMP Wed Feb 20 21:20:31 CST 2019
         */
         content = ReadSthFile("/proc/version");
         cmd += "Kernel Version: " + content.substr(content.find("#")) + '\n';
         cmd += "Compiler Version: " + content.substr(content.find("Hisilicon") + strlen("Hisilicon") + 1, content.find(")", content.find("Hisilicon")) - content.find("Hisilicon") - strlen("Hisilicon") - 1) + '\n';

         /*NOTE: software version
          * 样式
          * {
          *  "software_version": "0.14.5.20191203133232_alpha",
          *  "hardware_version": "892457216_v500_20180120"
          *  }
         */
         content = ReadSthFile("/version/version");
         cmd += "Software Version: " + content.substr(content.find("\"software_version\": ") + strlen("\"software_version\": ") + 1, content.find(",") - (content.find("\"software_version\": ") + strlen("\"software_version\": ") + 1) - 1) + '\n';

         /*NOTE: uuid
          * 样式
          * 38-BB-CD-22-A6-B5-49-80-80-AB-B8-9F-05-80-2B-93
         */
         content = ReadSthFile("/uuid/uuid");
         cmd += "UUID: " + content + '\n';

         cmd += "\" > /tmp/cmd.tmp";
         system(cmd.c_str());

         /**NOTE:
          * [sys_info]
          * CPU: 8.04%
          * Runing time: 0d.1h:18m:16s
          * OS Mem: 128M
          * Memory Status: total 119M | used 89M | free 30M
          * Flash Size: rootfs - | total 507.0M | used 20%
          * U-Boot Version: 20191223
          * Kernel Version: #61 SMP Thu Dec 19 15:19:18 CST 2019
          * Compiler Version: v500_20180120
          * Software Version: 1.0.1.20191224140647_beta
          * UUID: 38-BB-CD-22-A6-B5-49-80-80-AB-B8-9F-05-80-2B-93
          **/
         ret = this->SendFileMsg(js_ret, "/tmp/cmd.tmp", "[sys_info]");
         if(!ret){
             printf("get media.json failed!");
         }
     }





    return true;
}

/*bool CommandMap::SetVoHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    cJSON *out_same_in = cJSON_GetObjectItemCaseSensitive(value, "out_same_in");
    if (out_same_in && cJSON_IsNumber(out_same_in)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kOut_SameIn, out_same_in->valueint) ){
            COMMON_PRT("set out_same_in set failed");
        }
    }

    cJSON *luma = cJSON_GetObjectItemCaseSensitive(value, "luma");
    if (luma && cJSON_IsNumber(luma)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kLuma, luma->valueint) ){
            COMMON_PRT("set vo->luma set failed");
        }
    }

    cJSON *contrast = cJSON_GetObjectItemCaseSensitive(value, "contrast");
    if (contrast && cJSON_IsNumber(contrast)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kContrast, contrast->valueint)bconnect_ ){
            COMMON_PRT("set vo->contrast set failed");
        }
    }

    cJSON *hue = cJSON_GetObjectItemCaseSensitive(value, "hue");
    if (hue && cJSON_IsNumber(hue)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kHue, hue->valueint) ){
            COMMON_PRT("set vo->hue set failed");
        }
    }

    cJSON *saturation = cJSON_GetObjectItemCaseSensitive(value, "saturation");
    if (saturation && cJSON_IsNumber(saturation)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kSaturation, saturation->valueint) ){
            COMMON_PRT("set vo->saturation set failed");
        }
    }

    softwareconfig->SaveConfig();
    himppmaster->VoModuleDestroy();
    himppmaster->VoModeuleInit();

    return true;
}*/


bool CommandMap::RemoveFileHandler(cJSON *value)
{
    cJSON* js_ret = NULL;
    js_ret = cJSON_GetObjectItemCaseSensitive(value, "filename");
    if(js_ret && cJSON_IsString(js_ret)){
        char cmd[128] = "";
        if(!strcmp(js_ret->valuestring, "media.json") ||
           !strcmp(js_ret->valuestring, "software.ini") ||
           !strcmp(js_ret->valuestring, "net.ini") ||
           !strcmp(js_ret->valuestring, "node_name") ||
           !strcmp(js_ret->valuestring, "remserial.ini") ||
           !strcmp(js_ret->valuestring, "venc.ini")){

            sprintf(cmd, "rm /data/%s", js_ret->valuestring);
        }
        else{
            if(!strcmp(js_ret->valuestring, "load")      ||
               !strcmp(js_ret->valuestring, "dog.sh")    ||
               !strcmp(js_ret->valuestring, "netserver") ||
               !strcmp(js_ret->valuestring, "hi3531d_venc") ||
               !strcmp(js_ret->valuestring, "hi3521d_venc") ||
                strstr(js_ret->valuestring, ".so")){

                COMMON_PRT("Host want to remove [%s] was prevented.", js_ret->valuestring);

                this->ACK(CODE_SYS_ERR, 2, cmdfunc_.c_str());
                return true;
            }
            else
                sprintf(cmd, "rm %s", js_ret->valuestring);
        }

        COMMON_PRT("Host want to remove [%s]", cmd + 3);
        system(cmd);
    }

    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());

    return true;
}

bool CommandMap::SetHardwareNameHandler(cJSON *value)
{
    cJSON* js_ret = NULL;

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "hardware_name");
    if (js_ret) {
        char namefile[64] = "";
        sprintf(namefile, "%s/%s", VERSION_DIR, HW_NAME_FILE);
        FILE *f = fopen( namefile, "wb" );
        if(f){
            fprintf(f, "%s\n", js_ret->valuestring);
            fflush(f);
            fclose(f);
            f = NULL;
        }
        else
            return false;
    }
    else
        return false;

    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());

    return true;
}


bool CommandMap::SetSysGPIOHandler(cJSON *value)
{
    /** NOTE:
     * {
     * "Type":"System",
     * "Function":"setGpioReg",
     * "Value":{
     *      "io_name":"gpio20_0",
     *      "io_reg":"0x120F0100|0x1|0x121C0400", // "muxctrl_regXX|muxctrl_value|gpioXX_data_reg"
     *      "io_mode":"in",
     *      "io_write":"0x01"
     *      }
     * }
     **/
    extern unsigned int g_muxctrl_reg[gpio_max * 8][2];
    extern unsigned int g_gpio_base_reg[gpio_max];
    HI_S32 gpio_group = -1;
    HI_S32 gpio = -1;
    HI_U32 muxctrl_reg   = 0;
    HI_U32 muxctrl_val   = 0;
    HI_U32 gpio_base_reg = 0;
    HI_U32 io_mode  = 0;
    HI_U32 io_write = 0;

    cJSON *js_ret = NULL;
    //NOTE: io_name 和 io_reg 是二选一的命令,如果两者皆有,则优先使用 io_name
    js_ret = cJSON_GetObjectItemCaseSensitive(value, "io_name");
    if (js_ret && cJSON_IsString(js_ret)) {
        if(strncmp(js_ret->valuestring, "gpio", 4) == 0){
            vector<string> iolist;
            SplitString(js_ret->valuestring + 4, iolist, "_");

            if(iolist.size() != 2){
                printf("%s->io_name Invalid format", cmdfunc_.c_str());
                this->ACK(CODE_SYS_ERR, 1, cmdfunc_.c_str());
                return false;
            }

            gpio_group = str2int(iolist[0]);
            gpio = str2int(iolist[1]);
            if( (gpio_group >= 0 && gpio_group <= 24) ||
                (gpio >= 0 && gpio <= 7) ){
                muxctrl_reg   = g_muxctrl_reg[gpio_group * 8 + gpio][0];
                muxctrl_val   = g_muxctrl_reg[gpio_group * 8 + gpio][1];
                gpio_base_reg = g_gpio_base_reg[gpio_group];
            }
            else{
                printf("%s->gpio_group or gpio Invalid format", cmdfunc_.c_str());
                this->ACK(CODE_SYS_ERR, 1, cmdfunc_.c_str());
                return false;
            }
        }
    }
    else{
        js_ret = cJSON_GetObjectItemCaseSensitive(value, "io_reg");
        if (js_ret && cJSON_IsString(js_ret)) {
            vector<string> reglist;
            SplitString(js_ret->valuestring, reglist, "|");

            if(reglist.size() != 3){
                printf("%s->io_reg Invalid format", cmdfunc_.c_str());
                this->ACK(CODE_SYS_ERR, 1, cmdfunc_.c_str());
                return false;
            }

            char* end;
            uint gpio_data_reg = static_cast<HI_U32>(strtol(reglist[2].c_str(), &end, 16));
            muxctrl_reg   = static_cast<HI_U32>(strtol(reglist[0].c_str(), &end, 16));
            muxctrl_val   = static_cast<HI_U32>(strtol(reglist[1].c_str(), &end, 16));
            gpio_base_reg = gpio_data_reg & 0xFFFF0000; //取出高4位

            /** NOTE: 比如取出低4位并右移2位后得到了0x40=64,
             * 对64进行以2为底的log运行,得出2^6=64, 其中6即为gpio位 **/
            gpio = (int)(log((gpio_data_reg & 0xFFFF) >> 2) / log(2));
            if( gpio < 0 || gpio > 7 ){
                printf("%s->gpio_group or gpio Invalid format", cmdfunc_.c_str());
                this->ACK(CODE_SYS_ERR, 1, cmdfunc_.c_str());
                return false;
            }
        }
    }
    printf("gpio_group: %d, gpio: %d, muxctrl_reg: 0x%04x, muxctrl_val: 0x%02x, gpio_base_reg: 0x%04x", gpio_group, gpio, muxctrl_reg, muxctrl_val, gpio_base_reg);

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "io_mode");
    if (js_ret && cJSON_IsString(js_ret)) {
        if(!strcmp(js_ret->valuestring, "in")){
            io_mode = 1;
        }
        else if(!strcmp(js_ret->valuestring, "out")){
            io_mode = 0;
        }
        else{
            printf("%s->io_mode Invalid format", cmdfunc_.c_str());
            this->ACK(CODE_SYS_ERR, 1, cmdfunc_.c_str());
            return false;
        }
    }
    else{
        printf("%s->io_mode is missing", cmdfunc_.c_str());
        this->ACK(CODE_SYS_ERR, 1, cmdfunc_.c_str());
        return false;
    }

    //NOTE: 如果io_mode="out",才会去查询是否有 io_write
    if(io_mode == 0){ //out
        js_ret = cJSON_GetObjectItemCaseSensitive(value, "io_write");
        if (js_ret && cJSON_IsNumber(js_ret)) {
            io_write = js_ret->valueint;
        }
    }

    //NOTE: 如果没有解析到 muxctrl_reg 和 gpio_base_reg, 则报错并退出
    if(muxctrl_reg == muxctrl_reg_x || gpio_base_reg == 0){
        printf("%s->io_name or io_reg Invalid format", cmdfunc_.c_str());
        this->ACK(CODE_SYS_ERR, 1, cmdfunc_.c_str());
        return false;
    }

    //Step.1 先将引脚复用为GPIO
    if(muxctrl_reg != 0){
       Hi_SetReg(muxctrl_base_reg | muxctrl_reg, muxctrl_val);
        printf("muxctrl_reg: 0x%x, val: 0x%x", muxctrl_base_reg | muxctrl_reg, muxctrl_val);
    }
    else{
        printf("muxctrl is JTAG_EN");
    }

    uint gpio_data_reg = (gpio_base_reg | ((1 << gpio) << 2));
    if(io_mode == 1) { // in
        //Step.2 控制GPIO引脚方向(0x0400),做输入
        HI_U32 u32Value;
       Hi_GetReg(gpio_base_reg | gpio_dir_reg, &u32Value);
       Hi_SetReg(gpio_base_reg | gpio_dir_reg, u32Value & ((1 << gpio) ^ 0xFF));
        printf("gpio_dir_reg: 0x%x, val: 0x%x -> 0x%x", gpio_base_reg | gpio_dir_reg, u32Value, u32Value & ((1 << gpio) ^ 0xFF));

        //Step.3 读取值
       Hi_GetReg(gpio_data_reg, &u32Value);
        printf("gpio_data_reg: 0x%x, value: %d", gpio_data_reg, (u32Value >> gpio) & 0x1);

        char io_name[10] = "";
        sprintf(io_name, "gpio%d_%d", gpio_group, gpio);
        cJSON *value = cJSON_CreateObject();
        cJSON_AddStringToObject(  value, "io_name",  io_name);
        cJSON_AddNumberToObject(  value, "io_value", (u32Value >> gpio) & 0x1);

        cJSON *root = cJSON_CreateObject();
        cJSON_AddStringToObject(root, "Type",     cmdtype_.c_str());
        cJSON_AddStringToObject(root, "Function", cmdfunc_.c_str());
        cJSON_AddItemToObject(  root, "Value",    value);

        char *s = cJSON_PrintUnformatted(root);
        if(s){
            LetMeTellYou(s);
            cJSON_free(s);
        }
        cJSON_Delete(root);
    }
    else if(io_mode == 0){ //out
        //Step.2 控制GPIO引脚方向(0x0400),做输出
        HI_U32 u32Value;
       Hi_GetReg(gpio_base_reg | gpio_dir_reg, &u32Value);
       Hi_SetReg(gpio_base_reg | gpio_dir_reg, u32Value | (1 << gpio));
        printf("gpio_reg: 0x%x, val: 0x%x -> 0x%x", gpio_base_reg | gpio_dir_reg, u32Value, u32Value | (1 << gpio));

        //Step.3 写入值
        Hi_SetReg(gpio_data_reg, (io_write << gpio));
        printf("gpio_data_reg: 0x%x, val: 0x%x", gpio_data_reg, (io_write << gpio));
    }

    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());

    return true;
}





bool CommandMap::SetVoHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();

#ifdef __HI3521D_VENC__
    cJSON *hw_loop_enable = cJSON_GetObjectItemCaseSensitive(value, "hw_loop_enable");
    if (hw_loop_enable && cJSON_IsNumber(hw_loop_enable)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kHW_Loop_Enable, hw_loop_enable->valueint) ){
            COMMON_PRT("set hw_loop_enable set failed");
        }
    }
#endif

    cJSON *out_same_in = cJSON_GetObjectItemCaseSensitive(value, "out_same_in");
    if (out_same_in && cJSON_IsNumber(out_same_in)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kOut_SameIn, out_same_in->valueint) ){
            COMMON_PRT("set out_same_in set failed");
        }
    }

    cJSON *luma = cJSON_GetObjectItemCaseSensitive(value, "luma");
    if (luma && cJSON_IsNumber(luma)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kLuma, luma->valueint) ){
            COMMON_PRT("set vo->luma set failed");
        }
    }

    cJSON *contrast = cJSON_GetObjectItemCaseSensitive(value, "contrast");
    if (contrast && cJSON_IsNumber(contrast)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kContrast, contrast->valueint) ){
            COMMON_PRT("set vo->contrast set failed");
        }
    }

    cJSON *hue = cJSON_GetObjectItemCaseSensitive(value, "hue");
    if (hue && cJSON_IsNumber(hue)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kHue, hue->valueint) ){
            COMMON_PRT("set vo->hue set failed");
        }
    }

    cJSON *saturation = cJSON_GetObjectItemCaseSensitive(value, "saturation");
    if (saturation && cJSON_IsNumber(saturation)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kSaturation, saturation->valueint) ){
            COMMON_PRT("set vo->saturation set failed");
        }
    }


    cJSON *intfsync = cJSON_GetObjectItemCaseSensitive(value, "intfsync");
    if (intfsync && cJSON_IsString(intfsync)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kVoIntfSync, intfsync->valuestring) ){
            printf("set vo->intfsync set failed");
        }
    }

    cJSON *maxchn = cJSON_GetObjectItemCaseSensitive(value, "max_vo_chn");
    if (maxchn && cJSON_IsNumber(maxchn)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kVoChnMax, maxchn->valueint) ){
            printf("set vo->max chn set failed");
        }
    }

    cJSON *outputmodel = cJSON_GetObjectItemCaseSensitive(value, "vo_output_model");
    if (outputmodel && cJSON_IsString(outputmodel)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kVoOutputModel, outputmodel->valuestring) ){
            printf("set vo->outputmodel set failed");
        }
    }

    cJSON *force_output = cJSON_GetObjectItemCaseSensitive(value, "force_output");
    if (force_output) {
        if(cJSON_IsTrue(force_output)){
            if( !softwareconfig->SetConfig(SoftwareConfig::kForceOutput, "1") ){
                printf("set vo->force_output set failed");
            }
        }
        else if(cJSON_IsFalse(force_output)){
            if( !softwareconfig->SetConfig(SoftwareConfig::kForceOutput, "0") ){
                printf("set vo->force_output set failed");
            }
        }
    }





    softwareconfig->SaveConfig();

    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());


    SIZE_S vo_size = {viParam[0].u32W,viParam[0].u32H};

    if(edid_4k == 1)
     {
        himppmaster->VoModuleDestroy(0);
        usleep(2000);
        himppmaster->VoModeuleInit(vo_size,0);

    }
    else{

        himppmaster->VoModuleDestroy(1);
       // himppmaster->VoModuleDestroy(1);
        usleep(2000);
        himppmaster->VoModeuleInit(vo_size,1);
       // himppmaster->VoModeuleInit(vo_size,1);

    }




    return true;
}



bool CommandMap::GetShellHandler(cJSON *value)
{
    bool ret = false;
    cJSON* js_ret = NULL;

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "cmd");
    if (js_ret && cJSON_IsString(js_ret)) {
        char cmd[1024] = "";

        if(!strncmp(js_ret->valuestring, "rm", 2)       ||
           !strncmp(js_ret->valuestring, "reboot", 6)   ||
           !strncmp(js_ret->valuestring, "poweroff", 8) ||
           !strncmp(js_ret->valuestring, "halt", 4)     ||
           !strncmp(js_ret->valuestring, "kill", 4)){
            sprintf(cmd, "echo %s > /tmp/cmd.tmp", "\"rm/reboot/poweroff/halt/kill are not supported in this API\"");
        }
        else
            sprintf(cmd, "%s > /tmp/cmd.tmp", js_ret->valuestring);
        system(cmd);

        js_ret = cJSON_GetObjectItemCaseSensitive(value, "reply_way");
        if (js_ret && cJSON_IsString(js_ret)) {
            ret = this->SendFileMsg(js_ret, "/tmp/cmd.tmp", "[shell_cmd]");
            if(!ret){
                COMMON_PRT("get /tmp/cmd.tmp failed!");
            }
        }
    }

    return true;
}

bool CommandMap::SetTickMasterHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    cJSON *js_ret = NULL;

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "auto");
    if (js_ret) {
        if(cJSON_IsTrue(js_ret)){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickAuto, "1") ){
                COMMON_PRT("set tickmaster->auto set failed");
            }
        }
        else if(cJSON_IsFalse(js_ret)){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickAuto, "0") ){
                COMMON_PRT("set tickmaster->auto set failed");
            }
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "module");
    if (js_ret && cJSON_IsString(js_ret)) {
        if ( !strcmp(js_ret->valuestring, "master") ||
             !strcmp(js_ret->valuestring, "slave")){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickModule, js_ret->valuestring) ){
                COMMON_PRT("set tickmaster->module set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->module Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "rate");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if(js_ret->valueint > 0 && js_ret->valueint < 1024){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickRate, js_ret->valueint) ){
                COMMON_PRT("set tickmaster->rate set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->rate Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "offset");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if(js_ret->valueint > -900 && js_ret->valueint < 900){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickOffset, js_ret->valueint) ){
                COMMON_PRT("set tickmaster->offset set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->offset Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "master_delay");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if(js_ret->valueint >= -1 && js_ret->valueint < 16700){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickMDelay, js_ret->valueint) ){
                COMMON_PRT("set tickmaster->master_delay set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->offset Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "master_ip");
    if (js_ret && cJSON_IsString(js_ret)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kTickServerIp, js_ret->valuestring) ){
            COMMON_PRT("set tickmaster->master ip set failed");
        }
    }


    js_ret = cJSON_GetObjectItemCaseSensitive(value, "master_port");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kTickServerPort, js_ret->valueint) ){
            COMMON_PRT("set tickmaster->master port set failed");
        }
    }

    softwareconfig->SaveConfig();

    usleep(100);
    //NOTE: 重启tickMaster,完成主从节点设置的实时生效
    if (softwareconfig->GetConfig(SoftwareConfig::kTickModule) == "master"){
        if(gb_master == false){
            system("killall tickMaster");
            gb_master = true;
        }
    }
    else{
        if(gb_master == true){
            system("killall tickMaster");
            gb_master = false;
        }
    }

    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());

    return true;
}


bool CommandMap::GetMsgHandler(cJSON *value)
{
    bool ret = false;
//printf("*****************************************GetMsgHandler\n");
    cJSON *version = cJSON_GetObjectItemCaseSensitive(value, "get_version");
    if (version) {
        char filepath[24];
        sprintf(filepath, "%s/%s", VERSION_DIR, VERSION_FILE);
        ret = this->SendFileMsg(version, filepath, "[version]");
        if(!ret){
            COMMON_PRT("get version failed!");
        }
    }

    cJSON *uuid = cJSON_GetObjectItemCaseSensitive(value, "get_uuid");
    if (uuid) {
        char filepath[24];
        sprintf(filepath, "%s/%s", UUID_DIR, UUID_FILE);
        ret = this->SendFileMsg(uuid, filepath, "[uuid]");
        if(!ret){
            COMMON_PRT("get uuid failed!");
        }
    }

    cJSON *venc_config = cJSON_GetObjectItemCaseSensitive(value, "get_venc_config");
    if (venc_config) {
        ret = this->SendFileMsg(venc_config, filepath_, "[venc]");
        if(!ret){
            COMMON_PRT("get hi3531d_venc.ini failed!");
        }
    }

    cJSON *net_config = cJSON_GetObjectItemCaseSensitive(value, "get_net_config");
    if (net_config) {
        printf("********************get_net\n");
        ret = this->SendFileMsg(net_config, netfilepath_, "[net]");
        if(!ret){
            COMMON_PRT("get net.ini failed!");
        }
    }





    return true;
}

bool CommandMap::DeleteConfigHandler(cJSON *value)
{
    char command[50] = "";
    sprintf(command, "rm %s\n", filepath_);
    system(command);

    return true;
}

bool CommandMap::LoadEchoUdp()
{
    if(echoudp_ != NULL)
        delete echoudp_;

    echoudp_ = new UDPSocket();
    if(gClientPort != 0)
        echoudp_->CreateUDPClient(gClientIP, gClientPort); //设为非阻塞
    else
        return false;

    return true;
}

bool CommandMap::SendFileMsg(cJSON *value, const char *filepath, const char *key)
{
    if (value && cJSON_IsString(value)) {
        UDPSocket *udpsocket = new UDPSocket();
        if(!strcmp(value->valuestring, "echo")){
            udpsocket->CreateUDPClient(gClientIP, gClientPort);
        }
        else{
            vector<string> infolist;
            SplitString(value->valuestring, infolist, "@");

            udpsocket->CreateUDPClient(infolist[0].c_str(), atoi(infolist[1].c_str()));
        }

        ifstream in(filepath);
        ostringstream content;
        content << key << endl << in.rdbuf();


        if( udpsocket->SendTo(content.str().c_str(), content.str().length()) <= 0){
            COMMON_PRT("udp sendto failed!");
            delete udpsocket;
            return false;
        }

        delete udpsocket;
    }
    else{
        COMMON_PRT("get json Invalid format");
        return false;
    }

    return true;
}

bool CommandMap::LetMeTellYou(const char *msg)
{
    if((gClientPort != 0) &&
       (strlen(gClientIP) != 0)){
        COMMON_PRT("echo data: %s", msg);
        if( echoudp_->SendTo(gClientIP, gClientPort, msg, strlen(msg)) < 0 ){
            COMMON_PRT("echoudp sendto error: %s", strerror(errno));
            return false;
        }
    }

    return true;
}

void CommandMap::ClearCmdTypeAndFunc()
{
    cmdfunc_.clear();
    cmdtype_.clear();
}
void CommandMap::ACK(const char *code, const char *msg)
{
    char sendmsg[128] = "";
    sprintf(sendmsg, code, msg);
    this->LetMeTellYou(sendmsg);
}

void CommandMap::ACK(const char *code, int errcode, const char *msg)
{
    char sendmsg[128] = "";
    sprintf(sendmsg, code, msg, errcode);
    this->LetMeTellYou(sendmsg);
}
