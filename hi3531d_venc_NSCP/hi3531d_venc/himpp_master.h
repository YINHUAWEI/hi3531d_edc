#ifndef HIMPP_MASTER_H
#define HIMPP_MASTER_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "hi3531d_common/sample_comm.h"
#include "hi35xx_comm_fun.h"
#include "udpsocket/udpsocket.h"
#include "software_config.h"
#include "mytimer.h"
#include "global.h"

#define SNAP_LEN_MAX 1200
#define INITIVE    HI_TRUE

#define VI_CH0_REG  0x130D05F0
#define VI_CH8_REG  0x130D85F0
#define VI_CH16_REG 0x130E05F0
#define VI_CH24_REG 0x130E85F0

#define VI_CH4_REG      0x130D45F0
#define VI_CH12_REG     0x130DC5F0
#define VI_CH20_REG  0x130E45F0
#define VI_CH28_REG  0x130EC5F0
#define SNAPMAX   4

#ifdef __TINY_RTSP_SERVER__
#include <tiny_rtsp_server_i.h>
extern tiny_rtsp_server_i* g_prtspserver;
#else
#include "librtsps_i.h"
#include "aac/aac_encode.h"
extern aacenc *g_audio_aac[4];
#endif
extern int g_stream[8];
extern int osd_enable[VI_PORT_NUMBER];
extern bool l_startloop[4];
extern bool vpssbing_4k[8];
extern bool audio_running;
extern bool audio_running_ai[4];
extern bool  snap_loop[4];
 extern bool   snap_runlag[4];
 extern bool  mult_snap_runlag[4];
 extern int  kerrFrameNum ;
extern int vi_flag[VI_PORT_NUMBER];
extern HI_U32 osd_reset[VI_PORT_NUMBER];
extern pthread_t input_tid[4],input_tid1[4];
extern HI_U32 snaploop_flag[4];
extern AENC_CHN Aechn0Chn;
extern AENC_CHN Aechn1Chn;
extern AENC_CHN Aechn2Chn;
extern AENC_CHN Aechn3Chn;
extern HI_S32 theloop;
extern AENC_CHN Aechn[4];
extern  HI_U32 VencMax ;
extern IVE_MEM_INFO_S  pstMap;
extern HI_BOOL snapflag , snaploop[4],snapPth[4];
extern  int  errFrame[4];
extern SIZE_S  stMaxSize_;
extern HI_U32   edid_4k;
extern bool mult_snap[4];
extern  SAMPLE_VI_MODE_E gEnViMode1 ,gEnViMode2;
extern bool snapmcast_loop[4];
extern bool  gb_master;
extern HI_U8 *svAac[4];
extern int svAacNum[4];
extern bool startFlag[4];
extern int svpts[4];

extern bool ai_mm35;
bool swAudioFormat(int port);

class HimppMaster
{
public:
    HimppMaster();
    ~HimppMaster();

  bool InitHimpp1(SIZE_S stVdSize, SIZE_S *stVoSize, HI_U32 *u32DispFrmRt, \
                                VO_INTF_SYNC_E enIntfSync, PAYLOAD_TYPE_E enType, HI_S32 s32VdChnCnt, \
                                HI_S32 s32VpGrpCnt, HI_S32 s32VencChnCnt);
    bool InitHimpp(HI_S32 vdecMaxCnt,HI_S32 vpssMaxCnt, HI_S32 vencMaxCnt);
    bool ViModuleInit_1( VI_DEV ViDev, VI_CHN ViChn,VIDEO_NORM_E enNorm,SAMPLE_VI_MODE_E enViMode,SIZE_S stTargetSize , \
                         RECT_S stCapRect,HI_U32 SrcFrmRate,HI_U32 DrcFrmRate ,HI_U32 DrcFrmRate_S1);
    bool VencModuleInit_1(HI_S32 veNum,HI_U32 u32VeChnCnt, SIZE_S stSize,HI_S32 snapFlag);
    bool VpssModuleInitvo( HI_S32 vpstart,HI_S32 vpssGrp, HI_S32 vpssChn,SIZE_S vpchnsize);

    bool VpssModuleInit(HI_S32 vpstart,HI_S32 vpssGrp, HI_S32 vpssChn,SIZE_S vpchnsize);
    bool ViModuleInit(VI_DEV ViDev, VI_CHN ViChn,SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type,HI_S32 num);
    bool Vi1ModuleInit(SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type);
    bool Vi2ModuleInit(SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type);
    bool Vi3ModuleInit(SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type);
    bool VencModuleInit(SIZE_S stVencSize , SIZE_S stMinor1Size , SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp, int viPort, HI_BOOL setFlag);
//    bool Venc1ModuleInit(SIZE_S stVencSize,SIZE_S stMinor1Size,SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp);
//    bool Venc2ModuleInit(SIZE_S stVencSize,SIZE_S stMinor1Size,SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp);
//    bool Venc3ModuleInit(SIZE_S stVencSize,SIZE_S stMinor1Size,SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp);

    bool VdecModuleInit(HI_U32 u32VdCnt);
    bool VoModeuleInit(SIZE_S Vi_size_vo,HI_U32 vo_dev);

    bool BindPreViewSys(HI_S32 vpss_bin);
    bool BindPreViewSys_4k(HI_U32 vpss_bin,HI_S32 vpGrp);
    bool BindPreVoewSys(VO_LAYER vodev,VO_CHN voChN,VENC_CHN vestart,VENC_CHN veend);
    bool SnapBindPreViewSys(VI_DEV viDev,VI_CHN viChn);

    bool SetUserPic();
    bool EnableUserPic(VI_CHN ViChn);
    bool DisableUserPic(VI_CHN ViChn);
    bool StopViModule(VI_DEV ViDev, VI_CHN ViChn);
    bool VdecModuleDestroy(HI_U32 u32VdCnt);
    bool VencModuleDestroy(HI_U32 u32VeChnStart, HI_U32 u32VeChnCnt );
    bool SnapModuleDestroy(HI_U32 VencSnapChn);
    bool VoModuleDestroy(HI_U32 Vo_dev);
    bool VpssModuleDestroy();
    bool StartLoop(HI_U32 streamstart, HI_U32 streamend, HI_U32 vi_port);
    bool StartLoop_1(HI_U32 streamstart, HI_U32 streamend, HI_U32 vi_port);
    bool GetVencSnap(HI_U32 SnapChn, UDPSocket *socket, const char* clientip, const HI_U32 clientport, HI_U32 snaplen);
    HI_S32 VencSnapInit(HI_S32 veChn,SIZE_S stSize,HI_BOOL bindFlag);

    bool GetVencRateSnap(HI_U32 SnapChn, UDPSocket *socket, const char *clientip, const HI_U32 clientport, HI_U32 snaplen, HI_U32 sockfd, HI_U32 tcpflag);


    bool VencSnapDestroy(VENC_CHN veChn);
    bool AiModeuleInit(AUDIO_DEV AiDevId,AI_CHN AiChnId,AUDIO_SAMPLE_RATE_E enAiSampleRate);
    bool AencModeuleInit(HI_S32 aencNum,AUDIO_SAMPLE_RATE_E enAiSampleRate);
    bool StartAudioEnc(AENC_CHN AeChn_, HI_S32 AiChn);
    bool ModeuleBind();
    bool ModeuleAiBind(AUDIO_DEV AiDev);
    bool AudioModuleDestroy();
    bool SnapUNBindPreViewSys(VI_DEV viDev,VI_CHN viChn);
    bool StartAudioMcast(AUDIO_DEV AiDev,AI_CHN AiChn);
    bool SnapUNBindVpss(VENC_CHN snapVenc);
    bool  AiModeuleInit_1(AUDIO_DEV aiDev,AI_CHN aiChnCnt,HI_U32 numPerFrm,AUDIO_SAMPLE_RATE_E enAiSampleRate);
    bool  StartAudioEnc_1( AI_CHN AiChn_, AUDIO_DEV Aidev);
     bool snapModuleInit(VENC_CHN Snapchn);
     bool VencSnapInit_1(HI_S32 veChn,SNAPPARAM *SNAP);
    bool AiBindAenc(AENC_CHN aeChn,AUDIO_DEV aiDev,AI_CHN aiChn);
 bool VencModuleDestroy_1(HI_U32 u32VeChnStart, HI_U32 u32VeChnCnt,HI_S32 Snap);
public:
    bool resetVenc[4] = {false,false,false,false};    //用于退出VI-》VENC-》RTSP/组播线程
    HI_BOOL snap_running[4] = {HI_TRUE,HI_TRUE,HI_TRUE,HI_TRUE  };


private:
    //VIDEO_NORM_E enNorm_    = VIDEO_ENCODING_MODE_NTSC;
    //SAMPLE_RC_E enRcMode_   = SAMPLE_RC_CBR;
    VDEC_CHN VdChn_[2]={0,1};  //用于解码采集到的视频，并且作90度旋转
    ROTATE_E enRotate_ = ROTATE_NONE;
    HI_S32   VpssChnCnt_ = 1; //vpss的chn固定使用第0号通道，故个数为1
    HI_S32   VpssGrpCnt_ = 4; // = 5;
    VO_LAYER VoLayer_ = SAMPLE_VO_LAYER_VHD0;  // = SAMPLE_VO_LAYER_VHD0;
    VO_DEV   VoDev_ = SAMPLE_VO_DEV_DHD0;    // = SAMPLE_VO_DEV_DHD0;
    VI_DEV   ViDev_[4] = {1,3,5,7};    // = 1;
    VI_CHN   ViChn_[4] = {4,12,20,28};    // = 4;
    VENC_CHN VencChn_[16]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};  // = 0;
    VENC_CHN VencSnapChn_[SNAPMAX];  // = 1;
    VENC_CHN VencMinorChn_[2];
    HI_U32   VdChnCnt_; // = 1;
    HI_U32   VoChn_ = 0;    // = 0;
    HI_S32   VencStreamCnt_;
    HI_S32   VencChnCnt_;
    bool SetNosignal = false;

    AI_CHN   AiChn_ = 0;
    AENC_CHN AeChn_ = 0;
    AUDIO_DEV AiDev_;
    PAYLOAD_TYPE_E  enPayloadType_ = PT_G711A;
    HI_BOOL         bUserGetMode_ = HI_FALSE;

    HI_S32 s32AiChnCnt_;
    HI_S32 s32AencChnCnt_;

    // SIZE_S stMaxSize_ = {1920,1080};
  //  SIZE_S stMaxSize_ = {3840,2160};
    SIZE_S stVencSize_[4];
    SIZE_S stVenc1Size_[4];
    SIZE_S stVenc2Size_[4];
    SIZE_S stVenc3Size_[4];
    SIZE_S stVoSize_[VI_PORT_NUMBER];


    typedef struct snap_data
    {
        HI_U32   u32TotalPack;  //总包数
        HI_U32   u32CurrPack;   //当前包数
        HI_U8    pData[SNAP_LEN_MAX]; //图片信息
    }SNAP_DATA;
    bool SendSnap(HI_U8 *pu8Data, HI_U32 u32TotalLen, HI_U32 u32PackLen, UDPSocket *socket, const char* clientip, const uint clientport, HI_U32 SnapDelay,HI_BOOL snap_muil,HI_U32 Snap_chN);

};

#endif // HIMPP_MASTER_H
