﻿#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/netlink.h>
#include <fstream>
#include <sstream>
#include <sys/shm.h>

#include <semaphore.h>
#include <common.h>
#include "himpp_master.h"
#include "http_handler.h"
//#include "../common/singleton.h"
//#include "../common/common.h"
#include "software_config.h"
#include "commandmap.h"
//#include "common.h"
#define NETLINK_VIDEO_PROTOCOL     30
#define MAX_PLOAD        100
#define USER_PORT        100

#define MAXDATA BUFF_SIZE

HI_U32 viNumber = 0;
HI_U32 aiNumber = 0;
PARAMETER viParam[4];
HI_BOOL vo_size_vi;
HI_BOOL snapflag = HI_TRUE;
HI_BOOL snaploop[4] = {HI_FALSE,HI_FALSE,HI_FALSE,HI_FALSE};
HI_BOOL snapPth[4] = {HI_FALSE,HI_FALSE,HI_FALSE,HI_FALSE};
bool gb_master = false;
bool startFlag[4] = {true,true,true,true};
extern HI_U8 *svAac[4] = {NULL,NULL,NULL,NULL};
int svAacNum[4] = {0,0,0,0};
bool main_exit_while = true;
int svpts[4];
//SAMPLE_VI_MODE_E enViMode = SAMPLE_VI_MODE_8_1080P;
HI_U32  snaploop_flag[4] ={HI_TRUE,HI_TRUE,HI_TRUE,HI_TRUE};
struct KoData
{
    unsigned int frame_num;
    long sec;	/* Seconds.  */
    long usec;	/* Microseconds.  */
};

typedef struct master_data
{
    int     master_rate; //主节点调节中值
    KoData  ko_date;
}MASTER_DATA;




bool l_it66021fn_running = true;
bool l_it66021fn1_running = true;
bool l_it66021fn2_running = true;
bool l_it66021fn3_running = true;

bool l_gs2971a_running = false;
int  kerrFrameNum  ;
bool audio_running =  true;
bool audio_running_ai[4]  =  {true ,true,true,true};
bool  snap_runlag[4] = {true ,true,true,true};
bool mult_snap_runlag[4] = {true,true,true,true};
bool snap_loop[4] = {false,false,false,false};
AENC_CHN Aechn0Chn;
AENC_CHN Aechn1Chn;
AENC_CHN Aechn2Chn;
AENC_CHN Aechn3Chn;

AENC_CHN Aechn[4];


HI_BOOL audio_HDMI0 = HI_FALSE;
HI_BOOL audio_HDMI1 = HI_FALSE;
HI_BOOL audio_I2S = HI_FALSE;

HI_BOOL audio_HDMI0_Mcast = HI_FALSE;
HI_BOOL audio_HDMI1_Mcast = HI_FALSE;
HI_BOOL audio_I2S_Mcast = HI_FALSE;

HI_BOOL g_vi0_hdmi = HI_TRUE;
HI_BOOL g_vi1_hdmi = HI_TRUE;
HI_BOOL g_vi2_hdmi = HI_TRUE;
HI_BOOL g_vi3_hdmi = HI_TRUE;
HI_BOOL gstartLoop[4];
HI_BOOL venc_flag[4];
bool l_recvrunning = true;
bool  sil9022_star = true;
#define NETLINK_IT66021FNBASE 21
#define GROUP_IT66021FNBASE 5

#define MAX_PAYLOAD 1024
#define NETLINK_IT66021FN 21
#define GROUP_IT66021FN   5
#define NETLINK_IT66021FN1 22
#define GROUP_IT66021FN1   6
#define NETLINK_IT66021FN2 23
#define GROUP_IT66021FN2   7
#define NETLINK_IT66021FN3 24
#define GROUP_IT66021FN3   8
HI_S32 VencStreamCnt;
HI_S32 Venc1StreamCnt;
HI_S32 Venc2StreamCnt;
HI_S32 Venc3StreamCnt;

//#define 	VIDEO_INPUT_MODE_HDMI  		0x02
//#define   	VIDEO_SET_CAPTRUE_PARAM     0x04


#define   	VIDEO_INPUT_GET_STATUE		0x00
#define   	VIDEO_CAPTRUE_START         0x01
#define   	VIDEO_CAPTRUE_STOP          0x02
#define   	VIDEO_COCDEC_INIT           0x03
#define   	VIDEO_SET_CAPTRUE_PARAM     0x04

#define   	VIDEO_OUTPUT_GET_STATUE     0x10
#define   	VIDEO_SET_VO_PARAM     	    0x11
#define   	VIDEO_GET_SINK_CAPABILITY    0x12

/********** VIDEO INPUT MODE *******/
#define 	VIDEO_INPUT_MODE_NONE  		0x00
#define 	VIDEO_INPUT_MODE_VGA  		0x01
#define 	VIDEO_INPUT_MODE_HDMI  		0x02
#define 	VIDEO_INPUT_MODE_SDI  		0x03

/********** AUDIO INPUT MODE *******/
#define     AUDIO_8K_SAMPLERATE	         0x00
#define     AUDIO_12K_SAMPLERATE	     0x01
#define     AUDIO_16K_SAMPLERATE	     0x02
#define     AUDIO_24K_SAMPLERATE	     0x03
#define     AUDIO_32K_SAMPLERATE	     0x04
#define     AUDIO_48K_SAMPLERATE	     0x05
#define     AUDIO_11_025K_SAMPLERATE	 0x06
#define     AUDIO_22_05K_SAMPLERATE	 	 0x07
#define     AUDIO_44_1K_SAMPLERATE	 	 0x08




#define NODE_MODEL  "input_4hdmi"
struct SW_VERSION sw_version =
{
    "1.0.9.0",     //<主版本号>.<次版本号>.<修订版本号>
    //<阶段版本>
#ifdef __DEBUG__
    //    STAGE_BASE
    STAGE_ALPHA
    //    STAGE_BETA
#else
    //    STAGE_RC
    STAGE_RELEASE
#endif
};

typedef struct
{
    unsigned int input_src:2;	// 0: NONE   1: VGA   2:HDMI  3:SDI
    int 		 video_fmt:8;
    unsigned int sample:4;
    unsigned int mute_on:1;
    unsigned int stereo:1;		// 0: multi-channel   1: stereo
    unsigned int chip_num:4;		// 1: content encrypted
    unsigned int HDCP_encrypted:1;
    unsigned int fps:8;
    unsigned int reserved :3;
} Media_Input_Fmt;




typedef struct videofarmat {
    unsigned int video_ch;
    unsigned int video_width;
    unsigned int video_height;
    unsigned int video_freq;
    unsigned int video_interlaced;
    unsigned int video_hdcp;
    unsigned int augio_freq;
} Media_Input_Fmt_new;

typedef struct _user_msg_info
{
    struct nlmsghdr hdr;
    struct videofarmat vf;
} user_msg_info;




typedef enum HDMI_VIDEO_FMT_E
{
    HDMI_VIDEO_FMT_1080P_60 = 0,
    HDMI_VIDEO_FMT_1080P_50,
    HDMI_VIDEO_FMT_1080P_30,
    HDMI_VIDEO_FMT_1080P_25,
    HDMI_VIDEO_FMT_1080P_24,

    HDMI_VIDEO_FMT_1080i_60,
    HDMI_VIDEO_FMT_1080i_50,

    HDMI_VIDEO_FMT_720P_60,   //7
    HDMI_VIDEO_FMT_720P_50,

    HDMI_VIDEO_FMT_576P_50,
    HDMI_VIDEO_FMT_480P_60,

    HDMI_VIDEO_FMT_PAL,
    HDMI_VIDEO_FMT_PAL_N,
    HDMI_VIDEO_FMT_PAL_Nc,

    HDMI_VIDEO_FMT_NTSC,   //14
    HDMI_VIDEO_FMT_NTSC_J,
    HDMI_VIDEO_FMT_NTSC_PAL_M,

    HDMI_VIDEO_FMT_SECAM_SIN,
    HDMI_VIDEO_FMT_SECAM_COS,

    HDMI_VIDEO_FMT_861D_640X480_60,  //19
    HDMI_VIDEO_FMT_VESA_800X600_60,
    HDMI_VIDEO_FMT_VESA_1024X768_60,
    HDMI_VIDEO_FMT_VESA_1280X720_60,
    HDMI_VIDEO_FMT_VESA_1280X800_60,
    HDMI_VIDEO_FMT_VESA_1280X1024_60,
    HDMI_VIDEO_FMT_VESA_1366X768_60,
    HDMI_VIDEO_FMT_VESA_1440X900_60,  //26
    HDMI_VIDEO_FMT_VESA_1440X900_60_RB,
    HDMI_VIDEO_FMT_VESA_1600X900_60_RB,
    HDMI_VIDEO_FMT_VESA_1600X1200_60,
    HDMI_VIDEO_FMT_VESA_1680X1050_60,  //30
    HDMI_VIDEO_FMT_VESA_1920X1080_60,
    HDMI_VIDEO_FMT_VESA_1920X1200_60,
    HDMI_VIDEO_FMT_VESA_2048X1152_60,
    HDMI_VIDEO_FMT_2560x1440_30,
    HDMI_VIDEO_FMT_2560x1440_60,
    HDMI_VIDEO_FMT_2560x1600_60,
    HDMI_VIDEO_FMT_1920x2160_30,  //37

    HDMI_VIDEO_FMT_3840X2160P_24,
    HDMI_VIDEO_FMT_3840X2160P_25,
    HDMI_VIDEO_FMT_3840X2160P_30,  //40
    HDMI_VIDEO_FMT_3840X2160P_50,
    HDMI_VIDEO_FMT_3840X2160P_60,  //42

    HDMI_VIDEO_FMT_4096X2160P_24,
    HDMI_VIDEO_FMT_4096X2160P_25,
    HDMI_VIDEO_FMT_4096X2160P_30,
    HDMI_VIDEO_FMT_4096X2160P_50,
    HDMI_VIDEO_FMT_4096X2160P_60,

    HDMI_VIDEO_FMT_VESA_CUSTOMER_DEFINE,

    HDMI_VIDEO_FMT_BUTT
}HDMI_VIDEO_FMT_E;

typedef enum {
    VIN_RESOLUTION_1920x1080P  = 1,
    VIN_RESOLUTION_1440x900P  = 2,
    VIN_RESOLUTION_1366x768P  = 3,
    VIN_RESOLUTION_1280x1024P  = 4,
    VIN_RESOLUTION_1280x800P   = 5,
    VIN_RESOLUTION_1280x768P   = 6,
    VIN_RESOLUTION_1280x720P   = 7,
    VIN_RESOLUTION_1024x768P   = 8,
    VIN_RESOLUTION_800x600P   = 9,
    VIN_RESOLUTION_720x576P    = 10,
    VIN_RESOLUTION_720x480P    = 11,
    VIN_RESOLUTION_640x480P    = 12,
    VIN_RESOLUTION_3840x2160   = 13,
    VIN_RESOLUTION_UNKOWN      = -1

} e_Video_FMT;



struct msg_struct
{
    unsigned video_width;
    unsigned video_height;
    unsigned video_freq;
    unsigned video_interlaced;   //1-全I帧，识别为无信号
    unsigned video_hdcp;
    unsigned audio_freq;
};

//Media_Input_Fmt inputGetFmt[4];

typedef struct
{
    bool bVideoFmtSupported[HDMI_VIDEO_FMT_BUTT];
    unsigned char video_fmt;
    unsigned char connect;	// f: none  t: connectedd
    unsigned char src_mode;	// 3531d output fmt ,bt1120 or vga
} Media_Output_Fmt;





int fdfun = -1;
int fdfun1[4] = {-1,-1,-1,-1};
bool it6801Fun_false[4] = {true,true,true,true};
pthread_t input_tid[4] = {0,0,0,0};
pthread_t input_tid1[4] = {0,0,0,0};

bool it6801get_false = true;


int close_fd[4];
//SAMPLE_VI_MODE_E gEnViMode = SAMPLE_VI_MODE_4_1080P;
SAMPLE_VI_MODE_E gEnViMode = SAMPLE_VI_MODE_4_4k;

VIDEO_NORM_E gEnNorm = VIDEO_ENCODING_MODE_NTSC ;

int diff_num = 0;
uint diffNum[4] = {0,0,0,0};

pthread_t audio_tid[4];
pthread_t audio_tid1[4];
pthread_t snap_tid[4];
int  snap_port[4];
Media_Input_Fmt inputFmt[4];
bool fStart[4] = {true,true,true,true};
pthread_t tid_vi[4];
sem_t sem1,sem2,sem3,sem4,sem5,sem6;
HI_S32 theloop = 0;
int errFrame[4]  = {0,0,0,0};
HI_U32 Usevenchn = 2;

HI_U32 osd_reset[4] ={0,0,0,0};

HI_U32 VencMax = 8;

void* input1proc(void *arg);
void *snap0ServerProc(void *arg);
void *snap1ServerProc(void *arg);
void *snap2ServerProc(void *arg);
void *snap3ServerProc(void *arg);
void *rtspThread(void * arg);
void* it6801Fun(void* arg);
void* it6801Fun1(void* arg);
void * multiSnapProc(void * arg);
int getViParam(int vi,user_msg_info inputFmt,SIZE_S *viSize,RECT_S *viCapRect);



unsigned int netlink_group_mask(unsigned int group){
    return group ? 1<<(group-1):0;
}
#if 0
void* audioproc(void *arg)
{
    uint viport = *(uint*)arg;


    AENC_CHN AeChn = -1;
    switch (viport) {
    case 0:
        AeChn = Aechn[viport];
        break;
    case 1:
        AeChn = Aechn[viport];
        break;
    case 2:
        AeChn = Aechn[viport];
        break;
    case 3:
        AeChn = Aechn[viport];
        break;
    default:
        break;
    }
    printf("audioproc------AEchn-----%d---i==%d\n",AeChn,viport);
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    AUDIO_DEV Aidev = viport ;
    AI_CHN   aichn = 0;

#if 1

    if( AeChn == 0  ) {
        if(audio_HDMI0) {
            printf("have start HDMI0\n");
            return (void*)0;
        }
        else {
            printf(" - start vi:%d aenc:%d----\n",viport,AeChn);
            audio_HDMI0 = HI_TRUE;

            himppmaster->StartAudioEnc_1(aichn,Aidev);
            //  StartAudioEnc_s(aichn,Aidev);
        }
    }
    else if(AeChn == 1 ) {
        if(audio_HDMI1) {
            printf("have start HDMI1\n");
            return (void *)0;
        }
        else {
            audio_HDMI1 = HI_TRUE;
            printf("-----start vi:%d aenc:%d-----\n",viport,AeChn);
            himppmaster->StartAudioEnc_1(aichn, viport);


        }
    }
    else if(AeChn == 2)
    {

        himppmaster->StartAudioEnc_1(aichn,viport);
    }

    //    else if(AeChn == 2 ) {
    //        if(audio_I2S) {
    //            printf("have start 3.5mm\n");
    //            return (void*)0;
    //        }
    //        else {
    //            audio_I2S = HI_TRUE;
    //            printf("----start vi:%d aenc:%d----\n",viport,AeChn);
    //            himppmaster->StartAudioEnc_1(AeChn, viport);
    //        }
    //    }




#endif
    return NULL;
}
#endif

bool star_35mmai= true;

void* audioproc(void *arg)
{
    uint viport = *(uint*)arg;
    //Aechn[viport];

    AENC_CHN AeChn = viport;
    //    switch (viport) {
    //        case 0:
    //            AeChn = Aechn[viport];
    //            break;
    //        case 1:
    //            AeChn = Aechn[viport];
    //            break;
    //        case 2:
    //            AeChn = Aechn[viport];
    //            break;
    //        case 3:
    //            AeChn = Aechn[viport];
    //            break;
    //        default:
    //            break;
    //    }
    printf("audioproc------AEchn-----%d---i==%d\n",AeChn,viport);
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    AUDIO_DEV Aidev = viport ;
    AI_CHN   aichn = 0;

#if 1

    if( AeChn == 0  ) {


        audio_HDMI0 = HI_TRUE;

        himppmaster->StartAudioEnc_1(aichn,Aidev);
        //  StartAudioEnc_s(aichn,Aidev);

    }
    else if(AeChn == 1 ) {

        printf("-----start vi:%d aenc:%d-----\n",viport,AeChn);
        himppmaster->StartAudioEnc_1(aichn, viport);


    }
    else if(AeChn == 2)
    {

        printf("a");
        //    himppmaster->StartAudioEnc_1(aichn,viport);
    }










#endif
    return NULL;
}





void* audioMcastProc(void *arg)
{
    uint viport = *(uint*)arg;
    printf("start audio %d\n",viport);
    AENC_CHN AeChn = -1;
    switch (viport) {
    case 0:
        AeChn = Aechn[viport];
        break;
    case 1:
        AeChn = Aechn[viport];
        break;
    case 2:
        AeChn = Aechn[viport];
        break;
    case 3:
        AeChn = Aechn[viport];
        break;
    default:
        break;
    }
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    if(AeChn == 0 ) {
        if(audio_HDMI0_Mcast) {
            printf("have start HDMI0\n");
            return (void*)0;
        }
        else {
            printf(" -----start ai:%d multicast----\n",AeChn);
            audio_HDMI0_Mcast = HI_TRUE;
            himppmaster->StartAudioMcast(AeChn,0);

        }
    }
    else if(AeChn == 1) {
        if(audio_HDMI1_Mcast) {
            printf("have start HDMI1\n");
            return (void *)0;
        }
        else {
            audio_HDMI1_Mcast = HI_TRUE;
            printf("-----start ai:%d multicast-----\n",AeChn);
            himppmaster->StartAudioMcast(AeChn,0);
        }
    }
    else if(AeChn == 2) {
        if(audio_I2S_Mcast) {
            printf("have start 3.5mm\n");
            return (void*)0;
        }
        else {
            audio_I2S_Mcast = HI_TRUE;
            printf("----start ai:%d multicast----\n",AeChn);
            himppmaster->StartAudioMcast(AeChn,0);
        }
    }
    return NULL;
}

typedef SoftwareConfig Config; //把Class类SoftwareConfig的名称缩写以下
int crtl = 0;
void HandleSig(HI_S32 signo)
{
    if(crtl == 1)
        return ;

    printf("stop--HandleSig     master exit\n");
    crtl = 1;


    audio_running_ai[0]= false;
    audio_running_ai[1]= false ;
    audio_running_ai[2]= false ;
    audio_running_ai[3]= false ;

    aumcast_enable[0] = HI_FALSE;
    aumcast_enable[1] = HI_FALSE;
    aumcast_enable[2] = HI_FALSE;
    sil9022_star = false;


    l_gs2971a_running = false;
    int i= 0;

    for(i = 0; i < 4;i++)
    {

        mcast_enable[i] = 0;
        mcast_enable[i + 4 ] = 0;
        snap_loop[i] == false;

        it6801Fun_false[i] = false;
        snap_runlag[i] = false;
        veSnap[i].Snapmcast_enable =HI_FALSE;
        snapmcast_loop[i] = false;
        mult_snap_runlag[i] = false;
        l_startloop[i] = false;
    }

    it6801get_false = false;
    audio_running = false;
    audio_running_ai[0]= false;
    audio_running_ai[1]= false ;
    audio_running_ai[2]= false ;
    audio_running_ai[3]= false ;

    l_recvrunning = false;
    // printf("i sm is close\n");
    int sRet;
    sleep(1);

    //    for( i = 0;i< 8;i++){
    //        crtsps_closestream(g_stream[i]);
    //      //   printf("i am is strcmp(channel->valuestring, chn0 g_strea=%d\n",g_stream[i]);
    //    }
    sRet = crtsps_stop();

    //if(sRet < 0)
    sem_destroy(&sem1);
    sem_destroy(&sem2);
    sem_destroy(&sem3);
    sem_destroy(&sem4);


    Singleton<HimppMaster>::getInstance()->~HimppMaster();

    close(fdfun);
    exit(-1);
}

/*****************************************
 * add function : separation information form client
 * 1-- s_str: information from clinet
 * 2-- c_str: command from clinet
 * 3-- d_str: data from client
 * **************************************/
int GetInfo(const char *s_str,char *c_str,char *d_str)
{
    char str1 = '(';
    char str2 = ')';
    char str3 = '[';
    char str4 = ']';
    while( *s_str && (*s_str++ != str1));
    while(*s_str && (*s_str != str2))
        *c_str++ = *s_str++;
    *c_str = '\0';
    while(*s_str && (*s_str++ !=str3));
    while(*s_str && (*s_str !=str4))
        *d_str++ = *s_str++;
    *d_str ='\0';
    return 0;
}



/*
 * 通过获取IT66021驱动信息，同步到INI保存
 * */


RECT_S synViParam(HI_S32 viWidth, HI_S32 viHeight, HI_S32 srcFramRate,HI_S32 viNum,HI_S32  viInte)
{
    auto thisini = Singleton<Config>::getInstance();
    SIZE_S viSize;
    RECT_S stCapSIZE;
    HI_U32 srcFrRate;
    HI_BOOL cropFlag = HI_FALSE;
    viParam[viNum].u32ViWidth = viWidth;
    viParam[viNum].u32ViHeigth = viHeight;
    viParam[viNum].u32SrcFrmRate = srcFramRate;

    viSize.u32Width = viWidth;
    viSize.u32Height = viHeight;
    srcFrRate = srcFramRate;

    if((128 >=  viWidth) || (128 >= viHeight)) {    //无信号，1080P30FPS
        viSize.u32Width = 1920;
        viSize.u32Height = 1080;
        srcFrRate = 30;
        printf("---check no signal---\n");
    }

    if(viInte ==  1) {
        viSize.u32Width = 1920;
        viSize.u32Height = 1080;
        srcFrRate = 30;
        printf("---check Iframe signal---\n");
    }

    viParam[viNum].u32SrcFrmRate = srcFrRate;

    stCapSIZE.u32Width = viSize.u32Width;
    stCapSIZE.u32Height = viSize.u32Height;
    stCapSIZE.s32X = 0;
    stCapSIZE.s32Y = 0;

    //VENC SIZE DSTFRAMERAT <= VI SIZE  SRCFRAMERATE
    viParam[viNum].stVencSize = { min(viParam[viNum].stVencSize.u32Width,viSize.u32Width), \
                                  min(viParam[viNum].stVencSize.u32Height,viSize.u32Height) };
    printf("vi IT6602 get new master venc Size w:%d,h:%d\n",viParam[viNum].stVencSize.u32Width, \
           viParam[viNum].stVencSize.u32Height);
    viParam[viNum].stMinorSize = {min(viParam[viNum].stMinorSize.u32Width,viSize.u32Width), \
                                  min(viParam[viNum].stMinorSize.u32Height,viSize.u32Height)};
    printf("vi IT6602 get new minor venc Size w:%d,h:%d\n",viParam[viNum].stMinorSize.u32Width, \
           viParam[viNum].stMinorSize.u32Height);

    viParam[viNum].u32DstFrameRate[0] = min(srcFrRate,viParam[viNum].u32DstFrameRate[0]);
    viParam[viNum].u32DstFrameRate[1] = min(srcFrRate,viParam[viNum].u32DstFrameRate[1]);

    //VENC 主次码流 使能编码与VI输出一致
    if(viParam[viNum].venc_SAME_INPUT[0]) {
        viParam[viNum].stVencSize = {viSize.u32Width,viSize.u32Height};
        viParam[viNum].u32DstFrameRate[0] = srcFrRate;
    }

    if(viParam[viNum].venc_SAME_INPUT[1]) {
        viParam[viNum].stMinorSize = {viSize.u32Width,viSize.u32Height};
        viParam[viNum].u32DstFrameRate[1] = srcFrRate;
    }

    //VI裁剪使能，判断裁剪的SIZE 是否大于 VI SIZE，若大于，取消裁剪，并恢复裁剪初始化参数；若小于，编码SIZE <= 裁剪SIZE
    if(viParam[viNum].vi_crop_enable) {    //VI 有裁剪，裁剪SIZE <= VI SIZEVENC 编码SIZE <= 裁剪SIZE
        if(((viParam[viNum].u32X + viParam[viNum].u32W) > viSize.u32Width) ||   \
                ((viParam[viNum].u32Y + viParam[viNum].u32H) > viSize.u32Height)) {   //裁剪设置错误，取消VI裁剪
            viParam[viNum].vi_crop_enable = HI_FALSE;
            viParam[viNum].u32X = 0;
            viParam[viNum].u32Y = 0;
            viParam[viNum].u32W = 0;
            viParam[viNum].u32H = 0;

            cropFlag = HI_TRUE;     //恢复venc.ini无使能状态的裁剪参数
        }
        else {
            stCapSIZE.s32X = viParam[viNum].u32X;
            stCapSIZE.s32Y = viParam[viNum].u32Y;
            stCapSIZE.u32Width = viParam[viNum].u32W;
            stCapSIZE.u32Height = viParam[viNum].u32H;
            viParam[viNum].stVencSize = {min(viParam[viNum].u32W,viParam[viNum].stVencSize.u32Width),
                                         min(viParam[viNum].u32H,viParam[viNum].stVencSize.u32Height)};
            viParam[viNum].stMinorSize = {min(viParam[viNum].u32W,viParam[viNum].stMinorSize.u32Width),
                                          min(viParam[viNum].u32H,viParam[viNum].stMinorSize.u32Height)};

        }
    }
    switch (viNum) {
    case 0:
        thisini->SetConfig(Config::kChn0_VI_W, int2str(viParam[viNum].u32ViWidth));
        thisini->SetConfig(Config::kChn0_VI_H, int2str(viParam[viNum].u32ViHeigth));
        thisini->SetConfig(Config::kChn0_SrcFrmRate,int2str(srcFramRate));
        thisini->SetConfig(Config::kChn0_M_VENC_W,viParam[0].stVencSize.u32Width);
        thisini->SetConfig(Config::kChn0_M_VENC_H,viParam[0].stVencSize.u32Height);
        thisini->SetConfig(Config::kChn0_S0_VENC_W,viParam[0].stMinorSize.u32Width);
        thisini->SetConfig(Config::kChn0_S0_VENC_H,viParam[0].stMinorSize.u32Height);
        thisini->SetConfig(Config::kChn0_M_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[0]));
        thisini->SetConfig(Config::kChn0_S0_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[1]));
        if(cropFlag) {
            thisini->SetConfig(Config::kChn0_VI_Crop_Enable,(int)viParam[viNum].vi_crop_enable);
            thisini->SetConfig(Config::kChn0_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn0_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn0_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn0_VI_Height,viParam[viNum].u32H);
        }
        thisini->SaveConfig();
        break;
    case 1:
        thisini->SetConfig(Config::kChn1_VI_W, int2str(viParam[viNum].u32ViWidth));
        thisini->SetConfig(Config::kChn1_VI_H, int2str(viParam[viNum].u32ViHeigth));
        thisini->SetConfig(Config::kChn1_SrcFrmRate,int2str(srcFramRate));
        thisini->SetConfig(Config::kChn1_M_VENC_W,viParam[1].stVencSize.u32Width);
        thisini->SetConfig(Config::kChn1_M_VENC_H,viParam[1].stVencSize.u32Height);
        thisini->SetConfig(Config::kChn1_S0_VENC_W,viParam[1].stMinorSize.u32Width);
        thisini->SetConfig(Config::kChn1_S0_VENC_H,viParam[1].stMinorSize.u32Height);
        thisini->SetConfig(Config::kChn1_M_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[0]));
        thisini->SetConfig(Config::kChn1_S0_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[1]));
        if(cropFlag) {
            thisini->SetConfig(Config::kChn1_VI_Crop_Enable,(int)viParam[viNum].vi_crop_enable);
            thisini->SetConfig(Config::kChn1_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn1_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn1_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn1_VI_Height,viParam[viNum].u32H);
        }
        thisini->SaveConfig();
        break;
    case 2:
        thisini->SetConfig(Config::kChn2_VI_W, int2str(viParam[viNum].u32ViWidth));
        thisini->SetConfig(Config::kChn2_VI_H, int2str(viParam[viNum].u32ViHeigth));
        thisini->SetConfig(Config::kChn2_SrcFrmRate,int2str(srcFramRate));
        thisini->SetConfig(Config::kChn2_M_VENC_W,viParam[viNum].stVencSize.u32Width);
        thisini->SetConfig(Config::kChn2_M_VENC_H,viParam[viNum].stVencSize.u32Height);
        thisini->SetConfig(Config::kChn2_S0_VENC_W,viParam[viNum].stMinorSize.u32Width);
        thisini->SetConfig(Config::kChn2_S0_VENC_H,viParam[viNum].stMinorSize.u32Height);
        thisini->SetConfig(Config::kChn2_M_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[0]));
        thisini->SetConfig(Config::kChn2_S0_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[1]));
        if(cropFlag) {
            thisini->SetConfig(Config::kChn2_VI_Crop_Enable,(int)viParam[viNum].vi_crop_enable);
            thisini->SetConfig(Config::kChn2_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn2_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn2_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn2_VI_Height,viParam[viNum].u32H);
        }
        thisini->SaveConfig();
        break;
    case 3:
        thisini->SetConfig(Config::kChn3_VI_W, int2str(viParam[viNum].u32ViWidth));
        thisini->SetConfig(Config::kChn3_VI_H, int2str(viParam[viNum].u32ViHeigth));
        thisini->SetConfig(Config::kChn3_SrcFrmRate,int2str(srcFramRate));
        thisini->SetConfig(Config::kChn3_M_VENC_W,viParam[viNum].stVencSize.u32Width);
        thisini->SetConfig(Config::kChn3_M_VENC_H,viParam[viNum].stVencSize.u32Height);
        thisini->SetConfig(Config::kChn3_S0_VENC_W,viParam[viNum].stMinorSize.u32Width);
        thisini->SetConfig(Config::kChn3_S0_VENC_H,viParam[viNum].stMinorSize.u32Height);
        thisini->SetConfig(Config::kChn3_M_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[0]));
        thisini->SetConfig(Config::kChn3_S0_DstFrmRate, int2str(viParam[viNum].u32DstFrameRate[1]));
        if(cropFlag) {
            thisini->SetConfig(Config::kChn3_VI_Crop_Enable,(int)viParam[viNum].vi_crop_enable);
            thisini->SetConfig(Config::kChn3_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn3_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn3_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn3_VI_Height,viParam[viNum].u32H);
        }
        thisini->SaveConfig();
        break;
    default:
        break;
    }


    return stCapSIZE;
}


/*
bool getSize(RECT_S *stCapRect, HI_S32 viNum)
{
    auto thisini = Singleton<Config>::getInstance();
    stCapRect->s32X = 0;
    stCapRect->s32Y = 0;
    stCapRect->u32Width = viParam[viNum].u32ViWidth;
    stCapRect->u32Height = viParam[viNum].u32ViHeigth;
    if(viParam[viNum].venc_SAME_INPUT == HI_TRUE)
        viParam[viNum].stVencSize = {viParam[viNum].u32ViWidth,viParam[viNum].u32ViHeigth};
    if(viParam[viNum].vi_crop_enable == HI_TRUE) {
        viParam[viNum].stVencSize = {min(viParam[viNum].stVencSize.u32Width,viParam[viNum].u32W), min(viParam[viNum].stVencSize.u32Height,viParam[viNum].u32H) };

        viParam[viNum].stMinorSize = {min(viParam[viNum].stMinorSize.u32Width,viParam[viNum].u32W), min(viParam[viNum].stMinorSize.u32Height,viParam[viNum].u32H)};
        if((viParam[viNum].u32X + viParam[viNum].u32W <= viParam[viNum].u32ViWidth)&&(viParam[viNum].u32Y + viParam[viNum].u32H <= viParam[viNum].u32ViHeigth)){
            *stCapRect = {viParam[viNum].u32X, viParam[viNum].u32Y, viParam[viNum].u32W, viParam[viNum].u32H};
            if(viParam[viNum].venc_SAME_INPUT == HI_TRUE){
                thisini->SetConfig(Config::kChn0_M_VENC_W, int2str(viParam[viNum].u32W));
                thisini->SetConfig(Config::kChn0_M_VENC_H, int2str(viParam[viNum].u32H));
                viParam[viNum].stVencSize = {viParam[viNum].u32W, viParam[viNum].u32H};
            }
        }
    }


    return true;
}
*/
/*
 * IT6602 检测到输入源分辨率小于抓图分辨率；抓图分辨率自动降为输入源分辨率大小
 *
 * */
bool reSetSnap(int num,VENC_CHN snapChn,SIZE_S snapSize)
{
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->snap_running[num] = HI_FALSE;
    usleep(10000);
    himppmaster->SnapUNBindVpss(snapChn);
    himppmaster->VencSnapDestroy(snapChn);
    himppmaster->VencSnapInit(snapChn,snapSize,HI_TRUE);
    himppmaster->snap_running[num] = HI_TRUE;
    return true;
}



bool viSnapReset(int viPort,SIZE_S viSize)
{
    veSnap[viPort].snapSize.u32Width = viSize.u32Width;
    veSnap[viPort].snapSize.u32Height = viSize.u32Height;
    VENC_CHN snapVeChn = viPort + 8;
    auto thisini = Singleton<Config>::getInstance();
    switch (viPort) {
    case 0:
        thisini->SetConfig(Config::kChn0_Snap_W,veSnap[viPort].snapSize.u32Width);
        thisini->SetConfig(Config::kChn0_Snap_H,veSnap[viPort].snapSize.u32Height);
        reSetSnap(viPort,snapVeChn,viSize);
        break;
    case 1:
        thisini->SetConfig(Config::kChn1_Snap_W,veSnap[viPort].snapSize.u32Width);
        thisini->SetConfig(Config::kChn1_Snap_H,veSnap[viPort].snapSize.u32Height);
        reSetSnap(viPort,snapVeChn,viSize);
        break;
    case 2:
        thisini->SetConfig(Config::kChn2_Snap_W,veSnap[viPort].snapSize.u32Width);
        thisini->SetConfig(Config::kChn2_Snap_H,veSnap[viPort].snapSize.u32Height);
        reSetSnap(viPort,snapVeChn,viSize);
        break;
    case 3:
        thisini->SetConfig(Config::kChn3_Snap_W,veSnap[viPort].snapSize.u32Width);
        thisini->SetConfig(Config::kChn3_Snap_H,veSnap[viPort].snapSize.u32Height);
        reSetSnap(viPort,snapVeChn,viSize);
        break;
    default:
        break;
    }

    printf("check vi size < snap size ,reset snap\n");
    return true;

}

/*
 * 动态检测IT6602汇报的分辨率和帧率
 * 初始化输入，编码
 * 重启 VENC-RTSP-MULTICAST 线程
 *
 * */
HI_BOOL rtspFlag = HI_FALSE;
HI_BOOL G711Init = HI_FALSE;

#if 0
int getViParam(int vi,Media_Input_Fmt inputFmt,SIZE_S *viSize,RECT_S *viCapRect)
{
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    switch (inputFmt.video_fmt) {
    case VIN_RESOLUTION_3840x2160:
        viSize->u32Width = 3840;
        viSize->u32Height = 2160;
        //  gEnViMode = SAMPLE_VI_MODE_4_4k;
        break;
    case VIN_RESOLUTION_1920x1080P:
        viSize->u32Width = 1920;
        viSize->u32Height = 1080;
        break;
    case VIN_RESOLUTION_1440x900P:
        viSize->u32Width = 1440;
        viSize->u32Height = 900;
        break;
    case VIN_RESOLUTION_1366x768P:
        viSize->u32Width = 1366;
        viSize->u32Height = 768;
        break;
    case VIN_RESOLUTION_1280x1024P:
        viSize->u32Width = 1280;
        viSize->u32Height = 1024;
        break;
    case VIN_RESOLUTION_1280x800P:
        viSize->u32Width = 1280;
        viSize->u32Height = 800;
        break;
    case VIN_RESOLUTION_1280x768P:
        viSize->u32Width = 1280;
        viSize->u32Height = 768;
        break;
    case VIN_RESOLUTION_1280x720P:
        viSize->u32Width = 1280;
        viSize->u32Height = 720;
        break;
    case VIN_RESOLUTION_1024x768P:
        viSize->u32Width = 1024;
        viSize->u32Height = 768;
        break;
    case VIN_RESOLUTION_800x600P:
        viSize->u32Width = 800;
        viSize->u32Height = 600;
        break;
    case VIN_RESOLUTION_720x576P:
        viSize->u32Width = 720;
        viSize->u32Height = 576;
        break;
    case VIN_RESOLUTION_720x480P:
        viSize->u32Width = 720;
        viSize->u32Height = 480;
        break;
    case VIN_RESOLUTION_640x480P:
        viSize->u32Width = 640;
        viSize->u32Height = 480;
        break;
    case VIN_RESOLUTION_UNKOWN:
        viSize->u32Width = 0;
        viSize->u32Height = 0;
        break;
    default:
        break;
    }

    HI_U32 vi_framtare ,vi_0_framtare,vi_1_framtare;


    if(inputFmt.video_fmt == -1)
    {
        viParam[vi].u32ViHeigth = 0;
        viParam[vi].u32ViWidth = 0;

        viParam[vi].u32SrcFrmRate = 30;
        viParam[vi].u32DstFrameRate[0] = 30;
        viParam[vi].u32DstFrameRate[1] = 30;
        vi_framtare = 30;
        vi_0_framtare = 30 ;
        vi_1_framtare = 30;

    }
    else if(inputFmt.video_fmt != -1)
    {
        viParam[vi].u32ViHeigth = viSize->u32Height;
        viParam[vi].u32ViWidth = viSize->u32Width;



        viParam[vi].u32SrcFrmRate = inputFmt.fps;
        //  viParam[vi].u32DstFrameRate[0] = inputFmt.fps;
        //   viParam[vi].u32DstFrameRate[1] = inputFmt.fps;

        //         viParam[vi].u32SrcFrmRate      = 60;
        //         viParam[vi].u32DstFrameRate[0] =  60;
        //          viParam[vi].u32DstFrameRate[1]=  60;

        if(viParam[vi].venc_SAME_INPUT[0] == HI_TRUE )
        {
            viParam[vi].u32DstFrameRate[0] = inputFmt.fps;
            // viParam[vi].u32DstFrameRate[1] = inputFmt.fps;

        }
        if(viParam[vi].venc_SAME_INPUT[1] == HI_TRUE )
        {
            // viParam[vi].u32DstFrameRate[0] = inputFmt.fps;
            viParam[vi].u32DstFrameRate[1] = inputFmt.fps;
        }
        if(inputFmt.fps <= 20) {


            viParam[vi].u32SrcFrmRate = 30;
            viParam[vi].u32DstFrameRate[0] = 30;
            viParam[vi].u32DstFrameRate[1] = 30;

        }
        else if(inputFmt.fps >70) {
            viParam[vi].u32SrcFrmRate = 60;
            viParam[vi].u32DstFrameRate[0] = 60;
            viParam[vi].u32DstFrameRate[1] = 60;
        }


    }
    else
        printf("err \n");

    if(vi == 0)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn0_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_H))};
    }
    else if(vi == 1)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_H))};
    }
    else if(vi == 2)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_H))};
    }
    else if(vi == 3)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_H))};
    }
    else
        printf("err havent\n");
    if(inputFmt.video_fmt != -1)
    {
        if(viParam[vi].stVencSize.u32Height <= 0 || viParam[vi].stVencSize.u32Width <= 0  ||
                viParam[vi].stVencSize.u32Width > viParam[vi].u32ViWidth || viParam[vi].stVencSize.u32Height > viParam[vi].u32ViHeigth )
            viParam[vi].stVencSize  = {viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth};

        if(viParam[vi].stMinorSize.u32Height <= 0 || viParam[vi].stMinorSize.u32Width <= 0  ||
                viParam[vi].stMinorSize.u32Width > viParam[vi].u32ViWidth || viParam[vi].stMinorSize.u32Height > viParam[vi].u32ViHeigth )
            viParam[vi].stMinorSize= {viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth};
    }

    if((viParam[vi].u32ViHeigth == 0 )|| (viParam[vi].u32ViWidth == 0))
    {

        if((veSnap[vi].snapSize.u32Height > 1080 )|| (veSnap[vi].snapSize.u32Width > 1920 ) || (veSnap[vi].snapSize.u32Height <= 0) ||  (veSnap[vi].snapSize.u32Width <= 0));
        {
            //   printf("++++++++++++++++++++++++++++a\n");
            veSnap[vi].snapSize={320,180};
        }
    }
    else
    {
        if((veSnap[vi].snapSize.u32Height > viParam[vi].u32ViHeigth ) || (veSnap[vi].snapSize.u32Width > viParam[vi].u32ViWidth ) ||
                (veSnap[vi].snapSize.u32Height <= 0) || (veSnap[vi].snapSize.u32Width <= 0)){
            veSnap[vi].snapSize={320,180};

        }
    }
    if(viParam[vi].vi_crop_enable == HI_TRUE)
    {
        if((viParam[vi].u32W < veSnap[vi].snapSize.u32Width)  || (viParam[vi].u32H < veSnap[vi].snapSize.u32Height))
        {    veSnap[vi].snapSize = {320,180};

        }
    }





    if(((viParam[0].u32ViHeigth == 2160)||( viParam[0].u32ViWidth == 3840)) && ((viParam[1].u32ViHeigth == 2160)||( viParam[1].u32ViWidth == 3840) ))
    {

        SIZE_S  hdmi2_size = {0,0};
        thisini->SetConfig(SoftwareConfig::kChn2_VI_W,hdmi2_size.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_VI_H,hdmi2_size.u32Height);


    }


    switch (vi) {
    case 0:
        thisini->SetConfig(SoftwareConfig::kChn0_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn0_SrcFrmRate,viParam[0].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn0_M_DstFrmRate,viParam[0].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn0_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn0_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn0_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn0_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_Snap_H,veSnap[vi].snapSize.u32Height);

        break;
    case 1:
        thisini->SetConfig(SoftwareConfig::kChn1_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn1_SrcFrmRate,viParam[1].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn1_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn1_M_DstFrmRate,viParam[vi].u32DstFrameRate[0]);
        thisini->SetConfig(SoftwareConfig::kChn1_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn1_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_Snap_H,veSnap[vi].snapSize.u32Height);
        // thisini->SetConfig(SoftwareConfig::kChn1_VI_Crop_Enable,viParam[vi].vi_crop_enable);
        //thisini->SetConfig(SoftwareConfig::kChn1_VI_Height,viParam[vi].u32H);
        //thisini->SetConfig(SoftwareConfig::kChn1_VI_Width,viParam[vi].u32W);

        break;
    case 2:
        thisini->SetConfig(SoftwareConfig::kChn2_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn2_SrcFrmRate,viParam[2].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn2_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn2_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn2_M_DstFrmRate,viParam[vi].u32DstFrameRate[0]);
        thisini->SetConfig(SoftwareConfig::kChn2_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn2_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_Snap_H,veSnap[vi].snapSize.u32Height);
        break;
    case 3:
        thisini->SetConfig(SoftwareConfig::kChn3_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn3_SrcFrmRate,viParam[3].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn3_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn3_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn3_M_DstFrmRate,viParam[vi].u32DstFrameRate[0]);
        thisini->SetConfig(SoftwareConfig::kChn3_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn3_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_Snap_H,veSnap[vi].snapSize.u32Height);
        break;
    default:
        break;
    }





    thisini->SaveConfig();



    //  printf("get vi:%d w=%d,h=%d,fps=%d\n",vi,viSize->u32Width,viSize->u32Height,viParam[vi].u32SrcFrmRate);


    if((viParam[vi].vi_crop_enable == HI_TRUE ) && (inputFmt.video_fmt != -1 )) {
        printf("caprect  x=%d,y=%d,w=%d,h=%d\n",viParam[vi].u32X,viParam[vi].u32Y,viParam[vi].u32W,viParam[vi].u32H);

        if((viParam[vi].u32X == 0) && (viParam[vi].u32Y == 0)&&(viParam[vi].u32W == 0)&& (viParam[vi].u32H == 0))
            viParam[vi].vi_crop_enable = HI_FALSE;


        if((viParam[vi].u32X + viParam[vi].u32W <= viParam[vi].u32ViWidth)&&(viParam[vi].u32Y + viParam[vi].u32H <= viParam[vi].u32ViHeigth))
        {


            printf("aa\n");
            if((viParam[vi].stVencSize.u32Width >  viParam[vi].u32W) ||
                    (viParam[vi].stVencSize.u32Height > viParam[vi].u32H))
                viParam[vi].stVencSize = {viParam[vi].u32W, viParam[vi].u32H};
            if((viParam[vi].stMinorSize.u32Width > viParam[vi].u32W) ||
                    (viParam[vi].stMinorSize.u32Height > viParam[vi].u32H))
                viParam[vi].stMinorSize = {viParam[vi].u32W,viParam[vi].u32H};

            viCapRect->s32X = viParam[vi].u32X;
            viCapRect->s32Y = viParam[vi].u32Y;
            viCapRect->u32Width = viParam[vi].u32W;
            viCapRect->u32Height = viParam[vi].u32H;
            viSize->u32Width = viParam[vi].u32W;
            viSize->u32Height = viParam[vi].u32H;


        }
        else
        {
            viCapRect->s32X =  0;
            viCapRect->s32Y = 0;
            viCapRect->u32Width  =  viSize->u32Width;
            viCapRect->u32Height =  viSize->u32Height;
            viSize->u32Width     =  viSize->u32Width;
            viSize->u32Height    =  viSize->u32Height;

            viParam[vi].vi_crop_enable = HI_FALSE;
            // viParam[vi].u32W = 0;
            //  viParam[vi].u32H = 0;

        }

        printf("?1???????????????????????>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>==%d\n",viParam[vi].vi_crop_enable);




        //         if((viParam[vi].u32X + viParam[vi].u32W <= viParam[vi].u32ViWidth)&&(viParam[vi].u32Y + viParam[vi].u32H <= viParam[vi].u32ViHeigth)){
        //            viParam[vi].st = {viParam[vi].u32X, viParam[vi].u32Y, viParam[vi].u32W, viParam[vi].u32H};
        //            if(viParam.venc_same_as_input == 1){
        //                thisini->SetConfig(Config::kChn0_M_VENC_W, int2str(viParam.u32W));
        //                thisini->SetConfig(Config::kChn0_M_VENC_H, int2str(viParam.u32H));
        //                viParam.stVencSize = {viParam.u32W, viParam.u32H};
        //            }




    }
    else {
        viCapRect->s32X = 0;
        viCapRect->s32Y = 0;
        if(inputFmt.video_fmt == VIN_RESOLUTION_UNKOWN ) {
            viCapRect->u32Width =  1920;
            viCapRect->u32Height = 1080;
            viSize->u32Width = 1920;
            viSize->u32Height = 1080;
        }
        else {
            viCapRect->u32Width = viSize->u32Width;
            viCapRect->u32Height = viSize->u32Height;
        }
    }
    if(inputFmt.video_fmt == VIN_RESOLUTION_UNKOWN )
    {
        viParam[vi].stVencSize.u32Width   = 1920;
        viParam[vi].stVencSize.u32Height  = 1080;
        viParam[vi].stMinorSize.u32Width  = 1920;
        viParam[vi].stMinorSize.u32Height = 1080;
    }

    // if(((viParam[0].u32ViHeigth == 2160)||( viParam[0].u32ViWidth == 3840)) && ((viParam[1].u32ViHeigth == 2160)||( viParam[1].u32ViWidth == 3840) ))
    // {
    //     viParam[0].stVencSize = {3840,2160};
    //     viParam[1].stVencSize = {3840,2160};
    // }



    if(inputFmt.video_fmt != VIN_RESOLUTION_UNKOWN)
    {
        if(viParam[vi].venc_SAME_INPUT[0] ==1 )
        {
            viParam[vi].stVencSize.u32Width   =  viSize->u32Width;
            viParam[vi].stVencSize.u32Height  =  viSize->u32Height;


        }
        if(viParam[vi].venc_SAME_INPUT[1] ==1 )
        {
            viParam[vi].stMinorSize.u32Width  = viSize->u32Width;
            viParam[vi].stMinorSize.u32Height = viSize->u32Height;

        }
    }
    return HI_SUCCESS;

}
#endif

int getViParam(int vi,user_msg_info inputFmt,SIZE_S *viSize,RECT_S *viCapRect)
{
    auto thisini = Singleton<SoftwareConfig>::getInstance();


    viSize->u32Height = inputFmt.vf.video_height;
    viSize->u32Width = inputFmt.vf.video_width;


    HI_U32 vi_framtare ,vi_0_framtare,vi_1_framtare;


    if(inputFmt.vf.video_freq == 0)
    {
        viParam[vi].u32ViHeigth = 0;
        viParam[vi].u32ViWidth = 0;

        viParam[vi].u32SrcFrmRate = 30;
        viParam[vi].u32DstFrameRate[0] = 30;
        viParam[vi].u32DstFrameRate[1] = 30;
        vi_framtare = 30;
        vi_0_framtare = 30 ;
        vi_1_framtare = 30;

    }
    else if(inputFmt.vf.video_freq != 0)
    {
        viParam[vi].u32ViHeigth = viSize->u32Height;
        viParam[vi].u32ViWidth = viSize->u32Width;



        viParam[vi].u32SrcFrmRate = inputFmt.vf.video_freq;
        //  viParam[vi].u32DstFrameRate[0] = inputFmt.fps;
        //   viParam[vi].u32DstFrameRate[1] = inputFmt.fps;

        //         viParam[vi].u32SrcFrmRate      = 60;
        //         viParam[vi].u32DstFrameRate[0] =  60;
        //          viParam[vi].u32DstFrameRate[1]=  60;

        if(viParam[vi].venc_SAME_INPUT[0] == HI_TRUE )
        {
            viParam[vi].u32DstFrameRate[0] = inputFmt.vf.video_freq;
            // viParam[vi].u32DstFrameRate[1] = inputFmt.fps;

        }
        if(viParam[vi].venc_SAME_INPUT[1] == HI_TRUE )
        {
            // viParam[vi].u32DstFrameRate[0] = inputFmt.fps;
            viParam[vi].u32DstFrameRate[1] = inputFmt.vf.video_freq;
        }
        if(inputFmt.vf.video_freq <= 20) {


            viParam[vi].u32SrcFrmRate = 30;
            viParam[vi].u32DstFrameRate[0] = 30;
            viParam[vi].u32DstFrameRate[1] = 30;

        }
        else if((inputFmt.vf.video_freq > 70) || (inputFmt.vf.video_freq > 56)) {
            viParam[vi].u32SrcFrmRate = 60;
            viParam[vi].u32DstFrameRate[0] = 60;
            viParam[vi].u32DstFrameRate[1] = 60;
        }


    }
    else
        printf("err \n");

    if(vi == 0)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn0_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_VENC_H))};
    }
    else if(vi == 1)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn1_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_VENC_H))};
    }
    else if(vi == 2)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn2_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_VENC_H))};
    }
    else if(vi == 3)
    {
        viParam[vi].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn3_VI_Crop_Enable));
        viParam[vi].stVencSize =  {str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_VENC_H))};
        viParam[vi].stMinorSize = {str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_W)),
                                   str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_VENC_H))};
    }
    else
        printf("err havent\n");
    if(inputFmt.vf.video_freq != 0)
    {
        if(viParam[vi].stVencSize.u32Height <= 0 || viParam[vi].stVencSize.u32Width <= 0  ||
                viParam[vi].stVencSize.u32Width > viParam[vi].u32ViWidth || viParam[vi].stVencSize.u32Height > viParam[vi].u32ViHeigth )
            viParam[vi].stVencSize  = {viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth};

        if(viParam[vi].stMinorSize.u32Height <= 0 || viParam[vi].stMinorSize.u32Width <= 0  ||
                viParam[vi].stMinorSize.u32Width > viParam[vi].u32ViWidth || viParam[vi].stMinorSize.u32Height > viParam[vi].u32ViHeigth )
            viParam[vi].stMinorSize= {viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth};
    }

    if((viParam[vi].u32ViHeigth == 0 )|| (viParam[vi].u32ViWidth == 0))
    {

        if((veSnap[vi].snapSize.u32Height > 1080 )|| (veSnap[vi].snapSize.u32Width > 1920 ) || (veSnap[vi].snapSize.u32Height <= 0) ||  (veSnap[vi].snapSize.u32Width <= 0));
        {
            //   printf("++++++++++++++++++++++++++++a\n");
            veSnap[vi].snapSize={320,180};
        }
    }
    else
    {
        if((veSnap[vi].snapSize.u32Height > viParam[vi].u32ViHeigth ) || (veSnap[vi].snapSize.u32Width > viParam[vi].u32ViWidth ) ||
                (veSnap[vi].snapSize.u32Height <= 0) || (veSnap[vi].snapSize.u32Width <= 0)){
            veSnap[vi].snapSize={320,180};

        }
    }
    if(viParam[vi].vi_crop_enable == HI_TRUE)
    {
        if((viParam[vi].u32W < veSnap[vi].snapSize.u32Width)  || (viParam[vi].u32H < veSnap[vi].snapSize.u32Height))
        {    veSnap[vi].snapSize = {320,180};

        }
    }





    if(((viParam[0].u32ViHeigth == 2160)||( viParam[0].u32ViWidth == 3840)) && ((viParam[1].u32ViHeigth == 2160)||( viParam[1].u32ViWidth == 3840) ))
    {

        SIZE_S  hdmi2_size = {0,0};
        thisini->SetConfig(SoftwareConfig::kChn2_VI_W,hdmi2_size.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_VI_H,hdmi2_size.u32Height);


    }


    switch (vi) {
    case 0:
        thisini->SetConfig(SoftwareConfig::kChn0_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn0_SrcFrmRate,viParam[0].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn0_M_DstFrmRate,viParam[0].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn0_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn0_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn0_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn0_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn0_Snap_H,veSnap[vi].snapSize.u32Height);

        break;
    case 1:
        thisini->SetConfig(SoftwareConfig::kChn1_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn1_SrcFrmRate,viParam[1].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn1_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn1_M_DstFrmRate,viParam[vi].u32DstFrameRate[0]);
        thisini->SetConfig(SoftwareConfig::kChn1_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn1_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn1_Snap_H,veSnap[vi].snapSize.u32Height);
        // thisini->SetConfig(SoftwareConfig::kChn1_VI_Crop_Enable,viParam[vi].vi_crop_enable);
        //thisini->SetConfig(SoftwareConfig::kChn1_VI_Height,viParam[vi].u32H);
        //thisini->SetConfig(SoftwareConfig::kChn1_VI_Width,viParam[vi].u32W);

        break;
    case 2:
        thisini->SetConfig(SoftwareConfig::kChn2_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn2_SrcFrmRate,viParam[2].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn2_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn2_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn2_M_DstFrmRate,viParam[vi].u32DstFrameRate[0]);
        thisini->SetConfig(SoftwareConfig::kChn2_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn2_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn2_Snap_H,veSnap[vi].snapSize.u32Height);
        break;
    case 3:
        thisini->SetConfig(SoftwareConfig::kChn3_VI_W,viSize->u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_VI_H,viSize->u32Height);
        thisini->SetConfig(SoftwareConfig::kChn3_SrcFrmRate,viParam[3].u32SrcFrmRate);
        thisini->SetConfig(SoftwareConfig::kChn3_M_VENC_W,viParam[vi].stVencSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_M_VENC_H,viParam[vi].stVencSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn3_S0_VENC_W,viParam[vi].stMinorSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_S0_VENC_H,viParam[vi].stMinorSize.u32Height );
        thisini->SetConfig(SoftwareConfig::kChn3_M_DstFrmRate,viParam[vi].u32DstFrameRate[0]);
        thisini->SetConfig(SoftwareConfig::kChn3_S0_DstFrmRate,viParam[vi].u32DstFrameRate[1]);
        thisini->SetConfig(SoftwareConfig::kChn3_Snap_W,veSnap[vi].snapSize.u32Width);
        thisini->SetConfig(SoftwareConfig::kChn3_Snap_H,veSnap[vi].snapSize.u32Height);
        break;
    default:
        break;
    }





    thisini->SaveConfig();



    //  printf("get vi:%d w=%d,h=%d,fps=%d\n",vi,viSize->u32Width,viSize->u32Height,viParam[vi].u32SrcFrmRate);


    if((viParam[vi].vi_crop_enable == HI_TRUE ) && (inputFmt.vf.video_freq != 0 )) {
        printf("caprect  x=%d,y=%d,w=%d,h=%d\n",viParam[vi].u32X,viParam[vi].u32Y,viParam[vi].u32W,viParam[vi].u32H);

        if((viParam[vi].u32X == 0) && (viParam[vi].u32Y == 0)&&(viParam[vi].u32W == 0)&& (viParam[vi].u32H == 0))
            viParam[vi].vi_crop_enable = HI_FALSE;


        if((viParam[vi].u32X + viParam[vi].u32W <= viParam[vi].u32ViWidth)&&(viParam[vi].u32Y + viParam[vi].u32H <= viParam[vi].u32ViHeigth))
        {


            printf("aa\n");
            if((viParam[vi].stVencSize.u32Width >  viParam[vi].u32W) ||
                    (viParam[vi].stVencSize.u32Height > viParam[vi].u32H))
                viParam[vi].stVencSize = {viParam[vi].u32W, viParam[vi].u32H};
            if((viParam[vi].stMinorSize.u32Width > viParam[vi].u32W) ||
                    (viParam[vi].stMinorSize.u32Height > viParam[vi].u32H))
                viParam[vi].stMinorSize = {viParam[vi].u32W,viParam[vi].u32H};

            viCapRect->s32X = viParam[vi].u32X;
            viCapRect->s32Y = viParam[vi].u32Y;
            viCapRect->u32Width = viParam[vi].u32W;
            viCapRect->u32Height = viParam[vi].u32H;
            viSize->u32Width = viParam[vi].u32W;
            viSize->u32Height = viParam[vi].u32H;


        }
        else
        {
            viCapRect->s32X =  0;
            viCapRect->s32Y = 0;
            viCapRect->u32Width  =  viSize->u32Width;
            viCapRect->u32Height =  viSize->u32Height;
            viSize->u32Width     =  viSize->u32Width;
            viSize->u32Height    =  viSize->u32Height;

            viParam[vi].vi_crop_enable = HI_FALSE;
            // viParam[vi].u32W = 0;
            //  viParam[vi].u32H = 0;

        }

        printf("?1???????????????????????>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>==%d\n",viParam[vi].vi_crop_enable);




        //         if((viParam[vi].u32X + viParam[vi].u32W <= viParam[vi].u32ViWidth)&&(viParam[vi].u32Y + viParam[vi].u32H <= viParam[vi].u32ViHeigth)){
        //            viParam[vi].st = {viParam[vi].u32X, viParam[vi].u32Y, viParam[vi].u32W, viParam[vi].u32H};
        //            if(viParam.venc_same_as_input == 1){
        //                thisini->SetConfig(Config::kChn0_M_VENC_W, int2str(viParam.u32W));
        //                thisini->SetConfig(Config::kChn0_M_VENC_H, int2str(viParam.u32H));
        //                viParam.stVencSize = {viParam.u32W, viParam.u32H};
        //            }




    }
    else {
        viCapRect->s32X = 0;
        viCapRect->s32Y = 0;
        if(inputFmt.vf.video_freq == 0 ) {
            viCapRect->u32Width =  1920;
            viCapRect->u32Height = 1080;
            viSize->u32Width = 1920;
            viSize->u32Height = 1080;
        }
        else {
            viCapRect->u32Width = viSize->u32Width;
            viCapRect->u32Height = viSize->u32Height;
        }
    }
    if(inputFmt.vf.video_freq == 0 )
    {
        viParam[vi].stVencSize.u32Width   = 1920;
        viParam[vi].stVencSize.u32Height  = 1080;
        viParam[vi].stMinorSize.u32Width  = 1920;
        viParam[vi].stMinorSize.u32Height = 1080;
    }

    // if(((viParam[0].u32ViHeigth == 2160)||( viParam[0].u32ViWidth == 3840)) && ((viParam[1].u32ViHeigth == 2160)||( viParam[1].u32ViWidth == 3840) ))
    // {
    //     viParam[0].stVencSize = {3840,2160};
    //     viParam[1].stVencSize = {3840,2160};
    // }



    if(inputFmt.vf.video_freq != 0)
    {
        if(viParam[vi].venc_SAME_INPUT[0] ==1 )
        {
            viParam[vi].stVencSize.u32Width   =  viSize->u32Width;
            viParam[vi].stVencSize.u32Height  =  viSize->u32Height;


        }
        if(viParam[vi].venc_SAME_INPUT[1] ==1 )
        {
            viParam[vi].stMinorSize.u32Width  = viSize->u32Width;
            viParam[vi].stMinorSize.u32Height = viSize->u32Height;

        }
    }
    return HI_SUCCESS;

}






user_msg_info inputFmt_[4];

user_msg_info inputGetFmt_[4] ;
void* it6801Get(void* arg)
{
    int vi_dev = *(int*)arg;




    int skfd;
    int ret;
    user_msg_info u_info;
    socklen_t len;
    struct nlmsghdr *nlh = NULL;
    struct sockaddr_nl saddr, daddr;

    skfd = socket(AF_NETLINK, SOCK_RAW, NETLINK_VIDEO_PROTOCOL);
    if (skfd == -1) {
        perror("create socket error\n");
        // return -1;
    }

    memset(&saddr, 0, sizeof(saddr));
    saddr.nl_family = AF_NETLINK; //AF_NETLINK
    saddr.nl_pid = USER_PORT;
    saddr.nl_groups = 0;
    if (bind(skfd, (struct sockaddr *)&saddr, sizeof(saddr)) != 0) {
        perror("bind() error\n");
        close(skfd);
        // return -1;
    }

    memset(&daddr, 0, sizeof(daddr));
    daddr.nl_family = AF_NETLINK;
    daddr.nl_pid = 0;
    daddr.nl_groups = 0;

    //    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PLOAD));
    //    memset(nlh, 0, sizeof(struct nlmsghdr));
    //    nlh->nlmsg_len = NLMSG_SPACE(MAX_PLOAD);
    //    nlh->nlmsg_flags = 0;
    //    nlh->nlmsg_type = 0;
    //    nlh->nlmsg_seq = 0;
    //    nlh->nlmsg_pid = saddr.nl_pid;     //self port








    while(it6801get_false )
    {


        memset(&u_info, 0, sizeof(u_info));
        len = sizeof(struct sockaddr_nl);
        ret = recvfrom(skfd, &u_info, sizeof(user_msg_info), 0, (struct sockaddr *)&daddr, &len);
        if (!ret) {
            perror("recv form kernel error\n");
            close(skfd);
            exit(-1);
        }



        memcpy(&inputFmt_[u_info.vf.video_ch],&u_info,sizeof(user_msg_info));


        //       printf("vgetf->ch: %d, w: %d , h: %d, fps:%d, scan :%d\n", \
        //              inputFmt_[u_info.vf.video_ch].vf.video_ch, inputFmt_[u_info.vf.video_ch].vf.video_width, inputFmt_[u_info.vf.video_ch].vf.video_height, \
        //              inputFmt_[u_info.vf.video_ch].vf.video_freq, inputFmt_[u_info.vf.video_ch].vf.video_interlaced);



    }
    close(skfd);

    //  free((void *)nlh);

    return (void*)0;
}








void * rtspThread0(void * arg)
{
    HI_S32 viport = *(HI_S32*)arg;
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    // himppmaster->StartLoop(2*viport, (2 * viport) +1,viport);
    himppmaster->StartLoop(2*viport,2*(viport+1),viport);
    return (void*)0;
}
void * rtspThread1(void * arg)
{
    HI_S32 viport = *(HI_S32*)arg;
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    // himppmaster->StartLoop(2*viport, (2 * viport) +1,viport);
    himppmaster->StartLoop(2*viport,2*(viport+1),viport);
    return (void*)0;
}
void * rtspThread2(void * arg)
{
    HI_S32 viport = *(HI_S32*)arg;
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    // himppmaster->StartLoop(2*viport, (2 * viport) +1,viport);
    himppmaster->StartLoop(2*viport,2*(viport+1),viport);
    return (void*)0;
}


void* it6801Fun(void* arg)
{
    int vi_dev = *(int*)arg;
    int vi   ;

    int fd = -1;
    int i;


    SIZE_S stTargetSize = {0,0};
    RECT_S stCapRect;
    VI_DEV viDev = 0;
    VI_CHN viChn = 0;
    VENC_CHN veStar,veEnd;
    //    pthread_t input_tid[4];
    //    pthread_t audio_tid[4];
    bool b_detached = true;
    char rtspNum[8] = {0};
    video_type vType;

    SIZE_S vpchnsize ={0,0};

    /*
 * memcmp 结构体有内存间隙的BUG
 * 取消VI  bind vpss  采用PUSH模式；VI BIND  SWcolor模式有问题
 * */

    if (vi_dev == 1 )
        i = 0;
    else if(vi_dev == 3)
        i = 1;
    else if(vi_dev == 5)
        i = 2;
    else  if(vi_dev == 7)
        i = 3;


    HI_S32  vpstart = i;
    HI_S32  vpssGrpCnt = i + 1;
    HI_S32  vpssChn = 1;
    //    HI_S32  vpstart1 = i + 4;
    //    HI_S32  vpssGrpCnt1 = i + 5;
    //    HI_S32  vpssChn1 = 2;

    //    HI_S32 vpssGrpCnt_vo= 10, vpssChn_vo = 1;
    //    HI_S32 vpstart_vo = 8;


    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    /*
 * 音频设置--RTSP
 * enAiSampleRate -- 音频采样率 48K
 * bitRate -- 音频波特率 128K
 * coderFormat[x] -- 4路RTSP流编码类型
 * */

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(SoftwareConfig::kAiSampleRate));
    HI_S32 bitRate = str2int(thisini->GetConfig(SoftwareConfig::kAiBitRate));
    int param[2] = { 1, enAiSampleRate };
#if 1

    HI_BOOL G711Init = HI_FALSE;
    // for(i = 0; i <  viNumber; i++) {
    switch (coderFormat[i]) {
    case AUDIO_NULL:
        break;
    case AUDIO_AAC:
        g_audio_aac[i] = new aacenc();
        g_audio_aac[i]->open(bitRate, enAiSampleRate, AACLC);
        printf("start %d aac init\n",i);
        break;
    case AUDIO_G711A:
        G711Init = HI_TRUE;
        break;
    default:
        break;
    }






    if(G711Init)
        g711_init();
#endif
    memset(&inputGetFmt_[i],0,sizeof(user_msg_info));


    int aichn;
    int thefmt = 0,zeroflg = 1,j = 0,havent= 0,have = 1;
    //int zeroflg = 1;

    while(it6801Fun_false[i])
    {
        sem_wait(&sem1);// orderly pthread

        //        inputFmt[i].chip_num=i;
        //        ioctl(fdfun, VIDEO_INPUT_GET_STATUE, &inputFmt[i]);//获取VI分辨率
        //        if((inputFmt[i].video_fmt == -1) &&  (zeroflg == 1))
        //        {
        //            thefmt++;
        //            // have = 0;
        //        }
        //        else if(inputFmt[i].video_fmt != -1)
        //            thefmt   = 0;

        if((inputFmt_[i].vf.video_freq == 0) &&  (zeroflg == 1))
        {
            thefmt++;
            // have = 0;
        }
        else if(inputFmt_[i].vf.video_freq != 0)
            thefmt   = 0;



        //        if((inputFmt_[i].video_fmt != inputGetFmt[i].video_fmt) || (inputFmt[i].fps != inputGetFmt[i].fps)) {

        //            diffNum[i]++;


        //        }







        if(((inputFmt_[i].vf.video_height != inputGetFmt_[i].vf.video_height) && (inputFmt_[i].vf.video_width != inputGetFmt_[i].vf.video_width)  )   || (inputFmt_[i].vf.video_freq != inputGetFmt_[i].vf.video_freq)) {

            diffNum[i]++;
            printf("vgetf->ch: %d, w: %d , h: %d, fps:%d, scan :%d\n", \
                   inputFmt_[i].vf.video_ch, inputFmt_[i].vf.video_width, inputFmt_[i].vf.video_height, \
                   inputFmt_[i].vf.video_freq, inputFmt_[i].vf.video_interlaced);

        }

        //当未使能4k时插入4k输入时进入无信号
        if(((inputFmt_[i].vf.video_height > 1080) && (inputFmt_[i].vf.video_width > 1920))&& (edid_4k == 0 ) &&(zeroflg ==1))
        {
            inputFmt_[i].vf.video_freq  = 0;
            thefmt = 6;
        }




        if(((diffNum[i] >= 2 )&& (inputFmt_[i].vf.video_freq != 0) ) &&  !(  ((inputFmt_[i].vf.video_width > 1920 ) && (inputFmt_[i].vf.video_height > 1080))&& (edid_4k == 0 ))  ){

            zeroflg = 1;
            printf("duffNum----********have******-i=%d---fstart==%d video_fmt=%d \n",i,fStart[i],inputFmt_[i].vf.video_width);
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);
            if(edid_4k == 1){
                himppmaster->VoModuleDestroy(0);
               // SAMPLE_COMM_VPSS_Stop(i + 8, i+9, vpssChn);

            } else
                himppmaster->VoModuleDestroy(1);

            if(!fStart[i]) {

                mult_snap_runlag[i] = false;

                snap_runlag[i] = false;

                // himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );

                himppmaster->StopViModule(viDev,viChn);

                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                // himppmaster->VoModuleDestroy(1);

                if((inputGetFmt_[i].vf.video_height != inputFmt_[i].vf.video_height) || (inputGetFmt_[i].vf.video_width != inputFmt_[i].vf.video_width)) // this is edid_4k == 1 but vi is 1080p60  fps--
                {
                    SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                    //SAMPLE_COMM_VPSS_Stop(8,10,2);
                }

                usleep(20000);
            }
            else {




                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {

                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);
                    }
                    else {
                        //   printf("crtsps_openstream  CreateThread-----%d ---coder=%d\n",2*i+j,coderFormat[i]);
                        //  g711_encode((byte*)stStream.pStream, stStream.u32Len, g711);
                        aichn = 2*i+j;
                        audio_running =  true;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                                //COMMONgEnViMode2_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                        }


                    }
                }
            }

            //  inputFmt[i].video_fmt = VIN_RESOLUTION_3840x2160;
            //根据VI的数据 更改参数
            getViParam(i,inputFmt_[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }

            //  auto thisini = Singleton<SoftwareConfig>::getInstance();


            if((inputGetFmt_[i].vf.video_height != inputFmt_[i].vf.video_height) || (inputGetFmt_[i].vf.video_width != inputFmt_[i].vf.video_width)) // this is edid_4k == 1 but vi is 1080p60
            {

                if((inputFmt_[i].vf.video_height == 2160) &&  (inputFmt_[i].vf.video_width == 3840))
                    vpchnsize = {3840,2160};
                else
                    vpchnsize = {1920,1080};
            }


            SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};





            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

            if((inputFmt_[i].vf.video_freq == 0) && (thefmt >= 10 )) {
                l_startloop[i] = HI_TRUE;
                himppmaster->SetUserPic();


                himppmaster->EnableUserPic(viChn);

            }
            else if (inputFmt_[i].vf.video_freq != 0  )
            {
                thefmt = 0;
                l_startloop[i] = HI_TRUE;
                himppmaster->DisableUserPic(viChn);
                int userpic = 5;
                while( userpic)
                {
                    userpic--;
                    himppmaster->DisableUserPic(viChn +1);
                }

            }







            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};


            //  printf("venc-width==%d   venc--hight==%d\n",viParam[i].stVencSize.u32Width,viParam[i].stVencSize.u32Height);

            SIZE_S vo_size_win = {viParam[0].u32ViWidth,viParam[0].u32ViHeigth};

            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

            if(inputGetFmt_[i].vf.video_freq != inputFmt_[i].vf.video_freq)              //this is edid_4k == 1 but vi is 1080p60  fps--
            {

                 printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>vpss\n");
                himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
               // himppmaster->VpssModuleInit(8,9,vpssChn,vo_size_win);
            }
            memcpy(&inputGetFmt_[i],&inputFmt_[i],sizeof(user_msg_info));





            if(edid_4k == 1){
                himppmaster->VoModeuleInit(vo_size,0);
              //  himppmaster->VpssModuleInit(8,9,vpssChn,vo_size_win);
            }
            else
                himppmaster->VoModeuleInit(vo_size,1);




            vi_flag[i] = 1;



            if(fStart[i]){
                //   printf("===================================it======1\n");
                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT("pthread_create() failed: videv1\n");
                }

                himppmaster->BindPreViewSys(i);



            }

            if(veSnap[i].Snapmcast_enable)

            {  mult_snap_runlag[i] = true;
                mult_snap[i] = true;
                snap_runlag[i] = false;
                snap_tid[i] = CreateThread(multiSnapProc, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {
                mult_snap_runlag[i] = false;

                snap_runlag[i] = true;

                snap_tid[i] = CreateThread(snap0ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }



            usleep(2000);


        }

        else if((thefmt >= 10) && (inputFmt_[i].vf.video_freq == 0) ){


            if(edid_4k == 1)
                himppmaster->VoModuleDestroy(0);
            else
                himppmaster->VoModuleDestroy(1);


            vi_flag[i] = 0;
            printf("duffNum----*********havent*****-i=%d---fstart==%d\n",i,fStart[i]);
            //  gEnViMode1 = SAMPLE_VI_MODE_4_1080P;
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);
            thefmt = 0;
            zeroflg = 0;
            memcpy(&inputGetFmt_[i],&inputFmt_[i],sizeof(user_msg_info));
            if(!fStart[i]) {
                //   l_startloop[i] = HI_FALSE;
                mult_snap_runlag[i] = false;
                snap_runlag [i] = false;
                himppmaster->StopViModule(viDev,viChn);

                // himppmaster->VoModuleDestroy();
                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                usleep(20000);

            }
            else {

                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);


                    }
                    else {
                        //    printf("crtsps_openstream  CreateThread-----%d \n",2*i+j);

                        aichn = 2*i+j;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                                COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                        }


                    }
                }

            }

            getViParam(i,inputGetFmt_[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }


            //printf("gEnViMode2????????????????? =%d\n",gEnViMode1);
            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

            l_startloop[i] = HI_TRUE;
            himppmaster->SetUserPic();


            himppmaster->EnableUserPic(viChn);
            // himppmaster->EnableUserPic(viChn +1);

            printf("venc %d start  venc flag:%d\n",i,venc_flag[i]);
            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};

            vpchnsize = {1920,1080};


            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);



            printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>vpsss  zeor\n");
            himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
            //  himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);



            SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};
            if(edid_4k == 1)
                himppmaster->VoModeuleInit(vo_size,0);
            else
                himppmaster->VoModeuleInit(vo_size,1);

            //           himppmaster->VpssModuleInitvo(vpstart_vo,vpssGrpCnt_vo,vpssChn_vo,stTargetSize);
            //           himppmaster->BindPreVoewSys(0,0,3,4);


            if(fStart[i]){
                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT(" () failed: videv1\n");
                }

                himppmaster->BindPreViewSys(i);





            }
            if(veSnap[i].Snapmcast_enable)

            {
                mult_snap_runlag[i] = true;
                mult_snap[i] = true;
                snap_runlag[i] = false;
                snap_tid[i] = CreateThread(multiSnapProc, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {
                mult_snap_runlag[i] = false;
                snap_runlag[i] = true;

                snap_tid[i] = CreateThread(snap0ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }



            usleep(2000);




        }





        //   usleep(1000);


        sem_post(&sem2);
    }

    delete g_audio_aac[i];
    return (void*)0;
}



void* it6801Fun_1(void* arg)
{
    int vi_dev = *(int*)arg;
    int vi   ;

    int fd = -1;
    int i;


    SIZE_S stTargetSize = {0,0};
    RECT_S stCapRect;
    VI_DEV viDev = 0;
    VI_CHN viChn = 0;
    VENC_CHN veStar,veEnd;
    //    pthread_t input_tid[4];
    //    pthread_t audio_tid[4];
    bool b_detached = true;
    char rtspNum[8] = {0};
    video_type vType;

    SIZE_S vpchnsize ={0,0};

    /*
 * memcmp 结构体有内存间隙的BUG
 * 取消VI  bind vpss  采用PUSH模式；VI BIND  SWcolor模式有问题
 * */

    if (vi_dev == 1 )
        i = 0;
    else if(vi_dev == 3)
        i = 1;
    else if(vi_dev == 5)
        i = 2;
    else  if(vi_dev == 7)
        i = 3;


    HI_S32  vpstart = i;
    HI_S32  vpssGrpCnt = i + 1;
    HI_S32  vpssChn = 1;
    //    HI_S32  vpstart1 = i + 4;
    //    HI_S32  vpssGrpCnt1 = i + 5;
    //    HI_S32  vpssChn1 = 2;

    //    HI_S32 vpssGrpCnt_vo= 10, vpssChn_vo = 1;
    //    HI_S32 vpstart_vo = 8;


    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    /*
 * 音频设置--RTSP
 * enAiSampleRate -- 音频采样率 48K
 * bitRate -- 音频波特率 128K
 * coderFormat[x] -- 4路RTSP流编码类型
 * */

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(SoftwareConfig::kAiSampleRate));
    HI_S32 bitRate = str2int(thisini->GetConfig(SoftwareConfig::kAiBitRate));
    int param[2] = { 1, enAiSampleRate };
#if 1

    HI_BOOL G711Init = HI_FALSE;
    // for(i = 0; i <  viNumber; i++) {
    switch (coderFormat[i]) {
    case AUDIO_NULL:
        break;
    case AUDIO_AAC:
        g_audio_aac[i] = new aacenc();
        g_audio_aac[i]->open(bitRate, enAiSampleRate, AACLC);
        printf("start %d aac init\n",i);
        break;
    case AUDIO_G711A:
        G711Init = HI_TRUE;
        break;
    default:
        break;
    }






    if(G711Init)
        g711_init();
#endif
    memset(&inputGetFmt_[i],0,sizeof(user_msg_info));


    int aichn;
    int thefmt = 0,zeroflg = 1,j = 0,havent= 0,have = 1;
    //int zeroflg = 1;

    while(it6801Fun_false[i])
    {
        sem_wait(&sem2);// orderly pthread

        //        inputFmt[i].chip_num=i;
        //        ioctl(fdfun, VIDEO_INPUT_GET_STATUE, &inputFmt[i]);//获取VI分辨率
        //        if((inputFmt[i].video_fmt == -1) &&  (zeroflg == 1))
        //        {
        //            thefmt++;
        //            // have = 0;
        //        }
        //        else if(inputFmt[i].video_fmt != -1)
        //            thefmt   = 0;



        if((inputFmt_[i].vf.video_freq == 0) &&  (zeroflg == 1))
        {
            thefmt++;
            // have = 0;
        }
        else if(inputFmt_[i].vf.video_freq != 0)
            thefmt   = 0;







        if(((inputFmt_[i].vf.video_height != inputGetFmt_[i].vf.video_height) && (inputFmt_[i].vf.video_width != inputGetFmt_[i].vf.video_width)  )   || (inputFmt_[i].vf.video_freq != inputGetFmt_[i].vf.video_freq)) {

            diffNum[i]++;
            printf("vgetf->ch: %d, w: %d , h: %d, fps:%d, scan :%d\n", \
                   inputFmt_[i].vf.video_ch, inputFmt_[i].vf.video_width, inputFmt_[i].vf.video_height, \
                   inputFmt_[i].vf.video_freq, inputFmt_[i].vf.video_interlaced);

        }

        //当未使能4k时插入4k输入时进入无信号
        if(((inputFmt_[i].vf.video_height > 1080) && (inputFmt_[i].vf.video_width > 1920))&& (edid_4k == 0 ) &&(zeroflg ==1))
        {
            inputFmt_[i].vf.video_freq  = 0;
            thefmt = 6;
        }




        if(((diffNum[i] >= 2 )&& (inputFmt_[i].vf.video_freq != 0) ) &&  !(  ((inputFmt_[i].vf.video_width > 1920 ) && (inputFmt_[i].vf.video_height > 1080))&& (edid_4k == 0 ))  ){

            zeroflg = 1;
            printf("duffNum----********have******-i=%d---fstart==%d video_fmt=%d \n",i,fStart[i],inputFmt_[i].vf.video_width);
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);
            if(edid_4k == 0)
                himppmaster->VoModuleDestroy(0);

            if(!fStart[i]) {

                mult_snap_runlag[i] = false;

                snap_runlag[i] = false;

                // himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );

                himppmaster->StopViModule(viDev,viChn);

                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                // himppmaster->VoModuleDestroy(1);

                if((inputGetFmt_[i].vf.video_height != inputFmt_[i].vf.video_height) || (inputGetFmt_[i].vf.video_width != inputFmt_[i].vf.video_width)) // this is edid_4k == 1 but vi is 1080p60  fps--
                {
                    SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                    //SAMPLE_COMM_VPSS_Stop(8,10,2);
                }

                usleep(20000);
            }
            else {




                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {

                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);
                    }
                    else {
                        //   printf("crtsps_openstream  CreateThread-----%d ---coder=%d\n",2*i+j,coderFormat[i]);
                        //  g711_encode((byte*)stStream.pStream, stStream.u32Len, g711);
                        aichn = 2*i+j;
                        audio_running =  true;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                                //COMMONgEnViMode2_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                        }


                    }
                }
            }

            //  inputFmt[i].video_fmt = VIN_RESOLUTION_3840x2160;
            //根据VI的数据 更改参数
            getViParam(i,inputFmt_[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }

            //  auto thisini = Singleton<SoftwareConfig>::getInstance();


            if((inputGetFmt_[i].vf.video_height != inputFmt_[i].vf.video_height) || (inputGetFmt_[i].vf.video_width != inputFmt_[i].vf.video_width)) // this is edid_4k == 1 but vi is 1080p60
            {

                if((inputFmt_[i].vf.video_height == 2160) &&  (inputFmt_[i].vf.video_width == 3840))
                    vpchnsize = {3840,2160};
                else
                    vpchnsize = {1920,1080};
            }


            SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};





            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

            if((inputFmt_[i].vf.video_freq == 0) && (thefmt >= 10 )) {
                l_startloop[i] = HI_TRUE;
                himppmaster->SetUserPic();


                himppmaster->EnableUserPic(viChn);

            }
            else if (inputFmt_[i].vf.video_freq != 0  )
            {
                thefmt = 0;
                l_startloop[i] = HI_TRUE;
                himppmaster->DisableUserPic(viChn);
                int userpic = 5;
                while( userpic)
                {
                    userpic--;
                    himppmaster->DisableUserPic(viChn +1);
                }

            }







            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};


            //  printf("venc-width==%d   venc--hight==%d\n",viParam[i].stVencSize.u32Width,viParam[i].stVencSize.u32Height);


            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

            if(inputGetFmt_[i].vf.video_freq != inputFmt_[i].vf.video_freq)              //this is edid_4k == 1 but vi is 1080p60  fps--
            {

                //  printf();
                himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
            }
            memcpy(&inputGetFmt_[i],&inputFmt_[i],sizeof(user_msg_info));





            if(edid_4k == 0)
                himppmaster->VoModeuleInit(vo_size,0);



            vi_flag[i] = 1;



            if(fStart[i]){
                //   printf("===================================it======1\n");
                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT("pthread_create() failed: videv1\n");
                }

                himppmaster->BindPreViewSys(i);



            }

            if(veSnap[i].Snapmcast_enable)

            {  mult_snap_runlag[i] = true;
                mult_snap[i] = true;
                snap_runlag[i] = false;
                snap_tid[i] = CreateThread(multiSnapProc, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {
                mult_snap_runlag[i] = false;

                snap_runlag[i] = true;

                snap_tid[i] = CreateThread(snap0ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }



            usleep(2000);


        }

        else if((thefmt >= 10) && (inputFmt_[i].vf.video_freq == 0) ){


            if(edid_4k == 0)
                himppmaster->VoModuleDestroy(0);


            vi_flag[i] = 0;
            printf("duffNum----*********havent*****-i=%d---fstart==%d\n",i,fStart[i]);
            //  gEnViMode1 = SAMPLE_VI_MODE_4_1080P;
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);
            thefmt = 0;
            zeroflg = 0;
            memcpy(&inputGetFmt_[i],&inputFmt_[i],sizeof(user_msg_info));
            if(!fStart[i]) {
                //   l_startloop[i] = HI_FALSE;
                mult_snap_runlag[i] = false;
                snap_runlag [i] = false;
                himppmaster->StopViModule(viDev,viChn);

                // himppmaster->VoModuleDestroy();
                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                usleep(20000);

            }
            else {

                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);


                    }
                    else {
                        //    printf("crtsps_openstream  CreateThread-----%d \n",2*i+j);

                        aichn = 2*i+j;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                                COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                        }


                    }
                }

            }

            getViParam(i,inputGetFmt_[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }


            //printf("gEnViMode2????????????????? =%d\n",gEnViMode1);
            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

            l_startloop[i] = HI_TRUE;
            himppmaster->SetUserPic();


            himppmaster->EnableUserPic(viChn);
            // himppmaster->EnableUserPic(viChn +1);

            printf("venc %d start  venc flag:%d\n",i,venc_flag[i]);
            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};

            vpchnsize = {1920,1080};


            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

            himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
            //  himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);
            SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};
            //            if(edid_4k == 1)
            //                himppmaster->VoModeuleInit(vo_size,0);
            //            else
            //                himppmaster->VoModeuleInit(vo_size,1);


            if(edid_4k == 0)
                himppmaster->VoModeuleInit(vo_size,0);

            //           himppmaster->VpssModuleInitvo(vpstart_vo,vpssGrpCnt_vo,vpssChn_vo,stTargetSize);
            //           himppmaster->BindPreVoewSys(0,0,3,4);


            if(fStart[i]){
                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT(" () failed: videv1\n");
                }

                himppmaster->BindPreViewSys(i);





            }
            if(veSnap[i].Snapmcast_enable)

            {
                mult_snap_runlag[i] = true;
                mult_snap[i] = true;
                snap_runlag[i] = false;
                snap_tid[i] = CreateThread(multiSnapProc, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {
                mult_snap_runlag[i] = false;
                snap_runlag[i] = true;

                snap_tid[i] = CreateThread(snap0ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }



            usleep(2000);




        }





        usleep(1000);


        sem_post(&sem3);
    }

    delete g_audio_aac[i];
    return (void*)0;
}



void* it6801Fun_2(void* arg)
{
    int vi_dev = *(int*)arg;
    int vi   ;

    int fd = -1;
    int i;


    SIZE_S stTargetSize = {0,0};
    RECT_S stCapRect;
    VI_DEV viDev = 0;
    VI_CHN viChn = 0;
    VENC_CHN veStar,veEnd;
    //    pthread_t input_tid[4];
    //    pthread_t audio_tid[4];
    bool b_detached = true;
    char rtspNum[8] = {0};
    video_type vType;

    SIZE_S vpchnsize ={0,0};

    /*
 * memcmp 结构体有内存间隙的BUG
 * 取消VI  bind vpss  采用PUSH模式；VI BIND  SWcolor模式有问题
 * */

    if (vi_dev == 1 )
        i = 0;
    else if(vi_dev == 3)
        i = 1;
    else if(vi_dev == 5)
        i = 2;
    else  if(vi_dev == 7)
        i = 3;


    HI_S32  vpstart = i;
    HI_S32  vpssGrpCnt = i + 1;
    HI_S32  vpssChn = 1;
    //    HI_S32  vpstart1 = i + 4;
    //    HI_S32  vpssGrpCnt1 = i + 5;
    //    HI_S32  vpssChn1 = 2;

    //    HI_S32 vpssGrpCnt_vo= 10, vpssChn_vo = 1;
    //    HI_S32 vpstart_vo = 8;


    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    /*
 * 音频设置--RTSP
 * enAiSampleRate -- 音频采样率 48K
 * bitRate -- 音频波特率 128K
 * coderFormat[x] -- 4路RTSP流编码类型
 * */

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(SoftwareConfig::kAiSampleRate));
    HI_S32 bitRate = str2int(thisini->GetConfig(SoftwareConfig::kAiBitRate));
    int param[2] = { 1, enAiSampleRate };
#if 1

    HI_BOOL G711Init = HI_FALSE;
    // for(i = 0; i <  viNumber; i++) {
    switch (coderFormat[i]) {
    case AUDIO_NULL:
        break;
    case AUDIO_AAC:
        g_audio_aac[i] = new aacenc();
        g_audio_aac[i]->open(bitRate, enAiSampleRate, AACLC);
        printf("start %d aac init\n",i);
        break;
    case AUDIO_G711A:
        G711Init = HI_TRUE;
        break;
    default:
        break;
    }






    if(G711Init)
        g711_init();
#endif
    memset(&inputGetFmt_[i],0,sizeof(user_msg_info));


    int aichn;
    int thefmt = 0,zeroflg = 1,j = 0,havent= 0,have = 1;
    //int zeroflg = 1;

    while(it6801Fun_false[i])
    {
        sem_wait(&sem3);// orderly pthread

        //        inputFmt[i].chip_num=i;
        //        ioctl(fdfun, VIDEO_INPUT_GET_STATUE, &inputFmt[i]);//获取VI分辨率
        //        if((inputFmt[i].video_fmt == -1) &&  (zeroflg == 1))
        //        {
        //            thefmt++;
        //            // have = 0;
        //        }
        //        else if(inputFmt[i].video_fmt != -1)
        //            thefmt   = 0;





        //        if((inputFmt_[i].video_fmt != inputGetFmt[i].video_fmt) || (inputFmt[i].fps != inputGetFmt[i].fps)) {

        //            diffNum[i]++;


        //        }

        if((inputFmt_[i].vf.video_freq == 0) &&  (zeroflg == 1))
        {
            thefmt++;
            // have = 0;
        }
        else if(inputFmt_[i].vf.video_freq != 0)
            thefmt   = 0;






        if(((inputFmt_[i].vf.video_height != inputGetFmt_[i].vf.video_height) && (inputFmt_[i].vf.video_width != inputGetFmt_[i].vf.video_width)  )   || (inputFmt_[i].vf.video_freq != inputGetFmt_[i].vf.video_freq)) {

            diffNum[i]++;
            printf("vgetf->ch: %d, w: %d , h: %d, fps:%d, scan :%d\n", \
                   inputFmt_[i].vf.video_ch, inputFmt_[i].vf.video_width, inputFmt_[i].vf.video_height, \
                   inputFmt_[i].vf.video_freq, inputFmt_[i].vf.video_interlaced);

        }

        //当未使能4k时插入4k输入时进入无信号
        if(((inputFmt_[i].vf.video_height > 1080) && (inputFmt_[i].vf.video_width > 1920))&& (edid_4k == 0 ) &&(zeroflg ==1))
        {
            inputFmt_[i].vf.video_freq  = 0;
            thefmt = 6;
        }


        if( edid_4k == 0 ){

            if(((diffNum[i] >= 2 )&& (inputFmt_[i].vf.video_freq != 0) ) &&  !(  ((inputFmt_[i].vf.video_width > 1920 ) && (inputFmt_[i].vf.video_height > 1080))&& (edid_4k == 0 ))  ){

                zeroflg = 1;
                printf("duffNum----********have******-i=%d---fstart==%d video_fmt=%d \n",i,fStart[i],inputFmt_[i].vf.video_width);
                diffNum[i] = 0;
                viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
                viChn = (4 * vi_dev);
                veStar = 2*i;
                veEnd = 2*(i+1);

                if(!fStart[i]) {

                    mult_snap_runlag[i] = false;

                    snap_runlag[i] = false;

                    // himppmaster->VencModuleDestroy_1(HI_FALSE,HI_FALSE,chnwho );

                    himppmaster->StopViModule(viDev,viChn);

                    himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                    // himppmaster->VoModuleDestroy(1);

                    if((inputGetFmt_[i].vf.video_height != inputFmt_[i].vf.video_height) || (inputGetFmt_[i].vf.video_width != inputFmt_[i].vf.video_width)) // this is edid_4k == 1 but vi is 1080p60  fps--
                    {
                        SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                        //SAMPLE_COMM_VPSS_Stop(8,10,2);
                    }

                    usleep(20000);
                }
                else {




                    for(int j = 0; j < 2; j++) {
                        memset(rtspNum,0,sizeof(rtspNum));
                        sprintf(rtspNum,"/%d",2*i+j);


                        switch ( viParam[i].enType[j]){

                        case PT_H265:
                            vType = VIDEO_H265;
                            v_type[2*i+j] = VIDEO_H265;

                            break;
                        case PT_H264:
                            vType = VIDEO_H264;
                            v_type[2*i+j] = VIDEO_H264;
                            break;
                        default:
                            break;
                        }
                        if(coderFormat[i] == AUDIO_NULL) {

                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);
                        }
                        else {
                            //   printf("crtsps_openstream  CreateThread-----%d ---coder=%d\n",2*i+j,coderFormat[i]);
                            //  g711_encode((byte*)stStream.pStream, stStream.u32Len, g711);
                            aichn = 2*i+j;
                            audio_running =  true;
                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                            if(j== 0){
                                audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                                if(audio_tid[i] == 0){
                                    //COMMONgEnViMode2_PRT("pthread_create() failed: net udpBroadCastProc");
                                }
                            }


                        }
                    }
                }

                //  inputFmt[i].video_fmt = VIN_RESOLUTION_3840x2160;
                //根据VI的数据 更改参数
                getViParam(i,inputFmt_[i],&stTargetSize,&stCapRect);
                if(stTargetSize.u32Width == 0) {
                    stTargetSize.u32Width = 1920;
                    stTargetSize.u32Height = 1080;
                }

                //  auto thisini = Singleton<SoftwareConfig>::getInstance();


                if((inputGetFmt_[i].vf.video_height != inputFmt_[i].vf.video_height) || (inputGetFmt_[i].vf.video_width != inputFmt_[i].vf.video_width)) // this is edid_4k == 1 but vi is 1080p60
                {

                    if((inputFmt_[i].vf.video_height == 2160) &&  (inputFmt_[i].vf.video_width == 3840))
                        vpchnsize = {3840,2160};
                    else
                        vpchnsize = {1920,1080};
                }


                SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};





                himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

                if((inputFmt_[i].vf.video_freq == 0) && (thefmt >= 10 )) {
                    l_startloop[i] = HI_TRUE;
                    himppmaster->SetUserPic();


                    himppmaster->EnableUserPic(viChn);

                }
                else if (inputFmt_[i].vf.video_freq != 0  )
                {
                    thefmt = 0;
                    l_startloop[i] = HI_TRUE;
                    himppmaster->DisableUserPic(viChn);
                    int userpic = 5;
                    while( userpic)
                    {
                        userpic--;
                        himppmaster->DisableUserPic(viChn +1);
                    }

                }







                viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                         min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};


                //  printf("venc-width==%d   venc--hight==%d\n",viParam[i].stVencSize.u32Width,viParam[i].stVencSize.u32Height);


                himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

                if(inputGetFmt_[i].vf.video_freq != inputFmt_[i].vf.video_freq)              //this is edid_4k == 1 but vi is 1080p60  fps--
                {

                    //  printf();
                    himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
                }
                memcpy(&inputGetFmt_[i],&inputFmt_[i],sizeof(user_msg_info));





                vi_flag[i] = 1;



                if(fStart[i]){
                    //   printf("===================================it======1\n");
                    fStart[i] = false;
                    input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                    if(input_tid[i] == 0){
                        SAMPLE_PRT("pthread_create() failed: videv1\n");
                    }

                    himppmaster->BindPreViewSys(i);



                }

                if(veSnap[i].Snapmcast_enable)
                {   mult_snap_runlag[i] = true;
                    mult_snap[i] = true;
                    snap_runlag[i] = false;
                    snap_tid[i] = CreateThread(multiSnapProc, 0, SCHED_FIFO, b_detached, &i);

                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }
                else
                {
                    mult_snap_runlag[i] = false;

                    snap_runlag[i] = true;

                    snap_tid[i] = CreateThread(snap0ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }



                usleep(2000);


            }

            else if((thefmt >= 10) && (inputFmt_[i].vf.video_freq == 0) ){




                vi_flag[i] = 0;
                printf("duffNum----*********havent*****-i=%d---fstart==%d\n",i,fStart[i]);
                //  gEnViMode1 = SAMPLE_VI_MODE_4_1080P;
                diffNum[i] = 0;
                viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
                viChn = (4 * vi_dev);
                veStar = 2*i;
                veEnd = 2*(i+1);
                thefmt = 0;
                zeroflg = 0;
                memcpy(&inputGetFmt_[i],&inputFmt_[i],sizeof(user_msg_info));
                if(!fStart[i]) {
                    //   l_startloop[i] = HI_FALSE;
                    mult_snap_runlag[i] = false;
                    snap_runlag [i] = false;
                    himppmaster->StopViModule(viDev,viChn);

                    // himppmaster->VoModuleDestroy();
                    himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                    SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                    usleep(20000);

                }
                else {

                    for(int j = 0; j < 2; j++) {
                        memset(rtspNum,0,sizeof(rtspNum));
                        sprintf(rtspNum,"/%d",2*i+j);


                        switch ( viParam[i].enType[j]){

                        case PT_H265:
                            vType = VIDEO_H265;
                            v_type[2*i+j] = VIDEO_H265;

                            break;
                        case PT_H264:
                            vType = VIDEO_H264;
                            v_type[2*i+j] = VIDEO_H264;
                            break;
                        default:
                            break;
                        }
                        if(coderFormat[i] == AUDIO_NULL) {
                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);


                        }
                        else {
                            //    printf("crtsps_openstream  CreateThread-----%d \n",2*i+j);

                            aichn = 2*i+j;
                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                            if(j== 0){
                                audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                                if(audio_tid[i] == 0){
                                    COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                                }
                            }


                        }
                    }

                }

                getViParam(i,inputGetFmt_[i],&stTargetSize,&stCapRect);
                if(stTargetSize.u32Width == 0) {
                    stTargetSize.u32Width = 1920;
                    stTargetSize.u32Height = 1080;
                }


                //printf("gEnViMode2????????????????? =%d\n",gEnViMode1);
                himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode1,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

                l_startloop[i] = HI_TRUE;
                himppmaster->SetUserPic();


                himppmaster->EnableUserPic(viChn);
                // himppmaster->EnableUserPic(viChn +1);

                printf("venc %d start  venc flag:%d\n",i,venc_flag[i]);
                viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                         min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};

                vpchnsize = {1920,1080};


                himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

                himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
                //  himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);
                SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};
                //            if(edid_4k == 1)
                //                himppmaster->VoModeuleInit(vo_size,0);
                //            else
                //                himppmaster->VoModeuleInit(vo_size,1);


                //           himppmaster->VpssModuleInitvo(vpstart_vo,vpssGrpCnt_vo,vpssChn_vo,stTargetSize);
                //           himppmaster->BindPreVoewSys(0,0,3,4);


                if(fStart[i]){
                    fStart[i] = false;
                    input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                    if(input_tid[i] == 0){
                        SAMPLE_PRT(" () failed: videv1\n");
                    }

                    himppmaster->BindPreViewSys(i);





                }
                if(veSnap[i].Snapmcast_enable)

                {
                    mult_snap_runlag[i] = true;
                    mult_snap[i] = true;
                    snap_runlag[i] = false;
                    snap_tid[i] = CreateThread(multiSnapProc, 0, SCHED_FIFO, b_detached, &i);

                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }
                else
                {
                    mult_snap_runlag[i] = false;
                    snap_runlag[i] = true;

                    snap_tid[i] = CreateThread(snap0ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }



                usleep(2000);




            }

        }
        else{

            thefmt = 0;
        }



        usleep(1000);


        sem_post(&sem1);
    }

    delete g_audio_aac[i];
    return (void*)0;
}


#if 0
void* it6801Fun_1(void* arg)
{
    int vi_dev = *(int*)arg;
    int vi ;

    int fd = -1;
    int i;

    //    Media_Input_Fmt inputFmt[4];
    SIZE_S stTargetSize = {0,0};
    RECT_S stCapRect;
    VI_DEV viDev = 0;
    VI_CHN viChn = 0;
    VENC_CHN veStar,veEnd;
    //    pthread_t input_tid[4];
    //    pthread_t audio_tid[4];
    bool b_detached = true;
    char rtspNum[8] = {0};
    video_type vType;

    /*
 * memcmp 结构体有内存间隙的BUG
 * 取消VI  bind vpss  采用PUSH模式；VI BIND  SWcolor模式有问题
 * */

    if (vi_dev == 1 )
        i = 0;
    else if(vi_dev == 3)
        i = 1;
    else if(vi_dev == 5)
        i = 2;
    else  if(vi_dev == 7)
        i = 3;

    HI_S32  vpstart = i;
    HI_S32  vpssGrpCnt = i + 1;
    HI_S32  vpssChn = 1;
    HI_S32  vpstart1 = i + 4;
    HI_S32  vpssGrpCnt1 = i + 5;
    HI_S32  vpssChn1 = 2;
    SIZE_S vpchnsize ={1920,1080};





    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    /*
 * 音频设置--RTSP
 * enAiSampleRate -- 音频采样率 48K
 * bitRate -- 音频波特率 128K
 * coderFormat[x] -- 4路RTSP流编码类型
 * */

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(SoftwareConfig::kAiSampleRate));
    HI_S32 bitRate = str2int(thisini->GetConfig(SoftwareConfig::kAiBitRate));
    int param[2] = { 1, enAiSampleRate };
#if 1

    HI_BOOL G711Init = HI_FALSE;
    // for(i = 0; i <  viNumber; i++) {
    switch (coderFormat[i]) {
    case AUDIO_NULL:
        break;
    case AUDIO_AAC:
        g_audio_aac[i] = new aacenc();
        g_audio_aac[i]->open(bitRate, enAiSampleRate, AACLC);
        printf("start %d aac init\n",i);
        break;
    case AUDIO_G711A:
        G711Init = HI_TRUE;
        break;
    default:
        break;
    }






    if(G711Init)
        g711_init();
#endif



    int aichn;
    int thefmt = 0,zeroflg = 1,j = 0,havent= 0,have = 1;
    //int zeroflg = 1;

    while(it6801Fun_false[i])
    {
        sem_wait(&sem2);// orderly pthread

        inputFmt[i].chip_num=i;
        ioctl(fdfun, VIDEO_INPUT_GET_STATUE, &inputFmt[i]);
        if((inputFmt[i].video_fmt == -1) &&  (zeroflg == 1))
        {
            thefmt++;
            // have = 0;
        }
        else if(inputFmt[i].video_fmt != -1)
            thefmt   = 0;






        // printf("_________________________video_fmt==%d _____fps=%d\n", inputFmt[i].video_fmt,inputFmt[i].fps );
        //     printf(" thefmt==%d ch:%d input_src=%d video_fmt=%d fps=%d sample%d mute_on%d stereo=%d chip_num=%d reserved=%d hdcp_encrtted=%d\n", thefmt,i,inputFmt[i].input_src,inputFmt[i].video_fmt,inputFmt[i].fps
        //            ,inputFmt[i].sample,inputFmt[i].mute_on,inputFmt[i].stereo,inputFmt[i].chip_num,inputFmt[i].reserved,inputFmt[i].HDCP_encrypted
        //            );


        if((inputFmt[i].video_fmt != inputGetFmt[i].video_fmt) || (inputFmt[i].fps != inputGetFmt[i].fps)) {

            diffNum[i]++;


            //                printf("diff==%d-diffNum=%d--format %d,fps %d,old format %d,fps %d\n",i,diffNum[i],inputFmt[i].video_fmt,inputFmt[i].fps,
            //                        inputGetFmt[i].video_fmt,inputGetFmt[i].fps);


        }


        if((inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)&& (edid_4k == 0 ) &&(zeroflg ==1))
        {
            inputFmt[i].video_fmt = -1;
            thefmt = 6;
        }

        if(((diffNum[i] >= 2 )&& (inputFmt[i].video_fmt != -1) ) &&  !((inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)&& (edid_4k == 0 ))  ){
            zeroflg = 1;
            printf("duffNum----********have******-i=%d---fstart==%d video_fmt=%d \n",i,fStart[i],inputFmt[i].video_fmt);
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);

            if(edid_4k == 0)
                himppmaster->VoModuleDestroy(0);

            if(!fStart[i]) {

                mult_snap_runlag[i] = false;
                snap_runlag[i] =false;

                himppmaster->StopViModule(viDev,viChn);

                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);

                if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt) // this is edid_4k == 1 but vi is 1080p60  fps--
                {
                    SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                    //SAMPLE_COMM_VPSS_Stop(i+ 4 , i+6, vpssChn);
                }
                usleep(20000);

            }
            else {

                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);


                    }
                    else {
                        //  g711_encode((byte*)stStream.pStream, stStream.u32Len, g711);
                        aichn = 2*i+j;
                        audio_running =  true;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                            }
                            int ai35mm = 2;
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &ai35mm);
                            if(audio_tid[i] == 0){
                            }

                        }


                    }
                }
            }

            getViParam(i,inputFmt[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }



            if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt)
            {

                if(inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)
                    vpchnsize = {3840,2160};
                else
                    vpchnsize = {1920,1080};
            }







            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode2,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate , viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

            if(inputFmt[i].video_fmt == -1 && thefmt >= 10 ) {
                l_startloop[i] = HI_TRUE;
                himppmaster->SetUserPic();


                himppmaster->EnableUserPic(viChn);

            }
            else if (inputFmt[i].video_fmt != -1  )
            {
                thefmt = 0;
                l_startloop[i] = HI_TRUE;
                himppmaster->DisableUserPic(viChn);

            }






            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};





            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

            if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt)
            {
                himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
                //  himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);

            }
            memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));
            SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};
            if(edid_4k == 0)
                himppmaster->VoModeuleInit(vo_size,0);


            vi_flag[i] = 1;


            if(fStart[i]){

                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT("pthread_create() failed: videv1\n");
                }
                himppmaster->BindPreViewSys(i);





            }

            if(veSnap[i].Snapmcast_enable)

            {   mult_snap[i]= true;
                mult_snap_runlag[i] = true;
                snap_runlag[i] =false;
                snap_tid[i] = CreateThread(multiSnapProc1, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {

                mult_snap_runlag[i] = false;
                snap_runlag[i] =true;
                snap_tid[i] = CreateThread(snap1ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }


            usleep(2000);


        }



#if  1
        else if((thefmt >= 10) && (inputFmt[i].video_fmt == -1) ){

            vi_flag[i] = 0;
            printf("duffNum----*********havent*****-i=%d---fstart==%d\n",i,fStart[i]);
            // gEnViMode2 = SAMPLE_VI_MODE_4_1080P;
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);
            thefmt = 0;
            zeroflg = 0;
            if(edid_4k == 0)
                himppmaster->VoModuleDestroy(0);
            memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));
            if(!fStart[i]) {
                //   l_startloop[i] = HI_FALSE;


                mult_snap_runlag[i] =false;
                snap_runlag[i] =false;

                himppmaster->StopViModule(viDev,viChn);


                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                //  SAMPLE_COMM_VPSS_Stop(i+4 , i+5, vpssChn);
                usleep(20000);

            }
            else {

                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);


                    }
                    else {


                        aichn = 2*i+j;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                                COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                            int ai35mm = 2;
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &ai35mm);
                            if(audio_tid[i] == 0){
                                //COMMONgEnViMode2_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                        }


                    }
                }

            }

            getViParam(i,inputGetFmt[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }




            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode2,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

            l_startloop[i] = HI_TRUE;
            himppmaster->SetUserPic();


            himppmaster->EnableUserPic(viChn);

            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};

            vpchnsize ={1920,1080};


            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);


            himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
            //   himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);
            SIZE_S vo_size = {stCapRect.u32Width,stCapRect.u32Height};
            if(edid_4k == 0)
                himppmaster->VoModeuleInit(vo_size,0);


            if(fStart[i]){
                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT(" () failed: videv1\n");
                }

                himppmaster->BindPreViewSys(i);





            }

            if(veSnap[i].Snapmcast_enable)

            {   mult_snap[i] = true;
                mult_snap_runlag[i] = true;
                snap_runlag[i] =false;
                snap_tid[i] = CreateThread(multiSnapProc1, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {

                mult_snap_runlag[i] = false;
                snap_runlag[i] =true;
                snap_tid[i] = CreateThread(snap1ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }


            usleep(2000);




        }
#endif




        usleep(1000);


        sem_post(&sem3);
    }
    delete g_audio_aac[i];

    return (void*)0;
}


void* it6801Fun_2(void* arg)
{
    int vi_dev = *(int*)arg;
    int vi ;

    int fd = -1;
    int i;

    //    Media_Input_Fmt inputFmt[4];
    SIZE_S stTargetSize = {0,0};
    RECT_S stCapRect;
    VI_DEV viDev = 0;
    VI_CHN viChn = 0;
    VENC_CHN veStar,veEnd;
    //    pthread_t input_tid[4];
    //    pthread_t audio_tid[4];
    bool b_detached = true;
    char rtspNum[8] = {0};
    video_type vType;

    /*
 * memcmp 结构体有内存间隙的BUG
 * 取消VI  bind vpss  采用PUSH模式；VI BIND  SWcolor模式有问题
 * */

    if (vi_dev == 1 )
        i = 0;
    else if(vi_dev == 3)
        i = 1;
    else if(vi_dev == 5)
        i = 2;
    else  if(vi_dev == 7)
        i = 3;


    HI_S32  vpstart = i;
    HI_S32  vpssGrpCnt = i + 1;
    HI_S32  vpssChn = 1;
    HI_S32  vpstart1 = i + 4;
    HI_S32  vpssGrpCnt1 = i + 5;
    HI_S32  vpssChn1 = 2;

    SIZE_S vpchnsize ={1920,1080};



    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    /*
 * 音频设置--RTSP
 * enAiSampleRate -- 音频采样率 48K
 * bitRate -- 音频波特率 128K
 * coderFormat[x] -- 4路RTSP流编码类型
 * */

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(SoftwareConfig::kAiSampleRate));
    HI_S32 bitRate = str2int(thisini->GetConfig(SoftwareConfig::kAiBitRate));
    int param[2] = { 1, enAiSampleRate };
#if 1

    HI_BOOL G711Init = HI_FALSE;
    // for(i = 0; i <  viNumber; i++) {
    switch (coderFormat[i]) {
    case AUDIO_NULL:
        break;
    case AUDIO_AAC:
        g_audio_aac[i] = new aacenc();
        g_audio_aac[i]->open(bitRate, enAiSampleRate, AACLC);
        printf("start %d aac init\n",i);
        break;
    case AUDIO_G711A:
        G711Init = HI_TRUE;
        break;
    default:
        break;
    }






    if(G711Init)
        g711_init();
#endif




    int aichn;
    int thefmt = 0,zeroflg = 1,j = 0,havent= 0,have = 1;
    //int zeroflg = 1;

    while(it6801Fun_false[i])
    {
        sem_wait(&sem3);// orderly pthread





        inputFmt[i].chip_num=i;
        ioctl(fdfun, VIDEO_INPUT_GET_STATUE, &inputFmt[i]);
        if((inputFmt[i].video_fmt == -1) &&  (zeroflg == 1))
        {
            thefmt++;
            // have = 0;
        }
        else if(inputFmt[i].video_fmt != -1)
            thefmt   = 0;


        //               if(thefmt >= 10)
        //                   have = 0;

        //   printf("_________________________video_fmt==%d _____fps=%d\n", inputFmt[i].video_fmt,inputFmt[i].fps );


#if 1
        if( edid_4k == 0 )
        {







            if((inputFmt[i].video_fmt != inputGetFmt[i].video_fmt) || (inputFmt[i].fps != inputGetFmt[i].fps)) {


                diffNum[i]++;



            }


            if((inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)&& (edid_4k == 0 ) &&(zeroflg ==1))
            {
                inputFmt[i].video_fmt = -1;
                thefmt = 6;
            }

            if(((diffNum[i] >= 2 )&& (inputFmt[i].video_fmt != -1) ) &&  !((inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)&& (edid_4k == 0 ))  ){
                zeroflg = 1;
                printf("duffNum----********have******-i=%d---fstart==%d video_fmt=%d \n",i,fStart[i],inputFmt[i].video_fmt);
                diffNum[i] = 0;
                viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
                viChn = (4 * vi_dev);
                veStar = 2*i;
                veEnd = 2*(i+1);

                //   memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));

                //                if(inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)
                //                {
                //                    break;
                //                }
                if(!fStart[i]) {


                    mult_snap_runlag[i] = false;
                    snap_runlag[i]= false;

                    himppmaster->StopViModule(viDev,viChn);

                    himppmaster->VencModuleDestroy_1(veStar,veEnd,i);



                    if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt) // 4k time   vi is 1080 fps--;
                    {
                        SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                    }

                    usleep(20000);

                }
                else {

                    for(int j = 0; j < 2; j++) {
                        memset(rtspNum,0,sizeof(rtspNum));
                        sprintf(rtspNum,"/%d",2*i+j);


                        switch ( viParam[i].enType[j]){

                        case PT_H265:
                            vType = VIDEO_H265;
                            v_type[2*i+j] = VIDEO_H265;

                            break;
                        case PT_H264:
                            vType = VIDEO_H264;
                            v_type[2*i+j] = VIDEO_H264;
                            break;
                        default:
                            break;
                        }
                        if(coderFormat[i] == AUDIO_NULL) {
                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);


                        }
                        else {
                            // printf("crtsps_openstream  CreateThread-----%d ---coder=%d\n",2*i+j,coderFormat[i]);
                            //  g711_encode((byte*)stStream.pStream, stStream.u32Len, g711);
                            aichn = 2*i+j;
                            audio_running =  true;
                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                            if(j== 0){
                                audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                                if(audio_tid[i] == 0){
                                    COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                                }
                            }


                        }
                    }
                }

                getViParam(i,inputFmt_[i],&stTargetSize,&stCapRect);
                if(stTargetSize.u32Width == 0) {
                    stTargetSize.u32Width = 1920;
                    stTargetSize.u32Height = 1080;
                }


                if(inputGetFmt_[i].vf.video_freq != inputFmt_[i].vf.video_freq)
                {

                    if(inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)
                        vpchnsize = {3840,2160};
                    else
                        vpchnsize = {1920,1080};
                }


                // printf("gEnViMode =%d\n",gEnViMode);

                himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate, viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

                if(inputFmt[i].video_fmt == -1 && thefmt >= 10 ) {
                    l_startloop[i] = HI_TRUE;
                    himppmaster->SetUserPic();

                    himppmaster->EnableUserPic(viChn);

                }
                else if (inputFmt[i].video_fmt != -1  )
                {
                    thefmt = 0;
                    l_startloop[i] = HI_TRUE;
                    himppmaster->DisableUserPic(viChn);

                }







                viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                         min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};


                //  printf("venc-width==%d   venc--hight==%d\n",viParam[i].stVencSize.u32Width,viParam[i].stVencSize.u32Height);


                himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);


                if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt)
                {
                    himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
                    //   himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);

                }
                memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));

                vi_flag[i] = 1;


                if(fStart[i]){

                    fStart[i] = false;
                    input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                    if(input_tid[i] == 0){
                        SAMPLE_PRT("pthread_create() failed: videv1\n");
                    }

                    himppmaster->BindPreViewSys(i);



                }

                if(veSnap[i].Snapmcast_enable)

                {
                    mult_snap[i] =true;
                    mult_snap_runlag[i] = true;
                    snap_runlag[i] = false;
                    snap_tid[i] = CreateThread(multiSnapProc2, 0, SCHED_FIFO, b_detached, &i);

                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }
                else
                {
                    mult_snap_runlag[i] = false;
                    snap_runlag[i]= true;
                    snap_tid[i] = CreateThread(snap2ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }



                usleep(2000);


            }



#if  1
            else if((thefmt >= 7) && (inputFmt[i].video_fmt == -1) ){
                // else if((thefmt >= 45) && (inputFmt[i].video_fmt == -1) ){
                vi_flag[i] = 0;
                printf("duffNum----*********havent*****-i=%d---fstart==%d\n",i,fStart[i]);

                diffNum[i] = 0;
                viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
                viChn = (4 * vi_dev);
                veStar = 2*i;
                veEnd = 2*(i+1);
                thefmt = 0;
                zeroflg = 0;
                memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));
                if(!fStart[i]) {


                    mult_snap_runlag[i] = false;
                    snap_runlag[i] =false;
                    usleep(20000);


                    himppmaster->StopViModule(viDev,viChn);


                    himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                    SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);


                }
                else {

                    for(int j = 0; j < 2; j++) {
                        memset(rtspNum,0,sizeof(rtspNum));
                        sprintf(rtspNum,"/%d",2*i+j);


                        switch ( viParam[i].enType[j]){



                        case PT_H265:
                            vType = VIDEO_H265;
                            v_type[2*i+j] = VIDEO_H265;

                            break;
                        case PT_H264:
                            vType = VIDEO_H264;
                            v_type[2*i+j] = VIDEO_H264;
                            break;
                        default:
                            break;
                        }
                        if(coderFormat[i] == AUDIO_NULL) {
                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);

                            printf("start rtsp %d  id ==%d-----%d---i=%d\n",2*i+j,g_stream[2*i+j],vType,i);
                        }
                        else {
                            printf("crtsps_openstream  CreateThread-----%d \n",2*i+j);

                            aichn = 2*i+j;
                            g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                            if(j== 0){
                                audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                                if(audio_tid[i] == 0){
                                    COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                                }
                            }


                        }
                    }

                }

                getViParam(i,inputGetFmt[i],&stTargetSize,&stCapRect);
                if(stTargetSize.u32Width == 0) {
                    stTargetSize.u32Width = 1920;
                    stTargetSize.u32Height = 1080;
                }

                himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate , viParam[i].u32DstFrameRate[0],viParam[i].u32DstFrameRate[1] );

                l_startloop[i] = HI_TRUE;
                himppmaster->SetUserPic();


                himppmaster->EnableUserPic(viChn);
                printf("venc %d start venc flag:%d\n",i,venc_flag[i]);
                viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                         min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};

                vpchnsize ={1920,1080};


                himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

                himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
                //   himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);



                if(fStart[i]){
                    fStart[i] = false;
                    input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                    if(input_tid[i] == 0){
                        SAMPLE_PRT("pthread_create() failed: videv1\n");
                    }

                    himppmaster->BindPreViewSys(i);





                }


                if(veSnap[i].Snapmcast_enable)

                {
                    mult_snap[i] =true;
                    mult_snap_runlag[i] = true;
                    snap_runlag[i] = false;
                    snap_tid[i] = CreateThread(multiSnapProc2, 0, SCHED_FIFO, b_detached, &i);

                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }
                else
                {
                    mult_snap_runlag[i] = false;
                    snap_runlag[i]= true;
                    snap_tid[i] = CreateThread(snap2ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                    if(snap_tid[i] == 0){
                        COMMON_PRT("pthread_create() failed: snapServerProc\n");
                    }
                }


                usleep(2000);




            }
#endif




            usleep(1000);


        }
        else
        {
            thefmt = 0;
            //  himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

        }

#endif
        sem_post(&sem1);
    }
    delete g_audio_aac[i];

    return (void*)0;
}


void* it6801Fun_3(void* arg)
{
    int vi_dev = *(int*)arg;
    int vi    ;

    int fd = -1;
    int i;

    //    Media_Input_Fmt inputFmt[4];
    SIZE_S stTargetSize = {0,0};
    RECT_S stCapRect;
    VI_DEV viDev = 0;
    VI_CHN viChn = 0;
    VENC_CHN veStar,veEnd;
    //    pthread_t input_tid[4];
    //    pthread_t audio_tid[4];
    bool b_detached = true;
    char rtspNum[8] = {0};
    video_type vType;



    if (vi_dev == 1 )
        i = 0;
    else if(vi_dev == 3)
        i = 1;
    else if(vi_dev == 5)
        i = 2;
    else  if(vi_dev == 7)
        i = 3;

    HI_S32  vpstart = i;
    HI_S32  vpssGrpCnt = i + 1;
    HI_S32  vpssChn = 1;
    HI_S32  vpstart1 = i + 4;
    HI_S32  vpssGrpCnt1 = i + 5;
    HI_S32  vpssChn1 = 2;
    SIZE_S vpchnsize ={1920,1080};

    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<SoftwareConfig>::getInstance();
    /*
 * 音频设置--RTSP
 * enAiSampleRate -- 音频采样率 48K
 * bitRate -- 音频波特率 128K
 * coderFormat[x] -- 4路RTSP流编码类型
 * */

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(SoftwareConfig::kAiSampleRate));
    HI_S32 bitRate = str2int(thisini->GetConfig(SoftwareConfig::kAiBitRate));
    int param[2] = { 1, enAiSampleRate };
#if 1

    HI_BOOL G711Init = HI_FALSE;
    // for(i = 0; i <  viNumber; i++) {
    switch (coderFormat[i]) {
    case AUDIO_NULL:
        break;
    case AUDIO_AAC:
        g_audio_aac[i] = new aacenc();
        g_audio_aac[i]->open(bitRate, enAiSampleRate, AACLC);
        printf("start %d aac init\n",i);
        break;
    case AUDIO_G711A:
        G711Init = HI_TRUE;
        break;
    default:
        break;
    }






    if(G711Init)
        g711_init();
#endif




    int aichn;
    int thefmt = 0,zeroflg = 1,j = 0,havent= 0,have = 1;
    //int zeroflg = 1;

    while(it6801Fun_false[i])
    {
        sem_wait(&sem4);// orderly pthread

        inputFmt[i].chip_num=i;
        ioctl(fdfun, VIDEO_INPUT_GET_STATUE, &inputFmt[i]);
        if((inputFmt[i].video_fmt == -1) &&  (zeroflg == 1))
        {
            // thefmt++;
            have = 0;
        }
        else if(inputFmt[i].video_fmt != -1)
            thefmt   = 0;



        //注意: have = 0; 为了不造益出

#if 0

        if((inputFmt[i].video_fmt != inputGetFmt[i].video_fmt) || (inputFmt[i].fps != inputGetFmt[i].fps)) {

            diffNum[i]++;


        }



        if((diffNum[i] >= 2 )&& (inputFmt[i].video_fmt != -1) ){
            zeroflg = 1;
            printf("duffNum----********have******-i=%d---fstart==%d\n",i,fStart[i]);
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);

            //  memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));

            if(inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)
            {
                break;
            }


            if(!fStart[i]) {

                mult_snap_runlag[i] = false;
                snap_runlag[i] = false;


                himppmaster->StopViModule(viDev,viChn);

                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt) // 4k time   vi is 1080 fps--;
                {
                    SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                }
                usleep(20000);

            }
            else {

                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);

                        printf("start rtsp %d  id ==%d-----%d---i=%d\n",2*i+j,g_stream[2*i+j],vType,i);
                    }
                    else {
                        // printf("crtsps_openstream  CreateThread-----%d ---coder=%d\n",2*i+j,coderFormat[i]);
                        //  g711_encode((byte*)stStream.pStream, stStream.u32Len, g711);
                        aichn = 2*i+j;
                        audio_running =  true;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                                COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                        }


                    }
                }
            }

            getViParam(i,inputFmt[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }

            if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt)
            {

                if(inputFmt[i].video_fmt == VIN_RESOLUTION_3840x2160)
                    vpchnsize = {3840,2160};
                else
                    vpchnsize = {1920,1080};
            }



            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate );

            if(inputFmt[i].video_fmt == -1 && thefmt >= 10 ) {
                l_startloop[i] = HI_TRUE;
                himppmaster->SetUserPic();
                printf("EnableUserPic --------video_fmt  == -1   i==%d---aaaaaaaaaaaaaaaaa\n",i);

                himppmaster->EnableUserPic(viChn);

            }
            else if (inputFmt[i].video_fmt != -1  )
            {
                thefmt = 0;
                l_startloop[i] = HI_TRUE;
                himppmaster->DisableUserPic(viChn);

            }







            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};


            //  printf("venc-width==%d   venc--hight==%d\n",viParam[i].stVencSize.u32Width,viParam[i].stVencSize.u32Height);


            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

            if(inputGetFmt[i].video_fmt != inputFmt[i].video_fmt)
            {
                himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
                //  himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);

            }
            memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));
            vi_flag[i] = 1;


            if(fStart[i]){
                //   printf("===================================it======1\n");
                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT("pthread_create() failed: videv1\n");
                }

                himppmaster->BindPreViewSys(i);




            }

            if(veSnap[i].Snapmcast_enable)
            {  mult_snap_runlag[i] = true;
                mult_snap[i] = true;
                snap_runlag[i] = false;

                snap_tid[i] = CreateThread(multiSnapProc3, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {
                mult_snap_runlag[i] = false;

                snap_runlag[i] = true;

                snap_tid[i] = CreateThread(snap3ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }


            usleep(2000);


        }




        // else if((thefmt >= 8) && (inputFmt[i].video_fmt == -1) ){
        else if((thefmt >= 50) && (inputFmt[i].video_fmt == -1) ){
            vi_flag[i] = 0;
            printf("duffNum----*********havent*****-i=%d---fstart==%d\n",i,fStart[i]);
            // have = 1;
            diffNum[i] = 0;
            viDev = vi_dev;      //vi dev 1 3 5 7 chn 4 12 20 28
            viChn = (4 * vi_dev);
            veStar = 2*i;
            veEnd = 2*(i+1);
            thefmt = 0;
            zeroflg = 0;
            memcpy(&inputGetFmt[i],&inputFmt[i],sizeof(Media_Input_Fmt));
            if(!fStart[i]) {
                //   l_startloop[i] = HI_FALSE;

                mult_snap_runlag[i] = false;
                snap_runlag[i] = false;

                himppmaster->StopViModule(viDev,viChn);


                himppmaster->VencModuleDestroy_1(veStar,veEnd,i);


                SAMPLE_COMM_VPSS_Stop(i , i+1, vpssChn);
                usleep(20000);

            }
            else {

                for(int j = 0; j < 2; j++) {
                    memset(rtspNum,0,sizeof(rtspNum));
                    sprintf(rtspNum,"/%d",2*i+j);


                    switch ( viParam[i].enType[j]){

                    case PT_H265:
                        vType = VIDEO_H265;
                        v_type[2*i+j] = VIDEO_H265;

                        break;
                    case PT_H264:
                        vType = VIDEO_H264;
                        v_type[2*i+j] = VIDEO_H264;
                        break;
                    default:
                        break;
                    }
                    if(coderFormat[i] == AUDIO_NULL) {
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,NULL);

                        printf("start rtsp %d  id ==%d-----%d---i=%d\n",2*i+j,g_stream[2*i+j],vType,i);
                    }
                    else {
                        //    printf("crtsps_openstream  CreateThread-----%d \n",2*i+j);

                        aichn = 2*i+j;
                        g_stream[2*i+j] = crtsps_openstream(vType,coderFormat[i],rtspNum,param);
                        if(j== 0){
                            audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, b_detached, &i);
                            if(audio_tid[i] == 0){
                                COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
                            }
                        }

                    }
                }

            }



            getViParam(i,inputGetFmt[i],&stTargetSize,&stCapRect);
            if(stTargetSize.u32Width == 0) {
                stTargetSize.u32Width = 1920;
                stTargetSize.u32Height = 1080;
            }

            himppmaster->ViModuleInit_1(viDev,viChn,gEnNorm,gEnViMode,stTargetSize,stCapRect, viParam[i].u32SrcFrmRate );

            l_startloop[i] = HI_TRUE;
            himppmaster->SetUserPic();
            printf("EnableUserPic --------video_fmt  == -1   i==%d---aaaaaaaaaaaaaaaaa\n",i);

            himppmaster->EnableUserPic(viChn);
            printf("venc %d start venc flag:%d\n",i,venc_flag[i]);
            viParam[i].stVencSize = {min(viParam[i].stVencSize.u32Width,stTargetSize.u32Width),
                                     min(viParam[i].stVencSize.u32Height,stTargetSize.u32Height)};

            vpchnsize ={1920,1080};
            //   printf("venc-width==%d   venc--hight==%d\n",viParam[i].stVencSize.u32Width,viParam[i].stVencSize.u32Height);

            //   himppmaster->VencModuleInit_1(i,Usevenchn,stTargetSize,HI_FALSE);
            himppmaster->VencModuleInit_1(i,Usevenchn,veSnap[i].snapSize,i);

            himppmaster->VpssModuleInit(vpstart,vpssGrpCnt,vpssChn,vpchnsize);
            //  himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize);



            if(fStart[i]){
                fStart[i] = false;
                input_tid[i] = CreateThread(rtspThread0, 0, SCHED_FIFO, b_detached, &i);
                if(input_tid[i] == 0){
                    SAMPLE_PRT("pthread_create() failed: videv1\n");
                }

                himppmaster->BindPreViewSys(i);





            }

            if(veSnap[i].Snapmcast_enable)
            {  mult_snap_runlag[i] = true;
                mult_snap[i] = true;
                snap_runlag[i] = false;

                snap_tid[i] = CreateThread(multiSnapProc3, 0, SCHED_FIFO, b_detached, &i);

                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }
            else
            {
                mult_snap_runlag[i] = false;

                snap_runlag[i] = true;

                snap_tid[i] = CreateThread(snap3ServerProc, 0, SCHED_FIFO, b_detached, &snap_port[i]);
                if(snap_tid[i] == 0){
                    COMMON_PRT("pthread_create() failed: snapServerProc\n");
                }
            }


            usleep(2000);




        }
#endif




        usleep(10000);


        sem_post(&sem1);
    }

    delete g_audio_aac[i];
    return (void*)0;
}
#endif


void * MultiSend(HI_VOID *arg){
    //创建UDP组播发送
    int vi = (*(int*)arg) * 2;

    struct timeval start,stop,diff;
    auto thisini = Singleton<Config>::getInstance();
    UDPSocket *MultiSendSocket = new UDPSocket();
    //MultiSendSocket->CreateUDPClient(string(thisini->GetConfig(Config::kMcastIp)).c_str(),
    //                               atoi(thisini->GetConfig(Config::kMcastPort).c_str()), false); //设为阻塞
    MultiSendSocket->CreateUDPClient(mcast_ip[vi],
                                     mcast_port[vi],false); //设为阻塞



    MultiSendSocket->BindLocalAddr(ETH0);
    //   int repeat_time = str2int(thisini->GetConfig(Config::kMcastNum));
    /*TransMit st_transmit_info;
    memcpy(&st_transmit_info,arg,sizeof(st_transmit_info));

    UDPSocket *MultiSendSocket = new UDPSocket();
    MultiSendSocket->CreateUDPClient(st_transmit_info.ip, st_transmit_info.port, false);
    MultiSendSocket->BindLocalAddr(gSysEthUser.c_str());*/

    //打开中断驱动通信文件
    char irqfile[24] = "";
    sprintf(irqfile, "/dev/interrupt%d", 0);
    KoData kodata;

    HI_S32 irqfd = open(irqfile, 0); //阻塞读取； NOTE: 非阻塞 O_RDONLY|O_NONBLOCK
    if (irqfd < 0){
        irqfd = -1;
        close(irqfd);
        printf("xxxxxxxxxopen %s error: %s.", irqfile, strerror(errno));
    }

    char    senddata[1300 + INFOHEADER] = "";                 //用于区分是否是新的一帧
    int     self_frame_id = 0;
    int     sync_count = 0;
    //    int     last_frame = 0;
    MyTimer *mcasttimer_ = NULL;
    mcasttimer_ = new MyTimer();
    mcasttimer_->Reset();
    mcasttimer_->Start();
    while(1){
        //需要检测是否有需要发送的,为了保证同步，需要等中断发送
        if(irqfd >= 0){
            read(irqfd, &kodata, sizeof(kodata));
        }

        mcasttimer_->Stop();
        if((mcasttimer_->stCostTime.msec * 1000 + mcasttimer_->stCostTime.usec) > 30000){
            //  printf("Self added!------------------------------------------%d.%d>\n",(int)mcasttimer_->stCostTime.msec,(int)mcasttimer_->stCostTime.usec);
            if(irqfd >= 0){
                read(irqfd, &kodata, sizeof(kodata));
            }
        }
        mcasttimer_->Reset();
        mcasttimer_->Start();

        self_frame_id = ((int)kodata.frame_num + glLocalTickDelta + 60) % 60;

        if(glDBWptr != (glDBRptr + 1) % 60){
            //说明有新数据
            //            if((last_frame + 1) % 60 != self_frame_id)
            //                printf("Send Frame: %d\n", self_frame_id);

            //            last_frame = self_frame_id;

            int i_frame_num = glDataBuf[glDBRptr][0];
            int len = glDataBuf[glDBRptr][1];
            senddata[3] = self_frame_id;        //帧号
            if(i_frame_num > 0){
                //这个包是I帧
                //  printf("i_frame_num %d\n",i_frame_num);

                char *tmpdata = (char*)&glDataBuf[glDBRptr][1];

                senddata[1] = 1;                //总包数
                senddata[2] = 0;                //当前包
                for(int j = 0; j < i_frame_num; j ++){
                    senddata[0] ++;     //包标号，仅仅用来防止丢包
                    len = *(int*)tmpdata;
                    tmpdata += sizeof(int);
                    printf("len %d\n",len);
                    memcpy(&senddata[INFOHEADER], tmpdata, len);
                    tmpdata += len;
                    for(int i = 0; i < repeat_time[vi]; i++){
                        //  printf("--------glDBRptr111 %d\n",glDBRptr);
                        MultiSendSocket->SendTo((const char*)senddata, len + INFOHEADER);
                        gettimeofday(&start, 0);
                        do{
                            gettimeofday(&stop, 0);
                            tim_subtract(&diff, &start, &stop);
                            //printf("diff.tv_sec: %d, diff.tv_usec: %d,mcast_delay %d\n", diff.tv_sec, diff.tv_usec, mcast_delay[i]);
                            // }while(diff.tv_usec < str2int(thisini->GetConfig(Config::kMcastDelay)));
                        }while(diff.tv_usec < mcast_delay[vi]);
                    }
                }
                //发送数据
                //打包
                //写入数据包头
                len = *(int*)tmpdata;
                tmpdata += sizeof(int);
                senddata[0] ++;     //包标号，仅仅用来防止丢包
                senddata[1] = (len+1299)/1300;  //总包数
                for(int i = 0; i < repeat_time[vi]; i++){
                    int tmplen = len;
                    senddata[2] = 0;           //当前包号
                    char* bkup_data = tmpdata;
                    while (tmplen > 0){
                        //准备发送数据
                        memcpy(&senddata[INFOHEADER], bkup_data, min(1300,tmplen));
                        //   printf("-----------glDBRptr222 %d\n",glDBRptr);
                        MultiSendSocket->SendTo((const char*)senddata, min(1300,tmplen) + INFOHEADER);
                        bkup_data += min(1300,tmplen);
                        tmplen  -= min(1300,tmplen);
                        senddata[2]++;                              //指向下一包

                        //usleep(10000 / repeat_time / senddata[1]);
                    }
                    gettimeofday(&start, 0);
                    do{
                        gettimeofday(&stop, 0);
                        tim_subtract(&diff, &start, &stop);
                        //printf("diff.tv_sec: %d, diff.tv_usec: %d,mcast_delay %d\n", diff.tv_sec, diff.tv_usec, mcast_delay[i]);
                        //}while(diff.tv_usec < str2int(thisini->GetConfig(Config::kMcastDelay)));
                    }while(diff.tv_usec < mcast_delay[vi]);
                }
            }
            else{
                //打包
                //写入数据包头

                char *tmpdata = (char*)&glDataBuf[glDBRptr][1];
                len = *(int*)tmpdata;
                tmpdata += sizeof(int);
                senddata[0] ++;     //包标号，仅仅用来防止丢包
                senddata[1] = (len+1299)/1300;  //总包数

                for(int i = 0; i < repeat_time[vi]; i++){
                    int tmplen = len;
                    senddata[2] = 0;           //当前包号
                    char* bkup_data = tmpdata;
                    while (tmplen > 0){
                        //准备发送数据
                        memcpy(&senddata[INFOHEADER], bkup_data, min(1300,tmplen));
                        //printf("-----------glDBRptr %d,%d\n",glDBRptr,tmplen);
                        MultiSendSocket->SendTo((const char*)senddata, min(1300,tmplen) + INFOHEADER);
                        bkup_data += min(1300,tmplen);
                        tmplen  -= min(1300,tmplen);
                        senddata[2]++;                              //指向下一包

                        //usleep(10000 / repeat_time / senddata[1]);
                    }
                    gettimeofday(&start, 0);
                    do{
                        gettimeofday(&stop, 0);
                        tim_subtract(&diff, &start, &stop);
                        //   printf("diff.tv_sec: %d, diff.tv_usec: %d,mcast_delay %d\n", diff.tv_sec, diff.tv_usec, mcast_delay[i]);
                        //  }while(diff.tv_usec < str2int(thisini->GetConfig(Config::kMcastDelay)));
                    }while(diff.tv_usec < mcast_delay[vi]);
                }
            }

            glDBRptr = (glDBRptr + 1) % 60;

            //            if(glFrameID == 0){
            //                sync_count ++;
            //                if(sync_count == 10){
            //                    sync_count = 0;
            //                    self_frame_id = 59;
            //                }
            //            }
        }
        //        self_frame_id = (self_frame_id + 1) % 60;

    }

    delete MultiSendSocket;
    MultiSendSocket = NULL;

    if(irqfd >= 0)
        close(irqfd);

    return (HI_VOID*)1;
}
















//检测SDI








#if 0

void* it66021fn(void*)
{
    uint counter = 0;
    char buff[1024] = { 0 };
    struct sockaddr_nl src_addr;
    struct nlmsghdr *nlh = NULL;
    struct iovec iov;
    struct msghdr msg;
    struct msg_struct* ms;
    int height;

    int sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_IT66021FN);
    if (sock_fd < 0) {
        printf("chris Error: %s\n", strerror(errno));
        COMMON_PRT("create socket error\n");
        return NULL;
    }
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = PF_NETLINK;
    src_addr.nl_pid = 0;
    src_addr.nl_groups = netlink_group_mask(GROUP_IT66021FN);

    int ret = bind(sock_fd, (struct sockaddr *)&src_addr, sizeof(src_addr));
    if (ret < 0) {
        close(sock_fd);
        COMMON_PRT("bind socket failed\n");
        return NULL;
    }
    nlh = (nlmsghdr*)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    if (!nlh) {
        close(sock_fd);
        COMMON_PRT("failed\n");
        return NULL;
    }
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    iov.iov_base = (void *)nlh;
    iov.iov_len = NLMSG_SPACE(MAX_PAYLOAD);

    memset(&msg, 0, sizeof(msg));
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    auto thisini = Singleton<Config>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    int now_w = str2int(thisini->GetConfig(Config::kChn0_VI_W));
    int now_h = str2int(thisini->GetConfig(Config::kChn0_VI_H));
    int now_f = str2int(thisini->GetConfig(Config::kChn0_SrcFrmRate));
    unsigned venc_SAME_INPUT = str2int(thisini->GetConfig(Config::kChn0_M_VENC_SAME_INPUT));
    int vi_crop_enable = str2int(thisini->GetConfig(Config::kChn0_VI_Crop_Enable));

    //COMMON_PRT("now_w: %d, now_h: %d, now_f: %d\n", now_w, now_h, now_f);

    while(l_it66021fn_running) {
        recvmsg(sock_fd, &msg, 0);
        memcpy(&buff, NLMSG_DATA(nlh), 1024);
        ms=(struct msg_struct*)buff;
        COMMON_PRT("IT66021FN message:width:%d height:%d frameRate:%d scanMode:%d audioSampleRate:%d\n", ms->video_width,ms->video_height,ms->video_freq,ms->video_interlaced,ms->audio_freq);
        if(ms->video_freq >= 40)
            ms->video_freq = 60;
        else
            ms->video_freq = 30;
        if((now_w != ms->video_width ||
            now_h != ms->video_height ||
            now_f != ms->video_freq) &&
                ms->video_width  > 0 &&
                ms->video_height > 0){
            COMMON_PRT("#############IT66021FN message:width:%d height:%d frameRate:%d scanMode:%d audioSampleRate:%d\n", ms->video_width, ms->video_height, ms->video_freq, ms->video_interlaced, ms->audio_freq);
#if 0
            int input_type = 1;
            now_w = ms->video_width;
            now_h = ms->video_height;
            now_f = ms->video_freq;
            height = now_h;
            if((now_w==1920)&&(now_h == 540)){
                height = 1080;
                input_type = 0;
            }
            thisini->SetConfig(Config::kChn0_VI_W, int2str(now_w));
            thisini->SetConfig(Config::kChn0_VI_H, int2str(height));
            thisini->SetConfig(Config::kChn0_SrcFrmRate, int2str(now_f));
            thisini->SetConfig(Config::kChn0_M_DstFrmRate, min(now_f,str2int(thisini->GetConfig(Config::kChn0_M_DstFrmRate))));
            thisini->SetConfig(Config::kChn0_S0_DstFrmRate, min(now_f,str2int(thisini->GetConfig(Config::kChn0_S0_DstFrmRate))));
            RECT_S stCapRect = {0, 0, now_w, height};
            if(venc_SAME_INPUT == 1){
                thisini->SetConfig(Config::kChn0_M_VENC_W, int2str(now_w));
                thisini->SetConfig(Config::kChn0_M_VENC_H, int2str(height));
                thisini->SetConfig(Config::kChn0_M_DstFrmRate, int2str(now_f));
            }

            thisini->SaveConfig();

            for(int i = 0;i<VencStreamCnt;i++){
                crtsps_closestream(g_stream[i]);
            }
            himppmaster->VencModuleDestroy(0,VencStreamCnt);
            himppmaster->SnapModuleDestroy(0);
            himppmaster->StopViModule(0,0);

            SIZE_S stVencSize = {str2int(thisini->GetConfig(Config::kChn0_M_VENC_W)),
                                 str2int(thisini->GetConfig(Config::kChn0_M_VENC_H))};
            SIZE_S stMinorSize = {str2int(thisini->GetConfig(Config::kChn0_S0_VENC_W)),
                                  str2int(thisini->GetConfig(Config::kChn0_S0_VENC_H))};
            SIZE_S stSnapSize = {str2int(thisini->GetConfig(Config::kChn0_Snap_W)),
                                 str2int(thisini->GetConfig(Config::kChn0_Snap_H))};
            HI_U32 u32Gop[3] = {str2int(thisini->GetConfig(Config::kChn0_M_Gop)),
                                str2int(thisini->GetConfig(Config::kChn0_S0_Gop)),
                                str2int(thisini->GetConfig(Config::kChn2_S0_Gop))};
            HI_U32 u32Profile[3] = {str2int(thisini->GetConfig(Config::kChn0_M_Profile)),
                                    str2int(thisini->GetConfig(Config::kChn0_S0_Profile)),
                                    str2int(thisini->GetConfig(Config::kChn2_S0_Profile))};
            HI_U32 u32BitRate[3] = {str2int(thisini->GetConfig(Config::kChn0_M_BitRate)),
                                    str2int(thisini->GetConfig(Config::kChn0_S0_BitRate)),
                                    str2int(thisini->GetConfig(Config::kChn2_S0_BitRate))};
            HI_U32 u32SrcFrmRate= str2int(thisini->GetConfig(Config::kChn0_SrcFrmRate));
            u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn0_M_DstFrmRate));
            u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn0_S0_DstFrmRate));
            PAYLOAD_TYPE_E enType[3] = {(PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn0_M_VENC_Type)),
                                        (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn0_S0_VENC_Type)),
                                        (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn2_S0_VENC_Type))};
            SAMPLE_RC_E enRcMode[3] = {(SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn0_M_RcMode)),
                                       (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn0_S0_RcMode)),
                                       (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn2_S0_RcMode))};
            HI_U32 u32MinQp[3] = {str2int(thisini->GetConfig(Config::kChn0_M_MIN_QP)),
                                  str2int(thisini->GetConfig(Config::kChn0_S0_MIN_QP)),
                                  str2int(thisini->GetConfig(Config::kChn2_S0_MIN_QP))};
            HI_U32 u32MaxQp[3] = {str2int(thisini->GetConfig(Config::kChn0_M_MAX_QP)),
                                  str2int(thisini->GetConfig(Config::kChn0_S0_MAX_QP)),
                                  str2int(thisini->GetConfig(Config::kChn2_S0_MAX_QP))};
            HI_U32 u32MinIProp[3] = {str2int(thisini->GetConfig(Config::kChn0_M_MINI_PROP)),
                                     str2int(thisini->GetConfig(Config::kChn0_S0_MINI_PROP)),
                                     str2int(thisini->GetConfig(Config::kChn2_S0_MINI_PROP))};
            HI_U32 u32MaxIProp[3] = {str2int(thisini->GetConfig(Config::kChn0_M_MAXI_PROP)),
                                     str2int(thisini->GetConfig(Config::kChn0_S0_MAXI_PROP)),
                                     str2int(thisini->GetConfig(Config::kChn2_S0_MAXI_PROP))};
            HI_U32 u32MinIQp[3] = {str2int(thisini->GetConfig(Config::kChn0_M_MinI_Qp)),
                                   str2int(thisini->GetConfig(Config::kChn0_S0_MinI_Qp)),
                                   str2int(thisini->GetConfig(Config::kChn2_S0_MinI_Qp))};
            HI_U32 u32IQp[3]   = {str2int(thisini->GetConfig(Config::kChn0_M_I_QP)),
                                  str2int(thisini->GetConfig(Config::kChn0_S0_I_QP)),
                                  str2int(thisini->GetConfig(Config::kChn2_S0_I_QP))};
            HI_U32 u32PQp[3]   = {str2int(thisini->GetConfig(Config::kChn0_M_P_QP)),
                                  str2int(thisini->GetConfig(Config::kChn0_S0_P_QP)),
                                  str2int(thisini->GetConfig(Config::kChn2_S0_P_QP))};
            HI_U32 u32BQp[3]   = {str2int(thisini->GetConfig(Config::kChn0_M_B_QP)),
                                  str2int(thisini->GetConfig(Config::kChn0_S0_B_QP)),
                                  str2int(thisini->GetConfig(Config::kChn2_S0_B_QP))};

            if(vi_crop_enable == 1){
                HI_U32 u32X = str2int(thisini->GetConfig(Config::kChn0_VI_X));
                HI_U32 u32Y = str2int(thisini->GetConfig(Config::kChn0_VI_Y));
                HI_U32 u32W = str2int(thisini->GetConfig(Config::kChn0_VI_Width));
                HI_U32 u32H = str2int(thisini->GetConfig(Config::kChn0_VI_Height));
                if((str2int(thisini->GetConfig(Config::kChn0_M_VENC_W)) > u32W) ||
                        (str2int(thisini->GetConfig(Config::kChn0_M_VENC_H)) > u32H))
                    stVencSize = {u32W, u32H};
                if((str2int(thisini->GetConfig(Config::kChn0_S0_VENC_W)) > u32W) ||
                        (str2int(thisini->GetConfig(Config::kChn0_S0_VENC_H)) > u32H))
                    stMinorSize = {u32W, u32H};
                if((u32X + u32W <= now_w)&&(u32Y + u32H <= height)){
                    stCapRect = {u32X, u32Y, u32W, u32H};
                    if(venc_SAME_INPUT == 1){
                        thisini->SetConfig(Config::kChn0_M_VENC_W, int2str(u32W));
                        thisini->SetConfig(Config::kChn0_M_VENC_H, int2str(u32H));
                        stVencSize = {u32W, u32H};
                    }
                }
            }
            himppmaster->ViModuleInit(SAMPLE_VI_MODE, stCapRect,input_type);
            himppmaster->VencModuleInit(stVencSize, stMinorSize, stMinorSize, stSnapSize,enType, enRcMode, u32Profile,
                                        u32Gop, u32BitRate, u32SrcFrmRate, u32DstFrameRate,
                                        u32MinQp, u32MaxQp, u32MinIQp, u32IQp, u32PQp, u32BQp, u32MinIProp, u32MaxIProp);
            AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));
            int param[2] = { 1, enAiSampleRate };
            coderFormat = (AuEncoderFormat)str2int(thisini->GetConfig(Config::kAiCoderFormat));
            if(coderFormat == 0){
                g_stream[0] = crtsps_openstream( v_type[0],AUDIO_AAC,"/0",param);
                g_stream[1] = crtsps_openstream( v_type[1],AUDIO_AAC,"/1",param);
            }else{
                g_stream[0] = crtsps_openstream( v_type[0],AUDIO_G711A,"/0",param);
                g_stream[1] = crtsps_openstream( v_type[1],AUDIO_G711A,"/1",param);
            }
            vi_flag[0] = 1;
        }

        // 有无信号
        if(ms->video_width == 0 || ms->video_height == 0){
            if(counter >= 1) //连续检测到15fps无信号，才启用用户图片插入
                himppmaster->EnableUserPic(0);
            else
                counter++;
            now_w = 0;
            now_h = 0;
            vi_flag[0] = 0;
            if((thisini->GetConfig(Config::kChn0_VI_W) != "0") ||
                    (thisini->GetConfig(Config::kChn0_VI_H) != "0")){
                thisini->SetConfig(Config::kChn0_VI_W, "0");
                thisini->SetConfig(Config::kChn0_VI_H, "0");
                thisini->SaveConfig();
            }
#endif
        }
        else{
            counter = 0;
            himppmaster->DisableUserPic(0);
        }

    }

    close(sock_fd);
    return NULL;
}



#endif

string ReadSthFile(const char *filepath)
{
    ifstream in_file(filepath);
    string result = "null";

    if(in_file && in_file.is_open()){
        ostringstream tmp;
        tmp << in_file.rdbuf();

        if(tmp.str()[tmp.str().size() - 1] == '\n')
            result = std::string(tmp.str().c_str(), tmp.str().length() - 1); //去掉字符串最后的 '/n'
        else
            result = tmp.str();
    }

    return result;
}

uint GetSystemInfo(char *buf_send, bool isOldFormat = true)
{
    char localip[20] = "";
    char macaddr[20] = "";
    uint delay = 0;
    // 读取版本号
    string ver = ReadSthFile("/version/version");

    // 读取uuid
    string uuid = ReadSthFile("/uuid/uuid");

    // 读取node name
    string name = ReadSthFile("/data/in_node_name");

    // 获取本地ip地址
    NetworkInfo *netinfo = new NetworkInfo();
    if( !netinfo->GetNetworkInfo(ETH0, NetworkInfo::kADDR, localip) ){
        COMMON_PRT("Get ip failed!\n");
        sprintf(localip, "%s", "get_ip_error");
    }
    if( !netinfo->GetNetworkInfo(ETH0, NetworkInfo::kHWADDR, macaddr) ){
        COMMON_PRT("Get mac failed!");
        sprintf(macaddr, "%s", "get_mac_error");
    }
    string timer( localip );
    // 取得ip段的最后一位
    timer = timer.c_str() + timer.find(".", timer.find(".", timer.find(".") + 1) + 1) + strlen(".");
    // 根据ip地址计算回复延迟
    delay = atoi(timer.c_str()) * 1000; //COMMON_PRT("delay = %d us\n", delay);
    usleep(delay);

    if(isOldFormat)
        sprintf(buf_send, "BKIP:%s,%s", localip, ver.c_str());
    else{
        sprintf(buf_send, "{\n\"node_model\":\"%s\",\n\"node_ip\":\"%s\",\n\"node_mac\":\"%s\",\n\"node_name\":\"%s\",\n\"version\":%s,\n\"uuid\":\"%s\"\n}", NODE_MODEL, localip, macaddr, name.c_str(), ver.c_str(), uuid.c_str());
    }

    return delay;
}

/*
 * 功能： 启动广播监听，并回复ip和版本号，扫描和回复格式兼容hi3536输出板子的扫描。
 * 回复 BKIP:xx.xx.xx.xx,<A>ver:x.x.x.x,rtsp://ip/0
 * 输入： void *arg：指定监听端口
 * 返回： 无
 */

void *udpBroadCastProc(void *arg)
{
    int  len = 0;
    uint port = *(uint*)arg;
    char buf[MAXDATA] = "";
    COMMON_PRT("starting broadcast listened at port: %d\n", port);

    UDPSocket *broadsocket = new UDPSocket();
    broadsocket->CreateUDPServer(port, false); //设为阻塞
    printf("broadcaset mcsocket creage ok\n");
#if 0
    broadsocket->AddBroadCast(ETH0); //加入eth0网卡的广播地址
#else
    broadsocket->AddBroadCast(); //使用255.255.255.255广播地址
    broadsocket->BindLocalAddr(ETH0);
#endif

    while(1){
        memset(buf, '\0', sizeof(buf));
        if( (len = broadsocket->RecvFrom(buf, MAXDATA)) > 0){
            buf[len] = '\0'; //COMMON_PRT("recv data: %s\n", buf);
            // printf("*************************1\n");
            char buf_send[128] = "";
            uint delay = 0;
            vector<string> cmdlist;

            SplitString(buf, cmdlist, ":");
            if(cmdlist[0] == "GTIP"){
                delay = GetSystemInfo(buf_send);
            }
            else if(cmdlist[0] == "FIND_NODE_UNI" ||
                    cmdlist[0] == "FIND_NODE_BOARD"){ // JSON格式回复
                delay = GetSystemInfo(buf_send, false);
            }
            else{
                COMMON_PRT("Scan instruction [%s] error.", cmdlist[0].c_str());
                continue;
            }

            if(delay == 0){
                sprintf(buf_send, "%s", "[ERROR_CODE_00]");
                COMMON_PRT("get system info failed!");
            }
            else{
                usleep(delay);
            }
            COMMON_PRT("send data: %s", buf_send);

            struct sockaddr_in clientaddr = broadsocket->GetClientSockaddr();
            if(cmdlist.size() == 1){ // NOTE: UDP回声回复

                for (uint i = 0; i < 3; i++){
                    if(cmdlist[0] == "GTIP" ||
                            cmdlist[0] == "FIND_NODE_UNI"){
                        broadsocket->SendTo(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr),
                                            ntohs(((struct sockaddr_in*)&clientaddr)->sin_port),
                                            buf_send,
                                            strlen(buf_send));

                        //    printf("***************1\n");

                    }
                    else if(cmdlist[0] == "FIND_NODE_BOARD"){
                        broadsocket->SendTo("255.255.255.255",
                                            ntohs(((struct sockaddr_in*)&clientaddr)->sin_port),
                                            buf_send,
                                            strlen(buf_send));
                        printf("***************2\n");
                    }

                }
            }
            else if(cmdlist.size() == 2){ //NOTE: 回复给指定端口
                //  printf("***************3\n");
                for (uint i = 0; i < 3; i++){

                    if(cmdlist[0] == "GTIP" ||
                            cmdlist[0] == "FIND_NODE_UNI"){
                        broadsocket->SendTo(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr),
                                            atoi(cmdlist[1].c_str()),
                                buf_send,
                                strlen(buf_send));
                    }
                    else if(cmdlist[0] == "FIND_NODE_BOARD"){
                        broadsocket->SendTo("255.255.255.255",
                                            atoi(cmdlist[1].c_str()),
                                buf_send,
                                strlen(buf_send));
                    }

                    usleep(delay/10);
                }
            }
            else{
                COMMON_PRT("The number of scan instruction [%d] error.", cmdlist.size());
                continue;
            }
        }
    }

    delete broadsocket;

    return (void*)1;
}

void* udpServerProc(void *arg)
{
    int  len = 0;
    uint port = *(uint*)arg;
    char data[MAXDATA]  = "";
    COMMON_PRT("starting udp server at port: %d\n", port);

    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false);
    printf("udpsv mcsocket creage ok\n");

    while( 1 ){
        memset(data, '\0', sizeof(data));
        if( (len = udpsocket->RecvFrom(data, MAXDATA)) > 0){
            COMMON_PRT("recv udp data: %s\n", data);
            struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
            sprintf(gClientIP, "%s", inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
            gClientPort = ntohs(((struct sockaddr_in*)&clientaddr)->sin_port);

            if(!strcmp(data, "RSET:")){ //重启指令
                udpsocket->EchoTo("(OK)", strlen("(OK)"));
                system("reboot\n");
                break;
            }
            else
            {
                JsonParse* jsonparse = new JsonParse(data);
                if(jsonparse->CheckJsonFormat()){
                    jsonparse->IterationParseStart();
                    printf("jsonparse\n");
                }
                else{
                    printf("Illegal format of JSON\n");
                }
            }
        }
        else
            break;
    }

    delete udpsocket;
    return (void*)0;
}

void *httpServerhandler(void*)
{
    // start http server
    auto thisini = Singleton<Config>::getInstance();
    auto http_server = Singleton<HttpServer>::getInstance();
    http_server->Init( thisini->GetConfig(Config::kWebPort) );
    // add http handler
    http_server->AddHandler("/reboot",       ResetHandler);
    http_server->AddHandler("/up_progress",  UpdataHandler);
    http_server->AddHandler("/transmit",     TransmitHandler);
    http_server->AddHandler("/set_osd",      SetosdHandler);
    http_server->AddHandler("/set_sys",      SetnetHandler);
    http_server->AddHandler("/set_adv",      SetadvHandler);
    http_server->AddHandler("/set_output",   SetpreviewHandler);

    //http_server->AddHandler("/get_status",       GetperplanHandler);
    http_server->AddHandler("/get_sys",      GetnetsettingHandler);
    //http_server->AddHandler("/get_input",  GetaudiosettingHandler);
    http_server->Start();

    return (void*)1;
}




void *snap0ServerProc(void *arg)
{
    int  len  = 0;
    HI_U32  snapwho = 0;
    uint port = *(uint*)arg;
    char data[100] = "";
    vector<string> content;

    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    int ret =  udpsocket->CreateUDPServer(port, false); //   venc  not userget  vencchn 12   because   cat /pore/umap/venc


    while(  snap_runlag[snapwho] == true){

#if 1
        memset(data, '\0', sizeof(data));
        content.clear();
        if( (len = udpsocket->RecvFrom(data, 100)) > 0){
            // printf("snaploop port=%d\n",port);
            if ( strstr(data, "JPEG:") ){
                //  snaploop[snapwho] = HI_TRUE;
                // snapPth[snapwho] = HI_TRUE;
                //printf("RecvFrom----=%s\n",data);
                if( strlen(data) == strlen("JPEG:")){
                    struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
                    content.push_back(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
                    content.push_back(int2str(ntohs(((struct sockaddr_in*)&clientaddr)->sin_port)));

                }
                else
                    SplitString(data + 5, content, ",");
                //   COMMON_PRT("--2--ip: %s, port: %s----0\n", content[0].c_str(), content[1].c_str());


                HI_U32 SnapChn = snapwho;
                //himppmaster->snap_running[0] =  HI_TRUE;
                snap_loop[SnapChn] = HI_TRUE;
                himppmaster->GetVencSnap(SnapChn, udpsocket, content[0].c_str(), (uint)str2int(content[1]), veSnap[0].snapLeng);

            }
            else if(!strncmp(data,THREAD_CANCEL,strlen(THREAD_CANCEL))) {
                printf("UMP set snap port ,cancel snap thread\n");
                break;
            }


        }
        else
        {

            snap_loop[snapwho] =HI_FALSE;
            usleep(2000);
        }
#endif
    }


    delete udpsocket;

    return 0;
}


void *snap1ServerProc(void *arg)
{
    int  len  = 0;
    HI_U32  snapwho = 1;
    uint port = *(uint*)arg;
    char data[100] = "";
    vector<string> content;

    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    int ret =  udpsocket->CreateUDPServer(port, false); //   venc  not userget  vencchn 12   because   cat /pore/umap/venc

    if(ret == -1)
    {
        printf("+++++error+++++++++++++CreateUDPServer\n");
    }

    while(  snap_runlag[snapwho] == true){
        memset(data, '\0', sizeof(data));
        content.clear();
        if( (len = udpsocket->RecvFrom(data, 100)) > 0){
            //  printf("snaploop port=%d\n",port);
            if ( strstr(data, "JPEG:") ){


                if( strlen(data) == strlen("JPEG:")){
                    struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
                    content.push_back(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
                    content.push_back(int2str(ntohs(((struct sockaddr_in*)&clientaddr)->sin_port)));

                }
                else
                    SplitString(data + 5, content, ",");



                //   COMMON_PRT(" vesnapdelay=%d\n",  veSnap[snapwho].snapDelay);


                HI_U32 SnapChn = snapwho;
                //himppmaster->snap_running[0] =  HI_TRUE;
                snap_loop[SnapChn] = HI_TRUE;
                himppmaster->GetVencSnap(SnapChn, udpsocket, content[0].c_str(), (uint)str2int(content[1]), veSnap[SnapChn].snapLeng);

            }
            else if(!strncmp(data,THREAD_CANCEL,strlen(THREAD_CANCEL))) {
                printf("UMP set snap port ,cancel snap thread\n");
                break;
            }


        }
        else
        {

            snap_loop[snapwho] =HI_FALSE;
            usleep(2000);
        }

    }


    delete udpsocket;

    return 0;
}
void *snap2ServerProc(void *arg)
{
    int  len  = 0,snapwho = 2;
    uint port = *(uint*)arg;
    char data[100] = "";
    vector<string> content;

    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false); //设为阻塞

    while( snap_runlag[snapwho] == true ){
        memset(data, '\0', sizeof(data));
        content.clear();
        if( (len = udpsocket->RecvFrom(data, 100)) > 0){

            if ( strstr(data, "JPEG:") ){
                snaploop[snapwho] = HI_TRUE;
                //  printf("RecvFrom----=%s\n",data);
                if( strlen(data) == strlen("JPEG:")){
                    struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
                    content.push_back(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
                    content.push_back(int2str(ntohs(((struct sockaddr_in*)&clientaddr)->sin_port)));

                }
                else
                    SplitString(data + 5, content, ",");
                //       COMMON_PRT("--2--ip: %s, port: %s\n", content[0].c_str(), content[1].c_str());

#if 1
                HI_U32 SnapChn = snapwho;
                //  himppmaster->snap_running[0] =  HI_TRUE;
                snap_loop[snapwho] = HI_TRUE;
                himppmaster->GetVencSnap(SnapChn, udpsocket, content[0].c_str(), (uint)str2int(content[1]), veSnap[0].snapLeng);
                //     COMMON_PRT("send snap over\n");
#endif
            }
            else if(!strncmp(data,THREAD_CANCEL,strlen(THREAD_CANCEL))) {
                printf("UMP set snap port ,cancel snap thread\n");
                break;
            }


        }
        else
        {
            snap_loop[snapwho] =HI_FALSE;
            usleep(2000);

            break;
        }
    }


    delete udpsocket;

    return 0;
}
void *snap3ServerProc(void *arg)
{
    int  len  = 0,snapwho = 3;
    uint port = *(uint*)arg;
    char data[100] = "";
    vector<string> content;

    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false); //设为阻塞

    while( snap_runlag[snapwho] == true ){
        memset(data, '\0', sizeof(data));
        content.clear();
        if( (len = udpsocket->RecvFrom(data, 100)) > 0){

            if ( strstr(data, "JPEG:") ){
                snaploop[snapwho] = HI_TRUE;

                if( strlen(data) == strlen("JPEG:")){
                    struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
                    content.push_back(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
                    content.push_back(int2str(ntohs(((struct sockaddr_in*)&clientaddr)->sin_port)));

                }
                else
                    SplitString(data + 5, content, ",");
                //        COMMON_PRT("--2--ip: %s, port: %s\n", content[0].c_str(), content[1].c_str());


                //  printf("glFrameID----=%d\n",glFrameID);

#if 1
                HI_U32 SnapChn = snapwho;
                //  himppmaster->snap_running[0] =  HI_TRUE;
                snap_loop[snapwho] =HI_TRUE;
                himppmaster->GetVencSnap(SnapChn, udpsocket, content[0].c_str(), (uint)str2int(content[1]), veSnap[0].snapLeng);
                //  COMMON_PRT("send snap over\n");
#endif
            }
            else if(!strncmp(data,THREAD_CANCEL,strlen(THREAD_CANCEL))) {
                printf("UMP set snap port ,cancel snap thread\n");
                break;
            }


        }
        else
        {
            snap_loop[snapwho] =HI_FALSE;

            usleep(2000);

            break;
        }
    }


    delete udpsocket;

    return 0;
}


void* multiSnapProc(void *arg)
{
    int vi  = 0;
    int  last_glFrameID  = 0;


    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    int socket1 = udpsocket->CreateUDPServer(0, false); //设为阻塞

    setsockopt(socket1, IPPROTO_IP, IP_TTL, (char *)&ttl, sizeof(ttl));


    while(  mult_snap_runlag[vi] == true){


        if(mult_snap[vi] == true)
        {

            if(veSnap[vi].Snapmcast_enable){
                snap_loop[vi] = HI_TRUE;
                himppmaster->GetVencRateSnap(8+vi, udpsocket, veSnap[vi].Snapmcast_ip, veSnap[vi].Snapmcast_port,veSnap[vi].snapLeng, 0, 0);
                mult_snap[vi] = false;
            }
        }
        else{
            //  snap_loop[vi] = HI_FALSE;

            sleep(1);

        }
    }

    delete udpsocket;

    printf("thread mule_snap =%d exit\n",8+vi);
    return 0;
}

void* multiSnapProc1(void *arg)
{
    int vi = 1;




    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    int socket1 = udpsocket->CreateUDPServer(0, false); //设为阻塞

    setsockopt(socket1, IPPROTO_IP, IP_TTL, (char *)&ttl, sizeof(ttl));

    while(  mult_snap_runlag[vi] == true){


        if(mult_snap[vi] == true)
        {

            if(veSnap[vi].Snapmcast_enable){



                snap_loop[vi] = HI_TRUE;
                himppmaster->GetVencRateSnap(8+vi, udpsocket, veSnap[vi].Snapmcast_ip, veSnap[vi].Snapmcast_port,veSnap[vi].snapLeng, 0, 0);


                mult_snap[vi] = false;

            }
        }
        else{
            //  snap_loop[vi] = HI_FALSE;

            sleep(1);

        }
    }

    delete udpsocket;
    return 0;
}
void* multiSnapProc2(void *arg)
{
    int vi =2;


    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    int socket1 = udpsocket->CreateUDPServer(0, false); //设为阻塞

    setsockopt(socket1, IPPROTO_IP, IP_TTL, (char *)&ttl, sizeof(ttl));

    while(  mult_snap_runlag[vi] == true){


        if(mult_snap[vi] == true)
        {

            if(veSnap[vi].Snapmcast_enable){



                snap_loop[vi] = HI_TRUE;
                himppmaster->GetVencRateSnap(8+vi, udpsocket, veSnap[vi].Snapmcast_ip, veSnap[vi].Snapmcast_port,veSnap[vi].snapLeng, 0, 0);


                mult_snap[vi] = false;

            }
        }
        else{
            //  snap_loop[vi] = HI_FALSE;

            sleep(1);

        }
    }

    delete udpsocket;
    return 0;
}

void* multiSnapProc3(void *arg)
{
    int vi = 3;
    int  last_glFrameID  = 0;
    //    if(vi == 1)
    //        vi = 0;
    //    else if(vi == 3)
    //        vi = 1;
    //    else if(vi == 5)
    //        vi = 2;
    //    else if(vi == 7)
    //        vi = 3;
    //    else
    //        return 0;
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    UDPSocket *udpsocket = new UDPSocket();
    int socket1 = udpsocket->CreateUDPServer(0, false); //设为阻塞

    setsockopt(socket1, IPPROTO_IP, IP_TTL, (char *)&ttl, sizeof(ttl));


    while(  mult_snap_runlag[vi] == true){


        if(mult_snap[vi] == true)
        {

            if(veSnap[vi].Snapmcast_enable){



                snap_loop[vi] = HI_TRUE;
                himppmaster->GetVencRateSnap(8+vi, udpsocket, veSnap[vi].Snapmcast_ip, veSnap[vi].Snapmcast_port,veSnap[vi].snapLeng, 0, 0);


                mult_snap[vi] = false;

            }
        }
        else{
            //  snap_loop[vi] = HI_FALSE;

            sleep(1);

        }
    }

    delete udpsocket;
    return 0;
}


#if 0



void *snap3ServerProc(void *arg)
{
    int  len  = 0;
    uint port = *(uint*)arg;
    char data[100] = "";
    vector<string> content;

    auto himppmaster = Singleton<HimppMaster>::getInstance();
    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false); //设为阻塞

    while( 1 ){
        memset(data, '\0', sizeof(data));
        content.clear();
        if( (len = udpsocket->RecvFrom(data, 100)) > 0){
            if ( strstr(data, "JPEG:") ){
                if( strlen(data) == strlen("JPEG:") ){
                    struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
                    content.push_back(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
                    content.push_back(int2str(ntohs(((struct sockaddr_in*)&clientaddr)->sin_port)));
                }
                else
                    SplitString(data + 5, content, ",");

                COMMON_PRT("ip: %s, port: %s\n", content[0].c_str(), content[1].c_str());
                //                g_isSendit = true;
                HI_U32 SnapChn = 11;
                himppmaster->GetVencSnap(SnapChn, udpsocket, content[0].c_str(), (uint)str2int(content[1]), veSnap[3].snapLeng);
                COMMON_PRT("send snap over\n");
            }
            else if(!strncmp(data,THREAD_CANCEL,strlen(THREAD_CANCEL))) {
                printf("UMP set snap port ,cancel snap thread\n");
                break;
            }
        }
        else
            break;
    }

    delete udpsocket;

    return 0;
}
#endif



bool SynStateDetect(bool sync_ok, const HI_U32 sync_offline, const HI_S32 clock_swing_sum)
{
    /** NOTE: 根读取VCXO上一秒的设定值的算术和，判断是否锁定
     *正常情况下，VXCO只有-512，-20，20，511四个值，每秒统计60次
     **/
    //正负摆动 < 10
    if( (sync_offline < 1200) &&
            (clock_swing_sum > (-20 * 10) && clock_swing_sum < (20 * 10)) &&
            !sync_ok){
        // NOTE: 新建同步成功的文件,该文件是提供给netserver获取节点同步状态用的
        FILE* sync_fd = NULL;
        sync_fd = fopen("/tmp/sync", "w");
        fprintf(sync_fd, "%d", 1);
        fflush(sync_fd);
        fclose(sync_fd);

        sync_ok = true;
    }
    // 正负一半以上的最大摆幅
    else if( (sync_offline >= 1200) ||
             ((clock_swing_sum < (-512 * 30) || clock_swing_sum > (511 * 30)) &&
              sync_ok)){
        // NOTE: 新建同步成功的文件,该文件是提供给netserver获取节点同步状态用的
        FILE* sync_fd = NULL;
        sync_fd = fopen("/tmp/sync", "w");
        fprintf(sync_fd, "%d", 0);
        fflush(sync_fd);
        fclose(sync_fd);

        sync_ok = false;
    }
    else if (access("/tmp/sync", F_OK) == -1){ // 如果指定的文件不存在
        sync_ok = false;
    }

    return sync_ok;
}


#if 0

//专门用于获取网络时间戳的线程
HI_VOID* GetTimeTick(HI_VOID *arg){
    uint sync_offline = 0;
    int last_time_offset = 0;
    system("rm /tmp/sync -rf"); // NOTE: 删除同步成功的文件,该文件是提供给netserver获取节点同步状态用的
    sleep(1);

    HI_U32 m1 = 1, m2 = 1;
    HI_S32 last_clock_swing_sum = 0;
    bool master_slave = false;
    HI_S32 tick_delta = 0;


    //打开中断驱动通信文件
    char irqfile[24] = "";
    sprintf(irqfile, "/dev/interrupt%d", 2);
    KoData kodata;

    HI_S32 irqfd = open(irqfile, 0); //阻塞读取； NOTE: 非阻塞 O_RDONLY|O_NONBLOCK
    if (irqfd < 0){
        irqfd = -1;
        printf("open %s error: %s.", irqfile, strerror(errno));
    }
    //共享内存的创建
    int shmid = -1;//接收句柄
    shmid = shmget(IPC_KEY, SHARE_MAX, IPC_CREAT | 0664);
    if(shmid < 0){
        perror("shmget errnr");
    }
    void *shm_start = shmat(shmid,NULL,0 );
    if(shm_start == (void *) -1){
        perror("shmat error");
    }
    TransMit msg = {50000, 0, 1, 1};
    memcpy(shm_start, &msg, sizeof(msg));


    while( 1 ){

        //Step 2
        //阻塞VS的中断，这个不容易丢掉
        if(irqfd >= 0)
            read(irqfd, &kodata, sizeof(kodata));

        //Step 3
        //看是否锁定，当锁定后，才能进行比对工作，锁定前都是假象
        if(!master_slave){
            //*** 最核心的操作，内存复制 ***
            memcpy(&msg, shm_start, sizeof(msg));
            //            printf("%d | %d | %d | %d\n", msg.clock_swing_sum, msg.tick_delta, msg.m1, msg.m2);

            int clock_swing_sum = msg.clock_swing_sum;
            // NOTE: 判断同步组播信号丢失
            if(clock_swing_sum == last_clock_swing_sum){
                if(sync_offline < 1200)
                    ++sync_offline;
            }
            else{
                sync_offline = 0;
                last_clock_swing_sum = clock_swing_sum;
            }
            // 检测是否同步的状态
            sync_ok = SynStateDetect(sync_ok, sync_offline, clock_swing_sum);

            if(sync_ok){
                tick_delta = msg.tick_delta;
                if(msg.m1)
                    m1 = msg.m1;
                if(msg.m2)
                    m2 = msg.m2;
            }

        }
        //Step 7: 本板全局时间戳 = 本板时间戳（驱动汇报） + 时间差（TickMaster汇报）
        glFrameID = (((int)(kodata.frame_num / m1) + tick_delta + 60) * m2) % 60;


        //printf("%d = %d + %d + %d\n",glFrameID, (int)kodata.frame_num, glLocalTickDelta, diff_index);
        Hi_SetReg(0x130d0710, glFrameID);//将FrameID写到寄存器暂存，interrupt_vi.ko中读
    }

    //delete udpsocket;

    if(irqfd >= 0)
        close(irqfd);
    shmdt(shm_start);

    return (HI_VOID*)1;
}
#endif

#if 1

HI_VOID* GetTimeTick(HI_VOID *arg){
    uint sync_offline = 0;
    int last_time_offset = 0;
    system("rm /tmp/sync -rf"); // NOTE: 删除同步成功的文件,该文件是提供给netserver获取节点同步状态用的
    sleep(1);

    HI_U32 m1 = 1, m2 = 1;
    HI_S32 last_clock_swing_sum = 0;
    bool master_slave = false;
    HI_S32 tick_delta = 0;


    //打开中断驱动通信文件
    char irqfile[24] = "";
    sprintf(irqfile, "/dev/interrupt%d", 2);
    KoData kodata;

    HI_S32 irqfd = open(irqfile, 0); //阻塞读取； NOTE: 非阻塞 O_RDONLY|O_NONBLOCK
    if (irqfd < 0){
        irqfd = -1;
        printf("open %s error: %s.", irqfile, strerror(errno));
    }
    //共享内存的创建
    int shmid = -1;//接收句柄
    shmid = shmget(IPC_KEY, SHARE_MAX, IPC_CREAT | 0664);
    if(shmid < 0){
        perror("shmget errnr");
    }
    void *shm_start = shmat(shmid,NULL,0 );
    if(shm_start == (void *) -1){
        perror("shmat error");
    }
    TransMit msg = {50000, 0, 1, 1};
    memcpy(shm_start, &msg, sizeof(msg));


    while( 1 ){

        //Step 2
        //阻塞VS的中断，这个不容易丢掉
        if(irqfd >= 0)
            read(irqfd, &kodata, sizeof(kodata));

        //Step 3
        //看是否锁定，当锁定后，才能进行比对工作，锁定前都是假象
        if(!master_slave){
            //*** 最核心的操作，内存复制 ***
            memcpy(&msg, shm_start, sizeof(msg));
            //            printf("%d | %d | %d | %d\n", msg.clock_swing_sum, msg.tick_delta, msg.m1, msg.m2);

            int clock_swing_sum = msg.clock_swing_sum;
            // NOTE: 判断同步组播信号丢失
            if(clock_swing_sum == last_clock_swing_sum){
                if(sync_offline < 1200)
                    ++sync_offline;
            }
            else{
                sync_offline = 0;
                last_clock_swing_sum = clock_swing_sum;
            }
            // 检测是否同步的状态
            sync_ok = SynStateDetect(sync_ok, sync_offline, clock_swing_sum);

            if(sync_ok){
                tick_delta = msg.tick_delta;
                if(msg.m1)
                    m1 = msg.m1;
                if(msg.m2)
                    m2 = msg.m2;
            }

        }
        else{
            sync_ok = true;
            tick_delta = 0;
            m1 = 1; m2 = 1;
        }
        //Step 7: 本板全局时间戳 = 本板时间戳（驱动汇报） + 时间差（TickMaster汇报）
        glFrameID = (((int)(kodata.frame_num / m1) + tick_delta + 60) * m2) % 60;


        //printf("%d = %d + %d + %d\n",glFrameID, (int)kodata.frame_num, glLocalTickDelta, diff_index);
        Hi_SetReg(0x130d0710, glFrameID);//将FrameID写到寄存器暂存，interrupt_vi.ko中读
    }

    //delete udpsocket;

    if(irqfd >= 0)
        close(irqfd);
    shmdt(shm_start);

    return (HI_VOID*)1;
}




#endif





void* input1proc(void *arg)
{
    uint in = *(uint *)arg;
    printf(" input number:%d\n",in);
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->StartLoop(VencStreamCnt*in, VencStreamCnt*(in+1), in);    //1-VENC
    return NULL;
}

void* input2proc(void *)
{
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->StartLoop(Venc1StreamCnt, Venc2StreamCnt, 2);
    return NULL;
}

void* input3proc(void *)
{
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->StartLoop(Venc2StreamCnt, Venc3StreamCnt, 3);
    return NULL;
}




bool InitParamte()
{
    /*
     * 读取板子型号，确定输入HDMI数量,确定VI设备号，通道号
     *
    */
    auto thisini = Singleton<Config>::getInstance();
    char tickModule[24];
    kerrFrameNum = str2int(thisini->GetConfig(Config::KErrFrameNum));

    sprintf(tickModule,"%s",string(thisini->GetConfig(Config::kTickModule)).c_str());
    if(!strncmp(tickModule,"master",strlen("master"))) {
        gb_master = true;
    }
    else {
        gb_master = false;
    }

    HI_S32 i = 0;
    viNumber = 4;
    for(i = 0; i < viNumber; i++) {
        memset(&viParam[i],0,sizeof(PARAMETER));
        switch (i) {
        case 0:
            viParam[0].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn0_M_RcMode));
            viParam[0].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn0_S0_RcMode));
            viParam[0].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn0_M_VENC_Type));
            viParam[0].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn0_S0_VENC_Type));
            viParam[0].enViMode = SAMPLE_VI_MODE;
            viParam[0].input_type = 1;
            viParam[0].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_SW_Color));
            viParam[0].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_M_McastEnable));
            viParam[0].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_S0_McastEnable));
            if(viParam[0].mcast_enable[0]) {
                sprintf(viParam[0].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn0_M_McastIp)).c_str());
                viParam[0].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn0_M_McastPort).c_str());
            }
            if(viParam[0].mcast_enable[1]) {
                sprintf(viParam[0].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn0_S0_McastIp)).c_str());
                viParam[0].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn0_S0_McastPort).c_str());
            }
            viParam[0].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_Osd_Enable));
            veSnap[0].snapQuality= str2int(thisini->GetConfig(Config::kChn0_Snap_Quality));
            veSnap[0].snapPort = str2int( thisini->GetConfig(Config::kChn0_SnapPort).c_str());
            veSnap[0].snapLeng = str2int(thisini->GetConfig(Config::kChn0_SnapLen).c_str());
            veSnap[0].snapDelay = str2int(thisini->GetConfig(Config::kChn0_Snap_Delay).c_str());
            viParam[0].stMinorSize = {str2int(thisini->GetConfig(Config::kChn0_S0_VENC_W)),
                                      str2int(thisini->GetConfig(Config::kChn0_S0_VENC_H))};
            veSnap[0].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_Snap_McastEnable));
            sprintf(veSnap[0].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn0_Snap_McastIp)).c_str());
            veSnap[0].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn0_Snap_McastPort).c_str());
            veSnap[0].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn0_Snap_McastFreq).c_str());

            veSnap[0].snapSize = {str2int(thisini->GetConfig(Config::kChn0_Snap_W)),
                                  str2int(thisini->GetConfig(Config::kChn0_Snap_H))};
            viParam[0].stVencSize = {str2int(thisini->GetConfig(Config::kChn0_M_VENC_W)),
                                     str2int(thisini->GetConfig(Config::kChn0_M_VENC_H))};
            viParam[0].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn0_M_BitRate));
            viParam[0].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn0_S0_BitRate));
            viParam[0].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_B_QP));
            viParam[0].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_B_QP));
            viParam[0].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn0_M_DstFrmRate));
            viParam[0].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn0_S0_DstFrmRate));
            viParam[0].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn0_M_Gop));
            viParam[0].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn0_S0_Gop));
            viParam[0].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_VI_Crop_Enable));
            if(viParam[0].vi_crop_enable) {
                viParam[0].u32X = str2int(thisini->GetConfig(Config::kChn0_VI_X));
                viParam[0].u32Y = str2int(thisini->GetConfig(Config::kChn0_VI_Y));
                viParam[0].u32W = str2int(thisini->GetConfig(Config::kChn0_VI_Width));
                viParam[0].u32H = str2int(thisini->GetConfig(Config::kChn0_VI_Height));
            }
            viParam[0].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_I_QP));
            viParam[0].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_I_QP));
            viParam[0].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MAXI_PROP));
            viParam[0].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MAXI_PROP));
            viParam[0].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MAX_QP));
            viParam[0].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MAX_QP));
            viParam[0].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MINI_PROP));
            viParam[0].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MINI_PROP));
            viParam[0].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MinI_Qp));
            viParam[0].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MinI_Qp));
            viParam[0].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MIN_QP));
            viParam[0].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MIN_QP));
            viParam[0].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_P_QP));
            viParam[0].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_P_QP));
            viParam[0].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn0_M_Profile));
            viParam[0].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn0_S0_Profile));
            viParam[0].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn0_SrcFrmRate));
            viParam[0].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn0_VI_H));
            viParam[0].u32ViWidth = str2int(thisini->GetConfig(Config::kChn0_VI_W));
            viParam[0].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_M_VENC_SAME_INPUT));
            viParam[0].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_S0_VENC_SAME_INPUT));
            break;
        case 1:
            viParam[1].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn1_M_RcMode));
            viParam[1].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn1_S0_RcMode));
            viParam[1].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn1_M_VENC_Type));
            viParam[1].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn1_S0_VENC_Type));
            viParam[1].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_SW_Color));
            viParam[1].enViMode = SAMPLE_VI_MODE;
            viParam[1].input_type = 1;
            viParam[1].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_M_McastEnable));
            viParam[1].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_S0_McastEnable));
            if(viParam[1].mcast_enable[0]) {
                sprintf(viParam[0].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn1_M_McastIp)).c_str());
                viParam[1].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn1_M_McastPort).c_str());
            }
            if(viParam[1].mcast_enable[1]) {
                sprintf(viParam[1].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn1_S0_McastIp)).c_str());
                viParam[1].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn1_S0_McastPort).c_str());
            }
            viParam[1].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_Osd_Enable));
            veSnap[1].snapQuality = str2int(thisini->GetConfig(Config::kChn1_Snap_Quality));
            veSnap[1].snapLeng = str2int(thisini->GetConfig(Config::kChn1_SnapLen).c_str());
            veSnap[1].snapDelay = str2int(thisini->GetConfig(Config::kChn1_Snap_Delay).c_str());
            veSnap[1].snapPort = str2int( thisini->GetConfig(Config::kChn1_SnapPort).c_str());
            viParam[1].stMinorSize = {str2int(thisini->GetConfig(Config::kChn1_S0_VENC_W)),
                                      str2int(thisini->GetConfig(Config::kChn1_S0_VENC_H))};
            veSnap[1].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_Snap_McastEnable));
            sprintf(veSnap[1].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn1_Snap_McastIp)).c_str());
            veSnap[1].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn1_Snap_McastPort).c_str());
            veSnap[1].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn1_Snap_McastFreq).c_str());
            veSnap[1].snapSize = {str2int(thisini->GetConfig(Config::kChn1_Snap_W)),
                                  str2int(thisini->GetConfig(Config::kChn1_Snap_H))};
            viParam[1].stVencSize = {str2int(thisini->GetConfig(Config::kChn1_M_VENC_W)),
                                     str2int(thisini->GetConfig(Config::kChn1_M_VENC_H))};
            viParam[1].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn1_M_BitRate));
            viParam[1].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn1_S0_BitRate));
            viParam[1].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_B_QP));
            viParam[1].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_B_QP));
            viParam[1].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn1_M_DstFrmRate));
            viParam[1].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn1_S0_DstFrmRate));
            viParam[1].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn1_M_Gop));
            viParam[1].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn1_S0_Gop));
            viParam[1].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_VI_Crop_Enable));
            if(viParam[1].vi_crop_enable) {
                viParam[1].u32X = str2int(thisini->GetConfig(Config::kChn1_VI_X));
                viParam[1].u32Y = str2int(thisini->GetConfig(Config::kChn1_VI_Y));
                viParam[1].u32W = str2int(thisini->GetConfig(Config::kChn1_VI_Width));
                viParam[1].u32H = str2int(thisini->GetConfig(Config::kChn1_VI_Height));
            }
            viParam[1].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_I_QP));
            viParam[1].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_I_QP));
            viParam[1].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MAXI_PROP));
            viParam[1].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MAXI_PROP));
            viParam[1].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MAX_QP));
            viParam[1].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MAX_QP));
            viParam[1].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MINI_PROP));
            viParam[1].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MINI_PROP));
            viParam[1].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MinI_Qp));
            viParam[1].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MinI_Qp));
            viParam[1].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MIN_QP));
            viParam[1].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MIN_QP));
            viParam[1].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_P_QP));
            viParam[1].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_P_QP));
            viParam[1].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn1_M_Profile));
            viParam[1].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn1_S0_Profile));
            viParam[1].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn1_SrcFrmRate));
            viParam[1].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn1_VI_H));
            viParam[1].u32ViWidth = str2int(thisini->GetConfig(Config::kChn1_VI_W));
            viParam[1].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_M_VENC_SAME_INPUT));
            viParam[1].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_S0_VENC_SAME_INPUT));
            break;
        case 2:
            viParam[2].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn2_M_RcMode));
            viParam[2].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn2_S0_RcMode));
            viParam[2].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn2_M_VENC_Type));
            viParam[2].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn2_S0_VENC_Type));
            viParam[2].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_SW_Color));
            viParam[2].enViMode = SAMPLE_VI_MODE;
            viParam[2].input_type = 1;
            viParam[2].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_M_McastEnable));
            viParam[2].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_S0_McastEnable));
            if(viParam[2].mcast_enable[0]) {
                sprintf(viParam[2].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn2_M_McastIp)).c_str());
                viParam[2].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn2_M_McastPort).c_str());
            }
            if(viParam[2].mcast_enable[1]) {
                sprintf(viParam[2].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn2_S0_McastIp)).c_str());
                viParam[2].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn2_S0_McastPort).c_str());
            }
            viParam[2].osd_enable  = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_Osd_Enable));
            veSnap[2].snapQuality = str2int(thisini->GetConfig(Config::kChn2_Snap_Quality));
            veSnap[2].snapLeng = str2int(thisini->GetConfig(Config::kChn2_SnapLen).c_str());
            veSnap[2].snapDelay = str2int(thisini->GetConfig(Config::kChn2_Snap_Delay).c_str());
            veSnap[2].snapPort = str2int( thisini->GetConfig(Config::kChn2_SnapPort).c_str());
            viParam[2].stMinorSize = {str2int(thisini->GetConfig(Config::kChn2_S0_VENC_W)),
                                      str2int(thisini->GetConfig(Config::kChn2_S0_VENC_H))};
            veSnap[2].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_Snap_McastEnable));
            sprintf(veSnap[2].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn2_Snap_McastIp)).c_str());
            veSnap[2].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn2_Snap_McastPort).c_str());
            veSnap[2].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn2_Snap_McastFreq).c_str());
            veSnap[2].snapSize = {str2int(thisini->GetConfig(Config::kChn2_Snap_W)),
                                  str2int(thisini->GetConfig(Config::kChn2_Snap_H))};
            viParam[2].stVencSize = {str2int(thisini->GetConfig(Config::kChn2_M_VENC_W)),
                                     str2int(thisini->GetConfig(Config::kChn2_M_VENC_H))};
            viParam[2].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn2_M_BitRate));
            viParam[2].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn2_S0_BitRate));
            viParam[2].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_B_QP));
            viParam[2].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_B_QP));
            viParam[2].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn2_M_DstFrmRate));
            viParam[2].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn2_S0_DstFrmRate));
            viParam[2].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn2_M_Gop));
            viParam[2].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn2_S0_Gop));
            viParam[2].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_VI_Crop_Enable));
            if(viParam[2].vi_crop_enable) {
                viParam[2].u32X = str2int(thisini->GetConfig(Config::kChn2_VI_X));
                viParam[2].u32Y = str2int(thisini->GetConfig(Config::kChn2_VI_Y));
                viParam[2].u32W = str2int(thisini->GetConfig(Config::kChn2_VI_Width));
                viParam[2].u32H = str2int(thisini->GetConfig(Config::kChn2_VI_Height));
            }
            viParam[2].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_I_QP));
            viParam[2].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_I_QP));
            viParam[2].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MAXI_PROP));
            viParam[2].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MAXI_PROP));
            viParam[2].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MAX_QP));
            viParam[2].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MAX_QP));
            viParam[2].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MINI_PROP));
            viParam[2].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MINI_PROP));
            viParam[2].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MinI_Qp));
            viParam[2].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MinI_Qp));
            viParam[2].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MIN_QP));
            viParam[2].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MIN_QP));
            viParam[2].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_P_QP));
            viParam[2].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_P_QP));
            viParam[2].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn2_M_Profile));
            viParam[2].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn2_S0_Profile));
            viParam[2].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn2_SrcFrmRate));
            viParam[2].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn2_VI_H));
            viParam[2].u32ViWidth = str2int(thisini->GetConfig(Config::kChn2_VI_W));
            viParam[2].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_M_VENC_SAME_INPUT));
            viParam[2].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_S0_VENC_SAME_INPUT));
            break;
        case 3:
            viParam[3].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn3_M_RcMode));
            viParam[3].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn3_S0_RcMode));
            viParam[3].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn3_M_VENC_Type));
            viParam[3].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn3_S0_VENC_Type));
            viParam[3].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_SW_Color));
            viParam[3].enViMode = SAMPLE_VI_MODE;
            viParam[3].input_type = 1;
            viParam[3].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_M_McastEnable));
            viParam[3].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_S0_McastEnable));
            if(viParam[3].mcast_enable[0]) {
                sprintf(viParam[3].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn3_M_McastIp)).c_str());
                viParam[3].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn3_M_McastPort).c_str());
            }
            if(viParam[3].mcast_enable[1]) {
                sprintf(viParam[3].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn3_S0_McastIp)).c_str());
                viParam[3].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn3_S0_McastPort).c_str());
            }

            veSnap[3].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_Snap_McastEnable));
            sprintf(veSnap[3].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn3_Snap_McastIp)).c_str());
            veSnap[3].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn3_Snap_McastPort).c_str());
            veSnap[3].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn3_Snap_McastFreq).c_str());
            viParam[3].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_Osd_Enable));
            veSnap[3].snapQuality = str2int(thisini->GetConfig(Config::kChn3_Snap_Quality));
            veSnap[3].snapLeng = str2int(thisini->GetConfig(Config::kChn3_SnapLen).c_str());
            veSnap[3].snapDelay = str2int(thisini->GetConfig(Config::kChn3_Snap_Delay).c_str());
            veSnap[3].snapPort = str2int( thisini->GetConfig(Config::kChn3_SnapPort).c_str());
            viParam[3].stMinorSize = {str2int(thisini->GetConfig(Config::kChn3_S0_VENC_W)),
                                      str2int(thisini->GetConfig(Config::kChn3_S0_VENC_H))};
            veSnap[3].snapSize = {str2int(thisini->GetConfig(Config::kChn3_Snap_W)),
                                  str2int(thisini->GetConfig(Config::kChn3_Snap_H))};
            viParam[3].stVencSize = {str2int(thisini->GetConfig(Config::kChn3_M_VENC_W)),
                                     str2int(thisini->GetConfig(Config::kChn3_M_VENC_H))};
            viParam[3].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn3_M_BitRate));
            viParam[3].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn3_S0_BitRate));
            viParam[3].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_B_QP));
            viParam[3].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_B_QP));
            viParam[3].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn3_M_DstFrmRate));
            viParam[3].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn3_S0_DstFrmRate));
            viParam[3].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn3_M_Gop));
            viParam[3].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn3_S0_Gop));
            viParam[3].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_VI_Crop_Enable));
            if(viParam[3].vi_crop_enable) {
                viParam[3].u32X = str2int(thisini->GetConfig(Config::kChn3_VI_X));
                viParam[3].u32Y = str2int(thisini->GetConfig(Config::kChn3_VI_Y));
                viParam[3].u32W = str2int(thisini->GetConfig(Config::kChn3_VI_Width));
                viParam[3].u32H = str2int(thisini->GetConfig(Config::kChn3_VI_Height));
            }
            viParam[3].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_I_QP));
            viParam[3].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_I_QP));
            viParam[3].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MAXI_PROP));
            viParam[3].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MAXI_PROP));
            viParam[3].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MAX_QP));
            viParam[3].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MAX_QP));
            viParam[3].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MINI_PROP));
            viParam[3].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MINI_PROP));
            viParam[3].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MinI_Qp));
            viParam[3].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MinI_Qp));
            viParam[3].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MIN_QP));
            viParam[3].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MIN_QP));
            viParam[3].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_P_QP));
            viParam[3].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_P_QP));
            viParam[3].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn3_M_Profile));
            viParam[3].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn3_S0_Profile));
            viParam[3].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn3_SrcFrmRate));
            viParam[3].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn3_VI_H));
            viParam[3].u32ViWidth = str2int(thisini->GetConfig(Config::kChn3_VI_W));
            viParam[3].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_M_VENC_SAME_INPUT));
            viParam[3].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_S0_VENC_SAME_INPUT));

            break;
        default:
            printf("paramte init error\n");
            break;
        }
    }

    venc_flag[0] = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn0_RTSP_Enable).c_str());
    if(venc_flag[0]) {
        printf("venc 0 true\n");

    }
    else {
        printf("venc 0 false\n");

    }
    venc_flag[1] = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn1_RTSP_Enable).c_str());
    if(venc_flag[1]) {
        printf("venc 1 true\n");

    }
    else {
        printf("venc 1 false\n");

    }
    venc_flag[2] = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn2_RTSP_Enable).c_str());
    if(venc_flag[2]) {
        printf("venc 2 true\n");

    }
    else {
        printf("venc 2 false\n");

    }
    venc_flag[3] = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kChn3_RTSP_Enable).c_str());
    if(venc_flag[3]) {
        printf("venc 3 true\n");

    }

    vo_size_vi = (HI_BOOL)str2int(thisini->GetConfig(SoftwareConfig::kOut_SameIn));









#if 1
    for(i = 0; i < 3; i++)
    {
        switch (i) {
        case 0:
            aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kHdmi0_Au_McastEnable));
            memset(aumcast_ip[i],0,IPSIZE);
            sprintf(aumcast_ip[i],"%s",thisini->GetConfig(Config::kHdmi0_Au_McastIp).c_str());
            aumcast_port[i] = str2int(thisini->GetConfig(Config::kHdmi0_Au_McastPort));

            break;
        case 1:
            aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kHdmi1_Au_McastEnable));
            memset(aumcast_ip[i],0,IPSIZE);
            sprintf(aumcast_ip[i],"%s",thisini->GetConfig(Config::kHdmi1_Au_McastIp).c_str());
            aumcast_port[i] = str2int(thisini->GetConfig(Config::kHdmi1_Au_McastPort));


            break;
        case 2:
            aumcast_enable[i] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kAnalog_Au_McastEnable));
            memset(aumcast_ip[i],0,IPSIZE);
            sprintf(aumcast_ip[i],"%s",thisini->GetConfig(Config::kAnalog_Au_McastIp).c_str());
            aumcast_port[i] = str2int(thisini->GetConfig(Config::kAnalog_Au_McastPort));
            break;
        default:
            break;
        }













        //        if(aumcast_enable[i]) {
        //  auMcast_tid[i] = CreateThread(startAudioMcastThread, 0, SCHED_FIFO, true, &i);
        //            printf("ai %d  multicast start ip:%s, port:%d\n",i,aumcast_ip[i],aumcast_port[i]);
        //            sleep(1);
        //        }
    }


#endif


    thisini->SaveConfig();



    edid_4k = str2int(thisini->GetConfig(Config::kEdid));
    if(edid_4k == 1)
    {
        gEnViMode2  = SAMPLE_VI_MODE_4_4k;
        gEnViMode1 = SAMPLE_VI_MODE_4_4k;
        gEnViMode = SAMPLE_VI_MODE_4_4k;
    }
    else
    {
        // stMaxSize_ = {1920,1080};
        gEnViMode2  = SAMPLE_VI_MODE_4_1080P;
        gEnViMode1 = SAMPLE_VI_MODE_4_1080P;
        gEnViMode  = SAMPLE_VI_MODE_4_1080P;
    }
    //    if(edid_4k == 1)
    //stMaxSize_ = {3840,2160};


    veSnap[0].snapLeng = 1200;
    veSnap[1].snapLeng = 1200;
    veSnap[2].snapLeng = 1200;
    veSnap[3].snapLeng = 1200;

    for(i  = 0 ;i < 4; i++)
    {


        if(veSnap[i].Snapmcast_enable == HI_TRUE)
        { snap_runlag[i] = false;
            mult_snap_runlag[i] = true;
        }
        else
        {
            mult_snap_runlag[i]= false;
            snap_runlag[i] = true;
        }

    }


    osd_enable[0] = str2int(thisini->GetConfig(Config::kChn0_Osd_Enable));
    osd_enable[1] = str2int(thisini->GetConfig(Config::kChn1_Osd_Enable));
    osd_enable[2] = str2int(thisini->GetConfig(Config::kChn2_Osd_Enable));
    osd_enable[3] = str2int(thisini->GetConfig(Config::kChn3_Osd_Enable));

    mcast_enable[0] =  str2int(thisini->GetConfig(Config::kChn0_M_McastEnable));
    mcast_enable[1] =  str2int(thisini->GetConfig(Config::kChn0_S0_McastEnable));
    mcast_enable[2] =  str2int(thisini->GetConfig(Config::kChn1_M_McastEnable));
    mcast_enable[3] =  str2int(thisini->GetConfig(Config::kChn1_S0_McastEnable));
    mcast_enable[4] =  str2int(thisini->GetConfig(Config::kChn2_M_McastEnable));
    mcast_enable[5] =  str2int(thisini->GetConfig(Config::kChn2_S0_McastEnable));
    mcast_enable[6] =  str2int(thisini->GetConfig(Config::kChn3_M_McastEnable));
    mcast_enable[7] =  str2int(thisini->GetConfig(Config::kChn3_S0_McastEnable));

    sprintf(mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn0_M_McastIp)).c_str());
    sprintf(mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn0_S0_McastIp)).c_str());
    sprintf(mcast_ip[2], "%s", string(thisini->GetConfig(Config::kChn1_M_McastIp)).c_str());
    sprintf(mcast_ip[3], "%s", string(thisini->GetConfig(Config::kChn1_S0_McastIp)).c_str());
    sprintf(mcast_ip[4], "%s", string(thisini->GetConfig(Config::kChn2_M_McastIp)).c_str());
    sprintf(mcast_ip[5], "%s", string(thisini->GetConfig(Config::kChn2_S0_McastIp)).c_str());
    sprintf(mcast_ip[6], "%s", string(thisini->GetConfig(Config::kChn3_M_McastIp)).c_str());
    sprintf(mcast_ip[7], "%s", string(thisini->GetConfig(Config::kChn3_S0_McastIp)).c_str());

    mcast_port[0] = atoi(thisini->GetConfig(Config::kChn0_M_McastPort).c_str());
    mcast_port[1] = atoi(thisini->GetConfig(Config::kChn0_S0_McastPort).c_str());
    mcast_port[2] = atoi(thisini->GetConfig(Config::kChn1_M_McastPort).c_str());
    mcast_port[3] = atoi(thisini->GetConfig(Config::kChn1_S0_McastPort).c_str());
    mcast_port[4] = atoi(thisini->GetConfig(Config::kChn2_M_McastPort).c_str());
    mcast_port[5] = atoi(thisini->GetConfig(Config::kChn2_S0_McastPort).c_str());
    mcast_port[6] = atoi(thisini->GetConfig(Config::kChn3_M_McastPort).c_str());
    mcast_port[7] = atoi(thisini->GetConfig(Config::kChn3_S0_McastPort).c_str());

    repeat_time[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_McastNum));
    repeat_time[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_McastNum));
    repeat_time[2] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_McastNum));
    repeat_time[3] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_McastNum));
    repeat_time[4] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_McastNum));
    repeat_time[5] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_McastNum));
    repeat_time[6] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_McastNum));
    repeat_time[7] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_McastNum));


    mcast_delay[0] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_M_McastDelay ));
    mcast_delay[1] = str2int(thisini->GetConfig(SoftwareConfig::kChn0_S0_McastDelay));
    mcast_delay[2] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_M_McastDelay ));
    mcast_delay[3] = str2int(thisini->GetConfig(SoftwareConfig::kChn1_S0_McastDelay));
    mcast_delay[4] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_M_McastDelay ));
    mcast_delay[5] = str2int(thisini->GetConfig(SoftwareConfig::kChn2_S0_McastDelay));
    mcast_delay[6] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_M_McastDelay ));
    mcast_delay[7] = str2int(thisini->GetConfig(SoftwareConfig::kChn3_S0_McastDelay));



    return true;
}




bool InitHisiSysterm()
{
    HI_S32 i = 0;
    HI_S32 vdecMax = 1;
    HI_S32 vpssMax = 64;
    HI_BOOL audioFlag[3]  = {HI_TRUE,HI_TRUE,HI_TRUE};
    HI_S32 vencMax = 8*viNumber;
    HI_S32 vpssGrpCnt_vo= 10, vpssChn_vo = 1; //融合旧版本，新版本开发软件设置
    HI_S32 vpstart_vo = 8,vpstart1 = 4;
    HI_S32 vpssGrpCnt1 = 8, vpssChn1 = 2;
    VENC_CHN Snapchn = Usevenchn;
    SIZE_S Snap_size ;
    SIZE_S vpchnsize ={1920,1080};
    //    if(edid_4k == 1 )
    //     vpchnsize = {3840,2160};
    //    else
    //        vpchnsize = {1920,1080};

    VencStreamCnt = 2;   //1 VI--->2 VENC
    aiNumber = 3;
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<Config>::getInstance();
    /*
     * 初始化系统,VI由IT66021初始化
     * */



    SIZE_S vpchnsize_4k = {1920,1080};

    if(edid_4k == HI_TRUE)
        vpchnsize_4k = {3840,2160};


    himppmaster->InitHimpp(vdecMax,vpssMax,vencMax);

    himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize_4k);

    himppmaster->VencSnapInit_1(Snapchn ,veSnap);
    SIZE_S stTargetSize = {0,0};

    //环出 ：为4K时上面的接口为设备0 ，为1080p是设备1。


    himppmaster->VoModeuleInit(stTargetSize,0);
    himppmaster->VoModeuleInit(stTargetSize,1);


    SIZE_S vpchnsize_4k_1 = {1920,1080};



  //  himppmaster->VpssModuleInit(vpstart1,vpssGrpCnt1,vpssChn1,vpchnsize_4k_1);


    himppmaster->VpssModuleInitvo(vpstart_vo,vpssGrpCnt_vo,vpssChn_vo,vpchnsize_4k_1);

    himppmaster->BindPreVoewSys(0,0,3,4);






    //音视频匹配
#if 1
    if(!strcmp(thisini->GetConfig(SoftwareConfig::kChn0_AiDev).c_str(),"HDMI"))
        Aechn[0] = 0;
    else if(!strcmp(thisini->GetConfig(SoftwareConfig::kChn0_AiDev).c_str(),"3.5mm"))
        Aechn[0] = 2;

    if(!strcmp(thisini->GetConfig(SoftwareConfig::kChn1_AiDev).c_str(),"HDMI"))
        Aechn[1] = 1;
    else if(!strcmp(thisini->GetConfig(SoftwareConfig::kChn1_AiDev).c_str(),"3.5mm"))
        Aechn[1] = 2;

    //if(!strcmp(thisini->GetConfig(SoftwareConfig::kChn2_AiDev).c_str(),"3.5mm"))
    Aechn[2] = 2;

    // if(!strcmp(thisini->GetConfig(SoftwareConfig::kChn3_AiDev).c_str(),"3.5mm"))
    Aechn[3] = 2;
#endif



    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));

    coderFormat[0] = (enum audio_type)str2int(thisini->GetConfig(SoftwareConfig::kChn0_AiCoderFormat));
    swAudioFormat(0);
    coderFormat[1] = (enum audio_type)str2int(thisini->GetConfig(SoftwareConfig::kChn1_AiCoderFormat));

    swAudioFormat(1);
    coderFormat[2] = (enum audio_type)str2int(thisini->GetConfig(SoftwareConfig::kChn2_AiCoderFormat));
    swAudioFormat(2);
    coderFormat[3] = (enum audio_type)str2int(thisini->GetConfig(SoftwareConfig::kChn3_AiCoderFormat));
    swAudioFormat(3);



    //  coderFormat[2] =  AUDIO_NULL;
    coderFormat[3] = AUDIO_NULL;




    for(i = 0; i < aiNumber; i++) {

        himppmaster->AiModeuleInit_1(i,2, SAMPLE_AUDIO_PTNUMPERFRM,enAiSampleRate);

    }



    return true;
}


void * ms9282Fun( void  * arg )
{
    int fd = -1;
    unsigned char flag;
    int i;
    int res;


    Media_Output_Fmt outputFmt;

    fd = open("/dev/ms9282", 0);
    if (fd<0)
    {
        printf("Open ms9282 dev error!\n");
        // return -1;
    }
    //step 1

    if(edid_4k == 0)
        outputFmt.video_fmt = HDMI_VIDEO_FMT_1080P_60;
    else if(edid_4k == 1)
        outputFmt.video_fmt = HDMI_VIDEO_FMT_3840X2160P_30;




    ioctl(fd, VIDEO_SET_VO_PARAM, &outputFmt);
    sleep(1);
    // while(1)
    //  {
    ioctl(fd, VIDEO_OUTPUT_GET_STATUE, &outputFmt);
    if(outputFmt.connect){
        if(flag==0){
            flag = 1;
            res = ioctl(fd, VIDEO_GET_SINK_CAPABILITY, &outputFmt);
            if(res == 0){
                for(i=0;i<HDMI_VIDEO_FMT_BUTT;){
                    printf("%02x ",outputFmt.bVideoFmtSupported[i]);
                    i++;
                    if(i%8 == 0) printf("\n");
                }
                printf("\n");
            }else{
                printf("get sink capability fault! \n");
            }
        }
    }else
        flag=0;

    printf("hdp: %d \n",outputFmt.connect );
    sleep(1);
    // }
    return 0;
}



void * sil9022Fun(void *arg)
{
    int fd = -1;
    unsigned char flag;
    int i;
    int res;


    Media_Output_Fmt outputFmt;

    fd = open("/dev/sil9022", 0);
    if (fd<0)
    {
        printf("Open sil9022 dev error!\n");
        return (void*)-1;
    }
    memset(&outputFmt,0,sizeof(Media_Output_Fmt));
    //step 1
    outputFmt.video_fmt = HDMI_VIDEO_FMT_1080P_60;


    if(edid_4k == 0)
        outputFmt.video_fmt = HDMI_VIDEO_FMT_1080P_60;
    //    else if(edid_4k == 1)
    //        outputFmt.video_fmt = HDMI_VIDEO_FMT_3840X2160P_30;


    ioctl(fd, VIDEO_SET_VO_PARAM, &outputFmt);
    sleep(1);
    while( sil9022_star == true )
    {

        ioctl(fd, VIDEO_OUTPUT_GET_STATUE, &outputFmt);
        if(outputFmt.connect){
            if(flag==0){
                flag = 1;
                //  printf("3hdp: %d ,src_mode=%x,vodeo_fmt=%c\n",outputFmt.connect,outputFmt.src_mode,outputFmt.video_fmt );
                res = ioctl(fd, VIDEO_GET_SINK_CAPABILITY, &outputFmt);
                //   printf("4hdp: %d ,src_mode=%x,vodeo_fmt=%c\n",outputFmt.connect,outputFmt.src_mode,outputFmt.video_fmt );

                if(res == 0){
                    for(i=0;i<HDMI_VIDEO_FMT_BUTT;){
                        printf("%02x ",outputFmt.bVideoFmtSupported[i]);
                        i++;
                        if(i%8 == 0) printf("\n");
                    }
                    printf("\n");
                }else{
                    printf("get sink capability faultd! \n");
                }
            }
        }else
            flag=0;








        sleep(3);
    }


    return (void*)0;
}
#if 0
void *recvMessageProc(void*)
{
    // 创建进程间的消息队列
    c_mainmsgid = CreateMessageQueue(c_mainmsgpath);
    SetMessageQueue(c_mainmsgid);
    //GetMessageQueueStatus(c_mainmsgid);

    struct msg_st msgdata;


    JsonParse* jsonparse = new JsonParse();
    while(l_recvrunning == true){
        memset(&msgdata, 0, sizeof(struct msg_st));

        if(msgrcv(c_mainmsgid, (void*)&msgdata,
                  BUFF_SIZE, c_mainmsgtype, 0) == -1){ // 阻塞读取。若设非阻塞，最后一个参数设为IPC_NOWAIT
            COMMON_PRT("msgrcv failed with errno: %s", strerror(errno));
            sleep(1);
            continue;
        }

        //printf("mig    1   --------------------  msgdata=%s\n",msgdata.mtext.msg);
        strncpy(gClientIP, msgdata.mtext.ip, SZ_IPADDR);
        gClientIP[SZ_IPADDR-1] = '\0';
        gClientPort = msgdata.mtext.port;
        COMMON_PRT("client ip: %s, port: %d. You recv message:", gClientIP, gClientPort);
        COMMON_PRT("%s", msgdata.mtext.msg);


        jsonparse->LoadJson(msgdata.mtext.msg);
        if(jsonparse->CheckJsonFormat()){
            jsonparse->IterationParseStart();
        }
        else{
            COMMON_PRT("Illegal format of JSON");
        }
        jsonparse->ReleaseJson(); //NOTE: Paired with LoadJson()

    }

    delete jsonparse;

    // 销毁进程间的消息队列
    //DeleteMessageQueue(c_mainmsgid);

    printf(" msg exit ------------------------\n");
    return (void*)1;
}
#endif

void *recvMessageProc(void*)
{
    // 创建进程间的消息队列
    c_mainmsgid = CreateMessageQueue(c_mainmsgpath);
    SetMessageQueue(c_mainmsgid);
    //GetMessageQueueStatus(c_mainmsgid);

    struct msg_st msgdata;

    JsonParse* jsonparse = new JsonParse();
    while(l_recvrunning){
        memset(&msgdata, 0, sizeof(struct msg_st));
        if(msgrcv(c_mainmsgid, (void*)&msgdata,
                  BUFF_SIZE, c_mainmsgtype, 0) == -1){ // 阻塞读取。若设非阻塞，最后一个参数设为IPC_NOWAIT
            COMMON_PRT("msgrcv failed with errno: %s", strerror(errno));
            sleep(1);
            continue;
        }

        strncpy(gClientIP, msgdata.mtext.ip, SZ_IPADDR);
        gClientIP[SZ_IPADDR-1] = '\0';
        gClientPort = msgdata.mtext.port;
        COMMON_PRT("client ip: %s, port: %d. You recv message:", gClientIP, gClientPort);
        COMMON_PRT("%s", msgdata.mtext.msg);

        jsonparse->LoadJson(msgdata.mtext.msg);
        if(jsonparse->CheckJsonFormat()){
            jsonparse->IterationParseStart();
        }
        else{
            COMMON_PRT("Illegal format of JSON");
        }
        jsonparse->ReleaseJson(); //NOTE: Paired with LoadJson()
    }
    delete jsonparse;

    // 销毁进程间的消息队列
    DeleteMessageQueue(c_mainmsgid);

    return (void*)1;
}

#if 0
void * get_vi_give_pts(void *)
{


    HI_U32 s32Ret = 0;
    VI_CHN vichn   = 5;
    VIDEO_FRAME_INFO_S stFrame,

            int vi_port = 0;
    HI_S32 s32MilliSec = 0;

    s32Ret = HI_MPI_VI_GetFrame(vichn,&stFrame, s32MilliSec);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%d get vi %d frame err:0x%x\n",theloop,vichn, s32Ret);

        //   getFlag = HI_FALSE;
        continue;
    }



    if(vi_port == 0){
        HI_U32 pu32Value1;
        for(int i=0;i<7;i++){
            Hi_GetReg(vi_addr[i], &pu32Value1);


            //  printf("get_reg=%d,i=%d\n",pu32Value1,i);

            if((pu32Value1 & 0xFFFFFF00) == stFrame.stVFrame.u32PhyAddr[0]){


                pts[vi_port] = ((pu32Value1 & 0xFF) + 3 + 60) % 60;
                Hi_SetReg(vi_addr[i], 0);
                printf("i==%d,vi_port=%d,pts=%d\n",i,i,pts[vi_port]);

                break;
            }
        }
    }




    s32Ret = HI_MPI_VI_ReleaseFrame(vichn, &stFrame);
    if (HI_SUCCESS != s32Ret)
    {
        printf("release vi frame err:0x%x\n", s32Ret);
    }







    return (void*)1;
}

#endif



#if 0
void *recvMessageProc(void*)
{
    extern HI_BOOL is_newcmd;
    extern mutex   l_mutlock;
    extern WndInfoMap  l_wndinfomap;

    // 创建进程间的消息队列
    c_mainmsgid = CreateMessageQueue(c_mainmsgpath);
    SetMessageQueue(c_mainmsgid);
    //GetMessageQueueStatus(c_mainmsgid);

    struct msg_st msgdata;
    auto cmdmap = Singleton<CommandMap>::getInstance();
    auto cmdmap_scode = Singleton<CommandMap_SCODe>::getInstance();

    JsonParse* jsonparse = new JsonParse();
    SCODe* scode = new SCODe();
    while(l_recvrunning){
        memset(&msgdata, 0, sizeof(struct msg_st));
        if(msgrcv(c_mainmsgid, (void*)&msgdata,
                  BUFF_SIZE, c_mainmsgtype, 0) == -1){ // 阻塞读取。若设非阻塞，最后一个参数设为IPC_NOWAIT
            log_e("msgrcv failed with errno: %s", strerror(errno));
            sleep(1);
            continue;
        }

        strncpy(gClientIP, msgdata.mtext.ip, SZ_IPADDR);
        gClientIP[SZ_IPADDR-1] = '\0';
        gClientPort = msgdata.mtext.port;
        printf("client ip: %s, port: %d. You recv message:", gClientIP, gClientPort);
        printf("%s", msgdata.mtext.msg);

        jsonparse->LoadJson(msgdata.mtext.msg);
        if(jsonparse->CheckJsonFormat()){
            log_d("DETECTED Json Format CMD");
            //NOTE: 指令的判断与执行入口
            jsonparse->IterationParseStart();

            if(gMCtrllProtocol == kJSON){
                l_mutlock.lock();
                l_wndinfomap = cmdmap->wndinfomap_;
                l_mutlock.unlock();

                cmdmap->AutoSaveCommandCnt();
            }

            is_newcmd = HI_TRUE;
        }
        else if(gMCtrllProtocol == kSCODe &&
                scode->CheckSCODeFormat(msgdata.mtext.msg)){
            log_d("DETECTED SCODe Format CMD");
            //NOTE: 指令的判断与执行入口
            vstring itemlist = scode->GetItemList(msgdata.mtext.msg);
            cmdmap_scode->ProcessCmd(itemlist[0], itemlist);

            l_mutlock.lock();
            l_wndinfomap = cmdmap_scode->wndinfomap_;
            l_mutlock.unlock();

            is_newcmd = HI_TRUE;
        }
        else{
            log_e("Currently Unsupported Protocols");
        }
        jsonparse->ReleaseJson(); //NOTE: Paired with LoadJson()

        // TODO: 判断某一值，使得可以安全结束这个while循环，不能在msgrcv阻塞等待的时候改变该值，这样while依然无法退出

#if 0
        struct msqid_ds msg_info;
        if(-1 != msgctl(c_mainmsgid, IPC_STAT, &msg_info)){
            if(MESSAGE_BUG_MAX - msg_info.msg_cbytes > 1000000){
                isLockNewCmd = true;
            }
            else{
                isLockNewCmd = false;
            }
        }
#endif
    }
    delete scode;
    delete jsonparse;

    // 销毁进程间的消息队列
    DeleteMessageQueue(c_mainmsgid);

    return (void*)1;
}

#endif
int  GetHardWareInfo(char* res)
{
    FILE* f = fopen("/version/hardware_info", "rb");
    if(!f) return 0;

    char buf[512] = {0};
    int len = fread(buf,1,512,f);
    if(len <= 0) {
        COMMON_PRT("fread hareware_info error\n");
        return 0;
    }


    char *pos1 = strstr(buf, "board\": \"");
    pos1 += strlen("board\": \"");
    if(!pos1)
        return 0;
    else{
        COMMON_PRT("get start:%s\n",pos1);
        char *pos2 = strchr(pos1, '\"');
        if(!pos2)
            return 0;
        else
            COMMON_PRT("get end:%s\n",pos2);


        strncpy(res, pos1, pos2 - pos1);
        return strlen(res);
    }

    return NULL;
}

HI_BOOL initGlobalParam(char *hardVersion)
{
    if(!strncmp(hardVersion,HI31D_4HDMI,strlen(HI31D_4HDMI))) {
        COMMON_PRT("4hdmi board ,4 it66021Fun\n");
        g_vi0_hdmi = HI_TRUE;
        g_vi1_hdmi = HI_TRUE;
        g_vi2_hdmi = HI_TRUE;
        g_vi3_hdmi = HI_TRUE;
        return HI_TRUE;

    }
    else if(!strncmp(hardVersion,HI31D_4MUX,strlen(HI31D_4MUX))){
        COMMON_PRT("4mux board,2 it66021Fun\n");
        g_vi0_hdmi = HI_FALSE;
        g_vi1_hdmi = HI_FALSE;
        g_vi2_hdmi = HI_TRUE;
        g_vi3_hdmi = HI_TRUE;
        return HI_TRUE;
    }
    else {
        COMMON_PRT("not get hardInfo\n");
        return HI_FALSE;
    }


}

bool readHdVersion(char *hd_version)
{
    FILE *hdFp = NULL;
    int rdLen = 0;
    char buff[256] = {0};

    char *st = NULL, *end = NULL;

    if(NULL == (hdFp = fopen(HDPATH,"r"))) {
        printf("open hardware_info error\n");
        return false;
    }
    memset(buff,0,256);
    memset(hd_version,0,32);

    rdLen = fread(buff,1,256,hdFp);
    if(rdLen > 0) {
        printf("read hardware_info:%s\n",buff);
        st = strstr(buff,"\"board\": \"");
        st += strlen("\"board\": \"");
        end = strchr(st,'\"');
        strncpy(hd_version,st,end-st);
        printf("get hdVersion:%s\n",hd_version);
    }
    else{
        fclose(hdFp);
        return false;
    }
    fclose(hdFp);
    return true;
}




int versionFlag = 0;   //0 -- 4HDMI 1 -- 4MIX

int main()
{
    bool b_detached = true; //设置线程为分离模式
    HI_U32 Stchn = 0,i = 0;
    // 1. 挂载系统中断信号，检测Ctrl+C按键
    signal(SIGHUP, HandleSig); //NOTE: 关闭终端
    signal(SIGINT, HandleSig); //NOTE: 检测Ctrl+C按键
    signal(SIGTERM, HandleSig); //NOTE: 检测killall 命令
    signal(SIGSEGV, HandleSig); //NOTE: 检测 Segmentation fault!

    char version[32] ={0};
    memset(version,0,sizeof(version));
    readHdVersion(version);





    // 2. 创建读取配置文件的单例
    auto thisini = Singleton<Config>::getInstance();
    thisini->ReadConfig();
    thisini->PrintConfig();
    thisini->SaveConfig();
    // thisini-> SetNetwork();
    InitParamte();     //读取配置，初始化配置参数
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    //3. 版本号
    /* NOTE: 生成版本号: <主版本号>.<次版本号>.<修订版本号>.<编译日期>_<阶段版本> */
    HI_U32 u32ChipId = 0;
    if ( HI_SUCCESS != HI_MPI_SYS_GetChipId(&u32ChipId)) //获取芯片ID
        u32ChipId = 0;
    if ( !GenerateVersion(sw_version, u32ChipId) )
        COMMON_PRT("generate version failed!\n");
    if ( !GenerateUUID() )
        COMMON_PRT("generate uuid failed!\n");



    if(!strncmp(version,F_HDMIVERSION,strlen(F_HDMIVERSION))) {
        printf("this chip is 4hdmi input\n");
        versionFlag = HDMI_4;
    }
    else if(!strncmp(version,F_MIXVERSION,strlen(F_MIXVERSION))) {
        printf("this chip is 4mix input\n");
        versionFlag = MIX_4;
    }




    versionFlag = HDMI_4;

    InitHisiSysterm();  //初始化海思系统，VPSS，VO
    pthread_t udpserver_tid = CreateThread(recvMessageProc, 99, SCHED_FIFO, b_detached, NULL);
    if(udpserver_tid == 0){
        COMMON_PRT("pthread_create() failed: net recvMessageProc");
    }



    //    pthread_t udpserver_tid = CreateThread(recvMessageProc, 99, SCHED_FIFO, b_detached, NULL);
    //    if(udpserver_tid == 0){
    //        COMMON_PRT("pthread_create() failed: net recvMessageProc");
    //    }




    //     9. 启动 http Server 的线程
    //    pthread_t http_tid = CreateThread(httpServerhandler, 0, SCHED_FIFO, b_detached, NULL);
    //    if(http_tid == 0){
    //        COMMON_PRT("pthread_create() failed: httpserverhandler");
    //    }


#if 1
    //12.输入组播
    if(gbTimeTick){
        //VoTTh
        //Hi_SetReg(0x1302c01c, 0x00010450);
        //PLL0 倍频系数,将VO刷新率改为1080P61Hz
        //Hi_SetReg(0x12040014, 0x01002064);

        extern HI_VOID* GetTimeTick(HI_VOID *arg);
        //priority = 99;
        pthread_t tid = CreateThread(GetTimeTick, 99, SCHED_FIFO, b_detached, NULL);
        if(tid == 0){
            printf("pthread_create() failed: GetTimeTick");
        }
    }
#endif


    if(crtsps_start((short)554) < 0){
        printf("rtsp server start failed!\n");
        return 0;
    }



    //    fdfun = open("/dev/hdmicapture",O_RDONLY);

    //    if (fdfun<0)
    //    {
    //        perror("Open it6801 dev error!");
    //        return  -1;
    //    }
    //    else {
    //        printf("open ok\n");
    //    }

    //    int x= 0;
    //    for( x  = 0 ; x < 4 ;x++)
    //    {
    //        memset(&inputFmt[x],0,sizeof(Media_Input_Fmt));
    //        memset(&inputGetFmt[x],0,sizeof(Media_Input_Fmt));
    //    }

    //    inputFmt[1].chip_num=1;
    //    inputFmt[1].input_src=VIDEO_INPUT_MODE_HDMI;
    //    //inputFmt[1].video_fmt=VIN_RESOLUTION_800x600P;
    //    //        inputFmt[1].chip_num=0;
    //    //        inputFmt[1].input_src=VIDEO_INPUT_MODE_VGA;


    //    inputFmt[1].video_fmt=VIN_RESOLUTION_1920x1080P;

    //    if(edid_4k == 1)
    //        inputFmt[1].video_fmt=VIN_RESOLUTION_3840x2160;


    //    ioctl(fdfun, VIDEO_SET_CAPTRUE_PARAM, &inputFmt[1]);



    sem_init(&sem1,0,1); // orderly pthread
    sem_init(&sem2,0,0);
    sem_init(&sem3,0,0);
    sem_init(&sem4,0,0);

    snap_port[0] = atoi( thisini->GetConfig(Config::kChn0_SnapPort).c_str() );
    snap_port[1] = atoi( thisini->GetConfig(Config::kChn1_SnapPort).c_str() );
    snap_port[2] = atoi( thisini->GetConfig(Config::kChn2_SnapPort).c_str() );
    snap_port[3] = atoi( thisini->GetConfig(Config::kChn3_SnapPort).c_str() );

    int mac_vi = 0;

    //    pthread_t multisend_tid = CreateThread(MultiSend, 0, SCHED_FIFO, b_detached,&mac_vi);
    //    if(multisend_tid == 0){
    //        COMMON_PRT("pthread_create() failed: multisend\n");
    //    }




    int vi_get = 1;


    pthread_t   tid_get  = CreateThread(it6801Get, 0, SCHED_FIFO, b_detached, &vi_get);
    if(tid_vi[0] == 0){
        printf("pthread_create() failed: 0timeCheckIt66021\n");
    }




    int vi_0 = 1;
    if( versionFlag == HDMI_4)
    {
        tid_vi[0] = CreateThread(it6801Fun, 0, SCHED_FIFO, b_detached, &vi_0);
        if(tid_vi[0] == 0){
            printf("pthread_create() failed: 0timeCheckIt66021\n");
        }
    }


    if( versionFlag == HDMI_4)
    {
        int vi_1 = 3;
        tid_vi[1] = CreateThread(it6801Fun_1, 0, SCHED_FIFO, b_detached, &vi_1);
        if(tid_vi[1] == 0){
            printf("pthread_create() failed: 0timeCheckIt66021\n");
        }
    }


    if(versionFlag == HDMI_4)
    { int vi_2 = 5;
        pthread_t tid_vi2 = CreateThread(it6801Fun_2, 0, SCHED_FIFO, b_detached, &vi_2);
        if(tid_vi2 == 0){
            printf("pthread_create() failed: 0timeCheckIt66021\n");
        }
    }

    //if(versionFlag == HDMI_4)
    //{
    //    int vi_3 = 7;
    //    pthread_t tid_vi3 = CreateThread(it6801Fun_3, 99, SCHED_FIFO, b_detached, &vi_3);
    //    if(tid_vi3 == 0){
    //        printf("pthread_create() failed: 0timeCheckIt66021\n");
    //    }

    //}

    //驱动与VO相关
    uint priority = 99;
    pthread_t vo_tid = CreateThread(sil9022Fun,priority,SCHED_FIFO,b_detached,NULL);
    if(vo_tid == 0) {
        printf("pthread_create() failed: sil9022Fun");
    }

    //3.5MM线程音频
    himppmaster->StartAudioEnc_1(0,2);


    //        uint priority = 99;
    //        pthread_t vo_tid = CreateThread(ms9282Fun,priority,SCHED_FIFO,b_detached,NULL);
    //        if(vo_tid == 0) {
    //               printf("pthread_create() failed: sil9022Fun");
    //        }






    while(main_exit_while == true)
        sleep(100000);




    sem_destroy(&sem1);
    sem_destroy(&sem2);
    sem_destroy(&sem3);
    sem_destroy(&sem4);

    // 14. 退出 void *it66021fn(void*) 线程

    l_gs2971a_running  = false;
    it6801Fun_false[0] =  false;
    it6801Fun_false[1] =  false;
    it6801Fun_false[2] =  false;
    it6801Fun_false[3] =  false;


    // 15. 销毁HiMpp开启的各个模块
    himppmaster->StopViModule(1,4);
    himppmaster->StopViModule(3,12);
    himppmaster->StopViModule(5,20);
    himppmaster->StopViModule(7,28);
    for(i = 0; i < 4;i++)
    {
        it6801Fun_false[i] = false;
        snap_runlag[i] = false;
        veSnap[i].Snapmcast_enable =HI_FALSE;
        snapmcast_loop[i] = false;
        mult_snap_runlag[i] = false;
    }
    if(snapflag == HI_TRUE)
    {
        for(i = 0 ; i <  4  ; i++)
        {
            himppmaster->VencModuleDestroy_1(Stchn, Stchn + 2 ,i);
            Stchn = ( Stchn + 2);
            //printf("Ch_n ===%d\n",Ch_n);
        }
    }
    else
        himppmaster->VencModuleDestroy_1(0,8,0);


    himppmaster->AudioModuleDestroy();

    return 1;
}
