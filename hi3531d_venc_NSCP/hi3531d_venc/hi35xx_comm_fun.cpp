#define LOG_TAG    "HI35xx_COMM_FUN"

#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "hi35xx_comm_fun.h"
#include <sys/ioctl.h>



#define HI35xx_SYS_ALIGN_WIDTH 16
enum audio_type coderFormat[4];

SAMPLE_MEMBUF_S l_stVbMem[POOL_MAX];

HI_HDMI_CALLBACK_FUNC_S g_stCallbackFunc,l_stCallbackFunc;
HDMI_ARGS_S      g_stHdmiArgs_;
HI_S32 SnapQuality[4];

HI_U32 HI35xx_COMM_SYS_CalcPicVbBlkSize(SIZE_S *stSize, PIXEL_FORMAT_E enPixFmt, HI_U32 u32AlignWidth,COMPRESS_MODE_E enCompFmt)
{

    HI_U32 u32Width 		= 0;
    HI_U32 u32Height 		= 0;
    HI_U32 u32BlkSize 		= 0;
    HI_U32 u32HeaderSize 	= 0;

    if (PIXEL_FORMAT_YUV_SEMIPLANAR_422 != enPixFmt && PIXEL_FORMAT_YUV_SEMIPLANAR_420 != enPixFmt)
    {
        SAMPLE_PRT("pixel format[%d] input failed!\n", enPixFmt);
        return HI_FAILURE;
    }

    if (16!=u32AlignWidth && 32!=u32AlignWidth && 64!=u32AlignWidth)
    {
        SAMPLE_PRT("system align width[%d] input failed!\n",\
                   u32AlignWidth);
        return HI_FAILURE;
    }
    if (704 == stSize->u32Width)
    {
        stSize->u32Width = 720;
    }
    //SAMPLE_PRT("w:%d, u32AlignWidth:%d\n", CEILING_2_POWER(stSize.u32Width,u32AlignWidth), u32AlignWidth);

    u32Width  = CEILING_2_POWER(stSize->u32Width, u32AlignWidth);
    u32Height = CEILING_2_POWER(stSize->u32Height,u32AlignWidth);

    if (PIXEL_FORMAT_YUV_SEMIPLANAR_422 == enPixFmt)
    {
        u32BlkSize = u32Width * u32Height * 2;
    }
    else
    {
        u32BlkSize = u32Width * u32Height * 3 / 2;
    }


    if(COMPRESS_MODE_SEG == enCompFmt)
    {
        VB_PIC_HEADER_SIZE(u32Width,u32Height,enPixFmt,u32HeaderSize);
    }

    u32BlkSize += u32HeaderSize;

    return u32BlkSize;
}
#if 1
HI_S32 hi35_SAMPLE_COMM_VENC_Start_1(VENC_CHN venc_chn ,PARAMETER vencParam,HI_U32 slave,SIZE_S size_s0_s1)
{

    HI_S32 SAMPLE_COMM_VENC_Start ;
    HI_S32 s32Ret;
    HI_U32 i = slave;
    VENC_CHN_ATTR_S stVencChnAttr;
    VENC_ATTR_H264_S stH264Attr;
    VENC_ATTR_H264_CBR_S    stH264Cbr;
    VENC_ATTR_H264_VBR_S    stH264Vbr;
    VENC_ATTR_H264_AVBR_S    stH264AVbr;
    VENC_ATTR_H264_FIXQP_S  stH264FixQp;
    VENC_ATTR_H265_S        stH265Attr;
    VENC_ATTR_H265_CBR_S    stH265Cbr;
    VENC_ATTR_H265_VBR_S    stH265Vbr;
    VENC_ATTR_H265_AVBR_S    stH265AVbr;
    VENC_ATTR_H265_FIXQP_S  stH265FixQp;
    VENC_ATTR_MJPEG_S stMjpegAttr;
    VENC_ATTR_MJPEG_FIXQP_S stMjpegeFixQp;
    VENC_ATTR_JPEG_S stJpegAttr;
    SIZE_S stPicSize;

   // SIZE_S size_s0_vi = {3840,2160};
    printf(" i ==%d    ==%d-type-%d- -rcmmode-%d---venc-w==%d h==%d-----vichn-----aa\n",i,venc_chn,vencParam.enType[i],vencParam.enRcMode[i],
        size_s0_s1.u32Width,size_s0_s1.u32Height );
    /******************************************
     step 1:  Create Venc Channel
     ******************************************/

//if((venc_chn == 6 ) || (venc_chn == 7))
//{
//    size_s0_vi = {1920,1080};
//}

 // SIZE_S size_s0_vi = {1920,1080};




    SIZE_S max_snap = {size_s0_s1.u32Width,size_s0_s1.u32Height};
    int snaph_4 = max_snap.u32Height % 2;
    int snapw_4 = max_snap.u32Width  % 2;



//    if(snaph_4 != 0)
//    {   size_s0_s1.u32Height =  size_s0_s1.u32Height + snaph_4;
//       vencParam.stMinorSize.u32Height = vencParam.stMinorSize.u32Height + snaph_4;

//    }
//    else if(snapw_4 != 0)
//    {     size_s0_s1.u32Width = size_s0_s1.u32Width + snapw_4 ;

//        vencParam.stMinorSize.u32Width = vencParam.stMinorSize.u32Width + snapw_4;

//    }


    HI_U32 diff_ip_qp = 0;


      if((vencParam.u32ViHeigth > 1080 ) || (vencParam.u32ViWidth > 1920))
      {
         diff_ip_qp = 5;
         // vencParam.enRcMode[i] = SAMPLE_RC_CBR;

      }



//HI_MPI_VGS_AddCoverTask
    stVencChnAttr.stVeAttr.enType = vencParam.enType[i];
    switch (vencParam.enType[i])
    {
        case PT_H264:
       {
        if(i == 0){
//            stH264Attr.u32MaxPicWidth =  vencParam.u32ViWidth ;//size_s0_s1.u32Width ;
//            stH264Attr.u32MaxPicHeight = vencParam.u32ViHeigth;//size_s0_s1.u32Height ;
//            stH264Attr.u32BufSize  =     vencParam.u32ViHeigth * vencParam.u32ViWidth * 2 ;
            stH264Attr.u32MaxPicWidth =   size_s0_s1.u32Width ;
            stH264Attr.u32MaxPicHeight =  size_s0_s1.u32Height ;
             stH264Attr.u32BufSize  =     size_s0_s1.u32Width * size_s0_s1.u32Height *2 ;
            stH264Attr.u32PicWidth =     size_s0_s1.u32Width;/*the picture width*/
            stH264Attr.u32PicHeight =    size_s0_s1.u32Height;/*the picture height*/

        }
        if(i ==1 )
        {
           size_s0_s1 = vencParam.stMinorSize;

//            stH264Attr.u32MaxPicWidth =  vencParam.u32ViWidth ;//size_s0_s1.u32Width ;
//            stH264Attr.u32MaxPicHeight = vencParam.u32ViHeigth;//size_s0_s1.u32Height ;
//            stH264Attr.u32BufSize  =     vencParam.u32ViHeigth * vencParam.u32ViWidth * 2 ;

            stH264Attr.u32MaxPicWidth =  size_s0_s1.u32Width ;
            stH264Attr.u32MaxPicHeight = size_s0_s1.u32Height ;
              stH264Attr.u32BufSize  =     size_s0_s1.u32Width * size_s0_s1.u32Height *2;
            stH264Attr.u32PicWidth =     size_s0_s1.u32Width;/*the picture width*/
            stH264Attr.u32PicHeight =    size_s0_s1.u32Height;/*the picture height*/



        }
          // stH264Attr.u32BufSize = stH264Attr.u32MaxPicHeight * stH264Attr.u32MaxPicWidth * 2;
            stH264Attr.u32Profile  = vencParam.u32Profile[i];/*0: baseline; 1:MP; 2:HP;  3:svc_t */
            stH264Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
            //stH264Attr.u32BFrameNum = 0;/* 0: not support B frame; >=1: number of B frames */
            //stH264Attr.u32RefNum = 1;/* 0: default; number of refrence frame*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH264e, &stH264Attr, sizeof(VENC_ATTR_H264_S));





            if (SAMPLE_RC_CBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264CBR;
                stH264Cbr.u32Gop            = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH264Cbr.u32SrcFrmRate      = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* input (vi) frame rate */
                stH264Cbr.fr32DstFrmRate     = vencParam.u32SrcFrmRate;//vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* target frame rate */
                // stH264Cbr.fr32DstFrmRate = vencParam.u32SrcFrmRate;
                stH264Cbr.u32BitRate = vencParam.u32BitRate[i];
                stH264Cbr.u32FluctuateLevel = 1; /* average bit rate */
                printf("gop=%d,time=%d,frmrate=%d,dsfrmrate=%d,bitate=%d,fluctuallevel=%d\n",stH264Cbr.u32Gop,stH264Cbr.u32StatTime,stH264Cbr.u32SrcFrmRate
                       ,stH264Cbr.fr32DstFrmRate,stH264Cbr.u32BitRate,stH264Cbr.u32FluctuateLevel);
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Cbr, &stH264Cbr, sizeof(VENC_ATTR_H264_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264FIXQP;
                stH264FixQp.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264FixQp.u32SrcFrmRate =  vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264FixQp.fr32DstFrmRate = vencParam.u32SrcFrmRate;//vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264FixQp.u32IQp = vencParam.u32IQp[i] + diff_ip_qp;//20;
                stH264FixQp.u32PQp = vencParam.u32PQp[i] + diff_ip_qp;//23;
                stH264FixQp.u32BQp = vencParam.u32BQp[i] + diff_ip_qp ;//23;
                printf("gop=%d--SrcFmRatr=%d--DStFramrate=%d--iqp==%d--pqp=%d--bqp=%d\n", stH264FixQp.u32Gop, stH264FixQp.u32SrcFrmRate,
                 stH264FixQp.fr32DstFrmRate ,  stH264FixQp.u32IQp , stH264FixQp.u32PQp,  stH264FixQp.u32BQp );
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264FixQp, &stH264FixQp, sizeof(VENC_ATTR_H264_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264VBR;
                stH264Vbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Vbr.u32StatTime = 1;
                stH264Vbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Vbr.fr32DstFrmRate = vencParam.u32SrcFrmRate;//vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Vbr.u32MinQp = vencParam.u32MinQp[i];//10;
                stH264Vbr.u32MinIQp = vencParam.u32MinIQp[i];//10;
                stH264Vbr.u32MaxQp = vencParam.u32MaxQp[i];//40;


                stH264Vbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Vbr, &stH264Vbr, sizeof(VENC_ATTR_H264_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264AVBR;
                stH264AVbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264AVbr.u32StatTime = 1;
                stH264AVbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264AVbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;


                stH264AVbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH264AVbr, sizeof(VENC_ATTR_H264_AVBR_S));
            }
            else
            {
                return HI_FAILURE;
            }
        }
        break;
        case PT_MJPEG:
        {
            stMjpegAttr.u32MaxPicWidth = vencParam.stVencSize.u32Width;
            stMjpegAttr.u32MaxPicHeight = vencParam.stVencSize.u32Height;
            stMjpegAttr.u32PicWidth = vencParam.stVencSize.u32Width;
            stMjpegAttr.u32PicHeight = vencParam.stVencSize.u32Height;
            stMjpegAttr.u32BufSize = vencParam.stVencSize.u32Width * vencParam.stVencSize.u32Height * 3;
            stMjpegAttr.bByFrame = HI_TRUE;  /*get stream mode is field mode  or frame mode*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrMjpege, &stMjpegAttr, sizeof(VENC_ATTR_MJPEG_S));
            if (SAMPLE_RC_FIXQP == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGFIXQP;
                stMjpegeFixQp.u32Qfactor        = 90;
                stMjpegeFixQp.u32SrcFrmRate      = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stMjpegeFixQp.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                memcpy(&stVencChnAttr.stRcAttr.stAttrMjpegeFixQp, &stMjpegeFixQp,
                       sizeof(VENC_ATTR_MJPEG_FIXQP_S));
            }
            else if (SAMPLE_RC_CBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGCBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32StatTime       = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32SrcFrmRate     = vencParam.u32SrcFrmRate;////(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32FluctuateLevel = 1;

                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32BitRate = vencParam.u32BitRate[i];
            }
            else if (SAMPLE_RC_VBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGVBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32StatTime = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//5;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MinQfactor = 50;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxQfactor = 95;

                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxBitRate = vencParam.u32BitRate[i];
            }
            else
            {
                SAMPLE_PRT("cann't support other mode in this version!\n");
                return HI_FAILURE;
            }
        }
        break;
        case PT_JPEG:
            stJpegAttr.u32PicWidth  = vencParam.stVencSize.u32Width;
            stJpegAttr.u32PicHeight = vencParam.stVencSize.u32Height;
            stJpegAttr.u32MaxPicWidth  = vencParam.stVencSize.u32Width;
            stJpegAttr.u32MaxPicHeight = vencParam.stVencSize.u32Height;
            stJpegAttr.u32BufSize   = vencParam.stVencSize.u32Width * vencParam.stVencSize.u32Height * 3;
            stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
            stJpegAttr.bSupportDCF  = HI_FALSE;
            memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));
            break;
        case PT_H265:
        {
        if(i == 0)
        {
            stH265Attr.u32MaxPicWidth  = size_s0_s1.u32Width;
            stH265Attr.u32MaxPicHeight = size_s0_s1.u32Height;
            stH265Attr.u32PicWidth     = size_s0_s1.u32Width;/*the picture width*/
            stH265Attr.u32PicHeight    = size_s0_s1.u32Height;/*the picture height*/
            stH265Attr.u32BufSize      = size_s0_s1.u32Width * size_s0_s1.u32Height * 2;/*stream buffer size*/
         }
        else if(i == 1)
        {
                                size_s0_s1 = vencParam.stMinorSize;
             stH265Attr.u32MaxPicWidth  = size_s0_s1.u32Width;
             stH265Attr.u32MaxPicHeight = size_s0_s1.u32Height;
             stH265Attr.u32PicWidth     = size_s0_s1.u32Width;/*the picture width*/
             stH265Attr.u32PicHeight    = size_s0_s1.u32Height;/*the picture height*/
             stH265Attr.u32BufSize      = size_s0_s1.u32Width * size_s0_s1.u32Height * 2;

        }

            if (vencParam.u32Profile[i] >= 1)
            {
                stH265Attr.u32Profile = 0;
            }/*0:MP; */
            else
            {
                stH265Attr.u32Profile  = vencParam.u32Profile[i];
            }/*0:MP*/
            stH265Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH265e, &stH265Attr, sizeof(VENC_ATTR_H265_S));
            if (SAMPLE_RC_CBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265CBR;
                stH265Cbr.u32Gop            = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH265Cbr.u32SrcFrmRate      = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* input (vi) frame rate */
                stH265Cbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* target frame rate */

                stH265Cbr.u32BitRate = vencParam.u32BitRate[i];
                stH265Cbr.u32FluctuateLevel = 1; /* average bit rate */
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Cbr, &stH265Cbr, sizeof(VENC_ATTR_H265_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265FIXQP;
                stH265FixQp.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265FixQp.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265FixQp.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265FixQp.u32IQp = vencParam.u32IQp[i] + diff_ip_qp;//20;
                stH265FixQp.u32PQp = vencParam.u32PQp[i] + diff_ip_qp;//23;
                stH265FixQp.u32BQp = vencParam.u32BQp[i] + diff_ip_qp;//25;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265FixQp, &stH265FixQp, sizeof(VENC_ATTR_H265_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265VBR;
                stH265Vbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Vbr.u32StatTime = 1;
                stH265Vbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//((VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Vbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Vbr.u32MinQp  = vencParam.u32MinQp[i];//10;
                stH265Vbr.u32MinIQp = vencParam.u32MinIQp[i];//10;
                stH265Vbr.u32MaxQp  = vencParam.u32MaxQp[i];//40;

                stH265Vbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Vbr, &stH265Vbr, sizeof(VENC_ATTR_H265_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265AVBR;
                stH265AVbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265AVbr.u32StatTime = 1;
                stH265AVbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265AVbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;


                stH265AVbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH265AVbr, sizeof(VENC_ATTR_H265_AVBR_S));
            }
            else
            {
                return HI_FAILURE;
            }
        }
        break;
        default:
            return HI_ERR_VENC_NOT_SUPPORT;
    }
    stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
    stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;

    s32Ret = HI_MPI_VENC_CreateChn(venc_chn, &stVencChnAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_CreateChn [%d] faild with %#x!\n", \
                   venc_chn, s32Ret);
        return s32Ret;
    }
    s32Ret = HI_MPI_VENC_StartRecvPic(venc_chn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic faild with%#x! \n", s32Ret);
        return HI_FAILURE;
    }

    if(vencParam.enType[i] == PT_JPEG){
        VENC_PARAM_JPEG_S stParamJpeg;
        s32Ret = HI_MPI_VENC_GetJpegParam(venc_chn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }

        stParamJpeg.u32Qfactor = 20;

        s32Ret = HI_MPI_VENC_SetJpegParam(venc_chn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
    }





    return HI_SUCCESS;
}
#endif

#if 0
HI_S32 hi35_SAMPLE_COMM_VENC_Start_1(VENC_CHN venc_chn ,PARAMETER vencParam,HI_U32 slave,SIZE_S size_s0_s1)
{

    HI_S32 SAMPLE_COMM_VENC_Start ;
    HI_S32 s32Ret;
    HI_U32 i = slave;
    VENC_CHN_ATTR_S stVencChnAttr;
    VENC_ATTR_H264_S stH264Attr;
    VENC_ATTR_H264_CBR_S    stH264Cbr;
    VENC_ATTR_H264_VBR_S    stH264Vbr;
    VENC_ATTR_H264_AVBR_S    stH264AVbr;
    VENC_ATTR_H264_FIXQP_S  stH264FixQp;
    VENC_ATTR_H265_S        stH265Attr;
    VENC_ATTR_H265_CBR_S    stH265Cbr;
    VENC_ATTR_H265_VBR_S    stH265Vbr;
    VENC_ATTR_H265_AVBR_S    stH265AVbr;
    VENC_ATTR_H265_FIXQP_S  stH265FixQp;
    VENC_ATTR_MJPEG_S stMjpegAttr;
    VENC_ATTR_MJPEG_FIXQP_S stMjpegeFixQp;
    VENC_ATTR_JPEG_S stJpegAttr;
    SIZE_S stPicSize;

    SIZE_S maxsize = {3840,2160} ;
    printf(" i ==%d    h=%d min w=%d   ==%d-type-%d- -rcmmode-%d---venc-w==%d h==%d----- -----aa\n",i,vencParam.stMinorSize.u32Height,vencParam.stMinorSize.u32Width,
           venc_chn,vencParam.enType[i],vencParam.enRcMode[i],
        size_s0_s1.u32Width,size_s0_s1.u32Height );
    /******************************************
     step 1:  Create Venc Channel
     ******************************************/



  HI_U32 diff_ip_qp = 0;


    if((vencParam.u32ViHeigth > 1080 ) && (vencParam.u32ViWidth > 1920))
    {
       diff_ip_qp = 5;
       // vencParam.enRcMode[i] = SAMPLE_RC_CBR;

    }









    stVencChnAttr.stVeAttr.enType = vencParam.enType[i];
    switch (vencParam.enType[i])
    {
        case PT_H264:
       {
        if(i == 0){
//            stH264Attr.u32MaxPicWidth =  vencParam.u32ViWidth ;//size_s0_s1.u32Width ;
//            stH264Attr.u32MaxPicHeight = vencParam.u32ViHeigth;//size_s0_s1.u32Height ;
//            stH264Attr.u32BufSize  =     vencParam.u32ViHeigth * vencParam.u32ViWidth ;/*stream buffer size*/
            stH264Attr.u32MaxPicWidth =   size_s0_s1.u32Width ;
            stH264Attr.u32MaxPicHeight =  size_s0_s1.u32Height ;
             stH264Attr.u32BufSize  =     size_s0_s1.u32Width * size_s0_s1.u32Height *2 ;/*stream buffer size*/
            stH264Attr.u32PicWidth =     size_s0_s1.u32Width;/*the picture width*/
            stH264Attr.u32PicHeight =    size_s0_s1.u32Height;/*the picture height*/

        }
        if(i ==1 )
        {
           size_s0_s1 = vencParam.stMinorSize;

//            stH264Attr.u32MaxPicWidth =  vencParam.u32ViWidth ;//size_s0_s1.u32Width ;
//            stH264Attr.u32MaxPicHeight = vencParam.u32ViHeigth;//size_s0_s1.u32Height ;
//            stH264Attr.u32BufSize  =     vencParam.u32ViHeigth * vencParam.u32ViWidth ;

            stH264Attr.u32MaxPicWidth =  size_s0_s1.u32Width ;
            stH264Attr.u32MaxPicHeight = size_s0_s1.u32Height ;
              stH264Attr.u32BufSize  =     size_s0_s1.u32Width * size_s0_s1.u32Height *2;/*stream buffer size*/
            stH264Attr.u32PicWidth =     size_s0_s1.u32Width;/*the picture width*/
            stH264Attr.u32PicHeight =    size_s0_s1.u32Height;/*the picture height*/



        }
          // stH264Attr.u32BufSize = stH264Attr.u32MaxPicHeight * stH264Attr.u32MaxPicWidth * 2;
            stH264Attr.u32Profile  = vencParam.u32Profile[i];/*0: baseline; 1:MP; 2:HP;  3:svc_t */
            stH264Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
            //stH264Attr.u32BFrameNum = 0;/* 0: not support B frame; >=1: number of B frames */
            //stH264Attr.u32RefNum = 1;/* 0: default; number of refrence frame*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH264e, &stH264Attr, sizeof(VENC_ATTR_H264_S));





            if (SAMPLE_RC_CBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264CBR;
                stH264Cbr.u32Gop            = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH264Cbr.u32SrcFrmRate      = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* input (vi) frame rate */
               // stH264Cbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* target frame rate */
                 stH264Cbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];
                stH264Cbr.u32BitRate = vencParam.u32BitRate[i];
                stH264Cbr.u32FluctuateLevel = 1; /* average bit rate */
                printf("gop=%d,time=%d,frmrate=%d,dsfrmrate=%d,bitate=%d,fluctuallevel=%d\n",stH264Cbr.u32Gop,stH264Cbr.u32StatTime,stH264Cbr.u32SrcFrmRate
                       ,stH264Cbr.fr32DstFrmRate,stH264Cbr.u32BitRate,stH264Cbr.u32FluctuateLevel);
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Cbr, &stH264Cbr, sizeof(VENC_ATTR_H264_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == vencParam.enRcMode[i])
            {

                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264FIXQP;
                stH264FixQp.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264FixQp.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264FixQp.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264FixQp.u32IQp = vencParam.u32IQp[i] +   diff_ip_qp;//20;
                stH264FixQp.u32PQp = vencParam.u32PQp[i] +   diff_ip_qp ;//23;
                stH264FixQp.u32BQp = vencParam.u32BQp[i] +   diff_ip_qp;//23;
                printf("gop=%d--SrcFmRatr=%d--DStFramrate=%d--iqp==%d--pqp=%d--bqp=%d\n", stH264FixQp.u32Gop, stH264FixQp.u32SrcFrmRate,
                 stH264FixQp.fr32DstFrmRate ,  stH264FixQp.u32IQp , stH264FixQp.u32PQp,  stH264FixQp.u32BQp );
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264FixQp, &stH264FixQp, sizeof(VENC_ATTR_H264_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264VBR;
                stH264Vbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Vbr.u32StatTime = 1;
                stH264Vbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Vbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264Vbr.u32MinQp = vencParam.u32MinQp[i];//10;
                stH264Vbr.u32MinIQp = vencParam.u32MinIQp[i];//10;
                stH264Vbr.u32MaxQp = vencParam.u32MaxQp[i];//40;


                stH264Vbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Vbr, &stH264Vbr, sizeof(VENC_ATTR_H264_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264AVBR;
                stH264AVbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264AVbr.u32StatTime = 1;
                stH264AVbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH264AVbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;


                stH264AVbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH264AVbr, sizeof(VENC_ATTR_H264_AVBR_S));
            }
            else
            {
                return HI_FAILURE;
            }
        }
        break;
        case PT_MJPEG:
        {
            stMjpegAttr.u32MaxPicWidth = vencParam.stVencSize.u32Width;
            stMjpegAttr.u32MaxPicHeight = vencParam.stVencSize.u32Height;
            stMjpegAttr.u32PicWidth = vencParam.stVencSize.u32Width;
            stMjpegAttr.u32PicHeight = vencParam.stVencSize.u32Height;
            stMjpegAttr.u32BufSize = vencParam.stVencSize.u32Width * vencParam.stVencSize.u32Height * 3;
            stMjpegAttr.bByFrame = HI_TRUE;  /*get stream mode is field mode  or frame mode*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrMjpege, &stMjpegAttr, sizeof(VENC_ATTR_MJPEG_S));
            if (SAMPLE_RC_FIXQP == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGFIXQP;
                stMjpegeFixQp.u32Qfactor        = 90;
                stMjpegeFixQp.u32SrcFrmRate      = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stMjpegeFixQp.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                memcpy(&stVencChnAttr.stRcAttr.stAttrMjpegeFixQp, &stMjpegeFixQp,
                       sizeof(VENC_ATTR_MJPEG_FIXQP_S));
            }
            else if (SAMPLE_RC_CBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGCBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32StatTime       = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32SrcFrmRate     = vencParam.u32SrcFrmRate;////(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32FluctuateLevel = 1;

                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32BitRate = vencParam.u32BitRate[i];
            }
            else if (SAMPLE_RC_VBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGVBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32StatTime = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//5;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MinQfactor = 50;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxQfactor = 95;

                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxBitRate = vencParam.u32BitRate[i];
            }
            else
            {
                SAMPLE_PRT("cann't support other mode in this version!\n");
                return HI_FAILURE;
            }
        }
        break;
        case PT_JPEG:
            stJpegAttr.u32PicWidth  = vencParam.stVencSize.u32Width;
            stJpegAttr.u32PicHeight = vencParam.stVencSize.u32Height;
            stJpegAttr.u32MaxPicWidth  = vencParam.stVencSize.u32Width;
            stJpegAttr.u32MaxPicHeight = vencParam.stVencSize.u32Height;
            stJpegAttr.u32BufSize   = vencParam.stVencSize.u32Width * vencParam.stVencSize.u32Height * 3;
            stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
            stJpegAttr.bSupportDCF  = HI_FALSE;
            memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));
            break;
        case PT_H265:
        {
        if(i == 0)
        {
            stH265Attr.u32MaxPicWidth  = size_s0_s1.u32Width;
            stH265Attr.u32MaxPicHeight = size_s0_s1.u32Height;
            stH265Attr.u32PicWidth     = size_s0_s1.u32Width;/*the picture width*/
            stH265Attr.u32PicHeight    = size_s0_s1.u32Height;/*the picture height*/
            stH265Attr.u32BufSize      = size_s0_s1.u32Width * size_s0_s1.u32Height * 2;/*stream buffer size*/
         }
        else if(i == 1)
        {
                                size_s0_s1 = vencParam.stMinorSize;
             stH265Attr.u32MaxPicWidth  = size_s0_s1.u32Width;
             stH265Attr.u32MaxPicHeight = size_s0_s1.u32Height;
             stH265Attr.u32PicWidth     = size_s0_s1.u32Width;/*the picture width*/
             stH265Attr.u32PicHeight    = size_s0_s1.u32Height;/*the picture height*/
             stH265Attr.u32BufSize      = size_s0_s1.u32Width * size_s0_s1.u32Height * 2;

        }

            if (vencParam.u32Profile[i] >= 1)
            {
                stH265Attr.u32Profile = 0;
            }/*0:MP; */
            else
            {
                stH265Attr.u32Profile  = vencParam.u32Profile[i];
            }/*0:MP*/
            stH265Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH265e, &stH265Attr, sizeof(VENC_ATTR_H265_S));
            if (SAMPLE_RC_CBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265CBR;
                stH265Cbr.u32Gop            = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH265Cbr.u32SrcFrmRate      = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* input (vi) frame rate */
                stH265Cbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30; /* target frame rate */

                stH265Cbr.u32BitRate = vencParam.u32BitRate[i];
                stH265Cbr.u32FluctuateLevel = 1; /* average bit rate */
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Cbr, &stH265Cbr, sizeof(VENC_ATTR_H265_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265FIXQP;
                stH265FixQp.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265FixQp.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265FixQp.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265FixQp.u32IQp = vencParam.u32IQp[i]  + diff_ip_qp;//20;
                stH265FixQp.u32PQp = vencParam.u32PQp[i]  + diff_ip_qp ;//23;
                stH265FixQp.u32BQp = vencParam.u32BQp[i] + diff_ip_qp;//25;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265FixQp, &stH265FixQp, sizeof(VENC_ATTR_H265_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265VBR;
                stH265Vbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Vbr.u32StatTime = 1;
                stH265Vbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//((VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Vbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265Vbr.u32MinQp  = vencParam.u32MinQp[i];//10;
                stH265Vbr.u32MinIQp = vencParam.u32MinIQp[i];//10;
                stH265Vbr.u32MaxQp  = vencParam.u32MaxQp[i];//40;

                stH265Vbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Vbr, &stH265Vbr, sizeof(VENC_ATTR_H265_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == vencParam.enRcMode[i])
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265AVBR;
                stH265AVbr.u32Gop = vencParam.u32Gop[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265AVbr.u32StatTime = 1;
                stH265AVbr.u32SrcFrmRate = vencParam.u32SrcFrmRate;//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;
                stH265AVbr.fr32DstFrmRate = vencParam.u32DstFrameRate[i];//(VIDEO_ENCODING_MODE_PAL == enNorm) ? 25 : 30;


                stH265AVbr.u32MaxBitRate = vencParam.u32BitRate[i];
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH265AVbr, sizeof(VENC_ATTR_H265_AVBR_S));
            }
            else
            {
                return HI_FAILURE;
            }
        }
        break;
        default:
            return HI_ERR_VENC_NOT_SUPPORT;
    }
    stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
    stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;

    s32Ret = HI_MPI_VENC_CreateChn(venc_chn, &stVencChnAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_CreateChn [%d] faild with %#x!\n", \
                   venc_chn, s32Ret);
        return s32Ret;
    }
    s32Ret = HI_MPI_VENC_StartRecvPic(venc_chn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic faild with%#x! \n", s32Ret);
        return HI_FAILURE;
    }

    if(vencParam.enType[i] == PT_JPEG){
        VENC_PARAM_JPEG_S stParamJpeg;
        s32Ret = HI_MPI_VENC_GetJpegParam(venc_chn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }

        stParamJpeg.u32Qfactor = 20;

        s32Ret = HI_MPI_VENC_SetJpegParam(venc_chn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
    }





    return HI_SUCCESS;
}
#endif




HI_S32 HI35xx_COMM_VI_Start_1(HI_BOOL vo_loop,HI_U32 did_4k,VI_DEV ViDev, VI_CHN ViChn, VIDEO_NORM_E enNorm, SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect,SIZE_S * pstTarSize,HI_U32 SrcFrmRate,HI_U32 DrcFrmRate)
{


    HI_S32 i = 0;
    HI_S32 s32Ret;
    SAMPLE_VI_PARAM_S stViParam;
    SIZE_S stTargetSize = {pstTarSize->u32Width,pstTarSize->u32Height};
    VI_CHN_ATTR_S stChnAttr;
    VI_CHN_BIND_ATTR_S stChnBindAttr;
        SAMPLE_VI_CHN_SET_E enViChnSet = VI_CHN_SET_NORMAL;


        int did_4k_dev = 0;


        s32Ret = SAMPLE_COMM_VI_StartDev(ViDev, enViMode,did_4k_dev);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("SAMPLE_COMM_VI_StartDev failed with %#x\n", s32Ret);
            return HI_FAILURE;
        }
 //   }

    /*** Start VI Chn ***/
    for( i = 0 ;i < 2 ; i ++)
       {

        RECT_S cap_recv = {stCapRect.s32X,stCapRect.s32Y,stCapRect.u32Width,stCapRect.u32Height};
        SIZE_S starge_size = {stTargetSize.u32Width,stTargetSize.u32Height};





       if((i == 1 ) && (ViDev != 1) )
          {
              cap_recv = {0,0,stTargetSize.u32Width,stTargetSize.u32Height};
              if(did_4k == 0)
              {
                  cap_recv = {0,0,1920,1080};
                  starge_size = {1920,1080};
              }
              else if(did_4k == 1)
              {
                  cap_recv = {0,0,3840,2160};
                  starge_size = {3840,2160};
              }
          }




         if(i  == 1)
         {
         s32Ret =  HI_MPI_VI_UnBindChn(ViChn + i);
         if (HI_SUCCESS != s32Ret)
         {
             SAMPLE_PRT("call HI_MPI_VI_BindChn failed with %#x\n", s32Ret);
             return HI_FAILURE;
         }
         else
         {
             printf("HI_MPI_VI_UnBindChn -----ok\n");
         }
       }

                s32Ret = HI_MPI_VI_GetChnBind(ViChn + i, &stChnBindAttr);
                if (HI_ERR_VI_FAILED_NOTBIND == s32Ret)
                {

                    stChnBindAttr.ViDev = ViChn/4 ;
                    stChnBindAttr.ViWay = 0 ;
                    s32Ret = HI_MPI_VI_BindChn(ViChn + i, &stChnBindAttr);
                    if (HI_SUCCESS != s32Ret)
                    {
                        SAMPLE_PRT("call HI_MPI_VI_BindChn=%d failed with %#x\n",ViChn +i,s32Ret);
                        return HI_FAILURE;
                    }
                }
                else if(s32Ret != HI_SUCCESS)
                {
                        SAMPLE_PRT("HI_MPI_VI_GetChnBind  failed with %#x!\n", s32Ret);

                }
                else
                {
                    printf("HI_MPI_VI_GetChnBind  sthnbindattr.dev==%d--chn==%d\n",stChnBindAttr.ViDev,ViChn + i);


                }

//                  memcpy(&stChnAttr.stCapRect, &stCapRect, sizeof(RECT_S));
//                  stChnAttr.stDestSize.u32Width = pstTarSize->u32Width;
//                  stChnAttr.stDestSize.u32Height = pstTarSize->u32Height;


                  memcpy(&stChnAttr.stCapRect, &cap_recv, sizeof(RECT_S));

                  stChnAttr.stDestSize.u32Width = starge_size.u32Width;
                  stChnAttr.stDestSize.u32Height = starge_size.u32Height;


                printf("DrcFrmRate=%d,Rate=%d set vi x=%d,y=%d,w=%d,h=%d  pstTarSizew  pstTarSize-w=%d  h=%d vimoude=%d\n",DrcFrmRate,SrcFrmRate,stChnAttr.stCapRect.s32X,stChnAttr.stCapRect.s32Y,    \
                stChnAttr.stCapRect.u32Width,stChnAttr.stCapRect.u32Height,pstTarSize->u32Width,pstTarSize->u32Height,enViMode);
                /* to show scale. this is a sample only, we want to show dist_size = D1 only */

                stChnAttr.enCapSel = VI_CAPSEL_BOTH;
                stChnAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;   /* sp420 or sp422 */
                stChnAttr.bMirror = (VI_CHN_SET_MIRROR == enViChnSet)?HI_TRUE:HI_FALSE;
                stChnAttr.bFlip = (VI_CHN_SET_FILP == enViChnSet)?HI_TRUE:HI_FALSE;
                stChnAttr.s32SrcFrameRate = SrcFrmRate;
                stChnAttr.s32DstFrameRate = DrcFrmRate;




                 stChnAttr.enScanMode = VI_SCAN_PROGRESSIVE;


                 //int i = 0;



//                if((ViDev == 3) && (did_4k_dev == 1)&&((ViChn +i) == 13))
                  //  stChnAttr.enCapSel = VI_CAPSEL_BOTH;

                s32Ret = HI_MPI_VI_SetChnAttr(ViChn +i, &stChnAttr);
                if (s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VI_SetChnAttr failed with %#x!\n", s32Ret);
                    return HI_FAILURE;
                }




                s32Ret = HI_MPI_VI_EnableChn(ViChn + i);
                if (s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VI_EnableChn failed with %#x!\n", s32Ret);
                    return HI_FAILURE;
                }


}


#if 0
        s32Ret = SAMPLE_COMM_VI_StartChn(ViChn, &stCapRect, &stTargetSize, enViMode, VI_CHN_SET_NORMAL );
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("call SAMPLE_COMM_VI_StarChn failed with %#x\n", s32Ret);
            return HI_FAILURE;
        }

#endif








    return HI_SUCCESS;
}
static HI_VOID SAMPLE_COMM_VO_HdmiConvertSync(VO_INTF_SYNC_E enIntfSync,
                                              HI_HDMI_VIDEO_FMT_E *penVideoFmt)
{
    switch (enIntfSync)
    {
        case VO_OUTPUT_PAL:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_PAL;
            break;
        case VO_OUTPUT_NTSC:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_NTSC;
            break;
        case VO_OUTPUT_1080P24:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_1080P_24;
            break;
        case VO_OUTPUT_1080P25:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_1080P_25;
            break;
        case VO_OUTPUT_1080P30:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_1080P_30;
            break;
        case VO_OUTPUT_720P50:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_720P_50;
            break;
        case VO_OUTPUT_720P60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_720P_60;
            break;
        case VO_OUTPUT_1080I50:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_1080i_50;
            break;
        case VO_OUTPUT_1080I60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_1080i_60;
            break;
        case VO_OUTPUT_1080P50:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_1080P_50;
            break;
        case VO_OUTPUT_1080P60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_1080P_60;
            break;
        case VO_OUTPUT_576P50:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_576P_50;
            break;
        case VO_OUTPUT_480P60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_480P_60;
            break;
        case VO_OUTPUT_800x600_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_VESA_800X600_60;
            break;
        case VO_OUTPUT_1024x768_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_VESA_1024X768_60;
            break;
        case VO_OUTPUT_1280x1024_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_VESA_1280X1024_60;
            break;
        case VO_OUTPUT_1366x768_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_VESA_1366X768_60;
            break;
        case VO_OUTPUT_1440x900_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_VESA_1440X900_60;
            break;
        case VO_OUTPUT_1280x800_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_VESA_1280X800_60;
            break;
        case VO_OUTPUT_1600x1200_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_VESA_1600X1200_60;
            break;
        case VO_OUTPUT_2560x1440_30:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_2560x1440_30;
            break;
        case VO_OUTPUT_2560x1600_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_2560x1600_60;
            break;
        case VO_OUTPUT_3840x2160_30:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_3840X2160P_30;
            break;
        case VO_OUTPUT_3840x2160_60:
            *penVideoFmt = HI_HDMI_VIDEO_FMT_3840X2160P_60;
            break;
        default :
            SAMPLE_PRT("Unkonw VO_INTF_SYNC_E value!\n");
            break;
    }

    return;
}

static HI_VOID RHDMI_HotPlug_Proc(HI_VOID *pPrivateData)
{
    HDMI_ARGS_S    stArgs;
    HI_S32         s32Ret = HI_FAILURE;
    HI_HDMI_ATTR_S stAttr;
    HI_HDMI_SINK_CAPABILITY_S stCaps;

    //printf("EVENT: HPD\n");

    CHECK_POINTER_NO_RET(pPrivateData);

    memset(&stAttr, 0, sizeof(HI_HDMI_ATTR_S));
    memset(&stArgs, 0, sizeof(HDMI_ARGS_S));
    memcpy(&stArgs, pPrivateData, sizeof(HDMI_ARGS_S));
    memset(&stCaps, 0, sizeof(HI_HDMI_SINK_CAPABILITY_S));


    s32Ret = HI_MPI_HDMI_GetSinkCapability(stArgs.enHdmi, &stCaps);
    if(s32Ret != HI_SUCCESS)
    {
        printf("get sink caps failed!\n");
    }
    else
    {
        printf("get sink caps success!\n");
    }

    s32Ret = HI_MPI_HDMI_GetAttr(stArgs.enHdmi, &stAttr);
    CHECK_RET_SUCCESS_NO_RET(s32Ret);

    s32Ret = HI_MPI_HDMI_SetAttr(stArgs.enHdmi, &stAttr);
    CHECK_RET_SUCCESS_NO_RET(s32Ret);

    HI_MPI_HDMI_Start(stArgs.enHdmi);

    return;
}

static HI_VOID RHDMI_UnPlug_Proc(HI_VOID *pPrivateData)
{
    HDMI_ARGS_S  stArgs;

    //printf("EVENT: UN-HPD\n");

    CHECK_POINTER_NO_RET(pPrivateData);

    memset(&stArgs, 0, sizeof(HDMI_ARGS_S));
    memcpy(&stArgs, pPrivateData, sizeof(HDMI_ARGS_S));

    HI_MPI_HDMI_Stop(stArgs.enHdmi);

    return;
}

static HI_VOID RHDMI_EventCallBack(HI_HDMI_EVENT_TYPE_E event, HI_VOID *pPrivateData)
{
    printf("i am is RHDMI_EventCallBack\n");
    switch ( event )
    {
        case HI_HDMI_EVENT_HOTPLUG:
          printf("RHDMI_HotPlug_Proc  i am  is--------------\n");
            RHDMI_HotPlug_Proc(pPrivateData);
            break;
        case HI_HDMI_EVENT_NO_PLUG:
         printf("RHDMI_UnPlug_Proc  i am  is--------------------\n");
            RHDMI_UnPlug_Proc(pPrivateData);
            break;
        default:
            break;
    }

    return;
}


HI_S32 HI35xx_COMM_HdmiInit(VO_INTF_SYNC_E enIntfSync, OUTPUT_MODEL enOutputModel, AIO_ATTR_S *pstAioAttr)
   {
       HI_S32              s32Ret = HI_FAILURE;
       HI_HDMI_VIDEO_FMT_E enVideoFmt = HI_HDMI_VIDEO_FMT_1080P_60;

       SAMPLE_COMM_VO_HdmiConvertSync(enIntfSync, &enVideoFmt);

       HI_MPI_HDMI_Init();

       s32Ret = HI_MPI_HDMI_Open(HI_HDMI_ID_0);
       if(s32Ret != HI_SUCCESS)
          printf("HDMI", "HI_MPI_HDMI_Open failed with %x", s32Ret);

      g_stHdmiArgs_.enHdmi = HI_HDMI_ID_0;
      g_stHdmiArgs_.eForceFmt = enVideoFmt;
      g_stHdmiArgs_.enOutputModel = enOutputModel;
       memset(&g_stHdmiArgs_.stAioAttr, 0, sizeof(AIO_ATTR_S));
       if (pstAioAttr != NULL){
           memcpy(&g_stHdmiArgs_.stAioAttr, pstAioAttr, sizeof(AIO_ATTR_S));
       }

       l_stCallbackFunc.pfnHdmiEventCallback = RHDMI_EventCallBack;
       l_stCallbackFunc.pPrivateData = &g_stHdmiArgs_;

       s32Ret = HI_MPI_HDMI_RegCallbackFunc(g_stHdmiArgs_.enHdmi, &l_stCallbackFunc);
       if(s32Ret != HI_SUCCESS)
          printf("HDMI", "HI_MPI_HDMI_RegCallbackFunc failed with %x", s32Ret);

       if(1){
           HI_HDMI_ATTR_S stAttr;
           HI_MPI_HDMI_GetAttr(g_stHdmiArgs_.enHdmi, &stAttr);

           if(enOutputModel == kForceDVI){
               stAttr.bEnableHdmi = HI_FALSE;
               stAttr.enDefaultMode = HI_HDMI_FORCE_DVI;
           }
           else if(enOutputModel == kForceHDMI){
               stAttr.bEnableHdmi = HI_TRUE;
               stAttr.enDefaultMode = HI_HDMI_FORCE_HDMI;
           }
           else{
               stAttr.bEnableHdmi = HI_FALSE;
               stAttr.enDefaultMode = HI_HDMI_FORCE_DVI;
           }

           stAttr.enVideoFmt = enVideoFmt;
           /*
            * HI_HDMI_VIDEO_MODE_RGB444,
            * HI_HDMI_VIDEO_MODE_YCBCR444,
            * HI_HDMI_VIDEO_MODE_YCBCR422
           */
           /** NOTE: ???????hdmi????????DVI????? **/
           stAttr.enVidOutMode = HI_HDMI_VIDEO_MODE_RGB444;
           stAttr.enDeepColorMode = HI_HDMI_DEEP_COLOR_24BIT;
           stAttr.bxvYCCMode = HI_FALSE;
           stAttr.bEnableVideo = HI_TRUE;

           stAttr.bEnableAudio = HI_TRUE;//HI_FALSE;
           stAttr.enSoundIntf = HI_HDMI_SND_INTERFACE_I2S;
           stAttr.bIsMultiChannel = HI_TRUE;//HI_FALSE;

           stAttr.enBitDepth = HI_HDMI_BIT_DEPTH_24;
           stAttr.bEnableAviInfoFrame = HI_TRUE;
           stAttr.bEnableAudInfoFrame = HI_TRUE;
           stAttr.bEnableSpdInfoFrame = HI_FALSE;
           stAttr.bEnableMpegInfoFrame = HI_FALSE;

           stAttr.bDebugFlag = HI_FALSE;
           stAttr.bHDCPEnable = HI_FALSE;
           stAttr.b3DEnable = HI_FALSE;

           // 2015.11.03 - by zhihao
           if (pstAioAttr != NULL)
           {
               stAttr.bEnableAudio = HI_TRUE;
               /**< ??Enable?? */
               stAttr.enSoundIntf = HI_HDMI_SND_INTERFACE_I2S;
               /**< HDMI????, ??HI_HDMI_SND_INTERFACE_I2S,??????AO?????? */
               stAttr.enSampleRate = (HI_HDMI_SAMPLE_RATE_E)pstAioAttr->enSamplerate;
               /**< PCM?????,??????AO??????? */
               stAttr.u8DownSampleParm = HI_FALSE;
               /**< PCM????downsample??????????0 */
               stAttr.enBitDepth = (HI_HDMI_BIT_DEPTH_E)(8 * (pstAioAttr->enBitwidth+1));
               /**< ????????16,??????AO??????? */
               stAttr.u8I2SCtlVbit = 0;
               /**< ???????0, I2S control (0x7A:0x1D) */
               stAttr.bEnableAviInfoFrame = HI_TRUE;
               /**< ???? AVI InfoFrame????? */
               stAttr.bEnableAudInfoFrame = HI_TRUE;;
               /**< ???? AUDIO InfoFrame????? */
           }

           HI_MPI_HDMI_SetAttr(g_stHdmiArgs_.enHdmi, &stAttr);
           HI_MPI_HDMI_Start(g_stHdmiArgs_.enHdmi);
       }

       return HI_SUCCESS;
   }




HI_S32 RSAMPLE_COMM_VO_HdmiStart(VO_INTF_SYNC_E enIntfSync)
{
    HI_HDMI_ATTR_S      stAttr;
    HI_HDMI_VIDEO_FMT_E enVideoFmt;


    SAMPLE_COMM_VO_HdmiConvertSync(enIntfSync, &enVideoFmt);

    HI_MPI_HDMI_Init();

    g_stHdmiArgs_.enHdmi = HI_HDMI_ID_0;
    g_stHdmiArgs_.eForceFmt = enVideoFmt;
   // g_stHdmiArgs_.enOutputModel = kForceHDMI;

     g_stCallbackFunc.pfnHdmiEventCallback = RHDMI_EventCallBack;
    g_stCallbackFunc.pPrivateData = &g_stHdmiArgs_;

    HI_MPI_HDMI_Open(HI_HDMI_ID_0);

    HI_MPI_HDMI_RegCallbackFunc(HI_HDMI_ID_0, &g_stCallbackFunc);

    HI_MPI_HDMI_GetAttr(HI_HDMI_ID_0, &stAttr);

    stAttr.bEnableHdmi = HI_TRUE;
  //  stAttr.enDefaultMode =  HI_HDMI_FORCE_HDMI; //yin

    stAttr.bEnableVideo = HI_TRUE;
    stAttr.enVideoFmt = enVideoFmt;

    stAttr.enVidOutMode = HI_HDMI_VIDEO_MODE_YCBCR444;
    stAttr.enDeepColorMode = HI_HDMI_DEEP_COLOR_OFF;//yin
   // stAttr.enDeepColorMode = HI_HDMI_DEEP_COLOR_24BIT;
    stAttr.bxvYCCMode = HI_FALSE;

    stAttr.bEnableAudio = HI_FALSE;//yin
    stAttr.enSoundIntf = HI_HDMI_SND_INTERFACE_I2S;
    stAttr.bIsMultiChannel = HI_FALSE;//yin

    stAttr.enBitDepth = HI_HDMI_BIT_DEPTH_16;//yin

    stAttr.bEnableAviInfoFrame = HI_TRUE;
    stAttr.bEnableAudInfoFrame = HI_TRUE;
    stAttr.bEnableSpdInfoFrame = HI_FALSE;
    stAttr.bEnableMpegInfoFrame = HI_FALSE;

    stAttr.bDebugFlag = HI_FALSE;
    stAttr.bHDCPEnable = HI_FALSE;

    stAttr.b3DEnable = HI_FALSE;

 //   stAttr.bEnableVidModeAdapt

    HI_MPI_HDMI_SetAttr(HI_HDMI_ID_0, &stAttr);

    //HI_MPI_HDMI_Start(HI_HDMI_ID_0);

    printf("HDMI start success.--\n");
    return HI_SUCCESS;
}
HI_S32 RSAMPLE_COMM_VO_StartChn(VO_LAYER VoLayer, SAMPLE_VO_MODE_E enMode)
{
    HI_S32 i;
    HI_S32 s32Ret = HI_SUCCESS;
    HI_U32 u32WndNum = 0;
    HI_U32 u32Square = 0;
    HI_U32 u32Width = 0;
    HI_U32 u32Height = 0;
    VO_CHN_ATTR_S stChnAttr;
    VO_VIDEO_LAYER_ATTR_S stLayerAttr;

    switch (enMode)
    {
        case VO_MODE_1MUX:
            u32WndNum = 1;
            u32Square = 1;
            break;
        case VO_MODE_4MUX:
            u32WndNum = 4;
            u32Square = 2;
            break;
        case VO_MODE_9MUX:
            u32WndNum = 9;
            u32Square = 3;
            break;
        case VO_MODE_16MUX:
            u32WndNum = 16;
            u32Square = 4;
            break;
        default:
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
    }

    s32Ret = HI_MPI_VO_GetVideoLayerAttr(VoLayer, &stLayerAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    u32Width = stLayerAttr.stImageSize.u32Width;
    u32Height = stLayerAttr.stImageSize.u32Height;

    for (i=0; i<u32WndNum; i++)
    {
        stChnAttr.stRect.s32X       = ALIGN_BACK((u32Width/u32Square) * (i%u32Square), 2);
        stChnAttr.stRect.s32Y       = ALIGN_BACK((u32Height/u32Square) * (i/u32Square), 2);
        stChnAttr.stRect.u32Width   = ALIGN_BACK(u32Width/u32Square, 2);
        stChnAttr.stRect.u32Height  = ALIGN_BACK(u32Height/u32Square, 2);
        stChnAttr.u32Priority       = 0;
        stChnAttr.bDeflicker        = (SAMPLE_VO_LAYER_VSD0 == VoLayer) ? HI_TRUE : HI_FALSE;


        if(VoLayer == SAMPLE_VO_LAYER_VPIP )
            stChnAttr.u32Priority = 1;

        printf("stchnAttrue  wigth=%d, height=%d\n",stChnAttr.stRect.u32Width,stChnAttr.stRect.u32Height);
        s32Ret = HI_MPI_VO_SetChnAttr(VoLayer, i, &stChnAttr);
        if (s32Ret != HI_SUCCESS)
        {
            printf("%s(%d):failed with %#x!\n",\
                   __FUNCTION__,__LINE__,  s32Ret);
            return HI_FAILURE;
        }

        s32Ret = HI_MPI_VO_EnableChn(VoLayer, i);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }
    }
    return HI_SUCCESS;
}



HI_S32 Hi35xx_COMM_VO_StartChn(VO_LAYER VoLayer, SAMPLE_VO_MODE_E enMode,RECT_S winRect)
{
    HI_S32 i;
    HI_S32 s32Ret = HI_SUCCESS;
    HI_U32 u32WndNum = 0;
    HI_U32 u32Square = 0;
    HI_U32 u32Width = 0;
    HI_U32 u32Height = 0;
    VO_CHN_ATTR_S stChnAttr;
    VO_VIDEO_LAYER_ATTR_S stLayerAttr;

    switch (enMode)
    {
        case VO_MODE_1MUX:
            u32WndNum = 1;
            u32Square = 1;
            break;
        case VO_MODE_4MUX:
            u32WndNum = 4;
            u32Square = 2;
            break;
        case VO_MODE_9MUX:
            u32WndNum = 9;
            u32Square = 3;
            break;
        case VO_MODE_16MUX:
            u32WndNum = 16;
            u32Square = 4;
            break;
        default:
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
    }

    s32Ret = HI_MPI_VO_GetVideoLayerAttr(VoLayer, &stLayerAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    u32Width = stLayerAttr.stImageSize.u32Width;
    u32Height = stLayerAttr.stImageSize.u32Height;

    for (i=0; i<u32WndNum; i++)
    {
//        stChnAttr.stRect.s32X       = ALIGN_BACK((u32Width/u32Square) * (i%u32Square), 2);
//        stChnAttr.stRect.s32Y       = ALIGN_BACK((u32Height/u32Square) * (i/u32Square), 2);
//        stChnAttr.stRect.u32Width   = ALIGN_BACK(u32Width/u32Square, 2);
//        stChnAttr.stRect.u32Height  = ALIGN_BACK(u32Height/u32Square, 2);
        stChnAttr.u32Priority       = 0;
        stChnAttr.bDeflicker        = (SAMPLE_VO_LAYER_VSD0 == VoLayer) ? HI_TRUE : HI_FALSE;

        stChnAttr.stRect.s32X       = winRect.s32X;
        stChnAttr.stRect.s32Y       = winRect.s32Y;
        stChnAttr.stRect.u32Width   = winRect.u32Width;
        stChnAttr.stRect.u32Height  = winRect.u32Height;

        if(VoLayer == SAMPLE_VO_LAYER_VPIP )
            stChnAttr.u32Priority = 1;

        printf("winRExt >> setchnAttrue  wigth=%d, height=%d\n",stChnAttr.stRect.u32Width,stChnAttr.stRect.u32Height);
        s32Ret = HI_MPI_VO_SetChnAttr(VoLayer, i, &stChnAttr);
        if (s32Ret != HI_SUCCESS)
        {
            printf("%s(%d):failed with %#x!\n",\
                   __FUNCTION__,__LINE__,  s32Ret);
            return HI_FAILURE;
        }

        s32Ret = HI_MPI_VO_EnableChn(VoLayer, i);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }
    }
    return HI_SUCCESS;
}




HI_S32 HI35xx_COMM_VO_PipStart(VO_DEV VoDev, HI_U32 u32Width, HI_U32 u32Height, HI_U32 u32DispFrmRt)
{
    VO_VIDEO_LAYER_ATTR_S stPipLayerAttr;
    HI_S32 s32Ret;
//SAMPLE_VO_LAYER_VHD1

    printf("HI35xx_COMM_VO_PipStart-----\n");
     HI_S32  vpvp_layep =   SAMPLE_VO_LAYER_VPIP;


         s32Ret = HI_MPI_VO_UnBindVideoLayer(vpvp_layep,SAMPLE_VO_DEV_DHD0);
         if(s32Ret != 0)
         {
             printf("error  un bindSAMPLE_VO_LAYER_VHD1 \n");
         }



//         if(VoDev != SAMPLE_VO_DEV_DHD0  )
//         {
             s32Ret = HI_MPI_VO_BindVideoLayer(vpvp_layep, VoDev); //VoLayer=2 只能是 PIP 层,VoDev 只能是接收动态绑定的设备[0, 1]。PIP 层默认绑定到 DHD0 上。
             if (HI_SUCCESS != s32Ret){
                 SAMPLE_PRT("HI_MPI_VO_BindVideoLayer(PIP) failed with %#x!loay=%d\n", s32Ret,VoDev);
                 return HI_FAILURE;
             }
     //    }
    stPipLayerAttr.bClusterMode = HI_FALSE; /* 视频层上的通道是否采用聚集的方式使用内存 */
    stPipLayerAttr.bDoubleFrame = HI_FALSE;
    stPipLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    stPipLayerAttr.stDispRect.s32X = 0;
    stPipLayerAttr.stDispRect.s32Y = 0;
    stPipLayerAttr.stDispRect.u32Height  = u32Height;
    stPipLayerAttr.stDispRect.u32Width   = u32Width;
    stPipLayerAttr.u32DispFrmRt = u32DispFrmRt;
    stPipLayerAttr.stImageSize.u32Height = u32Height;
    stPipLayerAttr.stImageSize.u32Width  = u32Width;

    s32Ret = SAMPLE_COMM_VO_StartLayer(vpvp_layep, &stPipLayerAttr);
    if(s32Ret != HI_SUCCESS){
        SAMPLE_PRT("SAMPLE_COMM_VO_StartLayer failed!\n");
        return HI_FAILURE;
    }



    return HI_SUCCESS;
}





#define GRAPHICS_LAYER_HC0 3
#define GRAPHICS_LAYER_G0  0
#define GRAPHICS_LAYER_G1  1
HI_S32 HI35xx_COMM_VO_StartGraphicLayer(POINT_S stRes, POINT_S stResVirtual, POINT_S stOffset)
{
    HI_S32 s32Ret;

    //启动并绑定图形层。
    s32Ret = HI_MPI_VO_UnBindGraphicLayer(GRAPHICS_LAYER_HC0, SAMPLE_VO_DEV_DHD0);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("UnBindGraphicLayer failed with %d!\n", s32Ret);
        return s32Ret;
    }

    s32Ret = HI_MPI_VO_BindGraphicLayer(GRAPHICS_LAYER_HC0, SAMPLE_VO_DEV_DHD0);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("BindGraphicLayer failed with %d!\n", s32Ret);
        return s32Ret;
    }

    //设置图形层。 2018.12.28 -by zhsun
    HI_CHAR file[12] = "/dev/fb0";
    struct fb_var_screeninfo stVarInfo;
    HIFB_LAYER_INFO_S stLayerinfo;
    HIFB_DDRZONE_S stDDRZonePara;
    /* 1. open framebuffer device overlay 0 */
    HI_S32 guifb = open(file, O_RDWR, 0);
    if(guifb < 0)
    {
        SAMPLE_PRT("open %s failed!\n",file);
        return s32Ret;
    }
    s32Ret = ioctl (guifb, FBIOGET_VSCREENINFO, &stVarInfo);
    if (s32Ret < 0)
    {
        SAMPLE_PRT("FBIOGET_VSCREENINFO failed!\n");
        close(guifb);
        return s32Ret;
    }

    struct fb_bitfield s_a32 = {24,8,0};
    struct fb_bitfield s_r32 = {16,8,0};
    struct fb_bitfield s_g32 = {8,8,0};
    struct fb_bitfield s_b32 = {0,8,0};
    stVarInfo.red            = s_r32;
    stVarInfo.green          = s_g32;
    stVarInfo.blue           = s_b32;
    stVarInfo.transp         = s_a32;
    stVarInfo.activate       = FB_ACTIVATE_NOW;
    stVarInfo.bits_per_pixel = 32;
    /*
      ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐   ╌╌╌
      ╎                ↑        ╎    ↑
      ╎        yoffset ↓        ╎    ╎
      ╎       ┌╌╌╌╌╌╌╌╌╌┐       ╎    ╎
      ╎←-----→╎         ╎       ╎  yres_virtual
      ╎xoffset╎ 显示图像 ╎ yres  ╎    ╎
      ╎       ╎         ╎       ╎    ╎
      ╎       └╌╌╌╌╌╌╌╌╌┘       ╎    ╎
      ╎          xres           ╎    ╎
      ╎                         ╎    ↓
      └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘   ╌╌╌
      |←------xres_virtual-----→|
     */
    stVarInfo.xres           = stRes.s32X;
    stVarInfo.yres           = stRes.s32Y;
    stVarInfo.xres_virtual   = stResVirtual.s32X;
    stVarInfo.yres_virtual   = stResVirtual.s32Y;
    stVarInfo.xoffset        = stOffset.s32X;
    stVarInfo.yoffset        = stOffset.s32Y;

    s32Ret = ioctl (guifb, FBIOPUT_VSCREENINFO, &stVarInfo);
    if (s32Ret < 0)
    {
        SAMPLE_PRT("FBIOPUT_VSCREENINFO failed!\n");
        close(guifb);
        return s32Ret;
    }

   #if 0
    HIFB_POINT_S stPoint = {stOffset.s32X, stOffset.s32Y};
    s32Ret = ioctl (guifb, FBIOPUT_SCREEN_ORIGIN_HIFB, &stPoint);
    if (s32Ret < 0)
    {
        SAMPLE_PRT("FBIOPUT_SCREEN_ORIGIN_HIFB failed!\n");
        close(guifb);
        return s32Ret;
    }
#endif

    s32Ret = ioctl(guifb, FBIOGET_LAYER_INFO, &stLayerinfo);
    if (s32Ret < 0)
    {
        SAMPLE_PRT("FBIOGET_LAYER_INFO failed!\n");
        close(guifb);
        return s32Ret;
    }

    stLayerinfo.u32Mask = 0;
    stLayerinfo.BufMode = HIFB_LAYER_BUF_NONE;
    stLayerinfo.u32Mask |= HIFB_LAYERMASK_BUFMODE;
    s32Ret = ioctl(guifb, FBIOPUT_LAYER_INFO, &stLayerinfo);
    if (s32Ret < 0)
    {
        SAMPLE_PRT("FBIOPUT_LAYER_INFO failed!\n");
        close(guifb);
        return s32Ret;
    }
    stDDRZonePara.u32StartSection = 0;
    stDDRZonePara.u32ZoneNums = 15;
    s32Ret = ioctl(guifb, FBIOPUT_MDDRDETECT_HIFB, &stDDRZonePara);
    if (s32Ret < 0)
    {
        SAMPLE_PRT("FBIOPUT_MDDRDETECT_HIFB failed!\n");
        close(guifb);
        return s32Ret;
    }
    /* 2. open compress */
    HI_BOOL bEnable = HI_TRUE;
    s32Ret = ioctl(guifb, FBIOPUT_COMPRESSION_HIFB, &bEnable);
    if (s32Ret < 0)
    {
        SAMPLE_PRT("FBIOPUT_COMPRESSION_HIFB failed!\n");
        close(guifb);
        return s32Ret;
    }

    return s32Ret;
}

   HI_VOID HI35xx_SMAPLE_VO_SIZE(SIZE_S Visize,VO_INTF_SYNC_E * VOintf)
    {
       SIZE_S viSize = Visize;

       if((viSize.u32Width == 1280 )&&(viSize.u32Height == 720))
               *VOintf = VO_OUTPUT_720P60;
       else if((viSize.u32Width == 1920 )&&(viSize.u32Height == 1080))
                 *VOintf = VO_OUTPUT_1080P60;
       else if((viSize.u32Width == 3840 )&&(viSize.u32Height == 2160))
                 *VOintf = VO_OUTPUT_3840x2160_30;
       else if((viSize.u32Width == 1920 )&&(viSize.u32Height == 2160))
                 *VOintf = VO_OUTPUT_1920x2160_30;
       else if((viSize.u32Width == 1024 )&&(viSize.u32Height == 768))
                 *VOintf = VO_OUTPUT_1024x768_60;
       else if((viSize.u32Width == 1440 )&&(viSize.u32Height == 900))
                 *VOintf = VO_OUTPUT_1440x900_60;
        else
          printf("have`nt HI35xx_SMAPLE_VO_SIZE\n");

    }


   HI_S32 HI35xx_COMM_VO_StartDevLayer( HI_U32 edid_4k,VO_DEV VoDev, VO_INTF_SYNC_E intfSync, HI_U32 dispWidth, HI_U32 dispHeight, HI_U32 frameRt, HI_U32 bgColor, OUTPUT_MODEL   HD_DVI_NO )
   {
       HI_S32                  s32Ret;
       VO_LAYER                VoLayer;
       VO_PUB_ATTR_S           stVoPubAttr;
       VO_VIDEO_LAYER_ATTR_S   stVoLayerAttr;
     //  OUTPUT_MODEL            HD_DVI_NO ;


       /*
        * 接口类型配置为 VO_INTF_CVBS 时，enIntfSync 的取值范围为[0, 3]。
        * 接口类型配置为 VO_INTF_BT1120、VO_INTF_HDMI、VO_INTF_VGA 时，
        * enIntfSync 的取值范围为[2, 22]。
        */



        switch (VoDev) {
           case SAMPLE_VO_DEV_DHD0:
           VoLayer = SAMPLE_VO_LAYER_VHD0;
           stVoPubAttr.enIntfSync = intfSync;
           if(intfSync >= VO_OUTPUT_1920x2160_30) /** NOTE: 这3种分辨率不支持VGA **/
           {

               stVoPubAttr.enIntfType = VO_INTF_HDMI;
            }
           else
           {
               stVoPubAttr.enIntfType = VO_INTF_VGA | VO_INTF_BT1120;
           }
           break;
           case SAMPLE_VO_DEV_DHD1:
               VoLayer = SAMPLE_VO_LAYER_VHD1;
               stVoPubAttr.enIntfSync = intfSync;
               stVoPubAttr.enIntfType = VO_INTF_HDMI;
               //  stVoPubAttr.enIntfType = VO_INTF_VGA | VO_INTF_BT1120;
               break;
           case SAMPLE_VO_DEV_DSD0:
               VoLayer = SAMPLE_VO_LAYER_VSD0;
               stVoPubAttr.enIntfSync = intfSync;
               stVoPubAttr.enIntfType = VO_INTF_HDMI | VO_INTF_VGA | VO_INTF_BT1120;
               break;
           default:
               VoLayer = SAMPLE_VO_LAYER_VHD0;
               stVoPubAttr.enIntfSync = intfSync;
               stVoPubAttr.enIntfType = VO_INTF_HDMI | VO_INTF_VGA | VO_INTF_BT1120;
               break;
       }
       stVoPubAttr.u32BgColor = bgColor;
   #if 0

   #endif
   #if 0 //计算pll值

   #endif

       if ((edid_4k == 1) && (SAMPLE_VO_LAYER_VHD0 == VoDev))
         stVoPubAttr.enIntfType = VO_INTF_HDMI;




//             if ((SAMPLE_VO_DEV_DHD0 == VoDev))
//          stVoPubAttr.enIntfType = VO_INTF_HDMI;
       //  stVoPubAttr.enIntfType = VO_INTF_HDMI | VO_INTF_VGA | VO_INTF_BT1120;

             printf(" stVoPubAttr.enIntfSync = intfSync dev h=%d\n", stVoPubAttr.enIntfSync = intfSync);

       s32Ret = SAMPLE_COMM_VO_StartDev(VoDev, &stVoPubAttr);
       if(s32Ret != HI_SUCCESS)
       {
           SAMPLE_PRT("SAMPLE_COMM_VO_StartDev fail for %#x!\n", s32Ret);
       }

       if (stVoPubAttr.enIntfType & VO_INTF_HDMI)
       {
   //        if (HI_SUCCESS != RSAMPLE_COMM_VO_HdmiStart(stVoPubAttr.enIntfSync))
   //        {
   //            SAMPLE_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
   //        }

           HI35xx_COMM_HdmiInit(stVoPubAttr.enIntfSync,HD_DVI_NO,NULL);
       }






       stVoLayerAttr.bClusterMode = HI_FALSE; /* 视频层上的通道是否采用聚集的方式使用内存 */
       stVoLayerAttr.bDoubleFrame = HI_FALSE;
       stVoLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
       stVoLayerAttr.stDispRect.u32Width = dispWidth;
       stVoLayerAttr.stDispRect.u32Height = dispHeight;
       stVoLayerAttr.u32DispFrmRt = frameRt;
       stVoLayerAttr.stImageSize.u32Width = stVoLayerAttr.stDispRect.u32Width;
       stVoLayerAttr.stImageSize.u32Height = stVoLayerAttr.stDispRect.u32Height;

       s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer, &stVoLayerAttr);
       if(s32Ret != HI_SUCCESS)
       {
           SAMPLE_PRT("SAMPLE_COMM_VO_StartLayer fail for %#x!\n", s32Ret);
       }





       return s32Ret;
   }




#if 0

HI_S32 HI35xx_COMM_VO_StartDevLayer( HI_U32 edid_4k,VO_DEV VoDev, VO_INTF_SYNC_E intfSync, HI_U32 dispWidth, HI_U32 dispHeight, HI_U32 frameRt, HI_U32 bgColor )
   {
       HI_S32                  s32Ret;
       VO_LAYER                VoLayer;
       VO_PUB_ATTR_S           stVoPubAttr;
       VO_VIDEO_LAYER_ATTR_S   stVoLayerAttr;

       /*
        * 接口类型配置为 VO_INTF_CVBS 时，enIntfSync 的取值范围为[0, 3]。
        * 接口类型配置为 VO_INTF_BT1120、VO_INTF_HDMI、VO_INTF_VGA 时，
        * enIntfSync 的取值范围为[2, 22]。
        */



        switch (VoDev) {
           case SAMPLE_VO_DEV_DHD0:
           VoLayer = SAMPLE_VO_LAYER_VHD0;
           stVoPubAttr.enIntfSync = intfSync;
           if(intfSync >= VO_OUTPUT_1920x2160_30) /** NOTE: 这3种分辨率不支持VGA **/
           {

               stVoPubAttr.enIntfType = VO_INTF_HDMI;
            }
           else
           {
               stVoPubAttr.enIntfType = VO_INTF_VGA | VO_INTF_BT1120;
            }
           break;
           case SAMPLE_VO_DEV_DHD1:
               VoLayer = SAMPLE_VO_LAYER_VHD1;
               stVoPubAttr.enIntfSync = intfSync;
               stVoPubAttr.enIntfType = VO_INTF_HDMI;
               break;
           case SAMPLE_VO_DEV_DSD0:
               VoLayer = SAMPLE_VO_LAYER_VSD0;
               stVoPubAttr.enIntfSync = intfSync;
               stVoPubAttr.enIntfType = VO_INTF_HDMI | VO_INTF_VGA | VO_INTF_BT1120;
               break;
           default:
               VoLayer = SAMPLE_VO_LAYER_VHD0;
               stVoPubAttr.enIntfSync = intfSync;
               stVoPubAttr.enIntfType = VO_INTF_HDMI | VO_INTF_VGA | VO_INTF_BT1120;
               break;
       }
       stVoPubAttr.u32BgColor = bgColor;
   #if 0

   #endif
   #if 0 //计算pll值

   #endif

             if ((edid_4k == 1) && (SAMPLE_VO_LAYER_VHD0 == VoDev))
          stVoPubAttr.enIntfType = VO_INTF_HDMI;
       //  stVoPubAttr.enIntfType = VO_INTF_HDMI | VO_INTF_VGA | VO_INTF_BT1120;

             printf(" stVoPubAttr.enIntfSync = intfSync dev h=%d\n", stVoPubAttr.enIntfSync = intfSync);

       s32Ret = SAMPLE_COMM_VO_StartDev(VoDev, &stVoPubAttr);
       if(s32Ret != HI_SUCCESS)
       {
           SAMPLE_PRT("SAMPLE_COMM_VO_StartDev fail for %#x!\n", s32Ret);
       }

       if (stVoPubAttr.enIntfType & VO_INTF_HDMI)
       {
           if (HI_SUCCESS != RSAMPLE_COMM_VO_HdmiStart(stVoPubAttr.enIntfSync))
           {
               SAMPLE_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
           }
       }






       stVoLayerAttr.bClusterMode = HI_FALSE; /* 视频层上的通道是否采用聚集的方式使用内存 */
       stVoLayerAttr.bDoubleFrame = HI_FALSE;
       stVoLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
       stVoLayerAttr.stDispRect.u32Width = dispWidth;
       stVoLayerAttr.stDispRect.u32Height = dispHeight;
       stVoLayerAttr.u32DispFrmRt = frameRt;
       stVoLayerAttr.stImageSize.u32Width = stVoLayerAttr.stDispRect.u32Width;
       stVoLayerAttr.stImageSize.u32Height = stVoLayerAttr.stDispRect.u32Height;

       s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer, &stVoLayerAttr);
       if(s32Ret != HI_SUCCESS)
       {
           SAMPLE_PRT("SAMPLE_COMM_VO_StartLayer fail for %#x!\n", s32Ret);
       }





       return s32Ret;
   }
#endif



HI_S32 HI35xx_COMM_VI_Start(VI_DEV ViDev, VI_CHN ViChn, SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type)
{

    HI_S32 i;
    HI_S32 s32Ret;
    SAMPLE_VI_PARAM_S stViParam;
    SIZE_S stTargetSize;

    /*** get parameter from Sample_Vi_Mode ***/
    /*s32Ret = SAMPLE_COMM_VI_Mode2Param(enViMode, &stViParam);
    if (HI_SUCCESS !=s32Ret)
    {
        SAMPLE_PRT("vi get param failed!\n");
        return HI_FAILURE;
    }
    SAMPLE_PRT("chris ViDevCnt=%d,ViDevInterval=%d,ViChnCnt=%d,ViChnInterval=%d\n",stViParam.s32ViDevCnt,stViParam.s32ViDevInterval,stViParam.s32ViChnCnt,stViParam.s32ViChnInterval);*/
    stTargetSize.u32Width  = stCapRect.u32Width;
    stTargetSize.u32Height = stCapRect.u32Height;

    SAMPLE_PRT("setvi stCapRect x=%d,y=%d,width=%d,height=%d\n",stCapRect.s32X,stCapRect.s32Y,stCapRect.u32Width,stCapRect.u32Height);
    SAMPLE_PRT("setvi stTargetSize width=%d,height=%d\n",stTargetSize.u32Width,stTargetSize.u32Height);

    /*** Start VI Dev ***/
    s32Ret = HI35xx_COMM_VI_StartDev(ViDev, enViMode);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VI_StartDev failed with %#x\n", s32Ret);
        return HI_FAILURE;
    }

    /*** Start VI Chn ***/
    s32Ret = HI35xx_COMM_VI_StartChn(ViChn, &stCapRect, &stTargetSize, input_type, VI_CHN_SET_NORMAL);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("call SAMPLE_COMM_VI_StarChn failed with %#x\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_StartChn(VI_CHN ViChn, RECT_S *pstCapRect, SIZE_S *pstTarSize,
                               HI_S32 input_type, SAMPLE_VI_CHN_SET_E enViChnSet)
{
    HI_S32 s32Ret;
    VI_CHN_ATTR_S stChnAttr;

    /* step  5: config & start vicap dev */
    memcpy(&stChnAttr.stCapRect, pstCapRect, sizeof(RECT_S));
    /* to show scale. this is a sample only, we want to show dist_size = D1 only */
    stChnAttr.stDestSize.u32Width = pstTarSize->u32Width;
    stChnAttr.stDestSize.u32Height = pstTarSize->u32Height;
    stChnAttr.enCapSel = VI_CAPSEL_BOTH;

    stChnAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;   /* sp420 or sp422 */
    stChnAttr.bMirror = (VI_CHN_SET_MIRROR == enViChnSet)?HI_TRUE:HI_FALSE;
    stChnAttr.bFlip = (VI_CHN_SET_FILP == enViChnSet)?HI_TRUE:HI_FALSE;
    stChnAttr.s32SrcFrameRate = -1;
    stChnAttr.s32DstFrameRate = -1;
    if(input_type == 0)
        stChnAttr.enScanMode =  VI_SCAN_INTERLACED;
    else if(input_type == 1)
        stChnAttr.enScanMode =  VI_SCAN_PROGRESSIVE;

    printf("enScanMode %d\n",(VI_SCAN_MODE_E)stChnAttr.enScanMode);

    s32Ret = HI_MPI_VI_SetChnAttr(ViChn, &stChnAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VI_EnableChn(ViChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_Stop(VI_DEV ViDev, VI_CHN ViChn)
{
    HI_S32 s32Ret;
    /*** Stop VI Chn ***/
    s32Ret = HI_MPI_VI_DisableChn(ViChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VI_StopChn failed with %#x\n",s32Ret);
        return HI_FAILURE;
    }
    s32Ret = HI_MPI_VI_DisableChn(ViChn +1);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VI_StopChn failed with %#x\n",s32Ret);
        return HI_FAILURE;
    }

    /*** Stop VI Dev ***/
    s32Ret = HI_MPI_VI_DisableDev(ViDev);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VI_StopDev %d failed with %#x\n",ViDev, s32Ret);
        return HI_FAILURE;
    }
    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_bind_vi_venc(VI_DEV ViDev,VI_CHN Vichn,VENC_CHN Vencchn)
{
    HI_S32 s32Ret = HI_SUCCESS;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;
    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = Vichn;

    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = Vencchn;
    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with vi--bind--venc--%#x!\n", s32Ret);
        return HI_FAILURE;
    }
    return s32Ret;

}



HI_S32 HI35xx_COMM_VI_BindVpss(VI_DEV ViDev, VPSS_GRP VpssGrp,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VPSS;
    stDestChn.s32DevId = VpssGrp;
    stDestChn.s32ChnId = 0;

    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

char *getFileAll(char *pBuf, char *fname,int *length)
{
    int  fileLight = 0;
    FILE *pFile;

    pFile = fopen(fname,"r");
    if(pFile == NULL)
    {
        printf("Open file %s fail\n",pFile);
        sprintf(pBuf, "%s", "null");
        return pBuf;
    }
   // printf("getFileAll");
    fseek(pFile,0,SEEK_END);
    fileLight = ftell(pFile);
    rewind(pFile);
    fread(pBuf,1,fileLight,pFile);
    pBuf[fileLight]=0;
    fclose(pFile);
    *length = fileLight;
    return pBuf;
}

HI_BOOL HI35xx_IVE_MAP(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *stSrcFrame, POINT_S stDstPoint, RECT_S stCropRect, HI_BOOL bTimeOut, HI_BOOL uv_IVE)
{
    IVE_HANDLE IveHandle;
    IVE_SRC_IMAGE_S stSrc;
    IVE_SRC_IMAGE_S stDst;
    HI_BOOL bInstant = HI_TRUE;
    HI_BOOL bFinish;
    HI_U32 s32Ret;

    // NOTE: 保证拷贝源的图像和目标图像有交集
    if(stDstPoint.s32X + (HI_S32)stCropRect.u32Width <= 0 ||
       stDstPoint.s32X >= (HI_S32)pstDstFrame->stVFrame.u32Width)
        return HI_TRUE;
    if(stDstPoint.s32Y + (HI_S32)stCropRect.u32Height <= 0 ||
       stDstPoint.s32Y >= (HI_S32)pstDstFrame->stVFrame.u32Height)
        return HI_TRUE;
    // NOTE: 计算落在目标图像中的拷贝源区域,并重新计算拷贝目标左上角坐标
    if(stDstPoint.s32X < 0){
        stCropRect.s32X = 0 - stDstPoint.s32X;
        stCropRect.u32Width -= stCropRect.s32X;
        stDstPoint.s32X = 0;
    }
    if(stDstPoint.s32Y < 0){
        stCropRect.s32Y = 0 - stDstPoint.s32Y;
        stCropRect.u32Height -= stCropRect.s32Y;
        stDstPoint.s32Y = 0;
    }
    if( stCropRect.s32X + stCropRect.u32Width >= pstDstFrame->stVFrame.u32Width)
        stCropRect.u32Width = pstDstFrame->stVFrame.u32Width - stDstPoint.s32X;
    if( stCropRect.s32Y + stCropRect.u32Height >= pstDstFrame->stVFrame.u32Height)
        stCropRect.u32Height = pstDstFrame->stVFrame.u32Height - stDstPoint.s32Y;

    // COPY ----- Y
    //增加支持4K
    stSrc.u16Stride[0]  = stSrcFrame->stVFrame.u32Stride[0];
    //stSrc.u16Stride  = stSrcFrame->stVFrame.u32Stride[1];
    stSrc.enType = IVE_IMAGE_TYPE_U8C1;

    stDst.enType = IVE_IMAGE_TYPE_U8C1;
    stDst.u16Stride[0]  = pstDstFrame->stVFrame.u32Stride[0];
    // stDst.u16Stride  = stDstFrame->stVFrame.u32Stride[1];

    //    int area = 0;
    POINT_S bk_dest_pt = stDstPoint;
    RECT_S bk_crop_rect = stCropRect;

    while((int)bk_crop_rect.u32Height != 0){
        bk_crop_rect.u32Width = stCropRect.u32Width;
        bk_crop_rect.s32X = stCropRect.s32X;
        bk_dest_pt.s32X = stDstPoint.s32X;

        while((int)bk_crop_rect.u32Width != 0){
            stSrc.u16Width = MIN((int)bk_crop_rect.u32Width, 1920);
            stSrc.u16Height = MIN((int)bk_crop_rect.u32Height, DMA_MIN_ROW);
            stSrc.pu8VirAddr[0] = (HI_U8*)stSrcFrame->stVFrame.pVirAddr[0] + (int)stSrc.u16Stride[0] * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.u32PhyAddr[0] = stSrcFrame->stVFrame.u32PhyAddr[0] + (int)stSrc.u16Stride[0] * bk_crop_rect.s32Y + bk_crop_rect.s32X;;

            stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[0] + (int)stDst.u16Stride[0] * bk_dest_pt.s32Y + bk_dest_pt.s32X;;
            stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[0] + (int)stDst.u16Stride[0] * bk_dest_pt.s32Y + bk_dest_pt.s32X;;
            stDst.u16Width =  stSrc.u16Width;
            stDst.u16Height  =  stSrc.u16Height;
            s32Ret=HI_MPI_IVE_Map(&IveHandle, &stSrc, &pstMap, &stDst, bInstant);
            if(HI_SUCCESS!=s32Ret){
                SAMPLE_PRT("HI_MPI_IVE_Map failed with %x", s32Ret);
                HI_MPI_SYS_MmzFree(stSrc.u32PhyAddr[0], stSrc.pu8VirAddr[0]);
                HI_MPI_SYS_MmzFree(stDst.u32PhyAddr[0], stDst.pu8VirAddr[0]);
                HI_MPI_SYS_MmzFree(pstMap.u32PhyAddr,pstMap.pu8VirAddr);
                return HI_FALSE;
            }

            // COPY ----- UV
            if(uv_IVE == HI_TRUE){
                stSrc.pu8VirAddr[0] = (HI_U8*)stSrcFrame->stVFrame.pVirAddr[1] + (int)stSrc.u16Stride[1] * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
                stSrc.u32PhyAddr[0] = stSrcFrame->stVFrame.u32PhyAddr[1] + (int)stSrc.u16Stride[1] * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
                stSrc.u16Width  =  MIN((int)bk_crop_rect.u32Width, 1920);
                stSrc.u16Height =  MIN((int)bk_crop_rect.u32Height, DMA_MIN_ROW) / 2;

                stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[1];
                stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[1] + (int)stDst.u16Stride[1] * bk_dest_pt.s32Y / 2 + bk_dest_pt.s32X;
                stDst.u16Width =  stSrc.u16Width;
                stDst.u16Height  =  stSrc.u16Height;

                s32Ret=HI_MPI_IVE_Map(&IveHandle, &stSrc, &pstMap, &stDst, bInstant);
                if(HI_SUCCESS!=s32Ret){
                    SAMPLE_PRT("HI_MPI_IVE_Map failed with %x", s32Ret);
                    HI_MPI_SYS_MmzFree(stSrc.u32PhyAddr[0], stSrc.pu8VirAddr[0]);
                    HI_MPI_SYS_MmzFree(stDst.u32PhyAddr[0], stDst.pu8VirAddr[0]);
                    HI_MPI_SYS_MmzFree(pstMap.u32PhyAddr,pstMap.pu8VirAddr);
                    return HI_FALSE;
                }
            }
            int act_w = MIN((int)bk_crop_rect.u32Width, 1920);
            bk_crop_rect.u32Width -= act_w;
            bk_crop_rect.s32X += act_w;
            bk_dest_pt.s32X += act_w;
        }
        int act_h = MIN((int)bk_crop_rect.u32Height, DMA_MIN_ROW);
        bk_crop_rect.u32Height -= act_h;
        bk_crop_rect.s32Y += act_h;
        bk_dest_pt.s32Y += act_h;
    }

    s32Ret = HI_MPI_IVE_Query(IveHandle, &bFinish, bTimeOut);
    if (HI_SUCCESS != s32Ret){
        return HI_FALSE;
    }

    return HI_TRUE;
}

HI_S32 HI35xx_COMM_VI_BindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;

    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_UnBindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;

    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_UnBindVpss(VI_DEV ViDev, VENC_CHN VpssGrp,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VPSS;
    stDestChn.s32DevId = VpssGrp;
    stDestChn.s32ChnId = 0;

    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}



HI_S32 HI35xx_COMM_VI_BindVo(VI_DEV ViDev, VI_CHN ViChn, VO_LAYER VoLayer, VO_CHN VoChn)
{
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VOU;
    stDestChn.s32DevId = VoLayer;
    stDestChn.s32ChnId = VoChn;

    return HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
}

HI_S32 HI35xx_COMM_VPSS_Start_1(HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr)
{
    VPSS_GRP VpssGrp;
    VPSS_CHN VpssChn;
    VPSS_GRP_ATTR_S stGrpAttr;
    VPSS_CHN_ATTR_S stChnAttr;
    VPSS_GRP_PARAM_S stVpssParam;
    VPSS_CHN_MODE_S stVpssMode;
    HI_S32 s32Ret;
    HI_S32 i, j;

    /*** Set Vpss Grp Attr ***/

    if(NULL == pstVpssGrpAttr)
    {
        stGrpAttr.u32MaxW = pstSize->u32Width;
        stGrpAttr.u32MaxH = pstSize->u32Height;
        stGrpAttr.enPixFmt = SAMPLE_PIXEL_FORMAT;

        stGrpAttr.bIeEn = HI_FALSE;
        stGrpAttr.bNrEn = HI_FALSE;
        stGrpAttr.bDciEn = HI_FALSE;
        stGrpAttr.bHistEn = HI_FALSE;
        stGrpAttr.bEsEn = HI_FALSE;
        stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
    }
    else
    {
        memcpy(&stGrpAttr,pstVpssGrpAttr,sizeof(VPSS_GRP_ATTR_S));
    }

    for(i=0; i<s32GrpCnt; i++)
    {
        VpssGrp = i;
        /*** create vpss group ***/
        s32Ret = HI_MPI_VPSS_CreateGrp(VpssGrp, &stGrpAttr);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_CreateGrp failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        /*** set vpss param ***/
        s32Ret = HI_MPI_VPSS_GetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        stVpssParam.u32IeStrength = 0;
        s32Ret = HI_MPI_VPSS_SetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        /*** enable vpss chn, with frame ***/
        for(j=0; j<s32ChnCnt; j++)
        {
            VpssChn = j;

            if((VpssChn == 0) && (VpssGrp < 4)){
                HI_U32 u32Depth = 3;
                s32Ret = HI_MPI_VPSS_GetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_GetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }
                stVpssMode.enChnMode = VPSS_CHN_MODE_USER;
                stVpssMode.enPixelFormat = PIXEL_FORMAT_YUV_SEMIPLANAR_420;
                stVpssMode.u32Width = 1920;
                stVpssMode.u32Height = 1080;
                s32Ret = HI_MPI_VPSS_SetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }
                s32Ret = HI_MPI_VPSS_SetDepth(VpssGrp,VpssChn,u32Depth);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetDepth failed with %#x\n", s32Ret);
                    return s32Ret;
                }
            }

            /* Set Vpss Chn attr */
            stChnAttr.bSpEn = HI_FALSE;
            stChnAttr.bUVInvert = HI_FALSE;
            stChnAttr.bBorderEn = HI_FALSE;
            stChnAttr.stBorder.u32Color = 0xffffff;
            stChnAttr.stBorder.u32LeftWidth = 2;
            stChnAttr.stBorder.u32RightWidth = 2;
            stChnAttr.stBorder.u32TopWidth = 2;
            stChnAttr.stBorder.u32BottomWidth = 2;

            s32Ret = HI_MPI_VPSS_SetChnAttr(VpssGrp, VpssChn, &stChnAttr);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_SetChnAttr failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }

            s32Ret = HI_MPI_VPSS_EnableChn(VpssGrp, VpssChn);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_EnableChn failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }



        }

        /*** start vpss group ***/
        s32Ret = HI_MPI_VPSS_StartGrp(VpssGrp);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_StartGrp failed with %#x\n", s32Ret);
            return HI_FAILURE;
        }

    }
    return HI_SUCCESS;
}





#if 0
HI_S32 HI35xx_COMM_VPSS_Start(HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr)
{
    VPSS_GRP VpssGrp;
    VPSS_CHN VpssChn;
    VPSS_CHN_MODE_S stVpssMode;
    HI_U32 u32Depth = 3;
    VPSS_GRP_ATTR_S stGrpAttr;
    VPSS_CHN_ATTR_S stChnAttr;
    VPSS_GRP_PARAM_S stVpssParam;
    HI_S32 s32Ret;
    HI_S32 i, j;

    /*** Set Vpss Grp Attr ***/

    if(NULL == pstVpssGrpAttr)
    {
        stGrpAttr.u32MaxW = pstSize->u32Width;
        stGrpAttr.u32MaxH = pstSize->u32Height;
        stGrpAttr.enPixFmt = SAMPLE_PIXEL_FORMAT;

        stGrpAttr.bIeEn = HI_FALSE;
        stGrpAttr.bNrEn = HI_TRUE;
        stGrpAttr.bDciEn = HI_FALSE;
        stGrpAttr.bHistEn = HI_FALSE;
        stGrpAttr.bEsEn = HI_FALSE;
        stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
    }
    else
    {
        memcpy(&stGrpAttr,pstVpssGrpAttr,sizeof(VPSS_GRP_ATTR_S));
    }


    for(i=0; i<s32GrpCnt; i++)
    {
        VpssGrp = i;
        /*** create vpss group ***/
        s32Ret = HI_MPI_VPSS_CreateGrp(VpssGrp, &stGrpAttr);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_CreateGrp failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        /*** set vpss param ***/
        s32Ret = HI_MPI_VPSS_GetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        stVpssParam.u32IeStrength = 0;
        s32Ret = HI_MPI_VPSS_SetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        /*** enable vpss chn, with frame ***/
        for(j=0; j<s32ChnCnt; j++)
        {
            VpssChn = j;
            if(VpssChn == 0){
                s32Ret = HI_MPI_VPSS_GetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_GetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }
                stVpssMode.enChnMode = VPSS_CHN_MODE_USER;
                stVpssMode.enPixelFormat = PIXEL_FORMAT_YUV_SEMIPLANAR_420;
                stVpssMode.u32Width = 1920;
                stVpssMode.u32Height = 1080;
                s32Ret = HI_MPI_VPSS_SetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }
                s32Ret = HI_MPI_VPSS_SetDepth(VpssGrp,VpssChn,u32Depth);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetDepth failed with %#x\n", s32Ret);
                    return s32Ret;
                }
            }
            /* Set Vpss Chn attr */
            stChnAttr.bSpEn = HI_FALSE;
            stChnAttr.bUVInvert = HI_FALSE;
            stChnAttr.bBorderEn = HI_FALSE;  //去边框
            stChnAttr.stBorder.u32Color = 0xffffff;
            stChnAttr.stBorder.u32LeftWidth = 2;
            stChnAttr.stBorder.u32RightWidth = 2;
            stChnAttr.stBorder.u32TopWidth = 2;
            stChnAttr.stBorder.u32BottomWidth = 2;

            s32Ret = HI_MPI_VPSS_SetChnAttr(VpssGrp, VpssChn, &stChnAttr);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_SetChnAttr failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }



            s32Ret = HI_MPI_VPSS_EnableChn(VpssGrp, VpssChn);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_EnableChn failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }
        }

        /*** start vpss group ***/
        s32Ret = HI_MPI_VPSS_StartGrp(VpssGrp);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_StartGrp failed with %#x\n", s32Ret);
            return HI_FAILURE;
        }

    }
    return HI_SUCCESS;
}
#endif

HI_S32 HI35xx_COMM_VPSS_Startvo(HI_S32 s32Grpstart,HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr,HI_U32 edid4k)
{
    VPSS_GRP VpssGrp;
    VPSS_CHN VpssChn;
    VPSS_GRP_ATTR_S stGrpAttr;
    VPSS_CHN_ATTR_S stChnAttr;
    VPSS_GRP_PARAM_S stVpssParam;
    VPSS_CHN_MODE_S stVpssMode;
    HI_S32 s32Ret;
    HI_S32 i, j;
   HI_S32 vi_gop = 0;
    /*** Set Vpss Grp Attr ***/

   printf("HI35xx_COMM_VPSS_Start  %d\n",s32Grpstart);
    if(NULL == pstVpssGrpAttr)
    {
//        stGrpAttr.u32MaxW = pstSize->u32Width;
//        stGrpAttr.u32MaxH = pstSize->u32Height;
        stGrpAttr.enPixFmt = PIXEL_FORMAT_YUV_SEMIPLANAR_420;

        stGrpAttr.bIeEn = HI_FALSE;
        stGrpAttr.bNrEn = HI_TRUE;
        stGrpAttr.bDciEn = HI_FALSE;
        stGrpAttr.bHistEn = HI_FALSE;
        stGrpAttr.bEsEn = HI_FALSE;
        stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
    }
    else
    {
        memcpy(&stGrpAttr,pstVpssGrpAttr,sizeof(VPSS_GRP_ATTR_S));
    }

    for(i= s32Grpstart; i<s32GrpCnt; i++)
    {
        VpssGrp = i;

        /*** create vpss group ***/
       // if((VpssGrp == 0) || (VpssGrp == 1))
        // if(VpssGrp == 0)

        if(edid4k == HI_TRUE)
         {
            stGrpAttr.u32MaxW = 3840;
            stGrpAttr.u32MaxH = 2160;
        }
        else
        {
            stGrpAttr.u32MaxW = 1920;
            stGrpAttr.u32MaxH = 1080;

        }

//            stGrpAttr.u32MaxW = pstSize->u32Width;
//            stGrpAttr.u32MaxH = pstSize->u32Height;

//        if(VpssGrp == 4)
//          s32ChnCnt = 2;


        s32Ret = HI_MPI_VPSS_CreateGrp(VpssGrp, &stGrpAttr);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_CreateGrp=%d failed with %#x!\n", VpssGrp,s32Ret);
            return HI_FAILURE;
        }

        /*** set vpss param ***/
        s32Ret = HI_MPI_VPSS_GetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        stVpssParam.u32IeStrength = 0;
        s32Ret = HI_MPI_VPSS_SetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        /*** enable vpss chn, with frame ***/

        for(j=0; j<s32ChnCnt; j++)
        {
            VpssChn = j;

#if 1
           // if((VpssChn == 0) && (VpssGrp < 4)){
           bool vpssmod_ = true;
            if(vpssmod_) {
                HI_U32 u32Depth = 3;
                s32Ret = HI_MPI_VPSS_GetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_GetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }

                stVpssMode.enChnMode = VPSS_CHN_MODE_USER;
                stVpssMode.enPixelFormat = PIXEL_FORMAT_YUV_SEMIPLANAR_420;


                     stVpssMode.u32Width = pstSize->u32Width;
                     stVpssMode.u32Height = pstSize->u32Height;

//                     stVpssMode.u32Width = 3600;
//                     stVpssMode.u32Height =  1890;


//                  stVpssMode.stAspectRatio.stVideoRect.s32X
//                  stVpssMode.stAspectRatio.stVideoRect.s32Y
                  stVpssMode.stAspectRatio.stVideoRect.u32Height = 3600;
                  stVpssMode.stAspectRatio.stVideoRect.u32Width = 1890;
//                     stVpssMode.enPixelFormat
//                     stVpssMode
//                     stVpssMode
//                     stVpssMode

                     //                      }
//                     else
//                     {
//                         stVpssMode.u32Width =  1920 ;
//                         stVpssMode.u32Height =   1080 ;
//                     }
                     stVpssMode.stFrameRate.s32SrcFrmRate = 60 ;
                     stVpssMode.stFrameRate.s32DstFrmRate = 60 ;

                s32Ret = HI_MPI_VPSS_SetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }
                s32Ret = HI_MPI_VPSS_SetDepth(VpssGrp,VpssChn,u32Depth);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetDepth failed with %#x\n", s32Ret);
                    return s32Ret;
                }
            }
#endif
            /* Set Vpss Chn attr */
            stChnAttr.bSpEn = HI_FALSE;
            stChnAttr.bUVInvert = HI_FALSE;
            stChnAttr.bBorderEn = HI_FALSE;
            stChnAttr.stBorder.u32Color = 0xffffff;
            stChnAttr.stBorder.u32LeftWidth = 2;
            stChnAttr.stBorder.u32RightWidth = 2;
            stChnAttr.stBorder.u32TopWidth = 2;
            stChnAttr.stBorder.u32BottomWidth = 2;

            s32Ret = HI_MPI_VPSS_SetChnAttr(VpssGrp, VpssChn, &stChnAttr);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_SetChnAttr failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }
#if 0
            if(VpssGrp < 4 )
            {
                    HI_MPI_VPSS_SetPreScaleMode( VpssGrp ,VPSS_PRESCALE_MODE_DEFAULT);
                    if (s32Ret != HI_SUCCESS)
                    {
                        SAMPLE_PRT("HI_MPI_VPSS_SetPreScaleMode failed with %#x\n", s32Ret);
                        return HI_FAILURE;
                    }


            }
            else
              {
                HI_MPI_VPSS_SetPreScaleMode( VpssGrp ,VPSS_PRESCALE_MODE_OTHER);
                if (s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetPreScaleMode failed with %#x\n", s32Ret);
                    return HI_FAILURE;
                }
             }
#endif
            s32Ret = HI_MPI_VPSS_EnableChn(VpssGrp, VpssChn);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_EnableChn failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }



        }

        /*** start vpss group ***/
        s32Ret = HI_MPI_VPSS_StartGrp(VpssGrp);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_StartGrp failed with %#x\n", s32Ret);
            return HI_FAILURE;
        }

        vi_gop++;


    }
    return HI_SUCCESS;
}





HI_S32 HI35xx_COMM_VPSS_Start(HI_S32 s32Grpstart,HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr,HI_U32 edid4k)
{
    VPSS_GRP VpssGrp;
    VPSS_CHN VpssChn;
    VPSS_GRP_ATTR_S stGrpAttr;
    VPSS_CHN_ATTR_S stChnAttr;
    VPSS_GRP_PARAM_S stVpssParam;
    VPSS_CHN_MODE_S stVpssMode;
    HI_S32 s32Ret;
    HI_S32 i, j;
   HI_S32 vi_gop = 0;
    /*** Set Vpss Grp Attr ***/

   printf("HI35xx_COMM_VPSS_Start  %d\n",s32Grpstart);
    if(NULL == pstVpssGrpAttr)
    {
//        stGrpAttr.u32MaxW = pstSize->u32Width;
//        stGrpAttr.u32MaxH = pstSize->u32Height;
        stGrpAttr.enPixFmt = PIXEL_FORMAT_YUV_SEMIPLANAR_420;

        stGrpAttr.bIeEn = HI_FALSE;
        stGrpAttr.bNrEn = HI_TRUE;
        stGrpAttr.bDciEn = HI_FALSE;
        stGrpAttr.bHistEn = HI_FALSE;
        stGrpAttr.bEsEn = HI_FALSE;
        stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
    }
    else
    {
        memcpy(&stGrpAttr,pstVpssGrpAttr,sizeof(VPSS_GRP_ATTR_S));
    }

    for(i= s32Grpstart; i<s32GrpCnt; i++)
    {
        VpssGrp = i;

        /*** create vpss group ***/
       // if((VpssGrp == 0) || (VpssGrp == 1))
        // if(VpssGrp == 0)

        if(edid4k == HI_TRUE)
         {
            stGrpAttr.u32MaxW = 3840;
            stGrpAttr.u32MaxH = 2160;
        }
        else
        {
            stGrpAttr.u32MaxW = 1920;
            stGrpAttr.u32MaxH = 1080;

        }

//        if(VpssGrp == 4)
//          s32ChnCnt = 2;


        s32Ret = HI_MPI_VPSS_CreateGrp(VpssGrp, &stGrpAttr);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_CreateGrp=%d failed with %#x!\n", VpssGrp,s32Ret);
            return HI_FAILURE;
        }

        /*** set vpss param ***/
        s32Ret = HI_MPI_VPSS_GetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        stVpssParam.u32IeStrength = 0;
        s32Ret = HI_MPI_VPSS_SetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        /*** enable vpss chn, with frame ***/

        for(j=0; j<s32ChnCnt; j++)
        {
            VpssChn = j;

            if((VpssChn == 0) && (VpssGrp < 4)){
                HI_U32 u32Depth = 3;
                s32Ret = HI_MPI_VPSS_GetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_GetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }
                stVpssMode.enChnMode = VPSS_CHN_MODE_USER;
                stVpssMode.enPixelFormat = PIXEL_FORMAT_YUV_SEMIPLANAR_420;

                 // stVpssMode.stFrameRate.s32DstFrmRate = viParam[vi_gop].u32SrcFrmRate;
               //   if((VpssGrp == 0) || (VpssGrp == 1))
                 //  if(VpssGrp == 0)
//                   {
//                     stVpssMode.u32Width = 3840;
//                     stVpssMode.u32Height = 2160;
//                   }
//                    else
//                   {
//                       stVpssMode.u32Width = 1920;
//                       stVpssMode.u32Height = 1080;
                  // }
//                     if(VpssGrp== 6)
//                     {
                     stVpssMode.u32Width = pstSize->u32Width;
                     stVpssMode.u32Height = pstSize->u32Height;
//                      }
//                     else
//                     {
//                         stVpssMode.u32Width =  1920 ;
//                         stVpssMode.u32Height =   1080 ;
//                     }

                s32Ret = HI_MPI_VPSS_SetChnMode(VpssGrp,VpssChn,&stVpssMode);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetChnMode failed with %#x\n", s32Ret);
                    return s32Ret;
                }
                s32Ret = HI_MPI_VPSS_SetDepth(VpssGrp,VpssChn,u32Depth);
                if(s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetDepth failed with %#x\n", s32Ret);
                    return s32Ret;
                }
            }

            /* Set Vpss Chn attr */
            stChnAttr.bSpEn = HI_FALSE;
            stChnAttr.bUVInvert = HI_FALSE;
            stChnAttr.bBorderEn = HI_FALSE;
            stChnAttr.stBorder.u32Color = 0xffffff;
            stChnAttr.stBorder.u32LeftWidth = 2;
            stChnAttr.stBorder.u32RightWidth = 2;
            stChnAttr.stBorder.u32TopWidth = 2;
            stChnAttr.stBorder.u32BottomWidth = 2;

            s32Ret = HI_MPI_VPSS_SetChnAttr(VpssGrp, VpssChn, &stChnAttr);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_SetChnAttr failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }

            if(VpssGrp < 4 )
            {
                    HI_MPI_VPSS_SetPreScaleMode( VpssGrp ,VPSS_PRESCALE_MODE_DEFAULT);
                    if (s32Ret != HI_SUCCESS)
                    {
                        SAMPLE_PRT("HI_MPI_VPSS_SetPreScaleMode failed with %#x\n", s32Ret);
                        return HI_FAILURE;
                    }


            }
            else
              {
                HI_MPI_VPSS_SetPreScaleMode( VpssGrp ,VPSS_PRESCALE_MODE_OTHER);
                if (s32Ret != HI_SUCCESS)
                {
                    SAMPLE_PRT("HI_MPI_VPSS_SetPreScaleMode failed with %#x\n", s32Ret);
                    return HI_FAILURE;
                }
             }

            s32Ret = HI_MPI_VPSS_EnableChn(VpssGrp, VpssChn);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_EnableChn failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }



        }

        /*** start vpss group ***/
        s32Ret = HI_MPI_VPSS_StartGrp(VpssGrp);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_StartGrp failed with %#x\n", s32Ret);
            return HI_FAILURE;
        }

        vi_gop++;


    }
    return HI_SUCCESS;
}







HI_S32 HI35xx_COMM_VO_BindVpss(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn)
{
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VPSS;
    stSrcChn.s32DevId = VpssGrp;
    stSrcChn.s32ChnId = VpssChn;

    stDestChn.enModId = HI_ID_VOU;
    stDestChn.s32DevId = VoLayer;
    stDestChn.s32ChnId = VoChn;

    return HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
}

VI_DEV_ATTR_S DEV_ATTR_chris_BT1120_STANDARD_BASE =
{
    /*interface mode*/
    VI_MODE_BT1120_STANDARD,
    /*work mode, 1/2/4 multiplex*/
    VI_WORK_MODE_1Multiplex,
    /* r_mask    g_mask    b_mask*/
    {0xFF000000,    0xFF0000},//{0xFF000000,    0xFF0000},

    /* for single/double edge, must be set when double edge*/
    VI_CLK_EDGE_SINGLE_UP,

    /*AdChnId*/
    {-1, -1, -1, -1},
    /*enDataSeq, just support yuv*/
    VI_INPUT_DATA_UVUV,//VI_INPUT_DATA_UVUV,

    /*sync info*/
    {
        /*port_vsync   port_vsync_neg     port_hsync        port_hsync_neg        */
        VI_VSYNC_PULSE, VI_VSYNC_NEG_HIGH, VI_HSYNC_VALID_SINGNAL,VI_HSYNC_NEG_HIGH,VI_VSYNC_NORM_PULSE,VI_VSYNC_VALID_NEG_HIGH,

        /*timing info*/
        /*hsync_hfb    hsync_act    hsync_hhb*/
        {0,            1920,        0,
         /*vsync0_vhb vsync0_act vsync0_hhb*/
         0,            1080,        0,
         /*vsync1_vhb vsync1_act vsync1_hhb*/
         0,            0,            0}
    },
    /*whether use isp*/
    VI_PATH_BYPASS,
    /*data type*/
    VI_DATA_TYPE_YUV,

    HI_FALSE

};

HI_S32 HI35xx_COMM_VI_StartDev(VI_DEV ViDev, SAMPLE_VI_MODE_E enViMode)
{
    HI_S32 s32Ret;
    VI_DEV_ATTR_S stViDevAttr;
    memset(&stViDevAttr,0,sizeof(stViDevAttr));

    switch (enViMode)
    {
        case SAMPLE_VI_MODE_8_1080P:
            memcpy(&stViDevAttr,&DEV_ATTR_chris_BT1120_STANDARD_BASE,sizeof(stViDevAttr));
            HI35xx_COMM_VI_SetMask(ViDev,&stViDevAttr);
            break;
        default:
            SAMPLE_PRT("vi input type[%d] is invalid!\n", enViMode);
            return HI_FAILURE;
    }

    s32Ret = HI_MPI_VI_SetDevAttr(ViDev, &stViDevAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VI_SetDevAttr failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VI_EnableDev(ViDev);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VI_EnableDev failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_SetMask(VI_DEV ViDev, VI_DEV_ATTR_S *pstDevAttr)
{
    switch (ViDev % 2)
    {
        case 0:
            pstDevAttr->au32CompMask[0] = 0xFF000000;
            if (VI_MODE_BT1120_STANDARD == pstDevAttr->enIntfMode)
            {
                pstDevAttr->au32CompMask[0] = 0xFF000000;
                pstDevAttr->au32CompMask[1] = 0xFF000000>>8;
            }
            else
            {
                pstDevAttr->au32CompMask[1] = 0x0;
            }
            break;

        case 1:
            pstDevAttr->au32CompMask[0] = 0xFF000000>>8;
            pstDevAttr->au32CompMask[1] = 0x0;
            break;
        default:
            SAMPLE_PRT("VISetMask failed!\n");
            return HI_FAILURE;
    }
}
HI_S32 HI35xx_COMM_VENC_SnapStart_1(VENC_CHN VencChn, SIZE_S pstSize,SIZE_S stMaxsize)
{
    HI_S32 s32Ret;
    VENC_CHN_ATTR_S stVencChnAttr;
    VENC_ATTR_JPEG_S stJpegAttr;
     VENC_PARAM_JPEG_S stParamJpeg;
      // VENC_PARAM_JPEG_S stParamJpeg;
     HI_U32 i = (VencChn - 8);


    /******************************************
     step 1:  Create Venc Channel
    ******************************************/
    SIZE_S max_snap = {pstSize.u32Width,pstSize.u32Height};

    int snaph_4 = max_snap.u32Height % 4;
    int snapw_4 = max_snap.u32Width  % 4;



    if(snaph_4 != 0)
        pstSize.u32Height = pstSize.u32Height + snaph_4;
    else if(snapw_4 != 0)
        pstSize.u32Width = pstSize.u32Width + snapw_4 ;






    stVencChnAttr.stVeAttr.enType = PT_JPEG;

//    stJpegAttr.u32MaxPicWidth  = stMaxsize.u32Width;

//    stJpegAttr.u32MaxPicHeight = stMaxsize.u32Height;
//    stJpegAttr.u32BufSize   = stMaxsize.u32Width * stMaxsize.u32Height * 3;

    stJpegAttr.u32MaxPicWidth  = pstSize.u32Width;

    stJpegAttr.u32MaxPicHeight = pstSize.u32Height;
    stJpegAttr.u32BufSize   = pstSize.u32Width * pstSize.u32Height * 2;

    stJpegAttr.u32PicWidth  = pstSize.u32Width;
    stJpegAttr.u32PicHeight = pstSize.u32Height;

    stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
    stJpegAttr.bSupportDCF  = HI_FALSE; //是否支持 Jpeg 的缩略图
    memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));


//    stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
//    stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;

    s32Ret = HI_MPI_VENC_CreateChn( VencChn , &stVencChnAttr);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_CreateChn [%d] faild with %#x!\n",\
                   VencChn, s32Ret);
        return s32Ret;
    }




    if(veSnap[i].Snapmcast_enable) {
        VENC_FRAME_RATE_S venc_jpeg_rate;
        s32Ret = HI_MPI_VENC_GetFrameRate(VencChn,&venc_jpeg_rate);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetFrameRate err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }

        venc_jpeg_rate.s32SrcFrmRate = viParam[i].u32SrcFrmRate;
        venc_jpeg_rate.s32DstFrmRate = veSnap[i].Snapmcast_freq;

        printf("jpeg venc srcRate=%d, dstRate=%d\n",venc_jpeg_rate.s32SrcFrmRate,venc_jpeg_rate.s32DstFrmRate);
        s32Ret = HI_MPI_VENC_SetFrameRate(VencChn,&venc_jpeg_rate);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetFrameRate err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
    }






    s32Ret = HI_MPI_VENC_StartRecvPic(VencChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic %d faild with%#x!\n", VencChn,s32Ret);
        return s32Ret;
    }



        s32Ret = HI_MPI_VENC_GetJpegParam(VencChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
       // viParam[i].SnapQuality = 50;

        stParamJpeg.u32Qfactor = veSnap[i].snapQuality;

        s32Ret = HI_MPI_VENC_SetJpegParam(VencChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }








//    s32Ret = HI_MPI_VENC_GetJpegParam(VencChn, &stParamJpeg);
//    if (HI_SUCCESS != s32Ret)
//    {
//        printf("HI_MPI_VENC_GetJpegParam err 0x%x\n",s32Ret);
//        return HI_FAILURE;
//    }

//    stParamJpeg.u32Qfactor = viParam[i].SnapQuality;

//    s32Ret = HI_MPI_VENC_SetJpegParam(VencChn, &stParamJpeg);
//    if (HI_SUCCESS != s32Ret)
//    {
//        printf("HI_MPI_VENC_SetJpegParam err 0x%x\n",s32Ret);
//        return HI_FAILURE;
//    }





    /******************************************
     step 3:  Start Recv Venc Pictures
    ******************************************/


    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VENC_SnapStart(VENC_CHN VencChn, SIZE_S *pstSize, HI_BOOL bBindVpss, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret;
    VENC_CHN_ATTR_S stVencChnAttr;
    VENC_ATTR_JPEG_S stJpegAttr;

    /******************************************
     step 1:  Create Venc Channel
    ******************************************/
    stVencChnAttr.stVeAttr.enType = PT_JPEG;

    stJpegAttr.u32MaxPicWidth  = pstSize->u32Width;
    stJpegAttr.u32MaxPicHeight = pstSize->u32Height;
    stJpegAttr.u32PicWidth  = pstSize->u32Width;
    stJpegAttr.u32PicHeight = pstSize->u32Height;
    stJpegAttr.u32BufSize   = pstSize->u32Width * pstSize->u32Height * 2;
    stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
    stJpegAttr.bSupportDCF  = HI_FALSE; //是否支持 Jpeg 的缩略图
    memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));

    s32Ret = HI_MPI_VENC_CreateChn(VencChn, &stVencChnAttr);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_CreateChn [%d] faild with %#x!\n",\
                   VencChn, s32Ret);
        return s32Ret;
    }

    /************* *****************************
     step 2:  Venc Chn bind to Vpss Chn
    ******************************************/
    if(bBindVpss){
        s32Ret = RSAMPLE_COMM_VENC_BindVpss(VencChn, VpssGrp, VpssChn);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("SAMPLE_COMM_VENC_BindVpss ve=%d,vpss=%d failed!\n",VencChn,VpssGrp);
            return s32Ret;
        }
    }

    /******************************************
     step 3:  Start Recv Venc Pictures
    ******************************************/
    s32Ret = HI_MPI_VENC_StartRecvPic(VencChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic %d faild with%#x!\n", VencChn,s32Ret);
        return s32Ret;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VENC_SnapStop(VENC_CHN VencChn, HI_BOOL bBindVpss, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret;

    /******************************************
     step 1:  Stop Recv Pictures
    ******************************************/
    s32Ret = HI_MPI_VENC_StopRecvPic(VencChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_StopRecvPic vechn[%d] failed with %#x!\n", VencChn, s32Ret);
        return s32Ret;
    }

    /******************************************
     step 2:  unbind
    ******************************************/
    if(bBindVpss){
        s32Ret = RSAMPLE_COMM_VENC_UnBindVpss(VencChn, VpssGrp, VpssChn);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("SAMPLE_COMM_VENC_UnBindVpss failed!\n");
            return s32Ret;
        }
    }

    /******************************************
     step 3:  Distroy Venc Channel
    ******************************************/
    s32Ret = HI_MPI_VENC_DestroyChn(VencChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", VencChn, s32Ret);
        return s32Ret;
    }

    return HI_SUCCESS;
}

/******************************************************************************
* function : venc bind vpss
******************************************************************************/
HI_S32 RSAMPLE_COMM_VENC_BindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = HI_SUCCESS;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;
    stSrcChn.enModId = HI_ID_VPSS;
    stSrcChn.s32DevId = VpssGrp;
    stSrcChn.s32ChnId = VpssChn;
    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;
    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    return s32Ret;
}

/******************************************************************************
* function : venc unbind vpss
******************************************************************************/
HI_S32 RSAMPLE_COMM_VENC_UnBindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = HI_SUCCESS;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;
    stSrcChn.enModId = HI_ID_VPSS;
    stSrcChn.s32DevId = VpssGrp;
    stSrcChn.s32ChnId = VpssChn;
    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;
    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    return s32Ret;
}

HI_S32 HI35xx_COMM_VENC_Start(VENC_CHN VencChn, PAYLOAD_TYPE_E enType, SIZE_S stMaxPicSize, SIZE_S stPicSize, SAMPLE_RC_E enRcMode, HI_U32  u32Profile,  HI_U32 u32Gop, HI_U32 u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 u32DstFrameRate, HI_U32 u32MinQp, HI_U32 u32MaxQp, HI_U32 u32MinIQp, HI_U32 u32IQp, HI_U32 u32PQp, HI_U32 u32BQp, HI_U32 u32MinIProp, HI_U32 u32MaxIProp,HI_BOOL setFlag)
{

//    printf("set vi frameRate %d  venc frameRate %d\n",u32SrcFrmRate,u32DstFrameRate);
    HI_S32 s32Ret;
    VENC_CHN_ATTR_S stVencChnAttr;
    VENC_ATTR_H264_S stH264Attr;
    VENC_ATTR_H264_CBR_S    stH264Cbr;
    VENC_ATTR_H264_VBR_S    stH264Vbr;
    VENC_ATTR_H264_AVBR_S    stH264AVbr;
    VENC_ATTR_H264_FIXQP_S  stH264FixQp;
    VENC_ATTR_H265_S        stH265Attr;
    VENC_ATTR_H265_CBR_S    stH265Cbr;
    VENC_ATTR_H265_VBR_S    stH265Vbr;
    VENC_ATTR_H265_AVBR_S    stH265AVbr;
    VENC_ATTR_H265_FIXQP_S  stH265FixQp;
    VENC_ATTR_MJPEG_S stMjpegAttr;
    VENC_ATTR_MJPEG_FIXQP_S stMjpegeFixQp;
    VENC_ATTR_JPEG_S stJpegAttr;


    if(setFlag) {
        s32Ret = HI_MPI_VENC_GetChnAttr(VencChn, &stVencChnAttr);
        stVencChnAttr.stVeAttr.enType = enType;
        switch (enType)
        {
            case PT_H264:
            {
                stH264Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
                stH264Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stH264Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
                stH264Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
                stH264Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 2;/*stream buffer size*/
                stH264Attr.u32Profile  = u32Profile;/*0: baseline; 1:MP; 2:HP;  3:svc_t */
                stH264Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode ?*/
                //stH264Attr.u32BFrameNum = 0;/* 0: not support B frame; >=1: number of B frames */
                //stH264Attr.u32RefNum = 1;/* 0: default; number of refrence frame*/
                memcpy(&stVencChnAttr.stVeAttr.stAttrH264e, &stH264Attr, sizeof(VENC_ATTR_H264_S));
                if (SAMPLE_RC_CBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264CBR;
                    stH264Cbr.u32Gop            = u32Gop;
                    stH264Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                    stH264Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                    stH264Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                    stH264Cbr.u32BitRate        = u32BitRate;
                    stH264Cbr.u32FluctuateLevel = 1; /* average bit rate */
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264Cbr, &stH264Cbr, sizeof(VENC_ATTR_H264_CBR_S));
                }
                else if (SAMPLE_RC_FIXQP == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264FIXQP;
                    stH264FixQp.u32Gop = u32Gop;
                    stH264FixQp.u32SrcFrmRate = u32SrcFrmRate;
                    stH264FixQp.fr32DstFrmRate = u32DstFrameRate;
                    stH264FixQp.u32IQp = u32IQp;
                    stH264FixQp.u32PQp = u32PQp;
                    stH264FixQp.u32BQp = u32BQp;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264FixQp, &stH264FixQp, sizeof(VENC_ATTR_H264_FIXQP_S));
                }
                else if (SAMPLE_RC_VBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264VBR;
                    stH264Vbr.u32Gop = u32Gop;
                    stH264Vbr.u32StatTime = 1;
                    stH264Vbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH264Vbr.fr32DstFrmRate = u32DstFrameRate;
                    stH264Vbr.u32MinQp = u32MinQp;
                    stH264Vbr.u32MinIQp = u32MinIQp;
                    stH264Vbr.u32MaxQp = u32MaxQp;
                    stH264Vbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264Vbr, &stH264Vbr, sizeof(VENC_ATTR_H264_VBR_S));
                }
                else if (SAMPLE_RC_AVBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264AVBR;
                    stH264AVbr.u32Gop = u32Gop;
                    stH264AVbr.u32StatTime = 1;
                    stH264AVbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH264AVbr.fr32DstFrmRate = u32DstFrameRate;
                    stH264Vbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH264AVbr, sizeof(VENC_ATTR_H264_AVBR_S));
                }
                else
                {
                    SAMPLE_PRT("error enRcMode\n");
                    return HI_FAILURE;
                }
            }
                break;
            case PT_MJPEG:
            {
                stMjpegAttr.u32MaxPicWidth = stMaxPicSize.u32Width;
                stMjpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stMjpegAttr.u32PicWidth = stPicSize.u32Width;
                stMjpegAttr.u32PicHeight = stPicSize.u32Height;
                stMjpegAttr.u32BufSize = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
                stMjpegAttr.bByFrame = HI_TRUE;  /*get stream mode is field mode  or frame mode*/
                memcpy(&stVencChnAttr.stVeAttr.stAttrMjpege, &stMjpegAttr, sizeof(VENC_ATTR_MJPEG_S));
                if (SAMPLE_RC_FIXQP == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGFIXQP;
                    stMjpegeFixQp.u32Qfactor        = 90;
                    stMjpegeFixQp.u32SrcFrmRate      = u32SrcFrmRate;
                    stMjpegeFixQp.fr32DstFrmRate = u32DstFrameRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrMjpegeFixQp, &stMjpegeFixQp,
                           sizeof(VENC_ATTR_MJPEG_FIXQP_S));
                }
                else if (SAMPLE_RC_CBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGCBR;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32StatTime       = 1;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32SrcFrmRate      = u32SrcFrmRate;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.fr32DstFrmRate = u32DstFrameRate;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32FluctuateLevel = 1;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32BitRate = u32BitRate;
                }
                else if (SAMPLE_RC_VBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGVBR;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32StatTime = 1;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32SrcFrmRate = u32SrcFrmRate;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.fr32DstFrmRate = 5;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MinQfactor = 50;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxQfactor = 95;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxBitRate = u32BitRate;
                }
                else
                {
                    SAMPLE_PRT("cann't support other mode in this version!\n");
                    return HI_FAILURE;
                }
            }
                break;
            case PT_JPEG:
                stJpegAttr.u32PicWidth  = stPicSize.u32Width;
                stJpegAttr.u32PicHeight = stPicSize.u32Height;
                stJpegAttr.u32MaxPicWidth  = stMaxPicSize.u32Width;
                stJpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stJpegAttr.u32BufSize   = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
                stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
                stJpegAttr.bSupportDCF  = HI_FALSE;
                memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));
                break;
            case PT_H265:
            {
                stH265Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
                stH265Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stH265Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
                stH265Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
                stH265Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 2;/*stream buffer size*/
                if (u32Profile >= 1)
                {
                    stH265Attr.u32Profile = 0;
                }/*0:MP; */
                else
                {
                    stH265Attr.u32Profile  = u32Profile;
                }/*0:MP*/
                stH265Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
                memcpy(&stVencChnAttr.stVeAttr.stAttrH265e, &stH265Attr, sizeof(VENC_ATTR_H265_S));
                if (SAMPLE_RC_CBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265CBR;
                    stH265Cbr.u32Gop            = u32Gop;
                    stH265Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                    stH265Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                    stH265Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                    stH265Cbr.u32BitRate        =  u32BitRate;
                    stH265Cbr.u32FluctuateLevel = 1; /* average bit rate */
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH265Cbr, &stH265Cbr, sizeof(VENC_ATTR_H265_CBR_S));
                }
                else if (SAMPLE_RC_FIXQP == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265FIXQP;
                    stH265FixQp.u32Gop = u32Gop;
                    stH265FixQp.u32SrcFrmRate = u32SrcFrmRate;
                    stH265FixQp.fr32DstFrmRate = u32DstFrameRate;
                    stH265FixQp.u32IQp = u32IQp;
                    stH265FixQp.u32PQp = u32PQp;
                    stH265FixQp.u32BQp = u32BQp;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH265FixQp, &stH265FixQp, sizeof(VENC_ATTR_H265_FIXQP_S));
                }
                else if (SAMPLE_RC_VBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265VBR;
                    stH265Vbr.u32Gop = u32Gop;
                    stH265Vbr.u32StatTime = 1;
                    stH265Vbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH265Vbr.fr32DstFrmRate = u32DstFrameRate;
                    stH265Vbr.u32MinQp  = u32MinQp;
                    stH265Vbr.u32MinIQp = u32MinIQp;
                    stH265Vbr.u32MaxQp  = u32MaxQp;
                    stH265Vbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH265Vbr, &stH265Vbr, sizeof(VENC_ATTR_H265_VBR_S));
                }
                else if (SAMPLE_RC_AVBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265AVBR;
                    stH265AVbr.u32Gop = u32Gop;
                    stH265AVbr.u32StatTime = 1;
                    stH265AVbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH265AVbr.fr32DstFrmRate = u32DstFrameRate;
                    stH265AVbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH265AVbr, sizeof(VENC_ATTR_H265_AVBR_S));
                }
                else
                {
                    return HI_FAILURE;
                }
            }
                break;
            default:
                return HI_ERR_VENC_NOT_SUPPORT;
        }
        stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
        stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;
        s32Ret = HI_MPI_VENC_SetChnAttr(VencChn, &stVencChnAttr);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("HI_MPI_VENC_SetChnAttr [%d] faild with %#x! ===\n", \
                       VencChn, s32Ret);
            return s32Ret;
        }
        s32Ret = HI_MPI_VENC_StartRecvPic(VencChn);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("HI_MPI_VENC_StartRecvPic faild with%#x! \n", s32Ret);
            return HI_FAILURE;
        }
        printf("----venc %d set ok------",VencChn);
        return HI_SUCCESS;

    }

    /******************************************
     step 1:  Create Venc Channel
     ******************************************/
    stVencChnAttr.stVeAttr.enType = enType;
    switch (enType)
    {
        case PT_H264:
        {
            stH264Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
            stH264Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stH264Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
            stH264Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
            stH264Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 2;/*stream buffer size*/
            stH264Attr.u32Profile  = u32Profile;/*0: baseline; 1:MP; 2:HP;  3:svc_t */
            stH264Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode ?*/
            //stH264Attr.u32BFrameNum = 0;/* 0: not support B frame; >=1: number of B frames */
            //stH264Attr.u32RefNum = 1;/* 0: default; number of refrence frame*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH264e, &stH264Attr, sizeof(VENC_ATTR_H264_S));
            if (SAMPLE_RC_CBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264CBR;
                stH264Cbr.u32Gop            = u32Gop;
                stH264Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH264Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                stH264Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                stH264Cbr.u32BitRate        = u32BitRate;
                stH264Cbr.u32FluctuateLevel = 1; /* average bit rate */
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Cbr, &stH264Cbr, sizeof(VENC_ATTR_H264_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264FIXQP;
                stH264FixQp.u32Gop = u32Gop;
                stH264FixQp.u32SrcFrmRate = u32SrcFrmRate;
                stH264FixQp.fr32DstFrmRate = u32DstFrameRate;
                stH264FixQp.u32IQp = u32IQp;
                stH264FixQp.u32PQp = u32PQp;
                stH264FixQp.u32BQp = u32BQp;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264FixQp, &stH264FixQp, sizeof(VENC_ATTR_H264_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264VBR;
                stH264Vbr.u32Gop = u32Gop;
                stH264Vbr.u32StatTime = 1;
                stH264Vbr.u32SrcFrmRate = u32SrcFrmRate;
                stH264Vbr.fr32DstFrmRate = u32DstFrameRate;
                stH264Vbr.u32MinQp = u32MinQp;
                stH264Vbr.u32MinIQp = u32MinIQp;
                stH264Vbr.u32MaxQp = u32MaxQp;
                stH264Vbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Vbr, &stH264Vbr, sizeof(VENC_ATTR_H264_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264AVBR;
                stH264AVbr.u32Gop = u32Gop;
                stH264AVbr.u32StatTime = 1;
                stH264AVbr.u32SrcFrmRate = u32SrcFrmRate;
                stH264AVbr.fr32DstFrmRate = u32DstFrameRate;
                stH264Vbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH264AVbr, sizeof(VENC_ATTR_H264_AVBR_S));
            }
            else
            {
                SAMPLE_PRT("error enRcMode\n");
                return HI_FAILURE;
            }
        }
            break;
        case PT_MJPEG:
        {
            stMjpegAttr.u32MaxPicWidth = stMaxPicSize.u32Width;
            stMjpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stMjpegAttr.u32PicWidth = stPicSize.u32Width;
            stMjpegAttr.u32PicHeight = stPicSize.u32Height;
            stMjpegAttr.u32BufSize = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
            stMjpegAttr.bByFrame = HI_TRUE;  /*get stream mode is field mode  or frame mode*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrMjpege, &stMjpegAttr, sizeof(VENC_ATTR_MJPEG_S));
            if (SAMPLE_RC_FIXQP == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGFIXQP;
                stMjpegeFixQp.u32Qfactor        = 90;
                stMjpegeFixQp.u32SrcFrmRate      = u32SrcFrmRate;
                stMjpegeFixQp.fr32DstFrmRate = u32DstFrameRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrMjpegeFixQp, &stMjpegeFixQp,
                       sizeof(VENC_ATTR_MJPEG_FIXQP_S));
            }
            else if (SAMPLE_RC_CBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGCBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32StatTime       = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32SrcFrmRate      = u32SrcFrmRate;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.fr32DstFrmRate = u32DstFrameRate;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32FluctuateLevel = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32BitRate = u32BitRate;
            }
            else if (SAMPLE_RC_VBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGVBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32StatTime = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32SrcFrmRate = u32SrcFrmRate;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.fr32DstFrmRate = 5;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MinQfactor = 50;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxQfactor = 95;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxBitRate = u32BitRate;
            }
            else
            {
                SAMPLE_PRT("cann't support other mode in this version!\n");
                return HI_FAILURE;
            }
        }
            break;
        case PT_JPEG:
            stJpegAttr.u32PicWidth  = stPicSize.u32Width;
            stJpegAttr.u32PicHeight = stPicSize.u32Height;
            stJpegAttr.u32MaxPicWidth  = stMaxPicSize.u32Width;
            stJpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stJpegAttr.u32BufSize   = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
            stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
            stJpegAttr.bSupportDCF  = HI_FALSE;
            memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));
            break;
        case PT_H265:
        {
            stH265Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
            stH265Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stH265Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
            stH265Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
            stH265Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 2;/*stream buffer size*/
            if (u32Profile >= 1)
            {
                stH265Attr.u32Profile = 0;
            }/*0:MP; */
            else
            {
                stH265Attr.u32Profile  = u32Profile;
            }/*0:MP*/
            stH265Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH265e, &stH265Attr, sizeof(VENC_ATTR_H265_S));
            if (SAMPLE_RC_CBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265CBR;
                stH265Cbr.u32Gop            = u32Gop;
                stH265Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH265Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                stH265Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                stH265Cbr.u32BitRate        =  u32BitRate;
                stH265Cbr.u32FluctuateLevel = 1; /* average bit rate */
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Cbr, &stH265Cbr, sizeof(VENC_ATTR_H265_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265FIXQP;
                stH265FixQp.u32Gop = u32Gop;
                stH265FixQp.u32SrcFrmRate = u32SrcFrmRate;
                stH265FixQp.fr32DstFrmRate = u32DstFrameRate;
                stH265FixQp.u32IQp = u32IQp;
                stH265FixQp.u32PQp = u32PQp;
                stH265FixQp.u32BQp = u32BQp;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265FixQp, &stH265FixQp, sizeof(VENC_ATTR_H265_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265VBR;
                stH265Vbr.u32Gop = u32Gop;
                stH265Vbr.u32StatTime = 1;
                stH265Vbr.u32SrcFrmRate = u32SrcFrmRate;
                stH265Vbr.fr32DstFrmRate = u32DstFrameRate;
                stH265Vbr.u32MinQp  = u32MinQp;
                stH265Vbr.u32MinIQp = u32MinIQp;
                stH265Vbr.u32MaxQp  = u32MaxQp;
                stH265Vbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Vbr, &stH265Vbr, sizeof(VENC_ATTR_H265_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265AVBR;
                stH265AVbr.u32Gop = u32Gop;
                stH265AVbr.u32StatTime = 1;
                stH265AVbr.u32SrcFrmRate = u32SrcFrmRate;
                stH265AVbr.fr32DstFrmRate = u32DstFrameRate;
                stH265AVbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH265AVbr, sizeof(VENC_ATTR_H265_AVBR_S));
            }
            else
            {
                return HI_FAILURE;
            }
        }
            break;
        default:
            return HI_ERR_VENC_NOT_SUPPORT;
    }
    stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
    stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;
    s32Ret = HI_MPI_VENC_CreateChn(VencChn, &stVencChnAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_CreateChn [%d] faild with %#x! ===\n", \
                   VencChn, s32Ret);
        return s32Ret;
    }


    if(enType == PT_JPEG){
        VENC_PARAM_JPEG_S stParamJpeg;
        s32Ret = HI_MPI_VENC_GetJpegParam(VencChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }

        stParamJpeg.u32Qfactor = viParam[VencChn-8].SnapQuality;
        printf("SnapQuality %d\n",stParamJpeg.u32Qfactor);

        s32Ret = HI_MPI_VENC_SetJpegParam(VencChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
    }
    if((enType == PT_H264)&&(SAMPLE_RC_CBR == enRcMode)){
        VENC_RC_PARAM_S stVencRcPara;

        s32Ret = HI_MPI_VENC_GetRcParam(VencChn, &stVencRcPara);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetRcParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
        //printf("MinIprop=%d,MaxIprop=%d\n",stVencRcPara.stParamH264Cbr.u32MinIprop,stVencRcPara.stParamH264Cbr.u32MaxIprop);
        //printf("MaxQp=%d,MinQp=%d\n",stVencRcPara.stParamH264Cbr.u32MaxQp,stVencRcPara.stParamH264Cbr.u32MinQp);

        stVencRcPara.stParamH264Cbr.u32MaxQp = u32MaxQp;
        stVencRcPara.stParamH264Cbr.u32MinQp = u32MinQp;
        stVencRcPara.stParamH264Cbr.u32MinIprop = u32MinIProp;
        stVencRcPara.stParamH264Cbr.u32MaxIprop = u32MaxIProp;

        s32Ret = HI_MPI_VENC_SetRcParam(VencChn, &stVencRcPara);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetRcParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }

        s32Ret = HI_MPI_VENC_GetRcParam(VencChn, &stVencRcPara);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetRcParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
        //printf("MinIprop=%d,MaxIprop=%d\n",stVencRcPara.stParamH264Cbr.u32MinIprop,stVencRcPara.stParamH264Cbr.u32MaxIprop);
        //printf("MaxQp=%d,MinQp=%d\n",stVencRcPara.stParamH264Cbr.u32MaxQp,stVencRcPara.stParamH264Cbr.u32MinQp);


    }

    s32Ret = HI_MPI_VENC_StartRecvPic(VencChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic faild with%#x! \n", s32Ret);
        return HI_FAILURE;
    }

//    VENC_FRAME_RATE_S pstFrameRate;
//    pstFrameRate.s32SrcFrmRate = u32SrcFrmRate;//输入帧率src必须大于或等于输出帧率dst,可减帧，不可增帧
//    pstFrameRate.s32DstFrmRate = u32DstFrameRate;//同时为-1，表示不进行帧率控制
//    printf("vi frameRate:%d  venc frameRate:%d\n",pstFrameRate.s32SrcFrmRate,pstFrameRate.s32DstFrmRate);
//    s32Ret = HI_MPI_VENC_SetFrameRate(VencChn, &pstFrameRate);
//    if(s32Ret != HI_SUCCESS )
//    {
//        SAMPLE_PRT("HI_MPI_VENC_SetFrameRate faild with%#x! \n", s32Ret);

//    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VO_StartChn(VO_LAYER VoLayer, VO_CHN VoChn)
{
    HI_S32 s32Ret = HI_SUCCESS;
    HI_U32 u32Width = 0;
    HI_U32 u32Height = 0;
    VO_CHN_ATTR_S stChnAttr;
    VO_VIDEO_LAYER_ATTR_S stLayerAttr;

    s32Ret = HI_MPI_VO_GetVideoLayerAttr(VoLayer, &stLayerAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    u32Width = stLayerAttr.stImageSize.u32Width;
    u32Height = stLayerAttr.stImageSize.u32Height;

    stChnAttr.stRect.s32X       = 0;
    stChnAttr.stRect.s32Y       = 0;
    stChnAttr.stRect.u32Width   = u32Width;
    stChnAttr.stRect.u32Height  = u32Height;
    stChnAttr.u32Priority       = 0;
    stChnAttr.bDeflicker        = (SAMPLE_VO_LAYER_VSD0 == VoLayer) ? HI_TRUE : HI_FALSE;

    s32Ret = HI_MPI_VO_SetChnAttr(VoLayer, VoChn, &stChnAttr);
    if (s32Ret != HI_SUCCESS)
    {
        printf("%s(%d):failed with %#x!\n",\
               __FUNCTION__,__LINE__,  s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VO_EnableChn(VoLayer, VoChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_VB_PoolDestroy()
{
    HI_S32 s32Ret ;
//    for(HI_U32 i = 0; i < VO_BUFF_MAX; i++){

//        if( HI_SUCCESS != s32Ret ){
//            log_e("HI_MPI_VB_ReleaseBlock failed errno 0x%x", s32Ret);
//        }
//    }
  // s32Ret = HI_MPI_VB_ReleaseBlock(VbBlk);

    for(int i = 0; i < VB_MAX_POOLS; i ++){
        s32Ret = HI_MPI_VB_DestroyPool( i );
    }

    return HI_SUCCESS;
}


HI_S32 HI35xx_COMM_VO_StopChn(VO_DEV VoDev, HI_U32 u32VoChn)
{
    HI_S32 s32Ret;

    VO_LAYER VoLayer;
    if (VoDev == SAMPLE_VO_DEV_DHD0)
        VoLayer = SAMPLE_VO_LAYER_VHD0;
    else if (VoDev == SAMPLE_VO_DEV_DHD1)
        VoLayer = SAMPLE_VO_LAYER_VHD1;
    else if (VoDev == SAMPLE_VO_DEV_DSD0)
        VoLayer = SAMPLE_VO_LAYER_VSD0;
    else
        VoLayer = SAMPLE_VO_LAYER_VHD0;

    s32Ret = HI_MPI_VO_ClearChnBuffer(VoLayer, u32VoChn, HI_TRUE);
    if (s32Ret != HI_SUCCESS){
        SAMPLE_PRT("HI_MPI_VO_ClearChnBuffer(chn:%d) failed with %#x!\n", u32VoChn, s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VO_DisableChn(VoLayer, u32VoChn);
    if (s32Ret != HI_SUCCESS){
        SAMPLE_PRT("HI_MPI_VO_DisableChn(chn:%d) failed with %#x!\n", u32VoChn, s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}


#if 0
HI_S32 HI35xx_COMM_VO_StopChn(VO_LAYER VoLayer, VO_CHN VoChn)
{
    HI_S32 s32Ret = HI_SUCCESS;

    s32Ret = HI_MPI_VO_DisableChn(VoLayer, VoChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return s32Ret;
}
#endif

HI_S32 HI35xx_COMM_VO_GetWH(VO_INTF_SYNC_E *enIntfSync, SIZE_S stVoSize, HI_U32 pu32Frm){

  //  printf("stvosizes w=%d,h=%d,fps=%d\n",stVoSize.u32Width,stVoSize.u32Height,pu32Frm);

    if((stVoSize.u32Width == 3840)&&(stVoSize.u32Height == 2160)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_3840x2160_60;
    else if((stVoSize.u32Width == 3840)&&(stVoSize.u32Height == 2160)&&(pu32Frm == 30))
        *enIntfSync = VO_OUTPUT_3840x2160_30;
    else if((stVoSize.u32Width == 1920)&&(stVoSize.u32Height == 1080)&&(pu32Frm == 30))
        *enIntfSync = VO_OUTPUT_1080P30;
    else if((stVoSize.u32Width == 1920)&&(stVoSize.u32Height == 1080)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1080P60;
    else if((stVoSize.u32Width == 1600)&&(stVoSize.u32Height == 1200)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1600x1200_60;
    else if((stVoSize.u32Width == 1680)&&(stVoSize.u32Height == 1050)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1680x1050_60;
    else if((stVoSize.u32Width == 1440)&&(stVoSize.u32Height == 900)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1440x900_60;
    else if((stVoSize.u32Width == 1280)&&(stVoSize.u32Height == 1024)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1280x1024_60;
    else if((stVoSize.u32Width == 1280)&&(stVoSize.u32Height == 720)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_720P60;
    else if((stVoSize.u32Width == 1024)&&(stVoSize.u32Height == 768)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1024x768_60;
    else if((stVoSize.u32Width == 800)&&(stVoSize.u32Height == 600)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_800x600_60;
    else if((stVoSize.u32Width == 720)&&(stVoSize.u32Height == 480)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_480P60;
    else if((stVoSize.u32Width == 1366)&&(stVoSize.u32Height == 768)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1366x768_60;
    else if((stVoSize.u32Width == 720)&&(stVoSize.u32Height == 480)&&(pu32Frm == 30))
        *enIntfSync = VO_OUTPUT_NTSC;
    else
        *enIntfSync = VO_OUTPUT_1080P60;



    return 0;

}

HI_S32 HI35xx_VB_PoolInit(SIZE_S stPoolSize, HI_U32 u32BlkCnt)
{
    VB_POOL hPool  = VB_INVALID_POOLID;
    HI_U32  u32BlkSize = stPoolSize.u32Width * stPoolSize.u32Height * 3 >> 1;

    hPool = HI_MPI_VB_CreatePool( u32BlkSize, u32BlkCnt, NULL);
    if (hPool == VB_INVALID_POOLID){

        return HI_FAILURE;
    }

    for(HI_U32 i = 0; i < POOL_MAX; i++){
        l_stVbMem[i].hPool = hPool;
        while((l_stVbMem[i].hBlock = HI_MPI_VB_GetBlock(l_stVbMem[i].hPool, u32BlkSize, NULL)) == VB_INVALID_HANDLE){
            ;
        }

        l_stVbMem[i].u32PhyAddr = HI_MPI_VB_Handle2PhysAddr(l_stVbMem[i].hBlock);
        l_stVbMem[i].pVirAddr = (HI_U8 *)HI_MPI_SYS_Mmap( l_stVbMem[i].u32PhyAddr, u32BlkSize );
        if(l_stVbMem[i].pVirAddr == NULL){

            HI_MPI_VB_ReleaseBlock(l_stVbMem[i].hBlock);
            return HI_FAILURE;
        }
    }

    return HI_SUCCESS;
}



HI_S32 HI35xx_COMM_SYS_MemConfig(HI_S32 s32VdChnCnt, HI_S32 s32VpssGrpCnt, HI_S32 s32VeChnCnt, VO_DEV VoDevId)
{
    HI_S32 i = 0;
    HI_S32 s32Ret = HI_SUCCESS;

    HI_CHAR * pcMmzName = NULL;
    MPP_CHN_S stMppChnVO;
    MPP_CHN_S stMppChnVPSS;
    MPP_CHN_S stMppChnVENC;
    MPP_CHN_S stMppChnVDEC;

    /* vdec chn config to mmz 'null' */
    /* vdec max chn is VDEC_MAX_CHN_NUM = 128*/
    for(i=0; i<s32VdChnCnt; i++)
    {
        stMppChnVDEC.enModId = HI_ID_VDEC;
        stMppChnVDEC.s32DevId = 0;
        stMppChnVDEC.s32ChnId = i;

        s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVDEC, pcMmzName);
        if (s32Ret)
        {
            SAMPLE_PRT("HI_MPI_SYS_SetMemConf ERR !\n");
            return HI_FAILURE;
        }
    }

    /* vpss group config to mmz 'null' */
    /* vpss max group is VPSS_MAX_GRP_NUM = 256*/
    for(i=0;i<s32VpssGrpCnt;i++)
    {
        stMppChnVPSS.enModId  = HI_ID_VPSS;
        stMppChnVPSS.s32DevId = i;
        stMppChnVPSS.s32ChnId = 0;

        s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVPSS, pcMmzName);
        if (s32Ret)
        {
            SAMPLE_PRT("HI_MPI_SYS_SetMemConf ERR !\n");
            return HI_FAILURE;
        }
    }

    /* venc chn config to mmz 'null' */
    /* venc max chn is VENC_MAX_CHN_NUM = 128*/
    for (i = 0;i < s32VeChnCnt; i++)
    {
        stMppChnVENC.enModId = HI_ID_VENC;
        stMppChnVENC.s32DevId = 0;
        stMppChnVENC.s32ChnId = i;

        s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVENC, pcMmzName);
        //printf("######## %d\n",i);
        if (s32Ret)
        {
            SAMPLE_PRT("HI_MPI_SYS_SetMemConf failed 0x%x!\n",s32Ret);
            return HI_FAILURE;
        }

    }

    /* vo config to mmz 'null' */
    stMppChnVO.enModId  = HI_ID_VOU;
    stMppChnVO.s32DevId = VoDevId;
    stMppChnVO.s32ChnId = 0;
    s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVO, pcMmzName);
    if (s32Ret)
    {
        SAMPLE_PRT("HI_MPI_SYS_SetMemConf ERR !\n");
        return HI_FAILURE;
    }

    return s32Ret;
}






/******************************************************************************
* function : Start Ai
******************************************************************************/
HI_S32 HI35xx_COMM_AUDIO_StartAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn, AIO_ATTR_S* pstAioAttr,
                                 AUDIO_SAMPLE_RATE_E enOutSampleRate, HI_BOOL bResampleEn, HI_VOID* pstAiVqeAttr, HI_U32 u32AiVqeType)
{
    HI_S32 s32Ret;

    if (pstAioAttr->u32ClkChnCnt == 0)
    {
        pstAioAttr->u32ClkChnCnt = 16;
    }

    s32Ret = HI_MPI_AI_SetPubAttr(AiDevId, pstAioAttr);
    if (s32Ret)
    {
        printf("%s: HI_MPI_AI_SetPubAttr(%d) failed with %#x\n", __FUNCTION__, AiDevId, s32Ret);
        return s32Ret;
    }

    s32Ret = HI_MPI_AI_Enable(AiDevId);
    if (s32Ret)
    {
        printf("%s: HI_MPI_AI_Enable(%d) failed with %#x\n", __FUNCTION__, AiDevId, s32Ret);
        return s32Ret;
    }

    s32Ret = HI_MPI_AI_EnableChn(AiDevId, s32AiChn);
    if (s32Ret)
    {
        printf("%s: HI_MPI_AI_EnableChn(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, s32AiChn, s32Ret);
        return s32Ret;
    }

    if (HI_TRUE == bResampleEn)
    {
        s32Ret = HI_MPI_AI_EnableReSmp(AiDevId, s32AiChn, enOutSampleRate);
        if (s32Ret)
        {
            printf("%s: HI_MPI_AI_EnableReSmp(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, s32AiChn, s32Ret);
            return s32Ret;
        }
    }

    if (NULL != pstAiVqeAttr)
    {
        HI_BOOL bAiVqe = HI_TRUE;
        switch (u32AiVqeType)
        {
            case 0:
                s32Ret = HI_SUCCESS;
                bAiVqe = HI_FALSE;
                break;
            case 1:
                s32Ret = HI_MPI_AI_SetVqeAttr(AiDevId, s32AiChn, SAMPLE_AUDIO_AO_DEV, s32AiChn, (AI_VQE_CONFIG_S *)pstAiVqeAttr);
                break;
            default:
                s32Ret = HI_FAILURE;
                break;
        }
        if (s32Ret)
        {
            printf("%s: HI_MPI_AI_SetVqeAttr(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, s32AiChn, s32Ret);
            return s32Ret;
        }

        if (bAiVqe)
        {
            s32Ret = HI_MPI_AI_EnableVqe(AiDevId, s32AiChn);
            if (s32Ret)
            {
                printf("%s: HI_MPI_AI_EnableVqe(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, s32AiChn, s32Ret);
                return s32Ret;
            }
        }
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_AUDIO_StopAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn,
                                HI_BOOL bResampleEn, HI_BOOL bVqeEn)
{
    HI_S32 s32Ret;

    if (HI_TRUE == bResampleEn)
    {
        s32Ret = HI_MPI_AI_DisableReSmp(AiDevId, s32AiChn);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
            return s32Ret;
        }
    }

    if (HI_TRUE == bVqeEn)
    {
        s32Ret = HI_MPI_AI_DisableVqe(AiDevId, s32AiChn);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
            return s32Ret;
        }
    }

    s32Ret = HI_MPI_AI_DisableChn(AiDevId, s32AiChn);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
        return s32Ret;
    }

//    s32Ret = HI_MPI_AI_Disable(AiDevId);
//    if (HI_SUCCESS != s32Ret)
//    {
//        printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
//        return s32Ret;
//    }

    return HI_SUCCESS;
}

HI_S32 Hi_SetReg(HI_U32 u32Addr, HI_U32 u32Value)
{
    HI_U32 *pu32Addr = NULL;
    HI_U32 u32MapLen = sizeof(u32Value);

    pu32Addr = (HI_U32 *)HI_MPI_SYS_Mmap(u32Addr, u32MapLen);
    if(NULL == pu32Addr)
    {
        return HI_FAILURE;
    }

    *pu32Addr = u32Value;

    return HI_MPI_SYS_Munmap(pu32Addr, u32MapLen);
}

HI_S32 Hi_GetReg(HI_U32 u32Addr, HI_U32 *pu32Value)
{
    HI_U32 *pu32Addr = NULL;
    HI_U32 u32MapLen;

    if (NULL == pu32Value)
    {
        return HI_ERR_SYS_NULL_PTR;
    }

    u32MapLen = sizeof(*pu32Value);
    pu32Addr = (HI_U32 *)HI_MPI_SYS_Mmap(u32Addr, u32MapLen);
    if(NULL == pu32Addr)
    {
        return HI_FAILURE;
    }

    *pu32Value = *pu32Addr;

    return HI_MPI_SYS_Munmap(pu32Addr, u32MapLen);
}

int tim_subtract(struct timeval *result, struct timeval *begin, struct timeval *end )
{
    if(begin->tv_sec > end->tv_sec)    return -1;

    if((begin->tv_sec == end->tv_sec) && (begin->tv_usec > end->tv_usec))    return -2;

    result->tv_sec  = (end->tv_sec - begin->tv_sec);
    result->tv_usec = (end->tv_usec - begin->tv_usec);

    if (result->tv_usec<0){
        result->tv_sec--;
        result->tv_usec += 1000000;
    }

    //if ( outPutFlag )
    //printf("diff time %ds%dms%dus\n", result->tv_sec, result->tv_usec/1000, result->tv_usec%1000);

    return 0;
}
