#ifndef Software_Config_H
#define Software_Config_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <iostream>

#include <readini.h>
#include "global.h"
#include "../common/version.h"

using namespace std;
#define ETH0_NAME "eth0"
#define ETH1_NAME "eth1"
#define DNS_NAME  "/etc/resolv.conf"
#define DATA_FILE "/data"
#define NET_CONFIG_FILE "net.ini"
#define VENC_CONFIG_FILE "venc.ini"
#define NODE_NAME_FILE  "in_node_name"
extern char filepath_[36];
extern char netfilepath_[36];

#define PARA_NAME_LEN 56
#define EnumtoStr(val) #val //把枚举变量的名称. 返回类型为const char[]

class SoftwareConfig
{
public:
    typedef enum {
        //network
        kEth0Ip = 0,
        kEth0Mask,
        kEth0Gateway,
        kEth0Dns,
        kEth0Mac,
        //
        kWebPort,
        kUdpSvPort,
        kChn0_SW_Color,
        kChn1_SW_Color,
        kChn2_SW_Color,
        kChn3_SW_Color,
        //audio
        kAiSampleRate,
        kChn0_AiCoderFormat,
        kChn1_AiCoderFormat,
        kChn2_AiCoderFormat,
        kChn3_AiCoderFormat,
        kAiBitRate,
        kChn0_AiDev,      //0:hdmi音频输入， 1：模拟音频输入
        kChn1_AiDev,
        kChn2_AiDev,      //0:hdmi音频输入， 1：模拟音频输入
        kChn3_AiDev,
        //vi
        kChn0_VI_W,
        kChn0_VI_H,
        kChn0_SrcFrmRate,//
        kChn1_VI_W,
        kChn1_VI_H,
        kChn1_SrcFrmRate,//
        kChn2_VI_W,
        kChn2_VI_H,
        kChn2_SrcFrmRate,//
        kChn3_VI_W,
        kChn3_VI_H,
        kChn3_SrcFrmRate,//
        //vi crop
        KErrFrameNum,
        kChn0_VI_Crop_Enable,
        kChn0_VI_X,
        kChn0_VI_Y,
        kChn0_VI_Width,
        kChn0_VI_Height,
        kChn1_VI_Crop_Enable,
        kChn1_VI_X,
        kChn1_VI_Y,
        kChn1_VI_Width,
        kChn1_VI_Height,
        kChn2_VI_Crop_Enable,
        kChn2_VI_X,
        kChn2_VI_Y,
        kChn2_VI_Width,
        kChn2_VI_Height,
        kChn3_VI_Crop_Enable,
        kChn3_VI_X,
        kChn3_VI_Y,
        kChn3_VI_Width,
        kChn3_VI_Height,
        //multicast
        kChn0_M_McastEnable,
        kChn0_M_McastIp,
        kChn0_M_McastPort,
        kChn0_M_McastLen,
        kChn0_M_McastDelay,
        kChn0_M_McastNum,
        kChn0_S0_McastEnable,
        kChn0_S0_McastIp,
        kChn0_S0_McastPort,
        kChn0_S0_McastLen,
        kChn0_S0_McastDelay,
        kChn1_M_McastEnable,
        kChn1_M_McastIp,
        kChn1_M_McastPort,
        kChn1_M_McastLen,
        kChn1_M_McastDelay,
        kChn1_M_McastNum,
        kChn1_S0_McastEnable,
        kChn1_S0_McastIp,
        kChn1_S0_McastPort,
        kChn1_S0_McastLen,
        kChn1_S0_McastDelay,
        kChn2_M_McastEnable,
        kChn2_M_McastIp,
        kChn2_M_McastPort,
        kChn2_M_McastLen,
        kChn2_M_McastDelay,
        kChn2_M_McastNum,
        kChn2_S0_McastEnable,
        kChn2_S0_McastIp,
        kChn2_S0_McastPort,
        kChn2_S0_McastLen,
        kChn2_S0_McastDelay,
        kChn3_M_McastEnable,
        kChn3_M_McastIp,
        kChn3_M_McastPort,
        kChn3_M_McastLen,
        kChn3_M_McastDelay,
        kChn3_M_McastNum,
        kChn3_S0_McastEnable,
        kChn3_S0_McastIp,
        kChn3_S0_McastPort,
        kChn3_S0_McastLen,
        kChn3_S0_McastDelay,
        kHdmi0_Au_McastEnable,
        kHdmi1_Au_McastEnable,
        kAnalog_Au_McastEnable,
        kHdmi0_Au_McastIp,
        kHdmi1_Au_McastIp,
        kAnalog_Au_McastIp,
        kHdmi0_Au_McastPort,
        kHdmi1_Au_McastPort,
        kAnalog_Au_McastPort,
        //venc(chn0 main)
        kChn0_M_VENC_W,
        kChn0_M_VENC_H,
        kChn0_M_VENC_SAME_INPUT,
        kChn0_M_VENC_Type,
        kChn0_M_Gop,       // 关键帧
        kChn0_M_RcMode,    // 码流控制
        kChn0_M_Profile,   // 编码质量等级
        kChn0_M_BitRate,   // 比特率
        kChn0_M_MIN_QP,
        kChn0_M_MAX_QP,
        kChn0_M_MINI_PROP, //最小IP比例
        kChn0_M_MAXI_PROP, //最大IP比例
        kChn0_M_MinI_Qp,
        kChn0_M_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn0_M_P_QP,
        kChn0_M_B_QP,
        kChn0_M_DstFrmRate,// 编码帧率
        //venc(chn0 sub0)
        kChn0_S0_VENC_W,
        kChn0_S0_VENC_H,
        kChn0_S0_VENC_SAME_INPUT,
        kChn0_S0_VENC_Type,
        kChn0_S0_Gop,       // 关键帧
        kChn0_S0_RcMode,    // 码流控制
        kChn0_S0_Profile,   // 编码质量等级
        kChn0_S0_BitRate,   // 比特率
        kChn0_S0_MIN_QP,
        kChn0_S0_MAX_QP,
        kChn0_S0_MINI_PROP,
        kChn0_S0_MAXI_PROP,
        kChn0_S0_MinI_Qp,
        kChn0_S0_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn0_S0_P_QP,
        kChn0_S0_B_QP,
        kChn0_S0_DstFrmRate,// 编码帧率
        //venc(chn1 main)
        kChn1_M_VENC_W,
        kChn1_M_VENC_H,
        kChn1_M_VENC_SAME_INPUT,
        kChn1_M_VENC_Type,
        kChn1_M_Gop,       // 关键帧
        kChn1_M_RcMode,    // 码流控制
        kChn1_M_Profile,   // 编码质量等级
        kChn1_M_BitRate,   // 比特率
        kChn1_M_MIN_QP,
        kChn1_M_MAX_QP,
        kChn1_M_MINI_PROP, //最小IP比例
        kChn1_M_MAXI_PROP, //最大IP比例
        kChn1_M_MinI_Qp,
        kChn1_M_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn1_M_P_QP,
        kChn1_M_B_QP,
        kChn1_M_DstFrmRate,// 编码帧率
        //venc(chn1 sub0)
        kChn1_S0_VENC_W,
        kChn1_S0_VENC_H,
        kChn1_S0_VENC_SAME_INPUT,
        kChn1_S0_VENC_Type,
        kChn1_S0_Gop,       // 关键帧
        kChn1_S0_RcMode,    // 码流控制
        kChn1_S0_Profile,   // 编码质量等级
        kChn1_S0_BitRate,   // 比特率
        kChn1_S0_MIN_QP,
        kChn1_S0_MAX_QP,
        kChn1_S0_MINI_PROP,
        kChn1_S0_MAXI_PROP,
        kChn1_S0_MinI_Qp,
        kChn1_S0_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn1_S0_P_QP,
        kChn1_S0_B_QP,
        kChn1_S0_DstFrmRate,// 编码帧率
        //venc(chn2 main)
        kChn2_M_VENC_W,
        kChn2_M_VENC_H,
        kChn2_M_VENC_SAME_INPUT,
        kChn2_M_VENC_Type,
        kChn2_M_Gop,       // 关键帧
        kChn2_M_RcMode,    // 码流控制
        kChn2_M_Profile,   // 编码质量等级
        kChn2_M_BitRate,   // 比特率
        kChn2_M_MIN_QP,
        kChn2_M_MAX_QP,
        kChn2_M_MINI_PROP, //最小IP比例
        kChn2_M_MAXI_PROP, //最大IP比例
        kChn2_M_MinI_Qp,
        kChn2_M_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn2_M_P_QP,
        kChn2_M_B_QP,
        kChn2_M_DstFrmRate,// 编码帧率
        //venc(chn2 sub0)
        kChn2_S0_VENC_W,
        kChn2_S0_VENC_H,
        kChn2_S0_VENC_SAME_INPUT,
        kChn2_S0_VENC_Type,
        kChn2_S0_Gop,       // 关键帧
        kChn2_S0_RcMode,    // 码流控制
        kChn2_S0_Profile,   // 编码质量等级
        kChn2_S0_BitRate,   // 比特率
        kChn2_S0_MIN_QP,
        kChn2_S0_MAX_QP,
        kChn2_S0_MINI_PROP,
        kChn2_S0_MAXI_PROP,
        kChn2_S0_MinI_Qp,
        kChn2_S0_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn2_S0_P_QP,
        kChn2_S0_B_QP,
        kChn2_S0_DstFrmRate,// 编码帧率
        //venc(chn3 main)
        kChn3_M_VENC_W,
        kChn3_M_VENC_H,
        kChn3_M_VENC_SAME_INPUT,
        kChn3_M_VENC_Type,
        kChn3_M_Gop,       // 关键帧
        kChn3_M_RcMode,    // 码流控制
        kChn3_M_Profile,   // 编码质量等级
        kChn3_M_BitRate,   // 比特率
        kChn3_M_MIN_QP,
        kChn3_M_MAX_QP,
        kChn3_M_MINI_PROP, //最小IP比例
        kChn3_M_MAXI_PROP, //最大IP比例
        kChn3_M_MinI_Qp,
        kChn3_M_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn3_M_P_QP,
        kChn3_M_B_QP,
        kChn3_M_DstFrmRate,// 编码帧率
        //venc(chn3 sub0)
        kChn3_S0_VENC_W,
        kChn3_S0_VENC_H,
        kChn3_S0_VENC_SAME_INPUT,
        kChn3_S0_VENC_Type,
        kChn3_S0_Gop,       // 关键帧
        kChn3_S0_RcMode,    // 码流控制
        kChn3_S0_Profile,   // 编码质量等级
        kChn3_S0_BitRate,   // 比特率
        kChn3_S0_MIN_QP,
        kChn3_S0_MAX_QP,
        kChn3_S0_MINI_PROP,
        kChn3_S0_MAXI_PROP,
        kChn3_S0_MinI_Qp,
        kChn3_S0_I_QP,      // 越小画值越高，与P_QP相差越小，呼吸效应越小
        kChn3_S0_P_QP,
        kChn3_S0_B_QP,
        kChn3_S0_DstFrmRate,// 编码帧率

        kChn0_Snap_McastEnable,  //组播抓图
        kChn0_Snap_McastIp,
        kChn0_Snap_McastPort,
        kChn0_Snap_McastFreq,
        kChn1_Snap_McastEnable,  //组播抓图
        kChn1_Snap_McastIp,
        kChn1_Snap_McastPort,
        kChn1_Snap_McastFreq,
        kChn2_Snap_McastEnable,  //组播抓图
        kChn2_Snap_McastIp,
        kChn2_Snap_McastPort,
        kChn2_Snap_McastFreq,
        kChn3_Snap_McastEnable,  //组播抓图
        kChn3_Snap_McastIp,
        kChn3_Snap_McastPort,
        kChn3_Snap_McastFreq,
        kChn0_SnapPort,
        kChn0_SnapLen,   // 回显手动拆包后每包大小
        kChn0_Snap_Delay,
        kChn0_Snap_W,
        kChn0_Snap_H,
        kChn0_Snap_Quality,
        kChn1_SnapPort,
        kChn1_SnapLen,   // 回显手动拆包后每包大小
        kChn1_Snap_Delay,
        kChn1_Snap_W,
        kChn1_Snap_H,
        kChn1_Snap_Quality,
        kChn2_SnapPort,
        kChn2_SnapLen,   // 回显手动拆包后每包大小
        kChn2_Snap_Delay,
        kChn2_Snap_W,
        kChn2_Snap_H,
        kChn2_Snap_Quality,
        kChn3_SnapPort,
        kChn3_SnapLen,   // 回显手动拆包后每包大小
        kChn3_Snap_Delay,
        kChn3_Snap_W,
        kChn3_Snap_H,
        kChn3_Snap_Quality,
        //osd(字符叠加)
        kChn0_Osd_Enable,
        kChn0_Osd_Name,
        kChn0_Osd_Color,
        kChn0_Osd_X,
        kChn0_Osd_Y,
        kChn0_Osd_Size,
        kChn0_Osd_Transparent,
        kChn0_Osd_BGColor,
        kChn1_Osd_Enable,
        kChn1_Osd_Name,
        kChn1_Osd_Color,
        kChn1_Osd_X,
        kChn1_Osd_Y,
        kChn1_Osd_Size,
        kChn1_Osd_Transparent,
        kChn1_Osd_BGColor,
        kChn2_Osd_Enable,
        kChn2_Osd_Name,
        kChn2_Osd_Color,
        kChn2_Osd_X,
        kChn2_Osd_Y,
        kChn2_Osd_Size,
        kChn2_Osd_Transparent,
        kChn2_Osd_BGColor,
        kChn3_Osd_Enable,
        kChn3_Osd_Name,
        kChn3_Osd_Color,
        kChn3_Osd_X,
        kChn3_Osd_Y,
        kChn3_Osd_Size,
        kChn3_Osd_Transparent,
        kChn3_Osd_BGColor,
        kChn0_RTSP_Enable,
        kChn1_RTSP_Enable,
        kChn2_RTSP_Enable,
        kChn3_RTSP_Enable,
        kEdid,
        //vo
        kHW_Loop_Enable,
        kOut_SameIn,
        kLuma,
        kContrast,
        kHue,
        kVoIntfSync ,
        kSaturation,
        kVoChnMax      ,
        kVoBackGround  ,
        kVoOutputModel ,
        kForceOutput   ,
//        kLuma_s1,
//        kContrast_s1,
//        kHue_s1,
//        kSaturation_s1,
        //tickmaster
        kChn0_VI_Module,
        kTickAuto,
        kTickModule,
        kTickRate,
        kTickOffset,
        kTickMDelay,
        kTickServerIp,
        kTickServerPort,
        kSoftWareConfigIDMax,
    }SoftWareConfigID;

    SoftwareConfig();
    ~SoftwareConfig();

    bool ReadConfig();
    bool SaveConfig();
    bool SaveNetConfig();
    void PrintConfig();
    bool LoadConfigToGlobal(bool bPrintThem = false);
    bool SetConfig(const SoftWareConfigID kId, string value);
    bool SetConfig(const SoftWareConfigID kId, int value);
    string GetConfig(const SoftWareConfigID kId);
    void do_set_net(const char *kIp, const char *kMask, const char *kGateway, const char *kDns, const char* kEthName );
    void do_set_mac(const char* kMac, const char* kEthName );
    void SetNetwork();


private:
    vector<string> configvalue_;
    //const char *const filepath_ = "./hi3531d_venc.ini"; //const data,const pointer
    char paramsnamelist[kSoftWareConfigIDMax][PARA_NAME_LEN]; //存放配置文件的前缀名称
};

#endif // Software_Config_H
